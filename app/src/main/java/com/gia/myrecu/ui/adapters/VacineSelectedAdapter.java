package com.gia.myrecu.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.messages.VacccineListUpdate;
import com.gia.myrecu.messages.VacineSelectedEditMessage;
import com.gia.myrecu.models.v1.Vaccin;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.tools.Utils;

import org.apache.commons.lang3.StringUtils;
import org.greenrobot.eventbus.EventBus;
import org.joda.time.DateTime;

import java.util.List;


@SuppressWarnings("ALL")
public class VacineSelectedAdapter extends RecyclerView.Adapter<VacineSelectedAdapter.VacineSelectedHolder> {


    Context ctx;
    List<Vaccin> vaccinSelectedList;


    public VacineSelectedAdapter(Context ctx, List<Vaccin> vaccinSelectedList) {
        this.ctx = ctx;
        this.vaccinSelectedList = vaccinSelectedList;
    }

    @NonNull
    @Override
    public VacineSelectedHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewVacineSelected = LayoutInflater.from(ctx).inflate(R.layout.items_vacine_selected_list, parent, false);
        return new VacineSelectedHolder(viewVacineSelected);
    }


    @Override
    public void onBindViewHolder(@NonNull final VacineSelectedHolder holder, int position) {
        final Vaccin vaccinSelected = vaccinSelectedList.get(holder.getAdapterPosition());

        String now = null;

        if (SPUtils.getInstance(Constants.VACCIN_PREFERENCES).getString(Constants.VACCIN_ADD_TYPE).equals("newest")
                || SPUtils.getInstance(Constants.VACCIN_PREFERENCES).getString(Constants.VACCIN_ADD_TYPE).equals("abn_newest")) {
             now = DateTime.now().toString("ddMMyyyy");

            holder.etVacineLot.setVisibility(View.VISIBLE);
            holder.etVacinePresence.setVisibility(View.VISIBLE);
            // holder.etVacineRappel.setVisibility(View.GONE);
            holder.linearDateRappelContainer.setVisibility(View.GONE);
        } else {
            holder.etVacineLot.setVisibility(View.GONE);
            //holder.etVacinePresence.setVisibility(View.GONE);
            holder.linearDatePresenceContainer.setVisibility(View.GONE);

            holder.etVacineRappel.setVisibility(View.VISIBLE);
        }

        holder.tvVacineLabel.setText(vaccinSelected.getVaccinLabel());

        holder.etVacinePresence.setText(now);
        holder.etVacineRappel.setText(now);

        holder.etVacineLot.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                vaccinSelected.setVaccinLot(s.toString().trim());
                EventBus.getDefault().post(new VacineSelectedEditMessage(vaccinSelected, holder.getAdapterPosition()));
            }
        });

        holder.etVacinePresence.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 8) {
                    vaccinSelected.setVaccinPresence(StringUtils.reverseDelimited(Utils.formatDate(s.toString().trim()), '-'));
                    EventBus.getDefault().post(new VacineSelectedEditMessage(vaccinSelected, holder.getAdapterPosition()));
                }
            }
        });

        holder.etVacineRappel.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 8) {
                    vaccinSelected.setVaccinRappel(StringUtils.reverseDelimited(Utils.formatDate(s.toString().trim()), '-'));
                    EventBus.getDefault().post(new VacineSelectedEditMessage(vaccinSelected, holder.getAdapterPosition()));
                }
            }
        });

        holder.imageDatePresence.setOnClickListener(v -> {
            Utils.pickDate(holder.etVacinePresence, ctx, false, false);
        });

        holder.imageDateRappel.setOnClickListener(v -> {
            Utils.pickDate(holder.etVacineRappel, ctx, false, false);
        });

        holder.rlVacineSelectedContainer.setOnClickListener(v -> {
            new AlertDialog.Builder(ctx)
                    .setMessage("Retirer le vaccin " + vaccinSelected.getVaccinLabel() + " ?")
                    .setPositiveButton("OUI", ((dialog, which) -> {
                        removeVacineSelected(holder.getAdapterPosition());
                    }))
                    .setNegativeButton("NON", ((dialog, which) -> {
                        dialog.dismiss();
                    }))
                    .create()
                    .show();
        });
    }


    public void removeVacineSelected(int position) {
        vaccinSelectedList.remove(position);
        notifyItemRemoved(position);
        EventBus.getDefault().post(new VacccineListUpdate(null, 0));
    }


    @Override
    public int getItemCount() {
        return vaccinSelectedList.size();
    }


    static class VacineSelectedHolder extends RecyclerView.ViewHolder {

        RelativeLayout rlVacineSelectedContainer;
        AppCompatTextView tvVacineLabel;
        AppCompatEditText etVacineLot;
        AppCompatEditText etVacineRappel;
        AppCompatEditText etVacinePresence;
        LinearLayout linearDatePresenceContainer;
        LinearLayout linearDateRappelContainer;
        AppCompatImageView imageDatePresence;
        AppCompatImageView imageDateRappel;

        VacineSelectedHolder(View itemView) {
            super(itemView);

            rlVacineSelectedContainer = itemView.findViewById(R.id.items_vacine_selected_list_list_rl_vacine_container);
            tvVacineLabel = itemView.findViewById(R.id.items_vacine_selected_list_list_tv_vacine_label);
            etVacineLot = itemView.findViewById(R.id.items_vacine_selected_list_list_et_vacine_lot);
            etVacineRappel = itemView.findViewById(R.id.items_vacine_selected_list_list_et_vacine_rappel);
            etVacinePresence = itemView.findViewById(R.id.items_vacine_selected_list_list_et_vacine_presence);

            imageDatePresence = itemView.findViewById(R.id.imageCalendarDatePresence);
            imageDateRappel = itemView.findViewById(R.id.imageCalendarDateRappel);

            linearDatePresenceContainer = itemView.findViewById(R.id.linearDatePresenceContainer);
            linearDateRappelContainer = itemView.findViewById(R.id.linearDateRappelContainer);
        }
    }
}
