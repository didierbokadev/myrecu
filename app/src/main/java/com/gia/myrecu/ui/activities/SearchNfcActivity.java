package com.gia.myrecu.ui.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;

import com.gia.myrecu.R;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.io.UnsupportedEncodingException;

import xdroid.toaster.Toaster;

public class SearchNfcActivity extends AppCompatActivity {


    NfcAdapter nfcAdapter;
    ProgressDialog progressDialog;
    AlertDialog alertDialog;
    AppCompatButton btnOk;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_nfc);

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Recherche en cours...");
        progressDialog.setCancelable(true);
    }


    private void readDataNdefMessage(NdefMessage ndefMessage) {
        try {
            NdefRecord[] ndefRecords = ndefMessage.getRecords();

            if (ndefRecords != null && ndefRecords.length > 0) {
                NdefRecord ndefRecord = ndefRecords[0];
                String tagContent = getTextFromNdefRecord(ndefRecord);
                showDialogAbonne(tagContent);
            } else {
                Toaster.toast("Ce badge est vide !");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    public String getTextFromNdefRecord(NdefRecord ndefRecord) {
        String tagContent = null;
        try {
            byte[] payload = ndefRecord.getPayload();
            String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16";
            int languageSize = payload[0] & 0063;
            tagContent = new String(payload, languageSize + 1, payload.length -
                    languageSize - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
        }
        return tagContent;
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        try {
            if (alertDialog != null) {
                if (alertDialog.isShowing()) {
                    alertDialog.dismiss();
                }
            }
            if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
                Toaster.toast("Badge détecté !");
                Parcelable[] parcelables = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);

                if (parcelables != null) {
                    if (parcelables.length > 0) {
                        readDataNdefMessage((NdefMessage) parcelables[0]);
                    }
                } else {
                    Toaster.toast("Aucune identification sur ce badge !");
                }
            } else {
                Toaster.toast("Peur");
            }
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    public static void disableForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        try {
            adapter.disableForegroundDispatch(activity);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    public static void enableForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        try {
            final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
            intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

            final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(),
                    0, intent, 0);

            IntentFilter[] filters = new IntentFilter[]{};
            adapter.enableForegroundDispatch(activity, pendingIntent, filters, null);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        try {
            enableForegroundDispatch(this, nfcAdapter);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        try {
            disableForegroundDispatch(this, nfcAdapter);
        } catch(Exception e) {
            e.printStackTrace();
        }
        super.onPause();
    }


    private void showDialogAbonne(String data) {
        try {
            LayoutInflater layoutInflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
            View viewResultNFC = layoutInflater.inflate(R.layout.view_dialog_abonne_result_nfc, null);

            AppCompatTextView tvNom = viewResultNFC.findViewById(R.id.view_dialog_abonne_result_nfc_tv_name);
            AppCompatTextView tvNaissance = viewResultNFC.findViewById(R.id.view_dialog_abonne_result_nfc_tv_naissance);
            AppCompatTextView tvFJ = viewResultNFC.findViewById(R.id.view_dialog_abonne_result_nfc_tv_statut_fj);
            AppCompatTextView tvDateFJ = viewResultNFC.findViewById(R.id.view_dialog_abonne_result_nfc_tv_date_fj);
            AppCompatTextView tvLotFJ = viewResultNFC.findViewById(R.id.view_dialog_abonne_result_nfc_tv_lot_fj);
            AppCompatTextView tvAbonnementDate = viewResultNFC.findViewById(R.id.view_dialog_abonne_result_nfc_tv_abonnement_date);

            AppCompatButton btnOk =viewResultNFC.findViewById(R.id.view_dialog_abonne_result_nfc_btn_ok);

            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (alertDialog != null) {
                        if (alertDialog.isShowing()) {
                            alertDialog.dismiss();
                        }
                    }
                }
            });

            String[] parserString = StringUtils.split(data, ';');

            String[] nomPrenomsAbonne = StringUtils.split(parserString[1], ':');
            String[] naissanceAbonne = StringUtils.split(parserString[2], ':');
            String[] abonDateAbo = StringUtils.split(parserString[4], ':');
            String[] statutAbonne = StringUtils.split(parserString[5], ':');
            String[] dateFJAbonne = StringUtils.split(parserString[6], ':');
            String[] lotFJAbonne = StringUtils.split(parserString[7], ':');

            tvNom.setText(StringUtils.replace(nomPrenomsAbonne[1], "|", " "));
            tvNaissance.setText(naissanceAbonne[1]);
            tvAbonnementDate.setText(abonDateAbo[1]);

            if (dateFJAbonne[1].equals("Non definie") || dateFJAbonne[1].equals("Non définie")) {
                tvDateFJ.setText(dateFJAbonne[1]);
            } else {
                tvDateFJ.setText(DateTime.parse(StringUtils.reverseDelimited(dateFJAbonne[1], '-'))
                        .toString("dd MMM yyyy"));
            }

            tvLotFJ.setText(lotFJAbonne[1]);
            tvFJ.setText(statutAbonne[1]);

            AlertDialog.Builder builder = new AlertDialog.Builder(SearchNfcActivity.this)
                    .setView(viewResultNFC)
                    .setCancelable(false);

            alertDialog = builder.create();
            alertDialog.show();
        } catch(Exception e) {
            e.printStackTrace();
        }
    }
}
