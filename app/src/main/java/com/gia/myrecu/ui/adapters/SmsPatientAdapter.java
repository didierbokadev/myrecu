package com.gia.myrecu.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Message;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.List;


@SuppressWarnings("ALL")
public class SmsPatientAdapter extends RecyclerView.Adapter<SmsPatientAdapter.SmsPatientHolder> {


    private Context ctx;
    private List<Message> messages;


    public SmsPatientAdapter(Context ctx, List<Message> messages) {
        this.ctx = ctx;
        this.messages = messages;
    }


    @NonNull
    @Override
    public SmsPatientHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View viewSmsPatient = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.patient_messages_items_list, viewGroup, false);
        return new SmsPatientHolder(viewSmsPatient);
    }


    @Override
    public void onBindViewHolder(@NonNull SmsPatientHolder smsPatientHolder, int i) {
        Message sms = messages.get(smsPatientHolder.getAdapterPosition());

        smsPatientHolder.tvMessageBody.setText(sms.getMessageBody());
        smsPatientHolder.tvMessageTitle.setText(sms.getMessageOrigin().concat("-").concat(sms.getMessageNature()));
        smsPatientHolder.tvMessageDate.setText(DateTime.parse(sms.getMessageDateEnvoi(), DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")).toString("dd-MM-yyyy, HH:mm:ss"));
    }


    @Override
    public int getItemCount() {
        return messages.size();
    }


    static class SmsPatientHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView tvMessageBody;
        private AppCompatTextView tvMessageTitle;
        private AppCompatTextView tvMessageDate;

        public SmsPatientHolder(@NonNull View itemView) {
            super(itemView);

            tvMessageBody = itemView.findViewById(R.id.patient_messages_items_list_tv_msg);
            tvMessageTitle = itemView.findViewById(R.id.patient_messages_items_list_tv_title);
            tvMessageDate = itemView.findViewById(R.id.patient_messages_items_list_tv_date);
        }
    }
}
