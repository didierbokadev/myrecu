package com.gia.myrecu.ui.activities;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Patient;
import com.gia.myrecu.models.v1.SearchData;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.ui.fragments.PatientsFoundedFragment;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;


@SuppressWarnings("ALL")
public class SearchActivity extends AppCompatActivity implements PatientsFoundedFragment.OnPatientsFoundedFragmentListener {


    AppCompatButton btnSearch;
    AppCompatEditText etNumeroRecu;
    AppCompatEditText etNumeroTelephone;
    AppCompatEditText etLogin;
    AppCompatEditText etPassport;
    // Manage sms sender
    //SmsManager smsManager;
    //StringBuilder datas;
    ProgressDialog progressDialog;
    String TAG = "SearchActivity";
    SearchData searchData;
    Gson gson;
    List<Patient> patientList;
    SearchEngine searchEngine;

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        gson = new GsonBuilder().serializeNulls().create();

        etNumeroRecu = findViewById(R.id.activity_search_et_num_recu);
        etNumeroTelephone = findViewById(R.id.activity_search_et_num_telephone);
        etLogin = findViewById(R.id.activity_search_et_login_pat);
        etPassport = findViewById(R.id.activity_search_et_passport_pat);
        btnSearch = findViewById(R.id.activity_search_btn_search);

        patientList = new ArrayList<>();

        btnSearch.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                etNumeroRecu.setText(null);
                etNumeroTelephone.setText(null);
                etLogin.setText(null);
                etPassport.setText(null);
                return true;
            }
        });

        etNumeroTelephone.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    launchSearch();
                    return true;
                }
                return false;
            }
        });

        etNumeroRecu.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    launchSearch();
                    return true;
                }
                return false;
            }
        });

        etPassport.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    launchSearch();
                    return true;
                }
                return false;
            }
        });

        etLogin.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                    launchSearch();
                    return true;
                }
                return false;
            }
        });

        btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchSearch();
            }
        });
    }


    private void launchSearch() {
        btnSearch.setEnabled(false);
        KeyboardUtils.hideSoftInput(SearchActivity.this);

        if (etNumeroTelephone.getText().length() > 0 && etNumeroTelephone.getText().length() != 10) {
            Toast.makeText(SearchActivity.this, "Mauvais numéro !", Toast.LENGTH_SHORT).show();
            btnSearch.setEnabled(true);
            return;
        } else {

            searchData = new SearchData();

            if (etNumeroRecu.getText().length() > 0) {
                if (StringUtils.isNotBlank(etLogin.getText().toString().trim())
                        || (StringUtils.isNotBlank(etPassport.getText().toString()))) {
                    showDialog();
                    btnSearch.setEnabled(true);
                    return;
                } else {
                    searchData.setSearchDataReqKey("recu");
                    searchData.setSearchDataReqValue(etNumeroRecu.getText().toString());
                }
            }

            if (etNumeroTelephone.getText().toString().length() > 0) {
                if (StringUtils.isNotBlank(etLogin.getText().toString().trim())
                        || (StringUtils.isNotBlank(etPassport.getText().toString()))) {
                    showDialog();
                    btnSearch.setEnabled(true);
                    return;
                } else {
                    searchData.setSearchDataReqKey("tel");
                    searchData.setSearchDataReqValue("225" + etNumeroTelephone.getText().toString());
                }
            }

            if (etNumeroRecu.getText().length() > 0 && (etNumeroTelephone.getText().length() > 0
                    && etNumeroTelephone.getText().length() == 8)) {
                if (StringUtils.isNotBlank(etLogin.getText().toString().trim())
                        || (StringUtils.isNotBlank(etPassport.getText().toString()))) {
                    showDialog();
                    btnSearch.setEnabled(true);
                    return;
                } else {
                    searchData.setSearchDataReqKey("both");
                    searchData.setSearchDataReqValue(etNumeroRecu.getText().toString() + "#"
                            + "225" + etNumeroTelephone.getText().toString());
                }
            }

            if (etLogin.getText().toString().trim().length() > 0) {
                if (StringUtils.isNotBlank(etNumeroTelephone.getText().toString().trim())
                        || StringUtils.isNotBlank(etNumeroRecu.getText().toString().trim())
                        || (StringUtils.isNotBlank(etPassport.getText().toString()))) {
                    showDialog();
                    btnSearch.setEnabled(true);
                    return;
                } else {
                    searchData.setSearchDataReqKey("login");
                    searchData.setSearchDataReqValue(etLogin.getText().toString().trim());
                }
            }

            if (etPassport.getText().toString().trim().length() > 0) {

                if (StringUtils.isNotBlank(etNumeroTelephone.getText().toString().trim())
                        || StringUtils.isNotBlank(etNumeroRecu.getText().toString().trim())
                        || (StringUtils.isNotBlank(etLogin.getText().toString()))) {
                    showDialog();
                    btnSearch.setEnabled(true);
                    return;
                } else {
                    searchData.setSearchDataReqKey("passeport");
                    searchData.setSearchDataReqValue(etPassport.getText().toString().trim());
                }
            }

            searchData.setSearchDataReqId(1);

            if (searchData.getSearchDataReqKey().length() == 0) {
                btnSearch.setEnabled(true);
                return;
            }

            String sms = gson.toJson(searchData);
            //PhoneUtils.sendSmsSilent(Constants.NUMBER, sms);
            progressDialog = new ProgressDialog(SearchActivity.this);
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setMessage("Recherche en cours...");
            progressDialog.show();

            searchEngine = new SearchEngine();
            searchEngine.execute();
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    private class SearchEngine extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(20, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();

                RequestBody formBody = new FormBody.Builder()
                        .add("reqKey", searchData.getSearchDataReqKey())
                        .add("reqValue", searchData.getSearchDataReqValue())
                        .add("d", BuildConfig.DATABASE)
                        .build();

                LogUtils.e("Url: " + UrlBanks.SEARCH_PATIENT);

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.SEARCH_PATIENT)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();
                String stringResponse = clientResponse.body().string();

                if (clientResponse.isSuccessful()) {
                    Type typeList = new TypeToken<ArrayList<Patient>>() {}.getType();

                    JSONObject jsonObject = new JSONObject(stringResponse);
                    String status = jsonObject.getString("statut");

                    if (status.equals("1")) {
                        JSONArray jsonArray = jsonObject.getJSONArray("data");
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        ArrayList<Patient> patientArrayList = gson.fromJson(jsonArray.toString(), typeList);

                        if (!patientList.isEmpty()) {
                            patientList.clear();
                        }

                        patientList.addAll(patientArrayList);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                btnSearch.setEnabled(true);
                                PatientsFoundedFragment.newInstance(patientList).show(getSupportFragmentManager(), "patientFounded");
                            }
                        });
                        searchEngine.cancel(true);
                        searchEngine = null;
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnSearch.setEnabled(true);
                                progressDialog.dismiss();
                            }
                        });
                        Toaster.toastLong(jsonObject.getString("message"));
                        searchEngine.cancel(true);
                        searchEngine = null;
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnSearch.setEnabled(true);
                            progressDialog.dismiss();
                        }
                    });
                    Toaster.toast("Une erreur est survenue");
                    searchEngine.cancel(true);
                    searchEngine = null;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnSearch.setEnabled(true);
                        progressDialog.dismiss();
                        Toaster.toast("Une erreur reseau est survenu. Ressayer à nouveau.");

                    }
                });
                searchEngine.cancel(true);
                searchEngine = null;
            }
            return null;
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                btnSearch.setEnabled(true);
            }
        }
    }


    private void showDialog() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(SearchActivity.this)
                .setCancelable(false)
                .setMessage(getResources().getString(R.string.txt_info_choose_criteary_search))
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }


    @Override
    public void onBackPressed() {
        if (searchEngine != null) {
            searchEngine.cancel(true);
            return;
        }

        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

}
