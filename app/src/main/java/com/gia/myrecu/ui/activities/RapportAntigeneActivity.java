package com.gia.myrecu.ui.activities;

import static com.gia.myrecu.tools.Constants.AGENT_ID;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;

import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioGroup;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v2.AntigenData;
import com.gia.myrecu.network.OPIApiClient;
import com.gia.myrecu.network.OPIApiServices;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

@SuppressWarnings("ALL")
public class RapportAntigeneActivity extends AppCompatActivity implements OnChartValueSelectedListener, DatePickerDialog.OnDateSetListener {


    Typeface typeFace;
    Calendar dateCalendar;
    int which = 1;
    List<String> labeleds = new ArrayList<>();
    private String dateClose;
    private String dateOpen;
    private String abonnementType = "1";

    AppCompatRadioButton rbVaccin;
    AppCompatRadioButton rbGrossesse;
    AppCompatRadioButton rbPlanning;
    LinearLayout linearDateClose;
    LinearLayout linearDateOpen;
    AppCompatTextView labelDateOpen;
    AppCompatTextView labelEmptyDatas;
    AppCompatTextView labelDateClose;
    AppCompatButton clickSearch;
    ProgressBar loading;
    PieChart pieAntigene;
    RadioGroup rgSelect;


    void fetchDatas() {
        OPIApiClient.getClient(this).create(OPIApiServices.class).getAntigensStats(
            "" + SPUtils.getInstance(AGENT_PREFERENCES).getInt(AGENT_ID, 0),
            "" + dateOpen,
            "" + dateClose,
            "" + abonnementType,
            "" + BuildConfig.DATABASE)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeWith(new DisposableSingleObserver<List<AntigenData>>() {
                @Override
                public void onSuccess(List<AntigenData> antigenDatas) {
                    loading.setVisibility(View.GONE);

                    if (antigenDatas.size() == 0) {
                        pieAntigene.setVisibility(View.GONE);
                        labelEmptyDatas.setVisibility(View.VISIBLE);
                    }

                    if (antigenDatas.size() > 0) {
                        setData(antigenDatas);

                        pieAntigene.setVisibility(View.VISIBLE);
                        labelEmptyDatas.setVisibility(View.GONE);
                    }
                }

                @Override
                public void onError(Throwable e) {
                    e.printStackTrace();
                }
            });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dateCalendar = Calendar.getInstance();

        setContentView(R.layout.activity_rapport_antigene);

        linearDateClose = findViewById(R.id.linearDateClose);
        linearDateOpen = findViewById(R.id.linearDateOpen);

        labelEmptyDatas = findViewById(R.id.labelEmptyDatas);
        labelDateClose = findViewById(R.id.dateClose);
        labelDateOpen = findViewById(R.id.dateOpen);

        rgSelect = findViewById(R.id.groupTypeAbonnementContainer);

        rbGrossesse = findViewById(R.id.radioPreg);
        rbPlanning = findViewById(R.id.radioPlan);
        rbVaccin = findViewById(R.id.radioVac);

        loading = findViewById(R.id.loadingAntigen);

        clickSearch = findViewById(R.id.click_search);
        pieAntigene = findViewById(R.id.chartAntigene);

        linearDateOpen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                which = 1;
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        RapportAntigeneActivity.this, RapportAntigeneActivity.this, dateCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH), dateCalendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.getDatePicker().setMaxDate(DateTime.now().getMillis());
                datePickerDialog.show();
            }
        });

        linearDateClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                which = 2;
                DatePickerDialog datePickerDialog = new DatePickerDialog(
                        RapportAntigeneActivity.this,
                        RapportAntigeneActivity.this, dateCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH), dateCalendar.get(Calendar.DAY_OF_MONTH));

                datePickerDialog.getDatePicker().setMaxDate(DateTime.now().getMillis());
                datePickerDialog.show();
            }
        });

        clickSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loading.setVisibility(View.VISIBLE);
                pieAntigene.setVisibility(View.GONE);
                labelEmptyDatas.setVisibility(View.GONE);
                fetchDatas();
            }
        });

        configChart(pieAntigene);
        onTypeAbonnementClicked(rbVaccin);

        labelDateOpen.setText(DateTime.now().minusWeeks(1).toString(DateTimeFormat.forPattern("dd/MM/yyyy")));
        labelDateClose.setText(DateTime.now().toString(DateTimeFormat.forPattern("dd/MM/yyyy")));

        dateOpen = DateTime.parse(labelDateOpen.getText().toString(), DateTimeFormat.forPattern("dd/MM/yyyy")).toString(DateTimeFormat.forPattern("yyyy/MM/dd"));
        dateClose = DateTime.parse(labelDateClose.getText().toString(), DateTimeFormat.forPattern("dd/MM/yyyy")).toString(DateTimeFormat.forPattern("yyyy/MM/dd"));
    }


    void configChart(PieChart pPieChart) {
        pPieChart.setUsePercentValues(true);
        pPieChart.getDescription().setEnabled(false);
        pPieChart.setExtraOffsets(5, 10, 5, 5);

        pPieChart.setDragDecelerationFrictionCoef(0.95f);

        typeFace = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");

        pPieChart.setCenterTextTypeface(Typeface.createFromAsset(getAssets(), "OpenSans-Light.ttf"));
        // pPieChart.setCenterText(generateCenterSpannableText());

        pPieChart.setExtraOffsets(20.f, 0.f, 20.f, 0.f);

        pPieChart.setDrawHoleEnabled(false);
        //pPieChart.setHoleColor(Color.WHITE);

        //pPieChart.setTransparentCircleColor(Color.WHITE);
        //pPieChart.setTransparentCircleAlpha(100);

        pPieChart.setHoleRadius(50.f);

        //pPieChart.setTransparentCircleRadius(61f);

        pPieChart.setDrawCenterText(false);

        pPieChart.setRotationAngle(0);
        // enable rotation of the chart by touch
        pPieChart.setRotationEnabled(false);
        pPieChart.setHighlightPerTapEnabled(true);

        // add a selection listener
        pPieChart.setOnChartValueSelectedListener(this);
        pPieChart.animateY(1400, Easing.EaseInOutQuad);

        Legend l = pPieChart.getLegend();
        l.setVerticalAlignment(Legend.LegendVerticalAlignment.TOP);
        l.setHorizontalAlignment(Legend.LegendHorizontalAlignment.CENTER);
        l.setOrientation(Legend.LegendOrientation.HORIZONTAL);
        l.setDrawInside(true);
        l.setEnabled(true);

        // fetchDatas();
    }


    // private void setData(int count, float range) {
    private void setData(List<AntigenData> datas) {
        ArrayList<PieEntry> entries = new ArrayList<>();
        // NOTE: The order of the entries when being added to the entries array determines their position around the center of
        // the chart.
        /*for (int i = 0; i < count; i++) {
            entries.add(new PieEntry((float) (Math.random() * range) + range / 5, labeleds.get(i % labeleds.size())));
        }*/

        for (AntigenData antigenData : datas) {
            entries.add(new PieEntry(Float.valueOf(antigenData.personnesVaccinees), antigenData.vaccin ));
        }

        PieDataSet dataSet = new PieDataSet(entries, "");
        dataSet.setSliceSpace(3f);
        dataSet.setSelectionShift(5f);

        // add a lot of colors
        ArrayList<Integer> colors = new ArrayList<>();

        for (int c : ColorTemplate.JOYFUL_COLORS) colors.add(c);
        for (int c : ColorTemplate.COLORFUL_COLORS) colors.add(c);
        for (int c : ColorTemplate.LIBERTY_COLORS) colors.add(c);
        for (int c : ColorTemplate.PASTEL_COLORS) colors.add(c);

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);
        //dataSet.setSelectionShift(0f);

        dataSet.setValueLinePart1OffsetPercentage(80.f);
        dataSet.setValueLinePart1Length(0.2f);
        dataSet.setValueLinePart2Length(0.4f);

        //dataSet.setXValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);
        dataSet.setYValuePosition(PieDataSet.ValuePosition.OUTSIDE_SLICE);

        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(11f);
        data.setValueTextColor(Color.BLACK);
        data.setValueTypeface(typeFace);
        pieAntigene.setData(data);

        // undo all highlights
        pieAntigene.highlightValues(null);
        pieAntigene.invalidate();
    }


    SpannableString generateCenterSpannableText() {
        SpannableString s = new SpannableString("ANTIGENES");
        s.setSpan(new RelativeSizeSpan(1.5f), 0, 14, 0);
        s.setSpan(new StyleSpan(Typeface.NORMAL), 14, s.length() - 15, 0);
        s.setSpan(new ForegroundColorSpan(Color.GRAY), 14, s.length() - 15, 0);
        s.setSpan(new RelativeSizeSpan(.65f), 14, s.length() - 15, 0);
        s.setSpan(new StyleSpan(Typeface.ITALIC), s.length() - 14, s.length(), 0);
        s.setSpan(new ForegroundColorSpan(ColorTemplate.getHoloBlue()), s.length() - 14, s.length(), 0);
        return s;
    }


    public void onTypeAbonnementClicked(View view) {
        boolean checkedAbonnement = ((AppCompatRadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioVac : // Show vaccine tools
                if (checkedAbonnement) {
                    abonnementType = "1";
                }
                break;

            case R.id.radioPlan: // Show planning tools
                if (checkedAbonnement) {
                    abonnementType = "2";
                }
                break;

            case R.id.radioPreg: // Show Pregnant tools
                if (checkedAbonnement) {
                    abonnementType = "3";
                }
                break;
        }
    }


    @Override
    public void onValueSelected(Entry e, Highlight h) {
        if (e == null)
            return;
        Log.i("VAL SELECTED", "Value: " + e.getY() + ", xIndex: " + e.getX() + ", DataSet index: " + h.getDataSetIndex());
        ToastUtils.showShort("VACCINES = " + (int) e.getY());

        //startActivity(new Intent(this, RapportAntigenDetailsActivity.class));
        //overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }


    /**
     * Called when nothing has been selected or an "un-select" has been made.
     */
    @Override
    public void onNothingSelected() {

    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String monthS = "";
        DateTime dateTime = new DateTime(year, (month + 1), dayOfMonth, 0, 0);

        if (which == 1) {
            labelDateOpen.setText(dateTime.toString("dd MMM yyyy"));
            dateOpen = dateTime.toString("yyyy-MM-dd");
        } else {
            labelDateClose.setText(dateTime.toString("dd MMM yyyy"));
            dateClose = dateTime.toString("yyyy-MM-dd");
        }
    }
}
