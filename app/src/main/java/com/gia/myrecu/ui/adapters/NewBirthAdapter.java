package com.gia.myrecu.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.gia.myrecu.R;
import com.gia.myrecu.models.v2.Patient;

import org.apache.commons.lang3.StringUtils;

import java.util.List;


@SuppressWarnings("ALL")
public class NewBirthAdapter extends RecyclerView.Adapter<NewBirthAdapter.NewBirthHolder> {


    private Context ctx;
    private List<Patient> patients;


    public NewBirthAdapter(Context ctx, List<Patient> patients) {
        this.ctx = ctx;
        this.patients = patients;
    }


    @NonNull
    @Override
    public NewBirthHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View viewNewBirth = LayoutInflater.from(ctx).inflate(R.layout.new_birthday_items_list, viewGroup, false);
        return new NewBirthHolder(viewNewBirth);
    }


    @Override
    public void onBindViewHolder(@NonNull NewBirthHolder newBirthHolder, int i) {
        Patient baby = patients.get(newBirthHolder.getAdapterPosition());

        newBirthHolder.tvBabySex.setText(baby.getSexePat());
        newBirthHolder.tvBabyName.setText(baby.getNomPat() + " " + baby.getPrenomsPat());
        newBirthHolder.tvBabyDate.setText(StringUtils.reverseDelimited(baby.getNaisaancePat(), '-'));
    }


    @Override
    public int getItemCount() {
        return patients.size();
    }


    static class NewBirthHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView tvBabyName;
        private AppCompatTextView tvBabyDate;
        private AppCompatTextView tvBabySex;
        private RelativeLayout rlContainer;

        public NewBirthHolder(@NonNull View itemView) {
            super(itemView);

            tvBabyDate = itemView.findViewById(R.id.new_birthday_items_list_tv_baby_birthday);
            tvBabyName = itemView.findViewById(R.id.new_birthday_items_list_tv_baby_name);
            tvBabySex = itemView.findViewById(R.id.new_birthday_items_list_tv_baby_sex);
        }
    }


}
