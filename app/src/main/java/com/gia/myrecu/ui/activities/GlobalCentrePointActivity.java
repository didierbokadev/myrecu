package com.gia.myrecu.ui.activities;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.JsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v2.Patient;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.ui.adapters.CentrePointAdapter;
import com.gia.myrecu.ui.adapters.CentrePointAdapter.OnCentrePointAdapterListener;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static com.gia.myrecu.tools.Constants.CENTRE_ID;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;


@SuppressWarnings("ALL")
public class GlobalCentrePointActivity extends AppCompatActivity implements OnCentrePointAdapterListener {


    RecyclerView rvList;
    ProgressBar pgLoading;
    LoadGlobalCenterPointAsync globalCenterPointAsync;
    OkHttpClient clientHttp;
    Type globalType = new TypeToken<ArrayList<Patient>>(){}.getType();
    List<Patient> patientList;
    CentrePointAdapter centrePointAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_global_centre_point);

        pgLoading = findViewById(R.id.activity_global_centre_point_pb_loading);
        rvList = findViewById(R.id.activity_global_centre_point_rv_list);

        patientList = new ArrayList<>();
        centrePointAdapter = new CentrePointAdapter(this, patientList);

        rvList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvList.setAdapter(centrePointAdapter);

        rvList.setHasFixedSize(true);

        globalCenterPointAsync = new LoadGlobalCenterPointAsync();
        globalCenterPointAsync.execute();
    }


    @Override
    public void throwCallPermission() {
        if (ContextCompat.checkSelfPermission(GlobalCentrePointActivity.this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 2017);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 2017) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                SPUtils.getInstance(Constants.AGENT_PREFERENCES).put(Constants.AGENT_CALL_PERMISSION, true);
            } else {
                SPUtils.getInstance(Constants.AGENT_PREFERENCES).put(Constants.AGENT_CALL_PERMISSION, false);
            }
        }
    }


    private class LoadGlobalCenterPointAsync extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            rvList.setVisibility(View.GONE);
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(90, TimeUnit.SECONDS)
                        .readTimeout(90, TimeUnit.SECONDS)
                        .build();

                HttpUrl.Builder urlBuilder = HttpUrl.parse(UrlBanks.CENTRE_POINT).newBuilder();
                urlBuilder.addQueryParameter("centreId", String.valueOf(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID, 0)));
                urlBuilder.addQueryParameter("d", BuildConfig.DATABASE);
                String globalUrl = urlBuilder.toString();

                Request globalRequest = new Request.Builder()
                        .method("GET", null)
                        .url(globalUrl)
                        .build();

                Response globalResponse = clientHttp.newCall(globalRequest).execute();
                String globalString = globalResponse.body().string();

                LogUtils.e("TAG", globalString);

                if (globalResponse.isSuccessful()) {
                    code = JsonUtils.getInt(globalString, "code");

                    if (code == 0) {
                        patientList.clear();
                        patientList.addAll((ArrayList<Patient>) GsonUtils.fromJson(JsonUtils.getJSONArray(globalString, "data", null).toString(), globalType));
                    }
                } else {
                    code = 2;
                }

            } catch (Exception ex) {
                ex.printStackTrace();
                code = 2;
            }
            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            pgLoading.setVisibility(View.GONE);

            switch (integer) {
                case 0 :
                    centrePointAdapter.notifyDataSetChanged();
                    rvList.setVisibility(View.VISIBLE);
                break;

                case 1 :
                case 2 :
                    ToastUtils.showShort("Pas de point à ce jour !");
                    rvList.setVisibility(View.GONE);
                break;
            }
        }
    }
}
