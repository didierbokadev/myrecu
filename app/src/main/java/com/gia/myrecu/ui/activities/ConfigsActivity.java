package com.gia.myrecu.ui.activities;

import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.AppController;
import com.gia.myrecu.R;
import com.gia.myrecu.databases.room.AndroidSmsDatabase;
import com.gia.myrecu.models.v1.Centre;
import com.gia.myrecu.models.v1.District;

import java.util.Collections;
import java.util.List;

import static com.gia.myrecu.tools.Constants.CENTRE_ID;
import static com.gia.myrecu.tools.Constants.CENTRE_NAME;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;
import static com.gia.myrecu.tools.Constants.DISTRICT_ID;
import static com.gia.myrecu.tools.Constants.DISTRICT_NAME;
import static com.gia.myrecu.tools.Constants.PHONE_IS_CONFIGURED;
import static com.gia.myrecu.tools.Constants.PHONE_PREFERENCES;


@SuppressWarnings("ALL")
public class ConfigsActivity extends AppCompatActivity {


    public static final String TAG = "ConfigsActivity";
    Toolbar toolbar;
    ActionBar actionBar;
    AppCompatSpinner spinnerDistrict;
    AppCompatSpinner spinnerCentre;
    Centre centre;
    List<District> districtList;
    List<Centre> centreList;
    ArrayAdapter adapterDistrict;
    ArrayAdapter adapterCentre;
    AndroidSmsDatabase database;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configs);
        findViewByIds();
        actionBar = getSupportActionBar();

        database = AndroidSmsDatabase.getDatabase(this);

        districtList = database.districtDao().getAllDistricts();
        centreList = database.centreDao().getAllCentres();

        Collections.sort(districtList, District.DistrictNameComparator);
        Collections.sort(centreList, Centre.CentreNameComparator);

        adapterDistrict = new ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, districtList);

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
            actionBar.setTitle(getString(R.string.config_app_center_and_district_title));
        } else {
            actionBar.setTitle(getString(R.string.config_app_center_and_district_title));
        }

        spinnerDistrict.setAdapter(adapterDistrict);
        spinnerCentre.setAdapter(adapterCentre);

        spinnerDistrict.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                District districtSelected = (District) parent.getSelectedItem();
                adapterCentre = new ArrayAdapter(ConfigsActivity.this, android.R.layout.simple_dropdown_item_1line);
                adapterCentre.clear();


                for (Centre centreFilter : centreList) {
                    if (centreFilter.getCentreDistrictId() == districtSelected.getDistrictId()) {
                        adapterCentre.add(centreFilter);
                    }
                }

                if (adapterCentre.getCount() > 0) {
                    adapterCentre.notifyDataSetChanged();
                    spinnerCentre.setAdapter(adapterCentre);
                } else {
                    SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).remove(CENTRE_NAME);
                    SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).remove(CENTRE_ID);
                    adapterCentre.clear();
                    spinnerCentre.setAdapter(adapterCentre);
                    //
                }

                SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).put(DISTRICT_NAME, districtSelected.getDistrictLabel());
                SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).put(DISTRICT_ID, districtSelected.getDistrictId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinnerCentre.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Centre centreSelected = (Centre) parent.getSelectedItem();
                SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).put(CENTRE_NAME, centreSelected.getCentreLabel());
                SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).put(CENTRE_ID, centreSelected.getCentreId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).remove(CENTRE_NAME);
                SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).remove(CENTRE_ID);
            }
        });

        if (SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(DISTRICT_ID, 0) != 0) {
            for (int districtPosition = 0; districtPosition < districtList.size(); districtPosition++) {
                if (districtList.get(districtPosition).getDistrictId() == SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(DISTRICT_ID)) {
                    spinnerDistrict.setSelection(districtPosition);
                }
            }

            for (int centrePosition = 0; centrePosition < centreList.size(); centrePosition++) {
                if (centreList.get(centrePosition).getCentreId() == SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID)) {
                    spinnerCentre.setSelection(centrePosition);
                }
            }
        }
    }


    private void findViewByIds() {
        toolbar = findViewById(R.id.activity_configs_tb_title);
        spinnerDistrict = findViewById(R.id.activity_configs_spin_select_district);
        spinnerCentre = findViewById(R.id.activity_configs_spin_select_site);
        setSupportActionBar(toolbar);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_confis_district, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int itemId = item.getItemId();

        switch (itemId) {
            case android.R.id.home:
                finish();
            return true;

            case R.id.menu_confis_district_settings:
                if (SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(DISTRICT_ID) < 0 || SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID) < 0) {
                    Toast.makeText(this, "La configuration de votre device est incomplete.", Toast.LENGTH_SHORT).show();
                } else {
                    SPUtils.getInstance(PHONE_PREFERENCES).put(PHONE_IS_CONFIGURED, true);
                    AppController.startNewTopActivity(this, StartActivity.class);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    protected void onResume() {
        super.onResume();

        SPUtils.getInstance(PHONE_PREFERENCES).put(PHONE_IS_CONFIGURED, false);
    }
}
