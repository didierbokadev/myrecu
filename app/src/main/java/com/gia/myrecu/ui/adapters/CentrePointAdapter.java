package com.gia.myrecu.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.PhoneUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Agent;
import com.gia.myrecu.models.v2.Patient;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.ui.activities.DashboardActivity;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.List;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_EXPIRATION;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_BIRTHDAY;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_DATE_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_EMAIL;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PHOTO_CARNET;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_SEXE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_STATUT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_TELEPHONE;


@SuppressWarnings("ALL")
public class CentrePointAdapter extends RecyclerView.Adapter<CentrePointAdapter.CentrePointHolder> {


    private Context ctx;
    private List<Patient> patients;
    OnCentrePointAdapterListener mListener;


    public CentrePointAdapter(Context ctx, List<Patient> patients) {
        this.ctx = ctx;
        this.patients = patients;

        if (ctx instanceof OnCentrePointAdapterListener) {
            mListener = (OnCentrePointAdapterListener) ctx;
        } else {
            throw new ClassCastException("Implement OnCentrePointAdapter");
        }
    }


    @NonNull
    @Override
    public CentrePointHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View centrePointView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.centre_global_point_items_list, viewGroup, false);
        return new CentrePointHolder(centrePointView);
    }


    @Override
    public void onBindViewHolder(@NonNull CentrePointHolder centrePointHolder, int i) {
        Patient patient = patients.get(centrePointHolder.getAdapterPosition());
        Agent agent = patient.getAgentBean();

        int age = DateTime.now().year().get() - DateTime.parse(patient.getNaisaancePat(), DateTimeFormat.forPattern("yyyy-MM-dd")).year().get();

        centrePointHolder.ivPatientGender.setImageResource(0);

        if (isBetween(age, 0, 2)) {
            centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_baby);
        } else if (isBetween(age, 3, 12)) {
            if (patient.getSexePat().equals("F"))
                centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_daughter);
            else
                centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_son);
        } else if (isBetween(age, 13, 17)) {
            if (patient.getSexePat().equals("F"))
                centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_daughter);
            else
                centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_son);
        } else if (isBetween(age, 18, 70)) {
            if (patient.getSexePat().equals("F"))
                centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_mother);
            else
                centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_father);
        } else if (isBetween(age, 71, 150)) {
            if (patient.getSexePat().equals("F"))
                centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_grandmother);
            else
                centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_grandfather);
        } else {
            if (patient.getSexePat().equals("F"))
                centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_mother);
            else
                centrePointHolder.ivPatientGender.setImageResource(R.drawable.ic_father);
        }

        centrePointHolder.ivCall.setOnClickListener(v -> {
            if (SPUtils.getInstance(Constants.AGENT_PREFERENCES).getBoolean(Constants.AGENT_CALL_PERMISSION, false)) {
                PhoneUtils.dial("+" + patient.getNumeroPat());
            } else {
                mListener.throwCallPermission();
            }
        });

        centrePointHolder.rlContainer.setOnClickListener(v -> {
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).clear();

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ID, String.valueOf(patient.getPatId()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_STATUT_FIEVRE_JAUNE, "0");

            if (patient.getFievreJaune().equals("0")) {
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE_FIEVRE_JAUNE, "null");
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOT_FIEVRE_JAUNE, "null");
            } else {
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE_FIEVRE_JAUNE, patient.getDateFievreJaune());
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOT_FIEVRE_JAUNE, patient.getLotFievreJaune());
            }

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PHOTO_CARNET, patient.getPhotoCarnetPat());

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_NAME, StringUtils.defaultString(patient.getNomPat() + " " + patient.getPrenomsPat()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_TELEPHONE, StringUtils.defaultString(patient.getNumeroPat()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_BIRTHDAY, StringUtils.defaultString(patient.getNaisaancePat()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_EMAIL, StringUtils.defaultString(patient.getEmailPat()));
            //SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION, StringUtils.reverseDelimited(StringUtils.defaultString(patient.getAbontExpiration()), '-'));
            //SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION, StringUtils.reverseDelimited(StringUtils.defaultString(patient.getAbontExpiration()), '-'));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION, patient.getAbontExpiration());
            //SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOGIN, StringUtils.defaultString(patient.get(), "INDEFINI"));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, patient.getSexePat());

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_ID, Integer.parseInt(patient.getFormule()));

            switch (patient.getFormule()) {
                case "2":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PREMUIM");
                    break;

                case "3":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PRIVILEGE");
                    break;

                case "12":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "GRATUIT");
                    break;

                default:
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "STANDARD");
            }


            ActivityUtils.startActivity(DashboardActivity.class, R.anim.slide_from_right, R.anim.slide_to_left);
        });

        centrePointHolder.tvPatientNom.setText(patient.getNomPat().concat(" " + patient.getPrenomsPat()));
        centrePointHolder.tvPatientNum.setText(patient.getNumeroPat());
        //centrePointHolder.tvAgentInfos.setText(agent.getAgentNomPrenoms().toUpperCase());
    }

    @Override
    public int getItemCount() {
        return patients.size();
    }


    static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }


    static class CentrePointHolder extends RecyclerView.ViewHolder {

        AppCompatTextView tvPatientNom;
        AppCompatTextView tvPatientNum;
        AppCompatTextView tvAgentInfos;
        AppCompatImageView ivPatientGender;
        AppCompatImageView ivCall;
        RelativeLayout rlContainer;

        CentrePointHolder(@NonNull View itemView) {
            super(itemView);

            tvAgentInfos = itemView.findViewById(R.id.centre_global_point_items_list_tv_agent_infos);
            tvPatientNom = itemView.findViewById(R.id.centre_global_point_items_list_tv_patient_nom);
            tvPatientNum = itemView.findViewById(R.id.centre_global_point_items_list_tv_patient_number);
            ivPatientGender= itemView.findViewById(R.id.centre_global_point_items_list_iv_gender);
            ivCall = itemView.findViewById(R.id.centre_global_point_items_list_iv_call);
            rlContainer = itemView.findViewById(R.id.centre_global_point_items_list_rl_container);
        }
    }

    public interface OnCentrePointAdapterListener {
        void throwCallPermission();
    }
}
