package com.gia.myrecu.ui.activities;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.JsonUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Message;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.ui.adapters.SmsPatientAdapter;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


@SuppressWarnings("ALL")
public class MessagesPatientActivity extends AppCompatActivity {


    List<Message> messageList;
    LoadMessagesPatientAsync loadMessagesPatientAsync;
    ProgressDialog loading;
    OkHttpClient clientMessages;
    Type typeMessages = new TypeToken<ArrayList<Message>>(){}.getType();
    SmsPatientAdapter patientSmsAdapter;
    RecyclerView rvList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_messages_patient);

        rvList = findViewById(R.id.activity_messages_patient_rv_list);

        clientMessages = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        messageList = new ArrayList<>();
        patientSmsAdapter = new SmsPatientAdapter(this, messageList);

        rvList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvList.setAdapter(patientSmsAdapter);

        loading = new ProgressDialog(this);
        loading.setMessage("Chargement...");

        loadMessagesPatientAsync = new LoadMessagesPatientAsync();
        loadMessagesPatientAsync.execute();
    }


    class LoadMessagesPatientAsync extends AsyncTask<Void, Void, Integer> {

        LoadMessagesPatientAsync() {}

        @Override
        protected void onPreExecute() {
            loading.show();
        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            try {
                HttpUrl.Builder agentUrlBuilder = HttpUrl.parse(UrlBanks.SMS_PATIENT).newBuilder();
                agentUrlBuilder.addQueryParameter("patId", SPUtils.getInstance(Constants.ABONNE_OLD_PREFERENCES).getString(Constants.ABONNE_OLD_ID, "1"));

                Request requestMessages = new Request.Builder()
                        .method("GET", null)
                        .url(agentUrlBuilder.build())
                        .build();

                Response responseMessages = clientMessages.newCall(requestMessages).execute();
                String stringMessage = responseMessages.body().string();

                if (responseMessages.isSuccessful()) {
                    code = JsonUtils.getInt(stringMessage, "code");

                    if (code == 0) {
                        messageList.clear();
                        messageList.addAll((ArrayList<Message>) GsonUtils.fromJson(JsonUtils.getJSONArray(stringMessage, "data", null).toString(), typeMessages));
                    }
                } else {
                    code = 2;
                }
            } catch (Exception ex) {
                code = 2;
                ex.printStackTrace();
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            try {
                loading.dismiss();
                //tvEmpty.setText("Aucun rendez-vous en attente");

                switch (integer) {
                    case 0 :
                        patientSmsAdapter.notifyDataSetChanged();
                        //tvEmpty.setVisibility(View.GONE);
                        rvList.setVisibility(View.VISIBLE);
                        break;

                    case 1 :
                        rvList.setVisibility(View.GONE);
                        //tvEmpty.setVisibility(View.VISIBLE);
                        break;

                    default:
                        rvList.setVisibility(View.GONE);
                        //tvEmpty.setText("Une erreur est survenue !");
                }

                //tvPatientsCount.setText(String.valueOf(patientList.size()) + " Patient(s)");
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }


    @Override
    public void onBackPressed() {
        loadMessagesPatientAsync.cancel(true);
        loadMessagesPatientAsync = null;
        super.onBackPressed();
    }
}
