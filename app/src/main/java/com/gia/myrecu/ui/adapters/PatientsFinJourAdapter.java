package com.gia.myrecu.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Patient;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.List;


@SuppressWarnings("ALL")
public class PatientsFinJourAdapter extends RecyclerView.Adapter<PatientsFinJourAdapter.PatientEnAttenteHolder> {


    private Context context;
    private List<Patient> patients;


    public PatientsFinJourAdapter(Context context, List<Patient> patients) {
        this.context = context;
        this.patients = patients;
    }


    @NonNull
    @Override
    public PatientEnAttenteHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View patientView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.patient_en_attente_items_list, viewGroup, false);
        return new PatientEnAttenteHolder(patientView);
    }


    @Override
    public void onBindViewHolder(@NonNull PatientEnAttenteHolder patientEnAttenteHolder, int i) {
        Patient patient = patients.get(patientEnAttenteHolder.getAdapterPosition());

        patientEnAttenteHolder.tvName.setText(patient.getPatientNomPrenoms());

        int age = DateTime.now().year().get() - DateTime.parse(StringUtils.reverseDelimited(patient.getPatientNaissance(), '-')).year().get();

        if (isBetween(age, 0, 2)) {
            patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_baby);
        } else if (isBetween(age, 3, 12)) {
            if (patient.getPatientSexe().equals("F"))
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_daughter);
            else
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_son);
        } else if (isBetween(age, 13, 17)) {
            if (patient.getPatientSexe().equals("F"))
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_daughter);
            else
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_son);
        } else if (isBetween(age, 18, 70)) {
            if (patient.getPatientSexe().equals("F"))
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_mother);
            else
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_father);
        } else if (isBetween(age, 71, 150)) {
            if (patient.getPatientSexe().equals("F"))
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_grandmother);
            else
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_grandfather);
        } else {
            if (patient.getPatientSexe().equals("F")) {
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_mother);
            } else {
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_father);
            }
        }


    }


    @Override
    public int getItemCount() {
        return patients.size();
    }


    static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

    static class PatientEnAttenteHolder extends RecyclerView.ViewHolder {

        AppCompatImageView ivPic;
        AppCompatTextView tvName;

        public PatientEnAttenteHolder(@NonNull View itemView) {
            super(itemView);
            ivPic = itemView.findViewById(R.id.patient_en_attente_items_list_iv_pic);
            tvName = itemView.findViewById(R.id.patient_en_attente_items_list_tv_name);
        }
    }
}
