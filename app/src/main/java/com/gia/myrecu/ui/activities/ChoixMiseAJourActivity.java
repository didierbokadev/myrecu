package com.gia.myrecu.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.tools.Constants;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.VACCIN_ADD_TYPE;


public class ChoixMiseAJourActivity extends AppCompatActivity {


    LinearLayout llInfosPersonnelles;
    LinearLayout llAddVaccin;
    LinearLayout llRdvVaccins;
    LinearLayout llProgrammeVaccins;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_mise_ajour);

        llInfosPersonnelles = findViewById(R.id.activity_choix_mise_ajour_btn_info_personnelle);
        llAddVaccin = findViewById(R.id.activity_choix_mise_ajour_btn_vaccin);
        llRdvVaccins = findViewById(R.id.activity_choix_mise_ajour_btn_rdv_vaccins);
        llProgrammeVaccins = findViewById(R.id.activity_choix_mise_ajour_btn_programme);

        setSupportActionBar((Toolbar) findViewById(R.id.activity_choix_mise_ajour_tb_title));
        getSupportActionBar().setSubtitle(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                .getString(ABONNE_OLD_NAME, ""));

        llInfosPersonnelles.setOnClickListener(v -> {
            startActivity(new Intent(ChoixMiseAJourActivity.this, UpdateInfosPersActivity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });

        llAddVaccin.setOnClickListener(v -> {
            SPUtils.getInstance(Constants.VACCIN_PREFERENCES).put(VACCIN_ADD_TYPE, "newest");
            startActivity(new Intent(ChoixMiseAJourActivity.this, SelectVacinesActivity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });

        llRdvVaccins.setOnClickListener(v -> {
            startActivity(new Intent(ChoixMiseAJourActivity.this, RDVVaciccinsActivity.class));
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });

        llProgrammeVaccins.setOnClickListener(v -> {
                startActivity(new Intent(ChoixMiseAJourActivity.this, SelectVacinesActivity.class));
                SPUtils.getInstance(Constants.VACCIN_PREFERENCES).put(VACCIN_ADD_TYPE, "scheduler");
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuId = item.getItemId();
        if (menuId == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
