package com.gia.myrecu.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.CalendrierVaccin;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.ui.adapters.VaccinAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.AGENT_ID;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;
import static com.gia.myrecu.tools.Constants.CENTRE_ID;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;


@SuppressWarnings("ALL")
public class VacAJourActivity extends AppCompatActivity {


    RecyclerView rvVaccinsAjours;
    VaccinAdapter vaccinAdapter;
    List<CalendrierVaccin> calendrierVaccinList;
    ProgressDialog loading;
    LinearLayout llContainerOkVac;
    AppCompatButton btnValidCarnet;
    String[] arrVaccinIdsToValid;
    ValidVaccine validVaccine;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vac_ajour);

        setSupportActionBar((Toolbar) findViewById(R.id.activity_vac_ajour_tb_title));
        getSupportActionBar().setSubtitle(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME, ""));

        rvVaccinsAjours = findViewById(R.id.activity_vac_ajour_rv_vac_ajour);
        llContainerOkVac = findViewById(R.id.activity_vac_ajour_ll_ok_vaccin);
        btnValidCarnet = findViewById(R.id.activity_vac_ajour_btn_valid_btn);

        calendrierVaccinList = new ArrayList<>();
        vaccinAdapter = new VaccinAdapter(VacAJourActivity.this, calendrierVaccinList);

        rvVaccinsAjours.setLayoutManager(new LinearLayoutManager(VacAJourActivity.this,
                LinearLayoutManager.VERTICAL, false));

        rvVaccinsAjours.setAdapter(vaccinAdapter);

        btnValidCarnet.setOnClickListener(v -> {
            if (arrVaccinIdsToValid != null) {
                if (arrVaccinIdsToValid.length == 0) {
                    ToastUtils.showShort("Aucune visite à valider !");
                    return;
                }

                validVaccine = new ValidVaccine();
                validVaccine.execute();
            }
        });

        // Load vaccins
        new LoadVacAJours().execute();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuId = item.getItemId();
        if (menuId == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }


    @SuppressLint("StaticFieldLeak")
    private class LoadVacAJours extends AsyncTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = new ProgressDialog(VacAJourActivity.this);
            loading.setMessage("Chargement des visites...");
            loading.setCancelable(false);
            loading.show();
        }

        @Override
        protected Object doInBackground(Object[] objects) {
            LogUtils.e("ID: " + SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ID));

            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();

                RequestBody formBody = new FormBody.Builder()
                        .add("patId", SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ID))
                        .add("d", BuildConfig.DATABASE)
                        .build();

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.LIST_VACCINS_A_JOUR)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();

                if (clientResponse.isSuccessful()) {
                    JSONObject json = new JSONObject(clientResponse.body().string());
                    String statut = json.getString("statut");

                    if (statut.equals("1")) {
                        Type typeList = new TypeToken<ArrayList<CalendrierVaccin>>() {
                        }.getType();

                        JSONArray jsonArray = json.getJSONArray("data");
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        ArrayList<CalendrierVaccin> calendrierVaccins = gson.fromJson(jsonArray.toString(), typeList);

                        calendrierVaccinList.addAll(calendrierVaccins);

                        int j = 0;

                        /*for (CalendrierVaccin cvLoop : calendrierVaccinList) {
                            if (cvLoop.getCalendrierFlagValidation().equals("0")) {
                                j = +1;
                            }
                        }*/

                        arrVaccinIdsToValid = new String[calendrierVaccinList.size()];

                        for (int i = 0; i < calendrierVaccinList.size(); i++) {
                            if (!calendrierVaccinList.get(i).getCalendrierFlagValidation().trim().equals("1")) {
                                arrVaccinIdsToValid[i] = calendrierVaccinList.get(i).getCalendrierVaccinId();
                            }
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                llContainerOkVac.setVisibility(View.GONE);
                                loading.dismiss();
                                vaccinAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                llContainerOkVac.setVisibility(View.VISIBLE);
                                Toaster.toast("Pas de visites à jour !");
                                loading.dismiss();
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            llContainerOkVac.setVisibility(View.VISIBLE);
                            Toaster.toast("Pas de visites à jour !");
                            loading.dismiss();
                        }
                    });
                }
            } catch (Exception ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        llContainerOkVac.setVisibility(View.VISIBLE);
                        Toaster.toast("Pas de visites à jour !");
                        loading.dismiss();
                    }
                });
                ex.printStackTrace();
                Toaster.toastLong("Une erreur de reseau est survenue. Ressayer plus tard.");
            }
            return null;
        }
    }


    private class ValidVaccine extends AsyncTask<Object, Object, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = new ProgressDialog(VacAJourActivity.this);
            loading.setMessage("Validation du carnet en cours...");
            loading.setCancelable(false);
            loading.show();
        }

        @Override
        protected Integer doInBackground(Object[] objects) {

            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(20, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();

                String calIdsGson = GsonUtils.toJson(arrVaccinIdsToValid);

                RequestBody formBody = new FormBody.Builder()
                        .add("patId", String.valueOf(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ID)))
                        .add("calIds", calIdsGson)
                        .add("usrId", String.valueOf(SPUtils.getInstance(AGENT_PREFERENCES).getInt(AGENT_ID, 0)))
                        .add("ctrId", String.valueOf(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID, 0)))
                        .add("d", BuildConfig.DATABASE)
                        .build();

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.VALID_USER_CALENDAR)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();
                String reponseStr = clientResponse.body().string();

                if (clientResponse.isSuccessful()) {
                    JSONObject json = new JSONObject(reponseStr);
                    String statut = json.getString("statut");

                    if (statut.equals("1")) {
                        return 1;
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Toaster.toastLong("Une erreur de reseau est survenue. Ressayer plus tard.");
            }
            return 0;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            if (integer == 1) {
                finish();
                //viewHolderGlobal.btnDoneVaccine.setVisibility(View.GONE);
                //notifyItemChanged(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getInt(ABONNE_OLD_VALID_VACCINE_ADAPTER_POSITION, 0));
            } else {
                Toaster.toastLong("Impossible de valider ce calendrier");
            }
            loading.dismiss();
        }
    }

}
