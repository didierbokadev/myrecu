package com.gia.myrecu.ui.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.PhoneUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Patient;
import com.gia.myrecu.ui.activities.DashboardActivity;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.List;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_EXPIRATION;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_PLANNING_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_TYPE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_BIRTHDAY;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_DATE_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_EMAIL;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOGIN;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PHOTO_CARNET;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE_END;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_STATUS;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_SEXE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_STATUT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_TELEPHONE;
import static com.gia.myrecu.tools.Constants.PATIENT_AGE_STATUS;
import static com.gia.myrecu.tools.Utils.detectAgeStatus;
import static com.gia.myrecu.tools.Utils.isBetween;

public class PatientMissedAdapter extends RecyclerView.Adapter<PatientMissedAdapter.PatientMissedHolder> {


    private Context ctx;
    private List<Patient> patients;
    private OnPatientMissedAdapterListener mListener;


    public PatientMissedAdapter(Context ctx, List<Patient> patients) {
        this.ctx = ctx;
        this.patients = patients;

        if (ctx instanceof OnPatientMissedAdapterListener) {
            mListener = (OnPatientMissedAdapterListener) ctx;
        } else {
            LogUtils.e("TAG", "Call permission");
        }
    }


    @NonNull
    @Override
    public PatientMissedHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View missedItemBinding = LayoutInflater.from(ctx).inflate(R.layout.missed_item, viewGroup, false);
        return new PatientMissedHolder(missedItemBinding);
    }


    @Override
    public void onBindViewHolder(@NonNull PatientMissedHolder patientMissedHolder, int i) {
        Patient missedPat = patients.get(patientMissedHolder.getAdapterPosition());

        patientMissedHolder.missedNane.setText(missedPat.getPatientNomPrenoms());
        patientMissedHolder.missedBirth.setText(missedPat.getPatientNaissance());
        patientMissedHolder.missedSex.setText(missedPat.getPatientTelephone());

        int age = DateTime.now().year().get() - DateTime.parse(StringUtils.reverseDelimited(missedPat.getPatientNaissance(), '-')).year().get();

        if (isBetween(age, 0, 2)) {
            patientMissedHolder.missedPic.setImageResource(R.drawable.ic_baby);
        } else if (isBetween(age, 3, 12)) {
            if (missedPat.getPatientSexe().equals("F"))
                patientMissedHolder.missedPic.setImageResource(R.drawable.ic_daughter);
            else
                patientMissedHolder.missedPic.setImageResource(R.drawable.ic_son);
        } else if (isBetween(age, 13, 17)) {
            if (missedPat.getPatientSexe().equals("F"))
                patientMissedHolder.missedPic.setImageResource(R.drawable.ic_daughter);
            else
                patientMissedHolder.missedPic.setImageResource(R.drawable.ic_son);
        } else if (isBetween(age, 18, 70)) {
            if (missedPat.getPatientSexe().equals("F"))
                patientMissedHolder.missedPic.setImageResource(R.drawable.ic_mother);
            else
                patientMissedHolder.missedPic.setImageResource(R.drawable.ic_father);
        } else if (isBetween(age, 71, 150)) {
            if (missedPat.getPatientSexe().equals("F"))
                patientMissedHolder.missedPic.setImageResource(R.drawable.ic_grandmother);
            else
                patientMissedHolder.missedPic.setImageResource(R.drawable.ic_grandfather);
        } else {
            if (missedPat.getPatientSexe().equals("F"))
                patientMissedHolder.missedPic.setImageResource(R.drawable.ic_mother);
            else
                patientMissedHolder.missedPic.setImageResource(R.drawable.ic_father);
        }

        patientMissedHolder.missedCall.setOnClickListener(v -> {
            PhoneUtils.dial("+" + missedPat.getPatientTelephone());
        });

        patientMissedHolder.missedContainer.setOnClickListener(v -> {
            Intent intent = new Intent(ctx, DashboardActivity.class);
            ctx.startActivity(intent);
            ((AppCompatActivity) ctx).overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).clear();

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ID, String.valueOf(missedPat.getPatientId()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_STATUT_FIEVRE_JAUNE, missedPat.getPatientStatutFJ());

            if (missedPat.getPatientStatutFJ().equals("0")) {
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE_FIEVRE_JAUNE, "null");
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOT_FIEVRE_JAUNE, "null");
            } else {
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE_FIEVRE_JAUNE, missedPat.getPatientDateFJ());
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOT_FIEVRE_JAUNE, missedPat.getPatientLotFJ());
            }

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PHOTO_CARNET, missedPat.getPatientPhotoCarnet());

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_NAME, StringUtils.defaultString(missedPat.getPatientNomPrenoms()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_TELEPHONE, StringUtils.defaultString(missedPat.getPatientTelephone()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_BIRTHDAY, StringUtils.defaultString(missedPat.getPatientNaissance()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_EMAIL, StringUtils.defaultString(missedPat.getPatientEmail()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION, missedPat.getPatientAbonnement());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_PLANNING_ID, missedPat.getPatientPlanningId());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOGIN, StringUtils.defaultString(missedPat.getPatientLogin(), "INDEFINI"));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, missedPat.getPatientSexe());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_ID, Integer.parseInt(missedPat.getPatientFormule()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_STATUS, (missedPat.getPatientPreg().equals("Y") ? "FEMME ENCEINTE" : "FEMME"));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE, missedPat.getPatientAgePreg());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE_END, missedPat.getPatientAccouch());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_TYPE_ID, missedPat.getPatientAbonnementType());


            switch (missedPat.getPatientFormule()) {
                case "2":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PREMUIM");
                    break;

                case "3":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PRIVILEGE");
                    break;

                case "12":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "GRATUIT");
                    break;

                default:
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "STANDARD");
            }

            SPUtils.getInstance().put(PATIENT_AGE_STATUS, detectAgeStatus(missedPat.getPatientNaissance()).toUpperCase());
        });
    }


    @Override
    public int getItemCount() {
        return patients.size();
    }


    static class PatientMissedHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView missedNane;
        private AppCompatTextView missedSex;
        private AppCompatTextView missedBirth;
        private AppCompatImageView missedCall;
        private AppCompatImageView missedPic;
        private LinearLayout missedContainer;

        public PatientMissedHolder(@NonNull View itemView) {
            super(itemView);

            missedNane = itemView.findViewById(R.id.missed_name);
            missedSex = itemView.findViewById(R.id.missed_sex);
            missedPic = itemView.findViewById(R.id.missed_image);
            missedBirth = itemView.findViewById(R.id.missed_birthday);
            missedCall = itemView.findViewById(R.id.image_call);
            missedContainer = itemView.findViewById(R.id.linear_container_infos);
        }
    }

    public interface OnPatientMissedAdapterListener {
        void throwCallPermission();
    }
}
