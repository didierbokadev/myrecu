package com.gia.myrecu.ui.adapters;


import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.PhoneUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Abonnement;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.List;

@SuppressWarnings("ALL")
public class AgentFinJourPointsAdapter extends RecyclerView.Adapter<AgentFinJourPointsAdapter.AgentFinJourPointsHolder> {


    private Context context;
    private List<Abonnement> abonnements;
    AppCompatTextView tvPatientsCount;
    private OnAgentPointAdapterListener mListener;


    public AgentFinJourPointsAdapter(Context context, List<Abonnement> abonnements, OnAgentPointAdapterListener pListener) {
        this.context = context;
        this.abonnements = abonnements;
        this.mListener = pListener;
    }


    @NonNull
    @Override
    public AgentFinJourPointsHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View patientView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.patients_fin_jour_items_list, viewGroup, false);
        return new AgentFinJourPointsHolder(patientView);
    }


    @Override
    public void onBindViewHolder(@NonNull AgentFinJourPointsHolder patientEnAttenteHolder, int i) {
        Abonnement abonnement = abonnements.get(patientEnAttenteHolder.getAdapterPosition());

        patientEnAttenteHolder.tvName.setText(abonnement.getAbonnementClientNom());
        patientEnAttenteHolder.tvNumber.setText(abonnement.getAbonnementClientNumero());

        try {
            int age = DateTime.now().year().get() - DateTime.parse(StringUtils.reverseDelimited(abonnement.getAbonnementClientNaissance(), '-')).year().get();

            if (isBetween(age, 0, 2)) {
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_baby);
            } else if (isBetween(age, 3, 12)) {
                if (abonnement.getAbonnementClientSexe().equals("F"))
                    patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_daughter);
                else
                    patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_son);
            } else if (isBetween(age, 13, 17)) {
                if (abonnement.getAbonnementClientSexe().equals("F"))
                    patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_daughter);
                else
                    patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_son);
            } else if (isBetween(age, 18, 70)) {
                if (abonnement.getAbonnementClientSexe().equals("F"))
                    patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_mother);
                else
                    patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_father);
            } else if (isBetween(age, 71, 150)) {
                if (abonnement.getAbonnementClientSexe().equals("F"))
                    patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_grandmother);
                else
                    patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_grandfather);
            } else {
                if (abonnement.getAbonnementClientSexe().equals("F")) {
                    patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_mother);
                } else {
                    patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_father);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        patientEnAttenteHolder.ivCall.setOnClickListener(v -> {
            // if (SPUtils.getInstance(Constants.AGENT_PREFERENCES).getBoolean(Constants.AGENT_CALL_PERMISSION, false)) {
                PhoneUtils.dial("+" + abonnement.getAbonnementClientNumero());
        });

        patientEnAttenteHolder.rlContainer.setOnClickListener(v -> {
            mListener.getClient(abonnement.getAbonnementId());
        });
    }


    @Override
    public int getItemCount() {
        return abonnements.size();
    }


    static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }


    static class AgentFinJourPointsHolder extends RecyclerView.ViewHolder {

        AppCompatImageView ivPic;
        AppCompatImageView ivCall;
        AppCompatTextView tvName;
        AppCompatTextView tvNumber;
        RelativeLayout rlContainer;

        public AgentFinJourPointsHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.patients_fin_jour_items_list_tv_name);
            ivPic = itemView.findViewById(R.id.patients_fin_jour_items_list_iv_pic);
            tvNumber = itemView.findViewById(R.id.patients_fin_jour_items_list_tv_number);
            ivCall = itemView.findViewById(R.id.patients_fin_jour_items_list_iv_call);
            rlContainer = itemView.findViewById(R.id.patients_fin_jour_items_list_rl_container);
        }
    }


    public interface OnAgentPointAdapterListener {
        void throwCallPermission();
        void getClient(int clientID);
    }
}
