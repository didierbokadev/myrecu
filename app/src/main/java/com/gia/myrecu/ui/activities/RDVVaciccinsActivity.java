package com.gia.myrecu.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import android.view.MenuItem;
import android.view.View;

import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;

public class RDVVaciccinsActivity extends AppCompatActivity implements View.OnClickListener {


    AppCompatButton btnVacAJours;
    AppCompatButton btnVacEffect;
    AppCompatButton btnVacManques;
    AppCompatButton btnVacAvenir;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rdvvaciccins);

        setupViews();

        setSupportActionBar(findViewById(R.id.activity_rdvvaciccins_tb_title));
        getSupportActionBar().setSubtitle(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME, ""));

        btnVacAvenir.setOnClickListener(this);
        btnVacAJours.setOnClickListener(this);
        btnVacManques.setOnClickListener(this);
    }


    public void setupViews() {
        btnVacAJours = findViewById(R.id.activity_rdvvaciccins_btn_a_jour);
        btnVacManques = findViewById(R.id.activity_rdvvaciccins_btn_vac_manques);
        btnVacAvenir= findViewById(R.id.activity_rdvvaciccins_btn_vac_a_venir);
    }


    @Override
    public void onClick(View v) {
        int idView = v.getId();

        switch (idView) {
            case R.id.activity_rdvvaciccins_btn_a_jour :
                Intent intentAjour = new Intent(RDVVaciccinsActivity.this, VacAJourActivity.class);
                startActivity(intentAjour);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            break;

            case R.id.activity_rdvvaciccins_btn_vac_manques :
                Intent intentMisses = new Intent(RDVVaciccinsActivity.this, VacMissedActivity.class);
                startActivity(intentMisses);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            break;

            case R.id.activity_rdvvaciccins_btn_vac_a_venir :
                Intent intenProchains = new Intent(RDVVaciccinsActivity.this, VacProchainsActivity.class);
                startActivity(intenProchains);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            break;
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int idMenu = item.getItemId();

        switch (idMenu) {
            case android.R.id.home :
                onBackPressed();
            break;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
