package com.gia.myrecu.ui.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.BusUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.PhoneUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Patient;
import com.gia.myrecu.ui.activities.DashboardActivity;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.List;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_EXPIRATION;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_PLANNING_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_BIRTHDAY;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_DATE_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_EMAIL;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOGIN;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PHOTO_CARNET;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE_END;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_STATUS;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_SEXE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_STATUT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_TELEPHONE;


@SuppressWarnings("ALL")
public class PatientEnAttenteAdapter extends RecyclerView.Adapter<PatientEnAttenteAdapter.PatientEnAttenteHolder> {


    private Context context;
    private List<Patient> patients;
    private OnPatientAttenteAdapterListener mListener;


    public PatientEnAttenteAdapter(Context context, List<Patient> patients) {
        this.context = context;
        this.patients = patients;

        if (context instanceof OnPatientAttenteAdapterListener) {
            mListener = (OnPatientAttenteAdapterListener) context;
        } else {
            LogUtils.e("TAG", "Call permission");
        }
    }


    @NonNull
    @Override
    public PatientEnAttenteHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View patientView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.patient_en_attente_items_list, viewGroup, false);
        return new PatientEnAttenteHolder(patientView);
    }


    @Override
    public void onBindViewHolder(@NonNull PatientEnAttenteHolder patientEnAttenteHolder, int i) {
        Patient patient = patients.get(patientEnAttenteHolder.getAdapterPosition());

        patientEnAttenteHolder.tvName.setText(patient.getPatientNomPrenoms());
        patientEnAttenteHolder.tvNumber.setText(patient.getPatientTelephone());

        int age = DateTime.now().year().get() - DateTime.parse(StringUtils.reverseDelimited(patient.getPatientNaissance(), '-')).year().get();

        if (isBetween(age, 0, 2)) {
            patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_baby);
        } else if (isBetween(age, 3, 12)) {
            if (patient.getPatientSexe().equals("F"))
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_daughter);
            else
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_son);
        } else if (isBetween(age, 13, 17)) {
            if (patient.getPatientSexe().equals("F"))
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_daughter);
            else
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_son);
        } else if (isBetween(age, 18, 70)) {
            if (patient.getPatientSexe().equals("F"))
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_mother);
            else
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_father);
        } else if (isBetween(age, 71, 150)) {
            if (patient.getPatientSexe().equals("F"))
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_grandmother);
            else
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_grandfather);
        } else {
            if (patient.getPatientSexe().equals("F")) {
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_mother);
            } else {
                patientEnAttenteHolder.ivPic.setImageResource(R.drawable.ic_father);
            }
        }

        if (patient.getPatientDatePres().trim().equals("0000-00-00") || patient.getPatientDatePres() == null || patient.getPatientDatePres().equals("") || patient.getPatientDatePres().length() == 0) {
            patientEnAttenteHolder.ivMissed.setVisibility(View.VISIBLE);
        } else {
            patientEnAttenteHolder.ivMissed.setVisibility(View.GONE);
            patientEnAttenteHolder.ivCall.setVisibility(View.GONE);
        }

        patientEnAttenteHolder.rlContainer.setOnClickListener(v -> {
            LogUtils.e("ID PATIENT : " + patient.getPatientId());

            Intent intent = new Intent(context, DashboardActivity.class);
            context.startActivity(intent);
            ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).clear();

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ID, String.valueOf(patient.getPatientId()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_STATUT_FIEVRE_JAUNE, patient.getPatientStatutFJ());

            if (patient.getPatientStatutFJ().equals("0")) {
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE_FIEVRE_JAUNE, "null");
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOT_FIEVRE_JAUNE, "null");
            } else {
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE_FIEVRE_JAUNE, patient.getPatientDateFJ());
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOT_FIEVRE_JAUNE, patient.getPatientLotFJ());
            }

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PHOTO_CARNET, patient.getPatientPhotoCarnet());

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_NAME, StringUtils.defaultString(patient.getPatientNomPrenoms()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_TELEPHONE, StringUtils.defaultString(patient.getPatientTelephone()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_BIRTHDAY, StringUtils.defaultString(patient.getPatientNaissance()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_EMAIL, StringUtils.defaultString(patient.getPatientEmail()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION, StringUtils.defaultString(patient.getPatientAbonnement()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOGIN, StringUtils.defaultString(patient.getPatientLogin(), "INDEFINI"));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_PLANNING_ID, patient.getPatientPlanningId());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, patient.getPatientSexe());

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_STATUS, (patient.getPatientPreg().equals("Y") ? "FEMME ENCEINTE" : "FEMME"));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE, patient.getPatientAgePreg());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE_END, patient.getPatientAccouch());

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_ID, Integer.parseInt(patient.getPatientFormule()));

            switch (patient.getPatientFormule()) {
                case "2":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PREMUIM");
                    break;

                case "3":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PRIVILEGE");
                    break;

                case "12":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "GRATUIT");
                    break;

                default:
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "STANDARD");
            }
        });

        patientEnAttenteHolder.ivCall.setOnClickListener(v -> {
                PhoneUtils.dial("+" + patient.getPatientTelephone());

        });

        if (patientEnAttenteHolder.getAdapterPosition() >= patients.size() - 1) {
            BusUtils.post("ReachedBottom", patientEnAttenteHolder.getAdapterPosition());
        }
    }


    @Override
    public int getItemCount() {
        return patients.size();
    }


    static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

    static class PatientEnAttenteHolder extends RecyclerView.ViewHolder {

        AppCompatImageView ivPic;
        AppCompatImageView ivCall;
        AppCompatImageView ivMissed;
        AppCompatTextView tvName;
        AppCompatTextView tvNumber;
        RelativeLayout rlContainer;

        public PatientEnAttenteHolder(@NonNull View itemView) {
            super(itemView);
            rlContainer = itemView.findViewById(R.id.patient_en_attente_items_list_rl_container);
            ivPic = itemView.findViewById(R.id.patient_en_attente_items_list_iv_pic);
            ivCall = itemView.findViewById(R.id.patient_en_attente_items_list_iv_call);
            ivMissed = itemView.findViewById(R.id.patient_en_attente_items_list_iv_rdv_missed);
            tvName = itemView.findViewById(R.id.patient_en_attente_items_list_tv_name);
            tvNumber = itemView.findViewById(R.id.patient_en_attente_items_list_tv_number);
        }
    }


    public interface OnPatientAttenteAdapterListener {
        void throwCallPermission();
    }
}
