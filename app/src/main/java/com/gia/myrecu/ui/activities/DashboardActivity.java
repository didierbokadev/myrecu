package com.gia.myrecu.ui.activities;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.io.ByteArrayOutputStream;
import java.util.Locale;

import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_EXPIRATION;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_BIRTHDAY;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_DATE_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_REABONNEMENT_EXPIRATION;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_SEXE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_STATUT_FIEVRE_JAUNE;


@SuppressWarnings("ALL")
public class DashboardActivity extends AppCompatActivity {


    Toolbar toolbar;
    ActionBar actionBar;
    LinearLayout llMiseAJour;
    LinearLayout llReabonnement;
    LinearLayout llDeclaration;
    LinearLayout llSms;
    AppCompatTextView tvDateAbonnement;
    NfcAdapter mNfcAdapter;
    LinearLayout llCreeBadge;
    AlertDialog dialog;
    boolean isClicked;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        findViewByIds();
        setSupportActionBar(toolbar);
        actionBar = getSupportActionBar();

        mNfcAdapter = NfcAdapter.getDefaultAdapter(this);

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_border_color_white_vector_24dp);
            actionBar.setSubtitle(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME, ""));
        }

        KeyboardUtils.hideSoftInput(this);

        llReabonnement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, ReabonnementActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });

        llMiseAJour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DashboardActivity.this, ChoixMiseAJourActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });

        isClicked = false;

        llCreeBadge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mNfcAdapter == null) {
                    Toaster.toast("Cette fonctionnalité n'est pas disponible pour ce appareil.");
                    return;
                }

                isClicked = true;

                AlertDialog.Builder builder = new AlertDialog.Builder(DashboardActivity.this)
                        .setMessage("Approcher une carte pour creer un badge !")
                        .setCancelable(true)
                        .setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                isClicked = false;
                                dialog.dismiss();
                            }
                        });
                dialog = builder.create();
                dialog.show();
            }
        });

        llDeclaration.setOnClickListener(v -> ActivityUtils.startActivity(DeclarationDashboardActivity.class, R.anim.slide_from_right, R.anim.slide_to_left));

        llSms.setOnClickListener(v -> {
            ActivityUtils.startActivity(MessagesPatientActivity.class, R.anim.slide_from_right, R.anim.slide_to_left);
        });
    }


    private void findViewByIds() {
        toolbar = findViewById(R.id.activity_dashboard_tb_title);
        llMiseAJour = findViewById(R.id.activity_dashboard_btn_mise_a_jour);
        llSms = findViewById(R.id.activity_dashboard_btn_sms);
        llReabonnement = findViewById(R.id.activity_dashboard_btn_reabonnement);
        llCreeBadge = findViewById(R.id.activity_dashboard_btn_write_nfc);
        llDeclaration = findViewById(R.id.activity_dashboard_btn_declaration);
        tvDateAbonnement = findViewById(R.id.activity_dashboard_tv_date_abonnement);
    }


    private NdefRecord createDataNdefRecord(String content) {
        try {
            byte[] languages;
            languages = Locale.getDefault().getLanguage().getBytes("UTF-8");

            final byte[] text = content.getBytes();
            final int languagueSize = languages.length;
            final int textSize = text.length;
            final ByteArrayOutputStream playload = new ByteArrayOutputStream(1 + languagueSize + textSize);

            playload.write((byte) languagueSize & 0x1F);
            playload.write(languages, 0, languagueSize);
            playload.write(text, 0, textSize);

            return new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0],
                    playload.toByteArray());
        } catch (Exception ex) {
            isClicked = false;
            dialog.dismiss();
            ex.printStackTrace();
        }

        return null;
    }


    private NdefMessage createDataNdefMessage(String content) {
        NdefRecord ndefRecord = createDataNdefRecord(content);
        NdefMessage ndefMessage = new NdefMessage(new NdefRecord[]{ndefRecord});
        return ndefMessage;
    }


    private void writeDatas(Tag tag, NdefMessage ndefMessage) {
        try {
            if (tag == null) {
                isClicked = false;
                dialog.dismiss();
                return;
            }

            Ndef ndef = Ndef.get(tag);

            if (ndef == null) {
                formatDatas(tag, ndefMessage);
            } else {
                ndef.connect();

                if (!ndef.isWritable()) {
                    ndef.close();
                    isClicked = false;
                    dialog.dismiss();
                    Toast.makeText(this, "Badge non créé !", Toast.LENGTH_SHORT).show();
                    return;
                }

                ndef.writeNdefMessage(ndefMessage);
                ndef.close();
                isClicked = false;
                dialog.dismiss();
                Toast.makeText(this, "Badge créé !", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            isClicked = false;
            dialog.dismiss();
            ex.printStackTrace();
        }
    }


    private void formatDatas(Tag tag, NdefMessage ndefMessage) {
        try {
            NdefFormatable ndefFormatable = NdefFormatable.get(tag);

            // Check if format is correct and had ndef format
            if (ndefFormatable == null) {
                Toast.makeText(this, "Error format ! Bad", Toast.LENGTH_SHORT).show();
            }
            // Otherwise
            ndefFormatable.connect();
            ndefFormatable.format(ndefMessage);
            ndefFormatable.close();
        } catch (Exception mmte) {
            isClicked = false;
            dialog.dismiss();
            mmte.printStackTrace();
        }
    }


    public static void disableForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        adapter.disableForegroundDispatch(activity);
    }


    public static void enableForegroundDispatch(final Activity activity, NfcAdapter adapter) {
        final Intent intent = new Intent(activity.getApplicationContext(), activity.getClass());
        intent.setFlags(Intent.FLAG_RECEIVER_REPLACE_PENDING);

        final PendingIntent pendingIntent = PendingIntent.getActivity(activity.getApplicationContext(),
                0, intent, 0);

        IntentFilter[] filters = new IntentFilter[]{};
        adapter.enableForegroundDispatch(activity, pendingIntent, filters, null);
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);

        if (isClicked) {
            if (intent.hasExtra(NfcAdapter.EXTRA_TAG)) {
                String messageToWrite;

                String abonneId = "abnId:" + String.valueOf(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getInt(ABONNE_OLD_ID));
                String abonneNomPrenoms = "abnNomPrens:" + String.valueOf(StringUtils.replace(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                        .getString(ABONNE_OLD_NAME), " ", "|"));
                String abonneNaiss = "abnNaiss:" + String.valueOf(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                        .getString(ABONNE_OLD_BIRTHDAY));
                String abonneSex = "abnSex:" + String.valueOf(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                        .getString(ABONNE_OLD_SEXE));
                String abonneAbonnement = "abnDateAbo:" + String.valueOf(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_EXPIRATION));
                String abonneFievJauStat = SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_STATUT_FIEVRE_JAUNE).equals("0") ? "Non vacciné(e)" : "Vacciné(e)";

                abonneFievJauStat = "abnFievJauStat:" + abonneFievJauStat;

                String abonneDateFJ = "abnFievJauDate:" + "Non definie";
                String abonneLotFJ = "abnFievJauLot:" + "Non definie";

                if (!SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_STATUT_FIEVRE_JAUNE).equals("0")) {
                    abonneDateFJ = "abnFievJauDate:" + SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_DATE_FIEVRE_JAUNE);
                    abonneLotFJ = "abnFievJauLot:" + SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_LOT_FIEVRE_JAUNE);
                }

                messageToWrite = abonneId.concat(";").concat(abonneNomPrenoms).concat(";")
                        .concat(abonneNaiss).concat(";").concat(abonneSex).concat(";").concat(abonneAbonnement)
                        .concat(";").concat(abonneFievJauStat).concat(";").concat(abonneDateFJ).concat(";")
                        .concat(abonneLotFJ);

                Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                NdefMessage ndefMessage = createDataNdefMessage(messageToWrite);
                writeDatas(tag, ndefMessage);
            }
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        if (mNfcAdapter != null) {
            enableForegroundDispatch(this, mNfcAdapter);
        }

        try {
            if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_REABONNEMENT_EXPIRATION, "0").equals("0")) {
                if (!SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_EXPIRATION, "defini-non").equals("defini-non")) {
                    DateTime dateTime = new DateTime(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_EXPIRATION));

                    tvDateAbonnement.setText(dateTime.toString("dd MMMM yyyy"));

                    if (dateTime.isBeforeNow()) {
                        tvDateAbonnement.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                    } else {
                        tvDateAbonnement.setTextColor(getResources().getColor(R.color.colorPrimaryTextLightBlack));
                    }
                }
            } else {
                if (!SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_EXPIRATION, "defini-non").equals("defini-non")) {
                    DateTime dateTime = new DateTime(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_REABONNEMENT_EXPIRATION));

                    tvDateAbonnement.setText(dateTime.toString("dd MMMM yyyy"));

                    if (dateTime.isBeforeNow()) {
                        tvDateAbonnement.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
                    } else {
                        tvDateAbonnement.setTextColor(getResources().getColor(R.color.colorPrimaryTextLightBlack));
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    @Override
    protected void onPause() {
        if (mNfcAdapter != null) {
            disableForegroundDispatch(this, mNfcAdapter);
        }
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
