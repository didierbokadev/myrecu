package com.gia.myrecu.ui.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Patient;
import com.gia.myrecu.network.OPIApiClient;
import com.gia.myrecu.network.OPIApiServices;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.ui.adapters.PatientMissedAdapter;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;


@SuppressWarnings("ALL")
public class PerduDeVueActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener, PatientMissedAdapter.OnPatientMissedAdapterListener {


    OPIApiServices apiServices;
    List<Patient> patientsMissedList;
    PatientMissedAdapter missedAdapter;

    AppCompatTextView labelDateOpen;
    AppCompatTextView labelTotal;
    AppCompatTextView labelEmptyData;
    AppCompatTextView labelDateClose;
    RecyclerView listPatients;

    AppCompatButton clickSearch;

    AppCompatImageView imageClose;

    LinearLayout linearDateOpen;
    LinearLayout linearDateClose;

    int centreId;
    Calendar dateCalendar;
    private String dateOpen = "";
    private String dateClose = "";
    ProgressDialog loading;
    int whichDateSelected = 0;
    private String abonnementType = "1";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perdu_de_vue);

        labelDateOpen = findViewById(R.id.dateOpen);
        labelDateClose = findViewById(R.id.dateClose);
        labelTotal = findViewById(R.id.missed_total);
        labelEmptyData = findViewById(R.id.activity_agent_rendez_vous_daily_tv_empty);
        listPatients = findViewById(R.id.patients_list);

        linearDateOpen = findViewById(R.id.linearDateOpen);
        linearDateOpen = findViewById(R.id.linearDateClose);

        clickSearch = findViewById(R.id.click_search);

        apiServices = OPIApiClient.getClient(this).create(OPIApiServices.class);
        centreId = SPUtils.getInstance(Constants.DISTRICT_CENTRE_PREFERENCES).getInt(Constants.CENTRE_ID, 0);

        dateCalendar = Calendar.getInstance();

        loading = new ProgressDialog(this);
        loading.setMessage("Chargement en cours...");
        loading.setCancelable(true);

        labelDateOpen.setText(DateTime.now().toString("dd-MM-yyyy"));
        labelDateClose.setText(DateTime.now().toString("dd-MM-yyyy"));

        patientsMissedList = new ArrayList<>();
        missedAdapter = new PatientMissedAdapter(this, patientsMissedList);
        listPatients.setAdapter(missedAdapter);

        linearDateOpen.setOnClickListener(v -> {
            whichDateSelected = 1;

            clickSearch.setVisibility(View.GONE);
            DatePickerDialog datePickerOpenDialog = new DatePickerDialog(
                    this, this, dateCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH), dateCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerOpenDialog.getDatePicker().setMaxDate(DateTime.now().getMillis());
            datePickerOpenDialog.show();
        });

        linearDateClose.setOnClickListener(v -> {
            whichDateSelected = 2;
            clickSearch.setVisibility(View.GONE);
            DatePickerDialog datePickerCloseDialog = new DatePickerDialog(
                    this, this, dateCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH), dateCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerCloseDialog.getDatePicker().setMaxDate(DateTime.now().getMillis());
            datePickerCloseDialog.show();
        });

        clickSearch.setOnClickListener(v -> {
            if (dateOpen.length() < 9) {
                ToastUtils.showShort("Selectionner la date");
                return;
            }

            loadMissed();
        });

        imageClose.setOnClickListener( v -> finish());
        dateOpen = DateTime.now().toString("yyyy-MM-dd");
        dateClose = DateTime.now().toString("yyyy-MM-dd");
    }


    void loadMissed() {
        loading.show();
        apiServices.getPatientMissed(
                "" + String.valueOf(centreId),
                "" + dateOpen,
                "" + dateClose,
                "" + abonnementType,
                "" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Patient>>() {
                    @Override
                    public void onSuccess(List<Patient> patients) {
                        patientsMissedList.clear();

                        patientsMissedList.addAll(patients);
                        missedAdapter.notifyDataSetChanged();

                        labelTotal.setText(patientsMissedList.size() + " patient(s)");
                        loading.dismiss();

                        if (patientsMissedList.size() > 0) {
                            labelEmptyData.setVisibility(View.GONE);
                        }

                        if (patientsMissedList.size() == 0) {
                            labelEmptyData.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                        loading.dismiss();
                    }
                });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 2017) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                SPUtils.getInstance(Constants.AGENT_PREFERENCES).put(Constants.AGENT_CALL_PERMISSION, true);
            } else {
                SPUtils.getInstance(Constants.AGENT_PREFERENCES).put(Constants.AGENT_CALL_PERMISSION, false);
            }
        }
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String monthS = "";
        DateTime dateTime = new DateTime(year, (month + 1), dayOfMonth, 0, 0);

        if (whichDateSelected == 1) {
            dateOpen = dateTime.toString("yyyy-MM-dd");
            labelDateOpen.setText(dateTime.toString("dd MMM yyyy"));
        }

        if (whichDateSelected == 2) {
            dateClose = dateTime.toString("yyyy-MM-dd");
            labelDateClose.setText(dateTime.toString("dd MMM yyyy"));
        }

        if (dateOpen.length() > 9 && dateClose.length() > 9) {
            clickSearch.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void throwCallPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 2017);
        }
    }


    public void onTypeAbonnementClicked(View view) {
        boolean checkedAbonnement = ((AppCompatRadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioVac : // Show vaccine tools
                if (checkedAbonnement) {
                    abonnementType = "1";
                }
                break;

            case R.id.radioPlan: // Show planning tools
                if (checkedAbonnement) {
                    abonnementType = "2";
                }
                break;

            case R.id.radioPreg: // Show Pregnant tools
                if (checkedAbonnement) {
                    abonnementType = "3";
                }
                break;
        }
    }
}
