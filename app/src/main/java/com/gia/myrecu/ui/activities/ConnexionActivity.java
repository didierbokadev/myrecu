package com.gia.myrecu.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.NetworkUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.AppController;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.tools.UrlBanks;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.gia.myrecu.tools.Constants.AGENT_ID;
import static com.gia.myrecu.tools.Constants.AGENT_LOGIN;
import static com.gia.myrecu.tools.Constants.AGENT_LOG_STATUT;
import static com.gia.myrecu.tools.Constants.AGENT_NAME;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;
import static com.gia.myrecu.tools.Constants.PHONE_IS_CONFIGURED;
import static com.gia.myrecu.tools.Constants.PHONE_PREFERENCES;


@SuppressWarnings("ALL")
public class ConnexionActivity extends AppCompatActivity {


    AppCompatButton btnConnection;
    AppCompatEditText etEmail;
    AppCompatEditText etPwd;
    String email = "";
    String pass = "";
    public static final String TAG = "ConnexionActivity";
    ProgressDialog dialog;
    ConnectionUser asyncConnectionUser;
    Handler handler;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);
        btnConnection = findViewById(R.id.activity_connexion_btn_log);
        etEmail = findViewById(R.id.activity_connexion_et_login);
        etPwd = findViewById(R.id.activity_connexion_et_pawd);

        handler = new Handler();

        dialog = new ProgressDialog(ConnexionActivity.this);
        dialog.setMessage("Connexion en cours...");
        dialog.setCancelable(false);

        btnConnection.setOnClickListener(v -> {
            btnConnection.setEnabled(false);
            if (NetworkUtils.isConnected() || NetworkUtils.isWifiConnected()) {
                    if (etEmail.getText().toString().length() > 0 && etPwd.getText().toString().length() > 0) {
                        asyncConnectionUser = new ConnectionUser();
                        asyncConnectionUser.execute();
                    } else {
                        btnConnection.setEnabled(true);
                        return;
                    }
            } else {
                btnConnection.setEnabled(true);
                Toast.makeText(ConnexionActivity.this, "Veuillez vous connecter à internet", Toast.LENGTH_SHORT).show();
                finish();
            }
        });

        etEmail.setText("didierboka@opisms.org");
        etPwd.setText("20201104**");
    }


    @SuppressLint("StaticFieldLeak")
    private class ConnectionUser extends AsyncTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            dialog.show();
            email = etEmail.getText().toString();
            pass = etPwd.getText().toString().trim();
        }

        @Override
        protected Object doInBackground(Object[] params) {

            OkHttpClient userHttp = new OkHttpClient.Builder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(90, TimeUnit.SECONDS)
                    .readTimeout(90, TimeUnit.SECONDS)
                    .build();

            RequestBody formBody = new FormBody.Builder()
                    .add("login", email)
                    .add("password", pass)
                    .add("d", BuildConfig.DATABASE)
                    .build();

            Request requestUser = new Request.Builder()
                    .post(formBody)
                    .url(UrlBanks.AGENT_CONNEXION)
                    .build();

            try {
                Response responseUser = userHttp.newCall(requestUser).execute();
                String responseString = responseUser.body().string();

                if (responseUser.isSuccessful()) {
                    parseUserConnection(responseString);
                } else {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            btnConnection.setEnabled(true);
                            dialog.dismiss();
                        }
                    });
                }
            } catch (IOException e) {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        btnConnection.setEnabled(true);
                        dialog.dismiss();
                    }
                });
            }
            return null;
        }
    }


    private void parseUserConnection(String s) {
        try {
            JSONObject jsonObject = new JSONObject(s);
            int status = jsonObject.getInt("statut");
            if (status == 1) {
                JSONArray jsonArray = jsonObject.getJSONArray("data");
                int userId = jsonArray.getJSONObject(0).getInt("agent_id");
                String userLogin = jsonArray.getJSONObject(0).getString("agent_login");
                String userNom = jsonArray.getJSONObject(0).getString("agent_name");
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                        Toast.makeText(ConnexionActivity.this, "Connecté", Toast.LENGTH_LONG).show();
                        if (SPUtils.getInstance(PHONE_PREFERENCES).getBoolean(PHONE_IS_CONFIGURED, false)) {

                            AppController.startNewTopActivity(ConnexionActivity.this, SearchActivity.class);
                            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        } else {
                            AppController.startNewTopActivity(ConnexionActivity.this, ConfigsActivity.class);
                            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                        }
                    }
                });
                SPUtils.getInstance(AGENT_PREFERENCES).put(AGENT_ID, userId);
                SPUtils.getInstance(AGENT_PREFERENCES).put(AGENT_LOGIN, userLogin);
                SPUtils.getInstance(AGENT_PREFERENCES).put(AGENT_NAME, userNom);
                SPUtils.getInstance(AGENT_PREFERENCES).put(AGENT_LOG_STATUT, true);
            } else {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        btnConnection.setEnabled(true);
                        dialog.dismiss();
                        Toast.makeText(ConnexionActivity.this, "Login ou mot de passe incorrect",
                                Toast.LENGTH_LONG).show();
                    }
                });
            }
        } catch (JSONException jse) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    btnConnection.setEnabled(true);
                    dialog.dismiss();
                }
            });
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

}
