package com.gia.myrecu.ui.activities;


import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.databases.room.AndroidSmsDatabase;
import com.gia.myrecu.models.CommonResponse;
import com.gia.myrecu.models.v1.Abonnement;
import com.gia.myrecu.models.v1.Patient;
import com.gia.myrecu.models.v1.Vaccin;
import com.gia.myrecu.models.v2.Carnet;
import com.gia.myrecu.models.v2.CarnetRequest;
import com.gia.myrecu.models.v2.CommuneModel;
import com.gia.myrecu.models.v2.NiveauClasseModel;
import com.gia.myrecu.models.v2.NiveauEtudeModel;
import com.gia.myrecu.models.v2.ProfessionModel;
import com.gia.myrecu.models.v2.QuartierModel;
import com.gia.myrecu.models.v2.RecuUsedResponse;
import com.gia.myrecu.models.v2.VilleModel;
import com.gia.myrecu.network.OPIApiClient;
import com.gia.myrecu.network.OPIApiServices;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.tools.ImageFilePath;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.tools.Utils;
import com.gia.myrecu.ui.adapters.BluetoothDeviceAdapter;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_NEW_ABONNEMENT_TYPE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_ABONNEMENT_TYPE_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_AMOUNT;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_AVAILABLE;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_DATE;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_EXPIRATION;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_FORMULE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_FORMULE_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_HOUR;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_LANGUE;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_LOGIN;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_NAISSANCE;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_NUMBER;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_PREGNANCY_AGE;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_PREGNANCY_AGE_BEGIN;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_PREGNANCY_AGE_END;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_PREGNANCY_STATUS;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_RECEIPT;
import static com.gia.myrecu.tools.Constants.ABONNE_NEW_SEXE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_EXPIRATION;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_TYPE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_BIRTHDAY;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_EMAIL;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOGIN;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PHOTO_CARNET;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE_BEGIN;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE_END;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_STATUS;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_SEXE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_TELEPHONE;
import static com.gia.myrecu.tools.Constants.ABONNE_PHOTO_CARNET;
import static com.gia.myrecu.tools.Constants.AGENT_ID;
import static com.gia.myrecu.tools.Constants.AGENT_NAME;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;
import static com.gia.myrecu.tools.Constants.CENTRE_ID;
import static com.gia.myrecu.tools.Constants.CENTRE_NAME;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;
import static com.gia.myrecu.tools.Constants.PATIENT_AGE_STATUS;
import static com.gia.myrecu.tools.Constants.PHONE_PREFERENCES;
import static com.gia.myrecu.tools.Constants.VACCIN_ADD_TYPE;
import static com.gia.myrecu.tools.Constants.VACCIN_PREFERENCES;
import static com.gia.myrecu.tools.Utils.pickDate;

@SuppressWarnings("ALL")
public class AbonnementActivity extends AppCompatActivity implements BluetoothDeviceAdapter.OnClickDeviceBluetooth {


    ProgressDialog dialog;
    AppCompatEditText etNom;
    AppCompatEditText etPrenoms;
    AppCompatEditText etNaissance;
    AppCompatEditText etTelephone;
    AppCompatEditText etNombreAnne;
    AppCompatEditText etAgeGrossesse;
    AppCompatEditText etLotPlanning;
    AppCompatEditText etExactDatePlanning;
    AppCompatEditText etPlanningMonth;
    AppCompatEditText inputChildNumber;
    AppCompatEditText etDateAccouchement;
    AppCompatSpinner spSexe;
    AppCompatSpinner spLangue;

    SearchableSpinner spVille;
    SearchableSpinner spCommune;
    SearchableSpinner spQuartier;
    AppCompatSpinner spEtude;
    AppCompatSpinner spClasse;
    AppCompatSpinner selectMessagePref;
    AppCompatSpinner selectMessageLang;
    SearchableSpinner spProfession;

    AppCompatImageView imageDatePlanning;
    AppCompatImageView imageDateAccouchement;
    AppCompatButton btnAbonnement;
    LinearLayout llPregnantContainer;

    String TAG = "AbonnementActivity";
    final String REGEX = "\\d{8}";
    RadioGroup rgFormule;
    RadioGroup rgAbonnmentType;
    AndroidSmsDatabase database;
    private Connection btConnection;
    private ZebraPrinter zbPrinter;
    ConnectBTDevice connectBTDevice;
    int counPrintTentative;

    DatePickerDialog dialogDatePicker;

    AppCompatImageView ivTakePhoto;
    AppCompatImageView ivImportPhoto;

    private String abonnementType = "1";
    String messagePref = "SMS";
    String messageLang = "FRANCAIS";

    BluetoothAdapter bluetoothAdapter;
    Set<BluetoothDevice> btDevicesAlreadyBondedList;
    BluetoothDevice bluetoothDevice;
    BluetoothDeviceAdapter bluetoothDeviceAdapter;
    List<BluetoothDevice> deviceList;
    AlertDialog dialogPrinters;

    LinearLayout linearFormuleContainer;
    LinearLayout linearPlanningMonthContainer;
    LinearLayout linearPlanningMonthLotContainer;
    LinearLayout linearPlanningMonthExactDateContainer;
    LinearLayout linearVaccinPlanningContainer;
    LinearLayout linearNbreAnneeContainer;
    LinearLayout linearNbreChildContainer;

    LinearLayout llPhotoCarnet;
    AppCompatImageView ivPhotoCarnet;

    RecyclerView rvListDevices;
    AppCompatButton btnCancel;

    LinearLayout llGrossessAgeContainer;
    LinearLayout llGrossessAgeEndContainer;

    AppCompatSpinner selectPregnantStatus;
    //AppCompatSpinner spRecus;
    SearchableSpinner spRecus;
    AppCompatSpinner spPlanningVaccin;

    String carnetPhotoPath;
    File fileGlobal;
    int photoMethod = 1;
    String preg;

    ArrayAdapter<Carnet> carnetAdapter;
    ArrayAdapter<VilleModel> villeAdapter;
    ArrayAdapter<CommuneModel> communeAdapter;
    ArrayAdapter<QuartierModel> quartierAdapter;
    ArrayAdapter<NiveauEtudeModel> etudeAdapter;
    ArrayAdapter<NiveauClasseModel> classeAdapter;
    ArrayAdapter<ProfessionModel> professionAdapter;

    List<Carnet> carnetsList;

    List<VilleModel> villesList;
    List<CommuneModel> communesList;
    List<QuartierModel> quartiersList;
    List<NiveauEtudeModel> etudesList;
    List<NiveauClasseModel> classesList;
    List<ProfessionModel> professionsList;

    ArrayAdapter<Vaccin> planningVaccinAdapter;
    List<Vaccin> planningVaccinsList;
    Vaccin planningVaccinSelected;
    CarnetRequest carnetRequest;
    String recuNumber = "";
    DateTimeFormatter dateTimeFormat;

    VilleModel ville;
    CommuneModel commune;
    QuartierModel quartier;

    NiveauEtudeModel etude;
    NiveauClasseModel classe;
    ProfessionModel profession;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_abonnement_client);

        dateTimeFormat = DateTimeFormat.forPattern("ddMMyyyy");

        database = AndroidSmsDatabase.getDatabase(this);
        carnetRequest = new CarnetRequest();
        carnetRequest.setTransacId("124578911115");

        deviceList = new ArrayList<>();
        btDevicesAlreadyBondedList = new HashSet<>();

        bluetoothDeviceAdapter = new BluetoothDeviceAdapter(this, deviceList);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
            Toast.makeText(this, "Bluetooth not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 2017);
        } else {
            btDevicesAlreadyBondedList = bluetoothAdapter.getBondedDevices();
        }

        if (btDevicesAlreadyBondedList.size() > 0) {
            // Al ready devices bonded
            for (BluetoothDevice pairedDevice : btDevicesAlreadyBondedList) {
                if (pairedDevice.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.IMAGING) {
                    bluetoothDevice = pairedDevice;
                    deviceList.add(bluetoothDevice);
                }
            }
            bluetoothDeviceAdapter.notifyDataSetChanged();
        }

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View printersView = inflater.inflate(R.layout.view_printers_bluetooth_presenter, null);

        rvListDevices = printersView.findViewById(R.id.view_printers_bluetooth_presenter_rv_list_devices);
        btnCancel = printersView.findViewById(R.id.view_printers_bluetooth_presenter_btn_cancel);
        rvListDevices.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvListDevices.setAdapter(bluetoothDeviceAdapter);

        btnCancel.setOnClickListener(v -> {
            etNom.setText(null);
            etPrenoms.setText(null);
            etNaissance.setText(null);
            etTelephone.setText(null);
            btnAbonnement.setEnabled(true);

            dialog.dismiss();
            createAddVaccinDialog();

            SPUtils.getInstance(PHONE_PREFERENCES).put("show_printer", false);
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Choisir une imprimante")
                .setView(printersView)
                .setCancelable(false);
        dialogPrinters = builder.create();

        counPrintTentative = 0;

        dialog = new ProgressDialog(AbonnementActivity.this);
        dialog.setMessage(getResources().getString(R.string.txt_patient_abonnement_wating));
        dialog.setCancelable(false);

        etNom = findViewById(R.id.activity_abonnement_et_nom_patient);
        spRecus = findViewById(R.id.activity_abonnement_sp_recu_patient);
        etPrenoms = findViewById(R.id.activity_abonnement_et_prenoms_patient);
        etNaissance = findViewById(R.id.inputAnneeNaissance);
        etTelephone = findViewById(R.id.activity_abonnement_et_telephone_patient);
        etNombreAnne = findViewById(R.id.activity_abonnement_et_nbre_annee);
        etAgeGrossesse = findViewById(R.id.inputAgeGrossesse);
        inputChildNumber = findViewById(R.id.inputChildNumber);
        etDateAccouchement = findViewById(R.id.activity_abonnement_et_gross_age_end);
        spSexe = findViewById(R.id.activity_abonnement_sp_sexe_patient);
        spLangue = findViewById(R.id.activity_abonnement_sp_langue_patient);
        selectMessageLang = findViewById(R.id.selectMessageLanguage);
        selectMessagePref = findViewById(R.id.selectMessagePreference);
        btnAbonnement = findViewById(R.id.activity_abonnement_btn_abonne_patient);
        rgFormule = findViewById(R.id.activity_abonnement_rg_container_formule);
        rgAbonnmentType = findViewById(R.id.activity_abonnement_rg_container_abonnement_type);
        ivPhotoCarnet = findViewById(R.id.activity_abonnement_iv_photo_carnet);
        ivImportPhoto = findViewById(R.id.activity_abonnement_iv_import_photo);
        ivTakePhoto = findViewById(R.id.activity_abonnement_iv_take_photo);
        imageDateAccouchement = findViewById(R.id.imageCalendarDateAccouchement);
        selectPregnantStatus = findViewById(R.id.select_pregnant_status);
        llPregnantContainer = findViewById(R.id.activity_abonnement_ll_pregnant_container);
        llGrossessAgeContainer = findViewById(R.id.linearAgeGrossesseContainer);
        llGrossessAgeEndContainer = findViewById(R.id.activity_abonnement_ll_pregnant_age_end_container);

        etExactDatePlanning = findViewById(R.id.editPlanningMonthExactDate);
        etLotPlanning = findViewById(R.id.editPlanningMonthLot);
        imageDatePlanning = findViewById(R.id.imageCalendarDatePlanning);
        linearPlanningMonthContainer = findViewById(R.id.linearPlanningMonthContainer);
        linearPlanningMonthLotContainer = findViewById(R.id.linearPlanningMonthLotContainer);
        linearPlanningMonthExactDateContainer = findViewById(R.id.linearPlanningMonthExactDateContainer);
        linearFormuleContainer = findViewById(R.id.linearFormuleContainer);
        etPlanningMonth = findViewById(R.id.editPllanningMonthDate);
        linearNbreAnneeContainer = findViewById(R.id.linearNbreAnneContainer);
        linearVaccinPlanningContainer = findViewById(R.id.linearVaccinPlanningContainer);

        spPlanningVaccin = findViewById(R.id.selectPlanningVaccin);
        spVille = findViewById(R.id.selectVillePatient);
        spCommune   = findViewById(R.id.selectCommunePatient);
        spQuartier  = findViewById(R.id.selectQuartierPatient);
        spEtude = findViewById(R.id.selectEtudePatient);
        spClasse = findViewById(R.id.selectClassePatient);
        spProfession = findViewById(R.id.selectProfessionPatient);

        carnetsList = new ArrayList<>();
        Carnet carnetSelector = new Carnet();
        carnetSelector.setId("0");
        carnetSelector.setRecuNum("Choisir recu");
        carnetRequest.setCentreId("0");
        carnetsList.add(carnetSelector);
        carnetsList.addAll(AndroidSmsDatabase.getDatabase(this).carnetDao().getAllCarnetsUnsed());
        carnetAdapter = new ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, carnetsList);
        spRecus.setTitle("Recherchez un numéro");
        spRecus.setAdapter(carnetAdapter);

        planningVaccinsList = new ArrayList<>();
        Vaccin vaccinIndicator = new Vaccin();
        vaccinIndicator.setVaccinId(0);
        vaccinIndicator.setVaccinLabel("Choisir la Methode de Planning");
        vaccinIndicator.setVaccinPeriode("-1");
        planningVaccinsList.add(vaccinIndicator);
        planningVaccinsList.addAll(AndroidSmsDatabase.getDatabase(this).vaccinDao().getPlanningVaccinsList());
        planningVaccinAdapter = new ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, planningVaccinsList);
        spPlanningVaccin.setAdapter(planningVaccinAdapter);

        etTelephone.setText("225");

        if (rgFormule.getCheckedRadioButtonId() == R.id.activity_abonnement_rb_standart_formule) {
            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_ID, 1);
            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_NAME, "STANDARD");
        }

        if (rgAbonnmentType.getCheckedRadioButtonId() == R.id.activity_abonnement_choice_vaccine) {
            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_ABONNEMENT_TYPE_ID, "1");
            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_ABONNEMENT_TYPE_NAME, "VACCINE");
        }

        spLangue.setSelection(2);

        spLangue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_LANGUE, parent.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spSexe.setSelection(1);

        spSexe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_SEXE, "M");
                    linearFormuleContainer.setVisibility(View.VISIBLE);
                    linearNbreAnneeContainer.setVisibility(View.VISIBLE);
                    llPregnantContainer.setVisibility(View.GONE);

                    llGrossessAgeContainer.setVisibility(View.GONE);
                    llGrossessAgeEndContainer.setVisibility(View.GONE);

                    linearVaccinPlanningContainer.setVisibility(View.GONE);
                    linearPlanningMonthContainer.setVisibility(View.GONE);
                    linearPlanningMonthLotContainer.setVisibility(View.GONE);
                } else {
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_SEXE, "F");
                    llPregnantContainer.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnAbonnement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abonnementType = SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ABONNEMENT_TYPE_ID);

                if (carnetRequest.getCentreId().equals("0")) {
                    ToastUtils.showShort("Selectionnez un numero de recu");
                    return;
                }

                btnAbonnement.setEnabled(false);

                if (abonnementType.equals("1")) {
                    if (SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_SEXE, "M").equals("F")
                            && SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_STATUS, "FEMME").equals("FEMME ENCEINTE")) {
                        if (etAgeGrossesse.getText().toString().trim().length() == 0 || Integer.parseInt(etAgeGrossesse.getText().toString().trim()) == 0) {
                            ToastUtils.showShort("Mauvais age de grossesse");
                            btnAbonnement.setEnabled(true);
                            return;
                        }

                        if (!etDateAccouchement.getText().toString().trim().matches(REGEX)) {
                            Toaster.toast("Mauvaise date d'accouchement");
                            btnAbonnement.setEnabled(true);
                            return;
                        }

                        preg = "Y";
                    } else {
                        preg = "N";
                    }
                }

                if (etNaissance.getText().toString().matches(REGEX)) {
                    etNaissance.setTextColor(getResources().getColor(R.color.colorPrimaryTextBlack));

                    if (!StringUtils.isBlank(etNom.getText()) && !StringUtils.isBlank(etPrenoms.getText())
                            && !StringUtils.isBlank(etNaissance.getText()) && !StringUtils.isBlank(etTelephone.getText())) {

                        if (abonnementType.equals("2")) {
                            if (planningVaccinSelected.getVaccinId() <= 0) {
                                Toaster.toast("Choisissez un planning, svp.");
                                return;
                            }

                            abonnementPFCustomer();
                        } else {
                            new AbonnementEngine().execute();
                        }
                    } else {
                        Toaster.toast("Remplissez correctement les champs");
                        btnAbonnement.setEnabled(true);
                    }
                } else {
                    etNaissance.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                    Toaster.toast("Mauvaise date de naissance");
                    btnAbonnement.setEnabled(true);
                }
            }
        });

        selectPregnantStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (id == 0) {
                    llGrossessAgeContainer.setVisibility(View.VISIBLE);
                    llGrossessAgeEndContainer.setVisibility(View.VISIBLE);

                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_PREGNANCY_STATUS, "FEMME ENCEINTE");
                } else {
                    llGrossessAgeContainer.setVisibility(View.GONE);
                    llGrossessAgeEndContainer.setVisibility(View.GONE);
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_PREGNANCY_STATUS, "FEMME");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ivTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoMethod = 1;
                dispatchTakePictureIntent();
            }
        });

        ivImportPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoMethod = 2;
                showFileChooser(1);
            }
        });

        etNombreAnne.setText("1");

        ivPhotoCarnet.setOnClickListener(v -> {
            if (SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_PHOTO_CARNET, "").equals("")) {
                ToastUtils.showShort("Ajouter une photo svp !");
            } else {
                View viewCarnet = LayoutInflater.from(AbonnementActivity.this).inflate(R.layout.layout_img_carnet, null);
                AppCompatImageView ivCarnet = viewCarnet.findViewById(R.id.layout_img_carnet_iv_carnet);

                AlertDialog.Builder dialogBuidler = new AlertDialog.Builder(AbonnementActivity.this)
                        .setView(viewCarnet);
                dialogBuidler.create().show();

                Glide.with(AbonnementActivity.this)
                        .asBitmap()
                        .load(Base64.decode(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_PHOTO_CARNET, ""), Base64.DEFAULT))
                        .into(ivCarnet);
            }
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_DENIED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE}, 2020);
        }

        spRecus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carnetRequest.setCentreId(carnetsList.get(position).getId());
                recuNumber = carnetsList.get(position).getRecuNum();

                LogUtils.e(GsonUtils.toJson(carnetRequest));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etPlanningMonth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 0) return;
                if (Integer.valueOf(StringUtils.getDigits(s.toString())) < 1) return;

                etExactDatePlanning.setText(Utils.addMomentDate("+" + s.toString().trim() + "months"));
            }
        });

        spPlanningVaccin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                planningVaccinSelected = planningVaccinsList.get(position);

                if (planningVaccinSelected.getVaccinId() > 0) ToastUtils.showLong("Planning de " + planningVaccinSelected.getVaccinPeriode());

                if (planningVaccinSelected.getVaccinId() > 0) {
                    linearPlanningMonthExactDateContainer.setVisibility(View.VISIBLE);
                    linearPlanningMonthLotContainer.setVisibility(View.VISIBLE);

                    if (planningVaccinSelected.getVaccinPeriode().equals("0")) {
                        etPlanningMonth.setText(null);
                        etExactDatePlanning.setText(null);
                        linearPlanningMonthContainer.setVisibility(View.VISIBLE);
                        return;
                    }

                    if (!planningVaccinSelected.getVaccinPeriode().equals("0")) {
                        etExactDatePlanning.setText(null);
                        linearPlanningMonthContainer.setVisibility(View.GONE);

                        etExactDatePlanning.setText(Utils.addMomentDate(planningVaccinSelected.getVaccinPeriode()));
                        return;
                    }
                }

                etPlanningMonth.setText(null);
                linearPlanningMonthContainer.setVisibility(View.GONE);
                linearPlanningMonthExactDateContainer.setVisibility(View.GONE);
                etExactDatePlanning.setText(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        selectMessageLang.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                messageLang = getResources().getStringArray(R.array.messages_language)[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        String[] resMessType = getResources().getStringArray(R.array.messages_preference);
        selectMessagePref.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                messagePref = resMessType[position];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        etAgeGrossesse.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                // Compute date end pregnancy 40weeks
                if (s.toString().trim().length() == 0) {
                    etDateAccouchement.setText(null);
                    return;
                };

                if (s.toString().trim().contains(".") || s.toString().trim().contains("-") || s.toString().trim().contains("+")
                        || s.toString().trim().contains("*") || s.toString().trim().contains("#") || s.toString().trim().contains("_")) {
                    etDateAccouchement.setText(null);
                    return;
                }

                String doneData = StringUtils.getDigits(s.toString());

                if (doneData.length() == 0) {
                    etDateAccouchement.setText(null);
                    return;
                }

                if (!(Integer.valueOf(Integer.parseInt(doneData)) instanceof Integer)) return;

                if (Integer.parseInt(doneData) < 1) return;

                int weekDeclared = Integer.parseInt(doneData);
                int expectWeeks = 40;

                int resultWeeks = expectWeeks - weekDeclared;

                etDateAccouchement.setText(DateTime.now().plusWeeks(resultWeeks).toString(dateTimeFormat));
            }
        });

        imageDatePlanning.setOnClickListener(v -> {
            pickDate(etExactDatePlanning, this, true, false);
        });

        imageDateAccouchement.setOnClickListener(v -> {
            pickDate(etDateAccouchement, this, true, false);
        });

        onAbonnementTypeClicked(rgAbonnmentType.findViewById(rgAbonnmentType.getCheckedRadioButtonId()));

        // TEST DES DONNEES DE TRAVAILS
        LogUtils.e("DISTRICT => " + SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(Constants.DISTRICT_ID));

        villesList      =   new ArrayList<>();
        communesList    =   new ArrayList<>();
        quartiersList   =   new ArrayList<>();

        etudesList      =   new ArrayList<>();
        classesList     =   new ArrayList<>();
        professionsList =   new ArrayList<>();

        setupVilleByRigion();
        setupNiveauEtude();
        setupProfession();

        if (ville == null) {
            setupCommunesByVille(database.villeDao().getAllVilles().get(0).getId());
        }

        if (etude == null) {
            setupNiveauClasseByEtude(database.niveauEtudeDao().getAllEtudes().get(0).getId());
        }
    }


    private void createAddVaccinDialog() {
        View viewAddVacuum = LayoutInflater.from(AbonnementActivity.this).inflate(R.layout.dialog_add_vacum_patient, null);
        AppCompatTextView tvQuestionVacuum = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_question_patient_add_vacuum);
        AppCompatTextView tvNon = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_non);
        AppCompatTextView tvOui = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_oui);

        tvQuestionVacuum.setText(getString(R.string.txt_question_add_vaccum_visite,
                "visites",
                SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NAME, "")));

        AlertDialog.Builder builderAddVacuum = new AlertDialog.Builder(AbonnementActivity.this)
                .setView(viewAddVacuum);

        tvNon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPrinters.dismiss();
                dialogPrinters = null;

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).clear();
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ID, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ID, "0"));
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PHOTO_CARNET, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_PHOTO_CARNET, ""));

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_NAME, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NAME)));
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_TELEPHONE, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NUMBER, "00000000")));

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_BIRTHDAY, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NAISSANCE)));
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_EMAIL, "");

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION,StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_EXPIRATION, "")));

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOGIN, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_LOGIN, ""), "INDEFINI"));
                //SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, spSexe.getSelectedItem().toString());
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_SEXE, "M"));
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_TYPE_ID, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ABONNEMENT_TYPE_ID, "1"));
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_STATUS, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_STATUS, "FEMME"));

                if (SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_STATUS, "FEMME").equals("FEMME ENCEINTE")) {
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_AGE));
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE_BEGIN, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_AGE_BEGIN));
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE_END, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_AGE_END));
                }

                Intent intentDashboardPatient = new Intent(AbonnementActivity.this, DashboardActivity.class);
                startActivity(intentDashboardPatient);

                disconnect();

                SPUtils.getInstance(ABONNE_NEW_PREFERENCES).clear();
                finish();
            }
        });

        tvOui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogPrinters.dismiss();
                dialogPrinters = null;

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).clear();

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ID, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ID, "0"));

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PHOTO_CARNET, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_PHOTO_CARNET, ""));

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_NAME, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NAME)));
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_TELEPHONE, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NUMBER, "00000000")));

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_BIRTHDAY, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NAISSANCE)));
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_EMAIL, "");

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION,StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_EXPIRATION, "")));

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOGIN, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_LOGIN, ""), "INDEFINI"));
                //SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, spSexe.getSelectedItem().toString());
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_SEXE, "M"));
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_TYPE_ID, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ABONNEMENT_TYPE_ID, "1"));
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_STATUS, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_STATUS, "FEMME"));

                if (SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_STATUS, "FEMME").equals("FEMME ENCEINTE")) {
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_AGE));
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE_BEGIN, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_AGE_BEGIN));
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE_END, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_AGE_END));
                }

                Intent intentAddVacuum = new Intent(AbonnementActivity.this, SelectVacinesActivity.class);
                SPUtils.getInstance(VACCIN_PREFERENCES).put(VACCIN_ADD_TYPE, "abn_newest");
                startActivity(intentAddVacuum);

                disconnect();

                SPUtils.getInstance(ABONNE_NEW_PREFERENCES).clear();
                finish();
            }
        });

        dialogPrinters = null;
        dialogPrinters = builderAddVacuum.create();

        dialogPrinters.show();
    }


    @Override
    public void onDeviceClicked(BluetoothDevice bluetoothDevice, int position) {
        if (dialogPrinters.isShowing()) {
            dialogPrinters.dismiss();
        }

        // CONNECT TO PRINTER
        connectBTDevice = new ConnectBTDevice(bluetoothDevice);
        connectBTDevice.execute();
    }


    class ConnectBTDevice extends AsyncTask<Object, Object, ZebraPrinter> {

        BluetoothDevice device;

        ConnectBTDevice(BluetoothDevice device) {
            this.device = device;
        }

        @Override
        protected void onPreExecute() {
            if (dialog != null) {
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            }
            counPrintTentative = counPrintTentative + 1;
        }

        @Override
        protected ZebraPrinter doInBackground(Object[] objects) {
            btConnection = new BluetoothConnection(device.getAddress());
            try {
                btConnection.open();
            } catch(Exception e) {
                e.printStackTrace();
                if (e instanceof ConnectionException) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            connectBTDevice.cancel(true);
                            connectBTDevice = null;
                            if (counPrintTentative > 3) {
                                finish();
                            }
                            connectBTDevice = new ConnectBTDevice(device);
                            connectBTDevice.execute();
                        }
                    });
                }
            }

            ZebraPrinter zebraPrinter = null;

            if (btConnection.isConnected()) {
                try {
                    zebraPrinter = ZebraPrinterFactory.getInstance(btConnection);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }

            return zebraPrinter;
        }


        @Override
        protected void onPostExecute(ZebraPrinter zebraPrinter) {
            zbPrinter = zebraPrinter;
            try {
                if (zbPrinter != null) {
                    btConnection.write(getConfigLabel(
                            SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME, ""),
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_HOUR, ""),
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_DATE, ""),
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_RECEIPT, ""),
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_AVAILABLE, ""),
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NAME, ""),
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_LOGIN, ""),
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NUMBER, ""),
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getInt(ABONNE_NEW_AMOUNT, 0),
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_FORMULE_NAME, ""),
                            SPUtils.getInstance(AGENT_PREFERENCES).getString(AGENT_NAME, "")
                    ));

                    etNom.setText(null);
                    etPrenoms.setText(null);
                    etNaissance.setText(null);
                    etTelephone.setText(null);
                    btnAbonnement.setEnabled(true);

                    dialog.dismiss();

                    View viewAddVacuum = LayoutInflater.from(AbonnementActivity.this).inflate(R.layout.dialog_add_vacum_patient, null);
                    AppCompatTextView tvQuestionVacuum = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_question_patient_add_vacuum);
                    AppCompatTextView tvNon = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_non);
                    AppCompatTextView tvOui = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_oui);

                    tvQuestionVacuum.setText(getString(R.string.txt_question_add_vaccum_visite,
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ABONNEMENT_TYPE_ID, "2").equals("1") ? "vaccins" : "visites",
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NAME, "")));

                    AlertDialog.Builder builderAddVacuum = new AlertDialog.Builder(AbonnementActivity.this)
                            .setView(viewAddVacuum);

                    tvNon.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogPrinters.dismiss();
                            dialogPrinters = null;

                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).clear();
                            finish();
                        }
                    });

                    tvOui.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            dialogPrinters.dismiss();
                            dialogPrinters = null;

                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).clear();

                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ID,
                                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ID, "0"));

                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PHOTO_CARNET,
                                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_PHOTO_CARNET, ""));

                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_NAME, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NAME)));
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_TELEPHONE, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NUMBER, "00000000")));

                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_BIRTHDAY, StringUtils.defaultString(etNaissance.getText().toString().trim()));
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_EMAIL, "");

                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION,StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_EXPIRATION, "")));

                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOGIN, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_LOGIN, ""), "INDEFINI"));
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, spSexe.getSelectedItem().toString());
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_TYPE_ID, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ABONNEMENT_TYPE_ID, "1"));
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_STATUS, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_PREGNANCY_STATUS, "FEMME"));

                            //Intent intentAddVacuum = new Intent(AbonnementActivity.this, UpdateVaccinActivity.class);
                            //intentAddVacuum.putExtra("from", "Abonnement");
                            //startActivity(intentAddVacuum);

                            disconnect();

                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).clear();
                            finish();
                        }
                    });

                    if (SPUtils.getInstance(Constants.PHONE_PREFERENCES).getString(Constants.PHONE_PRINT).equals("YES")) {
                        dialogPrinters = null;
                        dialogPrinters = builderAddVacuum.create();

                        dialogPrinters.show();
                    } else {
                        createAddVaccinDialog();
                    }
                }
            } catch(Exception e) {
                if (dialog != null) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                e.printStackTrace();
            }
        }
    }


    public void onFormuleClicked(View view) {
        // Is the button now checked?
        boolean checked = ((AppCompatRadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.activity_abonnement_rb_premuim_formule:
                if (checked) {
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_ID, 2);
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_NAME, "PREMUIM");
                }
            break;

            case R.id.activity_abonnement_rb_privilege_formule:
                if (checked) {
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_ID, 3);
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_NAME, "PRIVILEGE");
                }
            break;

            case R.id.activity_abonnement_rb_free_formule:
                if (checked) {
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_ID, 12);
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_NAME, "GRATUIT");
                }
            break;

            default :
                if (checked) {
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_ID, 1);
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_NAME, "STANDARD");
                }
        }
    }


    public void onAbonnementTypeClicked(View view) {
        // Is the button now checked?
        boolean checkedAbonnement = ((AppCompatRadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.activity_abonnement_choice_vaccine :
                if (checkedAbonnement) {

                    if (SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_SEXE, "M").equals("M")) {
                        linearFormuleContainer.setVisibility(View.VISIBLE);
                        linearNbreAnneeContainer.setVisibility(View.VISIBLE);
                        llPregnantContainer.setVisibility(View.GONE);

                        llGrossessAgeContainer.setVisibility(View.GONE);
                        llGrossessAgeEndContainer.setVisibility(View.GONE);

                        linearVaccinPlanningContainer.setVisibility(View.GONE);
                        linearPlanningMonthContainer.setVisibility(View.GONE);
                        linearPlanningMonthLotContainer.setVisibility(View.GONE);

                    } else {
                        linearFormuleContainer.setVisibility(View.VISIBLE);
                        linearNbreAnneeContainer.setVisibility(View.VISIBLE);
                        llPregnantContainer.setVisibility(View.VISIBLE);

                        llGrossessAgeContainer.setVisibility(View.VISIBLE);
                        llGrossessAgeEndContainer.setVisibility(View.VISIBLE);

                        linearVaccinPlanningContainer.setVisibility(View.GONE);
                        linearPlanningMonthContainer.setVisibility(View.GONE);
                        linearPlanningMonthLotContainer.setVisibility(View.GONE);
                    }

                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_ABONNEMENT_TYPE_ID, "1");
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_NAME, "VACCINE");
                }
            break;

            case R.id.activity_abonnement_choice_planing:
                if (checkedAbonnement) {
                    if (SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_SEXE, "M").equals("M")) {
                        ToastUtils.showShort("Pas de planning familial pour ce genre !");
                        rgAbonnmentType.check(R.id.activity_abonnement_choice_vaccine);

                        linearFormuleContainer.setVisibility(View.VISIBLE);
                        linearNbreAnneeContainer.setVisibility(View.VISIBLE);
                        llPregnantContainer.setVisibility(View.GONE);

                        llGrossessAgeContainer.setVisibility(View.GONE);
                        llGrossessAgeEndContainer.setVisibility(View.GONE);

                        linearVaccinPlanningContainer.setVisibility(View.GONE);
                        linearPlanningMonthContainer.setVisibility(View.GONE);
                        linearPlanningMonthLotContainer.setVisibility(View.GONE);
                        return;
                    }

                    if (SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_SEXE, "M").equals("M")) {
                        ToastUtils.showShort("Pas de planning familial pour ce genre !");
                        rgAbonnmentType.check(R.id.activity_abonnement_choice_vaccine);

                        linearFormuleContainer.setVisibility(View.VISIBLE);
                        linearNbreAnneeContainer.setVisibility(View.VISIBLE);
                        llPregnantContainer.setVisibility(View.GONE);

                        llGrossessAgeContainer.setVisibility(View.GONE);
                        llGrossessAgeEndContainer.setVisibility(View.GONE);

                        linearVaccinPlanningContainer.setVisibility(View.GONE);
                        linearPlanningMonthContainer.setVisibility(View.GONE);
                        linearPlanningMonthLotContainer.setVisibility(View.GONE);
                        return;
                    }

                    linearFormuleContainer.setVisibility(View.GONE);
                    linearNbreAnneeContainer.setVisibility(View.GONE);
                    llPregnantContainer.setVisibility(View.GONE);
                    llGrossessAgeContainer.setVisibility(View.GONE);
                    llGrossessAgeEndContainer.setVisibility(View.GONE);
                    linearPlanningMonthContainer.setVisibility(View.GONE);
                    linearPlanningMonthLotContainer.setVisibility(View.GONE);

                    linearVaccinPlanningContainer.setVisibility(View.VISIBLE);

                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_ABONNEMENT_TYPE_ID, "2");
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_ABONNEMENT_TYPE_NAME, "PLANNING");
                }
            break;
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            if (photoMethod == 1) {
                setPic();
            } else {
                Bundle extras = data.getExtras();
                setPic(data.getData());
            }
        }
    }


    private void setPic(Uri uriTraitment) {
        // Get the dimensions of the View
        int targetW = ivPhotoCarnet.getWidth();
        int targetH = ivPhotoCarnet.getHeight();


        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        carnetPhotoPath = ImageFilePath.getPath(this, uriTraitment);

        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoTakingW = bmOptions.outWidth;
        int photoTakingH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleTakingFactor = Math.min(photoTakingW/targetW, photoTakingH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleTakingFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        ivPhotoCarnet.setPadding(5, 5, 5, 5);
        ivPhotoCarnet.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        disconnect();
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }


    private byte[] getConfigLabel(String centre, String hour, String date, String recu, String valid,
                                  String abonne, String login, String telephone, int montant,
                                  String formule, String agent) {
        byte[] configLabel = null;

        String cpclConfigLabel =
                "! 0 200 200 690 1\r\n"
                        + "CENTER\r\n"
                        + "PCX 0 0 !<opis.pcx\r\n"
                        + "ML 25\r\n"
                        + "T 5 0 10 150\r\n"
                        + "CARNET DE VACCINATION\r\n"
                        + "ELECTRONIQUE\r\n"
                        + "ENDML\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 220 CENTRE : " + centre + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 250 HEURE : " + hour + "\r\n"
                        + "RIGHT\r\n"
                        + "T 0 2 0 250 DATE : " + date + "\r\n"
                        + "CENTER\r\n"
                        + "T 0 3 0 300 ABONNEMENT\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 340 RECU : " + recu + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 370 VALIDITE : " + valid + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 400 NOM : " + abonne + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 430 LOGIN : " + login + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 460 TELEPHONE : " + telephone +"\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 490 MONTANT : " + montant + " FCFA\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 520 TYPE FORMULE : " + formule +"\r\n"
                        + "CENTER\r\n"
                        + "T 0 2 0 560 www.opisms.org / 21-24-34-89\r\n"
                        + "CENTER\r\n"
                        + "T 0 2 0 585 NUM.VERT : 143 (APPEL GRATUIT)\r\n"
                        + "RIGHT\r\n"
                        + "T 0 2 0 615 AGENT : " + agent + "\r\n"
                        + "PRINT\r\n";
        configLabel = cpclConfigLabel.getBytes();
        return configLabel;
    }


    public void disconnect() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (btConnection != null) {
                        btConnection.close();
                        zbPrinter = null;
                        btConnection = null;
                    }
                } catch (ConnectionException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File carnetFile = null;
            try {
                carnetFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            // Continue only if the File was successfully created
            if (carnetFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.gia.myrecu.fileprovider",
                        carnetFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        carnetPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void setPic() {
        // Get the dimensions of the View
        int targetW = ivPhotoCarnet.getWidth();
        int targetH = ivPhotoCarnet.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        ivPhotoCarnet.setPadding(5, 5, 5, 5);
        ivPhotoCarnet.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    private void createImageFileCompressed() {
        File compressedFile = null;
        try {
            fileGlobal = new File(carnetPhotoPath);
            compressedFile = new Compressor(this)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(fileGlobal);
            if (FileUtils.isFileExists(compressedFile)) {
                final File finalCompressedFile = compressedFile;

                FileUtils.copy(compressedFile, fileGlobal, new FileUtils.OnReplaceListener() {
                    @Override
                    public boolean onReplace(File srcFile, File destFile) {
                        //Toaster.toast("File remplace");
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_PHOTO_CARNET, encodeFileToBase64Binary(finalCompressedFile));
                        return true;
                    }
                });
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String encodeFileToBase64Binary(File file) {
        String encodedBase64 = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedBase64;
    }


    private void showFileChooser(int pView) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent, "Choisir une photo"), pView);
        }
    }


    class AbonnementEngine extends AsyncTask<String, Void, Void> {

        String nom;
        String prenoms;
        String dateNaissance;
        String telephone;
        String nbreAnne;
        String ageGrossesse;
        String dateAccouchement;
        String abonnementType;
        String vaccinPlanning;
        String vaccinPlanningDate;
        String vaccinPlanningMonth;
        String vaccinPlanningId;
        String children;
        String messageType;
        String messageLanguage;

        AbonnementEngine() {
            this.nom = etNom.getText().toString();
            this.prenoms = etPrenoms.getText().toString();
            this.dateNaissance = etNaissance.getText().toString();
            this.telephone= etTelephone.getText().toString();
            this.nbreAnne= etNombreAnne.getText().toString();
            this.ageGrossesse = etAgeGrossesse.getText().toString().trim();
            this.dateAccouchement = etDateAccouchement.getText().toString().trim();
            this.children = inputChildNumber.getText().toString().trim();
            this.messageLanguage = messageLang;
            this.messageType = messagePref;

            this.abonnementType = SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ABONNEMENT_TYPE_ID);
        }

        @Override
        protected void onPreExecute() {
            if (dialog != null ) {
                dialog.show();
            } else {
                Toaster.toast("Progress is null");
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build();

                // 18Z3579

                RequestBody formBody = new FormBody.Builder()
                        .add("ctrId", String.valueOf(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID)))
                        .add("usrId", String.valueOf(SPUtils.getInstance(AGENT_PREFERENCES).getInt(AGENT_ID)))
                        .add("nPat", nom)
                        .add("pnPat", prenoms)
                        .add("dtPat", StringUtils.reverseDelimited(Utils.formatDate(dateNaissance), '-'))
                        .add("sxPat", SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_SEXE))
                        .add("lngPat", SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_LANGUE))
                        .add("tPat", telephone)
                        .add("abonType", SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ABONNEMENT_TYPE_ID))
                        .add("dur", nbreAnne)
                        .add("preg", preg)
                        .add("enfNbre", children)
                        .add("msgType", messageType)
                        .add("msgLng", messageLanguage)
                        .add("recu", recuNumber)
                        .add("agePreg", ageGrossesse)
                        .add("datePregEnd", StringUtils.reverseDelimited(Utils.formatDate(dateAccouchement), '-'))
                        .add("photoCarnet", SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_PHOTO_CARNET, ""))
                        .add("fm", String.valueOf(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getInt(ABONNE_NEW_FORMULE_ID)))
                        .add("d", BuildConfig.DATABASE)
                        .build();

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.ABONNEMENT_PATIENT)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();
                String reponse = clientResponse.body().string();

                LogUtils.e("Res ponse: " + reponse);

                if (clientResponse.isSuccessful()) {
                    if (BuildConfig.DATABASE == "default") updateRecuNumber();

                    final JSONObject json = new JSONObject(reponse);
                    String statut = json.getString("statut");

                    if (statut.equals("1")) {
                        //final String patId = json.getString("patId");
                        //final String numRecu = json.getString("nuRecu");
                        final String patId = json.getString("idpat");
                        final String login = json.getString("login");
                        final String heure = DateTime.now().toString("HH:mm");
                        final String day = DateTime.now().toString("dd-MM-yyyy");
                        String debutPreg = json.getString("debutPreg") != null ? json.getString("debutPreg") : "";
                        String finPreg = json.getString("datePregEnd") != null ? json.getString("datePregEnd") : "";
                        String agePreg = json.getString("agePreg") != null ? json.getString("agePreg") : "";;
                        String naissanceDate = json.getString("dtPat") != null ? json.getString("dtPat") : "";;

                        String dateExp = json.getString("dtExp");
                        DateTime date = new DateTime(dateExp);

                        final String abonnementDate = date.toString("dd MMMM yyyy", Locale.CANADA_FRENCH);

                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_LOGIN, login);
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_ID, patId);
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_RECEIPT, recuNumber);
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_AVAILABLE, abonnementDate);
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_EXPIRATION, dateExp);
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_HOUR, heure);
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_NAISSANCE, naissanceDate);
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_DATE, day);

                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_PREGNANCY_AGE, agePreg);
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_PREGNANCY_AGE_BEGIN, debutPreg);
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_PREGNANCY_AGE_END, finPreg);

                        Abonnement abonnement = new Abonnement();
                        abonnement.setAbonnementId(Integer.parseInt(patId));
                        abonnement.setAbonnementClientNaissance(naissanceDate);
                        abonnement.setAbonnementClientSexe(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_SEXE));
                        abonnement.setAbonnementCentre(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME));
                        abonnement.setAbonnementRecu(recuNumber);
                        abonnement.setAbonnementValidite(abonnementDate);
                        abonnement.setAbonnementClientNom(nom.concat(" ").concat(prenoms));
                        abonnement.setAbonnementClientNumero(telephone);
                        abonnement.setAbonnementMontant(json.getInt("mt"));
                        abonnement.setAbonnementFormule(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_FORMULE_NAME));
                        abonnement.setAbonnementType("ABONNEMENT");

                        /*Patient patientPersiste = new Patient();
                        patientPersiste.setPatientRemoteId(Integer.valueOf(patId));
                        patientPersiste.setPatientId(0);
                        patientPersiste.setPatientNomPrenoms(nom + " " + prenoms);
                        patientPersiste.setPatientDateExpiration(dateExp);
                        patientPersiste.setPatientSexe(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_SEXE));
                        patientPersiste.setPatientEmail("");
                        patientPersiste.setPatientLogin(login);
                        patientPersiste.setPatientTelephone(telephone);
                        patientPersiste.setPatientNaissance(naissanceDate);
                        patientPersiste.setPatientStatutFJ("0");
                        patientPersiste.setPatientPreg(preg);
                        patientPersiste.setPatientAbonnement(dateExp);
                        patientPersiste.setPatientPlanningId(0);
                        patientPersiste.setPatientAbonnementLabel(abonnementType);*/

                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_NUMBER, abonnement.getAbonnementClientNumero());
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_NAME, abonnement.getAbonnementClientNom());
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_AMOUNT, abonnement.getAbonnementMontant());
                        SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_NAME, abonnement.getAbonnementFormule());
                        SPUtils.getInstance().put(PATIENT_AGE_STATUS, Utils.detectAgeStatus(Utils.formatDate(dateNaissance)).toUpperCase());

                        abonnement.setAbonnementDate(DateTime.now().toString("yyyy-MM-dd"));
                        database.abonnementDao().createAbonnement(abonnement);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();

                                if (SPUtils.getInstance(Constants.PHONE_PREFERENCES).getBoolean("show_printer", true)) {
                                    dialog.setMessage("Impression du recu en cours...");
                                    dialogPrinters.show();
                                } else {
                                    createAddVaccinDialog();
                                }
                            }
                        });

                    } else {
                        String message = json.getString("message");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnAbonnement.setEnabled(true);
                                dialog.dismiss();
                            }
                        });
                        Toaster.toastLong(message);
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnAbonnement.setEnabled(true);
                            dialog.dismiss();
                        }
                    });
                    Toaster.toastLong("Echec abonnement.");
                }
            } catch (final Exception ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (ex instanceof SocketTimeoutException || ex instanceof UnknownHostException) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(AbonnementActivity.this)
                                    .setCancelable(false)
                                    .setMessage("Un problème est survénue. Veuillez réessayer.")
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                            builder.create().show();
                        }

                        btnAbonnement.setEnabled(true);
                        dialog.dismiss();
                    }
                });
                ex.printStackTrace();
                Toaster.toastLong("Echec abonnement.");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            btnAbonnement.setEnabled(true);
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }
    }


    void abonnementPFCustomer() {
        if (dialog != null ) {
            dialog.show();
        } else {
            Toaster.toast("Progress is null");
        }

        String centreID = String.valueOf(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID));
        String agentID = String.valueOf(SPUtils.getInstance(AGENT_PREFERENCES).getInt(AGENT_ID));
        String nomPatient = etNom.getText().toString().trim();
        String prenomsPatient = etPrenoms.getText().toString().trim();
        String naissancePatient = StringUtils.reverseDelimited(Utils.formatDate(etNaissance.getText().toString().trim()), '-');
        String sexePatient = SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_SEXE);
        String languePatient = SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_LANGUE);
        String telephonePatient = etTelephone.getText().toString().trim();
        String typeAbonnementPatient = SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ABONNEMENT_TYPE_ID);
        String planningID = String.valueOf(planningVaccinSelected.getVaccinId());
        String planningDate = planningVaccinSelected.getVaccinPeriode();
        String planningMonth = etPlanningMonth.getText().toString().trim().length() == 0
                ? "+1months"
                : Integer.parseInt(etPlanningMonth.getText().toString().trim()) <= 0
                    ? "+1months"
                    : "+" + etPlanningMonth.getText().toString().trim() + "months";
        String grossesseAgePatient = etAgeGrossesse.getText().toString().trim();
        String accouchementDatePatient = etDateAccouchement.getText().toString().trim();
        String recuPatient = recuNumber;
        String planningProbableDate = Utils.formatDate(StringUtils.reverseDelimited(etExactDatePlanning.getText().toString().trim(), '-'));

        if (planningID.equals("0")) {
            ToastUtils.showLong("Choisissez le type de planning, svp.");
            return;
        }

        OPIApiServices opismsApi = OPIApiClient.getClient(this).create(OPIApiServices.class);
        opismsApi.abonnementPlanningClient(
                "" + recuPatient,
                "" + centreID,
                "" + agentID,
                "" + nomPatient,
                "" + prenomsPatient,
                "" + naissancePatient,
                "" + sexePatient,
                "" + languePatient,
                "" + telephonePatient,
                "" + planningID,
                "" + planningProbableDate,
                "",
                "" + StringUtils.getDigits(planningMonth),
                "2",
                "" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Patient>() {
                    @Override
                    public void onSuccess(Patient patient) {
                        // Add vaccine
                        if (patient.getPatientAbonnementStatut() == 1) {
                            if (BuildConfig.DATABASE == "default") updateRecuNumber();

                            final String patId = String.valueOf(patient.getPatientRemoteId());
                            final String login = patient.getPatLogin();
                            final String heure = DateTime.now().toString("HH:mm");
                            final String day = DateTime.now().toString("dd-MM-yyyy");
                            String naissanceDate = patient.getPatientNaissance() != null ? patient.getPatientNaissance() : "1970-11-11";;
                            String dateExp = patient.getPatientDateExpiration();
                            DateTime date = new DateTime();

                            final String abonnementDate = date.toString("dd MMMM yyyy", Locale.CANADA_FRENCH);
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_LOGIN, login);
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_ID, patId);
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_RECEIPT, recuNumber);
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_AVAILABLE, abonnementDate);
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_EXPIRATION, dateExp);
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_HOUR, heure);
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_DATE, day);

                            Abonnement abonnement = new Abonnement();
                            abonnement.setAbonnementId(Integer.parseInt(patId));
                            abonnement.setAbonnementClientNaissance(StringUtils.reverseDelimited(naissanceDate, '-'));
                            abonnement.setAbonnementClientSexe("F");
                            abonnement.setAbonnementCentre(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME));
                            abonnement.setAbonnementRecu(recuNumber);
                            abonnement.setAbonnementValidite(abonnementDate);
                            abonnement.setAbonnementClientNom(nomPatient.concat(" ").concat(prenomsPatient));
                            abonnement.setAbonnementClientNumero(telephonePatient);
                            abonnement.setAbonnementMontant(0);
                            abonnement.setAbonnementFormule(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ABONNEMENT_TYPE_NAME));
                            abonnement.setAbonnementType("PLANNING FAMILIAL");

                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_NUMBER, abonnement.getAbonnementClientNumero());
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_NAME, abonnement.getAbonnementClientNom());
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_AMOUNT, abonnement.getAbonnementMontant());
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_AMOUNT, abonnement.getAbonnementMontant());
                            SPUtils.getInstance(ABONNE_NEW_PREFERENCES).put(ABONNE_NEW_FORMULE_NAME, abonnement.getAbonnementFormule());
                            SPUtils.getInstance().put(PATIENT_AGE_STATUS, Utils.detectAgeStatus(StringUtils.reverseDelimited(naissancePatient, '-')).toUpperCase());

                            abonnement.setAbonnementDate(DateTime.now().toString("yyyy-MM-dd"));
                            database.abonnementDao().createAbonnement(abonnement);

                            addPlanningCustomer(centreID, agentID, patient);
                        } else {
                            dialog.dismiss();
                            Toaster.toastLong("Impossible de finaliser l'abonnement !");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        dialog.dismiss();
                    }
                });
    }


    void addPlanningCustomer(String centreID, String agentID, Patient patientSaved) {
        OPIApiClient.getClient(this).create(OPIApiServices.class)
                .scheduleVaccin(
                        "" + centreID,
                        "" + agentID,
                        "" + patientSaved.getPatientRemoteId(),
                        "" + DateTime.now().toString(DateTimeFormat.forPattern("yyyy-MM-dd")),
                        "" + DateTime.now().toString(DateTimeFormat.forPattern("yyyy-MM-dd")),
                        "" + etLotPlanning.getText().toString().trim(),
                        "" + planningVaccinSelected.getVaccinId(),
                        "add",
                        "" + BuildConfig.DATABASE
                ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CommonResponse>() {
                    @Override
                    public void onSuccess(CommonResponse commonResponse) {
                        if (commonResponse.getStatut() == 1) {
                            Toaster.toastLong("Abonnement effectué");
                            Toaster.toastLong(commonResponse.getMessage());

                            schedulePlanningCustomer(centreID, agentID, patientSaved);
                        } else {
                            Toaster.toastLong(commonResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                });
    }


    void schedulePlanningCustomer(String centreID, String agentID, Patient patientSaved) {
        OPIApiClient.getClient(this).create(OPIApiServices.class).scheduleVaccin(
                        "" + centreID,
                      "" + agentID,
                    "" + patientSaved.getPatientRemoteId(),
                        "" + DateTime.now().plusMonths(1).toString(DateTimeFormat.forPattern("yyyy-MM-dd")),
                        "0000-00-00",
                        "" + "ND",
                        "" + planningVaccinSelected.getVaccinId(),
                        "scheduler",
                        "" + BuildConfig.DATABASE
                ).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CommonResponse>() {
                    @Override
                    public void onSuccess(CommonResponse commonResponse) {
                        dialog.dismiss();

                        if (commonResponse.getStatut() == 1) {
                            Toaster.toastLong(commonResponse.getMessage());
                            finish();
                        } else {
                            Toaster.toastLong(commonResponse.getMessage());
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        dialog.dismiss();
                        e.printStackTrace();
                    }
                });
    }


    void updateRecuNumber() {
        OPIApiServices opismsCarnetApi = OPIApiClient.getClient(this, true).create(OPIApiServices.class);
        opismsCarnetApi.updateRecuNumber(carnetRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<RecuUsedResponse>() {
                    @Override
                    public void onSuccess(RecuUsedResponse recuUsedResponse) {
                        if (recuUsedResponse.isVerifid()) {
                            AndroidSmsDatabase.getDatabase(AbonnementActivity.this).carnetDao().updateRecuNumber(carnetRequest.getCentreId());

                            carnetsList.clear();

                            Carnet carnetSelector = new Carnet();
                            carnetSelector.setId("0");
                            carnetSelector.setRecuNum("Choisir recu");

                            carnetRequest.setCentreId("0");
                            carnetsList.add(carnetSelector);

                            carnetsList.addAll(AndroidSmsDatabase.getDatabase(AbonnementActivity.this).carnetDao().getAllCarnetsUnsed());
                            carnetAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }


    void setupVilleByRigion() {
        villesList = database.villeDao().getAllVilles();
        villeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, villesList);

        spVille.setTitle("Selectionnez la ville");
        spVille.setAdapter(villeAdapter);

        spVille.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ville = villesList.get(position);
                setupCommunesByVille(ville.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    void setupCommunesByVille(int villeId) {
        communesList = new ArrayList<>();
        communesList = database.communeDao().getAllCommunesByVille(String.valueOf(villeId));

        communeAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, communesList);

        spCommune.setTitle("Selectionnez la commune");
        spCommune.setAdapter(communeAdapter);

        spCommune.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                commune = communesList.get(position);
                setupQuartiersByCommune(commune.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    void setupQuartiersByCommune(int communeId) {
        quartiersList = new ArrayList<>();
        quartiersList = database.quartierDao().getAllQuartiersByCommune(String.valueOf(communeId));

        quartierAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, quartiersList);

        spQuartier.setTitle("Selectionnez le quartier");
        spQuartier.setAdapter(quartierAdapter);

        spQuartier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                quartier = quartiersList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    void setupNiveauEtude() {
        etudesList.clear();
        etudesList = database.niveauEtudeDao().getAllEtudes();

        etudeAdapter = new ArrayAdapter<>(this,android.R.layout.simple_dropdown_item_1line, etudesList);
        spEtude.setAdapter(etudeAdapter);

        spEtude.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                etude = etudesList.get(position);
                setupNiveauClasseByEtude(etude.getId());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    void setupNiveauClasseByEtude(int niveauEtude) {
        classesList.clear();
        classesList = database.niveauClasseDao().getClassesByEtude(niveauEtude);

        classeAdapter = new ArrayAdapter<>(this,android.R.layout.simple_dropdown_item_1line, classesList);
        spClasse.setAdapter(classeAdapter);

        spClasse.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                classe = classesList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }


    void setupProfession() {
        professionsList.clear();
        professionsList = database.professionDao().getAllProfessions();

        professionAdapter = new ArrayAdapter<>(this,android.R.layout.simple_dropdown_item_1line, professionsList);
        spProfession.setAdapter(professionAdapter);

        spProfession.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                profession = professionsList.get(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }
}
