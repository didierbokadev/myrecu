package com.gia.myrecu.ui.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatTextView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.JsonUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.network.OPIApiClient;
import com.gia.myrecu.network.OPIApiServices;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.tools.UrlBanks;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

@SuppressWarnings("ALL")
public class DeclarationDashboardActivity extends AppCompatActivity implements DatePickerDialog.OnDateSetListener {


    LinearLayout llNaissance;
    LinearLayout llDeces;
    OkHttpClient clientHttp;
    DeclarerDecesAsync decesAsync;
    ProgressDialog decesLoading;
    String deathDate;
    String deathObserv;
    AppCompatButton btnDeclareDeath;
    AppCompatTextView tvDateDeath;
    AppCompatEditText etDeathObserv;
    AppCompatButton btnCancel;
    AlertDialog dialogDeath;
    OPIApiServices apiServices;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_declaration_dashboard);

        findViewByIds();

        apiServices = OPIApiClient.getClient(this).create(OPIApiServices.class);

        clientHttp = new OkHttpClient.Builder()
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .build();

        decesLoading = new ProgressDialog(this);
        decesLoading.setMessage("Déclaration en cours...");
        decesLoading.setCancelable(false);

        llDeces.setOnClickListener(v -> showDecesDialog());

        llNaissance.setOnClickListener(v -> ActivityUtils.startActivity(NewNaissanceActivity.class, R.anim.slide_from_right, R.anim.slide_to_left));
    }


    private void findViewByIds() {
        llDeces = findViewById(R.id.activity_declaration_dashboard_btn_deces);
        llNaissance = findViewById(R.id.activity_declaration_dashboard_btn_naissance);
    }


    private void showDecesDialog() {
        Calendar dateCalendar = Calendar.getInstance();

        View llDialogDeath = LayoutInflater.from(this).inflate(R.layout.dialog_death_declaration, null);

        etDeathObserv = llDialogDeath.findViewById(R.id.dialog_death_declaration_et_observation);
        btnCancel = llDialogDeath.findViewById(R.id.dialog_death_declaration_btn_cancel);
        tvDateDeath = llDialogDeath.findViewById(R.id.dialog_death_declaration_tv_date);
        btnDeclareDeath = llDialogDeath.findViewById(R.id.dialog_death_declaration_btn_confirm);

        tvDateDeath.setOnClickListener(v -> {
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    this, this, dateCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH), dateCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });

        etDeathObserv.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                deathObserv = s.toString().trim();
            }
        });


        AlertDialog.Builder decesBuilderDialog = new AlertDialog
                .Builder(this)
                .setView(llDialogDeath);
        dialogDeath = decesBuilderDialog.create();
        dialogDeath.show();


        btnDeclareDeath.setOnClickListener(v -> {
            decesLoading.show();
            declareDeath();
        });
    }


    private class DeclarerDecesAsync extends AsyncTask<Void, Void, Integer> {

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Integer doInBackground(Void... voids) {
            int code = 0;

            if (StringUtils.isBlank(deathDate)) {
                deathDate = DateTime.now().toString("yyyy-MM-dd");
            }

            try {
                RequestBody decesBody = new FormBody.Builder()
                        .add("patientId", SPUtils.getInstance(Constants.ABONNE_OLD_PREFERENCES).getString(Constants.ABONNE_OLD_ID, "0"))
                        .add("deathDate", deathDate)
                        .build();

                Request decesRequest = new Request.Builder()
                        .method("POST", decesBody)
                        .url(UrlBanks.DEFINE_DEATH)
                        .build();

                Response decesResponse = clientHttp.newCall(decesRequest).execute();
                String decesString = decesResponse.body().string();

                code = JsonUtils.getInt(decesString, "code");
            } catch (Exception ex) {
                ex.printStackTrace();
                code = 2;
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            decesLoading.dismiss();

            switch (integer) {
                case 0:
                    /*if (ActivityUtils.isActivityExistsInStack(DeclarationDashboardActivity.class)) {
                        ActivityUtils.finishActivity(DeclarationDashboardActivity.class);
                    }*/
                    dialogDeath.dismiss();
                    finish();
                    overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                    break;

                case 1:
                case 2:
                    ToastUtils.showShort("Une erreur est survenue !");
                    break;
            }
        }
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String monthS = "";
        DateTime dateTime = new DateTime(year, (month + 1), dayOfMonth, 0, 0);

        deathDate = dateTime.toString("yyyy-MM-dd");
        tvDateDeath.setText(dateTime.toString("dd MMM yyyy"));
    }



    void declareDeath() {
        apiServices.declareDeath(SPUtils.getInstance(Constants.ABONNE_OLD_PREFERENCES).getString(Constants.ABONNE_OLD_ID, "0"), deathDate, "" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableCompletableObserver() {
                    @Override
                    public void onComplete() {
                        ToastUtils.showShort("Declaration decès effectuée !");

                        decesLoading.dismiss();
                        finish();
                        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        decesLoading.dismiss();
                    }
                });
    }
}
