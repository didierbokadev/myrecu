package com.gia.myrecu.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Patient;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.ui.adapters.PatientFoundedAdapter;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("ALL")
public class PatientsFoundedFragment extends DialogFragment {


    private OnPatientsFoundedFragmentListener mListener;
    RecyclerView rvPatients;
    List<Patient> listPerson;
    PatientFoundedAdapter foundedAdapter;
    AppCompatTextView tvClose;


    public PatientsFoundedFragment() {
    }


    public static PatientsFoundedFragment newInstance(List<Patient> patients) {
        PatientsFoundedFragment fragment = new PatientsFoundedFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("patients", (ArrayList<? extends Parcelable>) patients);
        fragment.setArguments(args);
        fragment.setCancelable(false);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_patients_founded, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        rvPatients = view.findViewById(R.id.fragment_patients_founded_rv_patients);
        tvClose = view.findViewById(R.id.fragment_patients_founded_tv_close);
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnPatientsFoundedFragmentListener) {
            mListener = (OnPatientsFoundedFragmentListener) context;
        } else {
            throw new RuntimeException(context.toString() + " must implement OnPatientsFoundedFragmentListener");
        }
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        if (getArguments() != null) {
            listPerson = getArguments().getParcelableArrayList("patients");
        }

        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SPUtils.getInstance(Constants.ABONNE_OLD_PREFERENCES).clear();
                dismiss();
            }
        });

        foundedAdapter = new PatientFoundedAdapter(getActivity(), listPerson);
        rvPatients.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        rvPatients.setAdapter(foundedAdapter);
    }


    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnPatientsFoundedFragmentListener {
    }
}
