package com.gia.myrecu.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.CalendrierVaccin;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.ui.adapters.VaccinAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;

@SuppressWarnings("ALL")
public class VacProchainsActivity extends AppCompatActivity {


    LinearLayout llContainerOkVac;
    RecyclerView rvVaccinsProchains;
    VaccinAdapter vaccinAdapter;
    List<CalendrierVaccin> calendrierVaccinList;
    ProgressDialog loading;
    AppCompatImageView ivInternetIssue;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vac_prochains);

        ivInternetIssue = findViewById(R.id.activity_vac_prochains_iv_network_issue);
        rvVaccinsProchains = findViewById(R.id.activity_vac_prochains_rv_vac_prochains);
        llContainerOkVac = findViewById(R.id.activity_vac_prochains_ll_ok_vaccin);

        setSupportActionBar((Toolbar) findViewById(R.id.activity_vac_prochains_tb_title));
        getSupportActionBar().setSubtitle(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                .getString(ABONNE_OLD_NAME, ""));

        calendrierVaccinList = new ArrayList<>();
        vaccinAdapter = new VaccinAdapter(VacProchainsActivity.this, calendrierVaccinList);

        rvVaccinsProchains.setLayoutManager(new LinearLayoutManager(VacProchainsActivity.this,
                LinearLayoutManager.VERTICAL, false));

        rvVaccinsProchains.setAdapter(vaccinAdapter);

        // Load vaccins
        new LoadVacProchains().execute();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuId = item.getItemId();
        if (menuId == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("StaticFieldLeak")
    private class LoadVacProchains extends AsyncTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = new ProgressDialog(VacProchainsActivity.this);
            loading.setMessage("Chargement des visites...");
            loading.setCancelable(false);
            loading.show();
        }

        @Override
        protected Object doInBackground(Object[] objects) {

            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();

                RequestBody formBody = new FormBody.Builder()
                        .add("patId", SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ID))
                        .add("d", BuildConfig.DATABASE)
                        .build();

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.LIST_VACCINS_A_EFFECTUER)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();

                if (clientResponse.isSuccessful()) {
                    JSONObject json = new JSONObject(clientResponse.body().string());
                    String statut = json.getString("statut");

                    if (statut.equals("1")) {
                        Type typeList = new TypeToken<ArrayList<CalendrierVaccin>>() {
                        }.getType();

                        JSONArray jsonArray = json.getJSONArray("data");
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        ArrayList<CalendrierVaccin> calendrierVaccins = gson.fromJson(jsonArray.toString(), typeList);

                        calendrierVaccinList.addAll(calendrierVaccins);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ivInternetIssue.setVisibility(View.GONE);
                                loading.dismiss();
                                vaccinAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        final String mes = json.getString("message");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ivInternetIssue.setBackgroundResource(R.drawable.ic_ok_mark);
                                llContainerOkVac.setVisibility(View.VISIBLE);
                                Toaster.toast("Pas de visites a effectuer.");
                                loading.dismiss();
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            llContainerOkVac.setVisibility(View.VISIBLE);
                            Toaster.toast("Pas de vaccins a effectuer.");
                            loading.dismiss();
                        }
                    });
                }
            } catch (Exception ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        llContainerOkVac.setVisibility(View.VISIBLE);
                        Toaster.toast("Pas de visites à effectuer.");
                        loading.dismiss();
                    }
                });
                ex.printStackTrace();
                Toaster.toastLong("Une erreur de reseau est survenue. Ressayer plus tard.");
            }
            return null;
        }
    }

}
