package com.gia.myrecu.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.CalendrierVaccin;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.ui.adapters.VaccinAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;

@SuppressWarnings("ALL")
public class VacMissedActivity extends AppCompatActivity {


    RecyclerView rvVaccinsMisses;
    VaccinAdapter vaccinAdapter;
    List<CalendrierVaccin> calendrierVaccinList;
    ProgressDialog loading;
    AppCompatImageView ivInternetIssue;
    LinearLayout llContainerOkVac;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vac_missed);

        ivInternetIssue = findViewById(R.id.activity_vac_missed_iv_network_issue);
        rvVaccinsMisses = findViewById(R.id.activity_vac_missed_rv_vac_misses);
        llContainerOkVac = findViewById(R.id.activity_vac_missed_ll_ok_vaccin);

        setSupportActionBar((Toolbar) findViewById(R.id.activity_vac_missed_tb_title));
        getSupportActionBar().setSubtitle(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                .getString(ABONNE_OLD_NAME, ""));

        calendrierVaccinList = new ArrayList<>();
        vaccinAdapter = new VaccinAdapter(VacMissedActivity.this, calendrierVaccinList);

        rvVaccinsMisses.setLayoutManager(new LinearLayoutManager(VacMissedActivity.this,
                LinearLayoutManager.VERTICAL, false));

        rvVaccinsMisses.setAdapter(vaccinAdapter);

        // Load vaccins
        new LoadVacMisses().execute();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuId = item.getItemId();
        if (menuId == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }


    @SuppressLint("StaticFieldLeak")
    private class LoadVacMisses extends AsyncTask {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = new ProgressDialog(VacMissedActivity.this);
            loading.setMessage("Chargement des visites...");
            loading.setCancelable(false);
            loading.show();
        }

        @Override
        protected Object doInBackground(Object[] objects) {

            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(20, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();

                RequestBody formBody = new FormBody.Builder()
                        .add("patId", SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ID))
                        .add("d", BuildConfig.DATABASE)
                        .build();

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.LIST_VACCINS_MANQUES)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();

                if (clientResponse.isSuccessful()) {
                    JSONObject json = new JSONObject(clientResponse.body().string());
                    String statut = json.getString("statut");

                    if (statut.equals("1")) {
                        Type typeList = new TypeToken<ArrayList<CalendrierVaccin>>() {
                        }.getType();

                        JSONArray jsonArray = json.getJSONArray("data");
                        Gson gson = new GsonBuilder().serializeNulls().create();
                        ArrayList<CalendrierVaccin> calendrierVaccins = gson.fromJson(jsonArray.toString(), typeList);

                        for (CalendrierVaccin cal : calendrierVaccins) {
                            cal.setCalendrierVaccinType("2");
                            calendrierVaccinList.add(cal);
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                llContainerOkVac.setVisibility(View.GONE);
                                loading.dismiss();
                                vaccinAdapter.notifyDataSetChanged();
                            }
                        });
                    } else {
                        final String mes = json.getString("message");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                llContainerOkVac.setVisibility(View.VISIBLE);
                                //Toaster.toast(mes);
                                loading.dismiss();
                            }
                        });
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            llContainerOkVac.setVisibility(View.VISIBLE);
                            ivInternetIssue.setVisibility(View.VISIBLE);
                            //Toaster.toast("Pas de vaccins manqués.");
                            loading.dismiss();
                        }
                    });
                }
            } catch (Exception ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        llContainerOkVac.setVisibility(View.VISIBLE);
                        ivInternetIssue.setVisibility(View.VISIBLE);
                        //Toaster.toast("Pas de vaccins manqués.");
                        loading.dismiss();
                    }
                });
                ex.printStackTrace();
                Toaster.toastLong("Une erreur de reseau est survenue. Ressayer plus tard.");
            }
            return null;
        }

    }



}
