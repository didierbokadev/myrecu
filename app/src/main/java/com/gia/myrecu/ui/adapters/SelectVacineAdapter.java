package com.gia.myrecu.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.gia.myrecu.R;
import com.gia.myrecu.messages.VacineSelectedMessage;
import com.gia.myrecu.messages.VacineUnselectedMessage;
import com.gia.myrecu.models.v1.Vaccin;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class SelectVacineAdapter extends RecyclerView.Adapter<SelectVacineAdapter.SelectVacineHolder> {


    Context ctx;
    List<Vaccin> vaccins;


    public SelectVacineAdapter(Context ctx, List<Vaccin> vaccins) {
        this.ctx = ctx;
        this.vaccins = vaccins;
    }


    @NonNull
    @Override
    public SelectVacineHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View viewSelectVacine = LayoutInflater.from(ctx).inflate(R.layout.items_select_vacine_list, parent, false);
        return new SelectVacineHolder(viewSelectVacine);
    }


    @Override
    public void onBindViewHolder(@NonNull final SelectVacineHolder holder, int position) {
        final Vaccin vac = vaccins.get(holder.getAdapterPosition());

        holder.tvVacineLabel.setText(vac.getVaccinLabel());

        if (vac.getVaccinChecked().equals("1")) {
            holder.ivSelectVacine.setVisibility(View.VISIBLE);
        } else {
            holder.ivSelectVacine.setVisibility(View.GONE);
        }

        holder.rlVacineContainer.setOnClickListener(v -> {
            if (holder.ivSelectVacine.getVisibility() == View.VISIBLE) {
                EventBus.getDefault().post(new VacineUnselectedMessage(vac));
                holder.ivSelectVacine.setVisibility(View.GONE);
            } else {
                EventBus.getDefault().post(new VacineSelectedMessage(vac));
                holder.ivSelectVacine.setVisibility(View.VISIBLE);
            }
        });
    }


    @Override
    public int getItemCount() {
        return vaccins.size();
    }


    static class SelectVacineHolder extends RecyclerView.ViewHolder {

        AppCompatImageView ivSelectVacine;
        AppCompatTextView tvVacineLabel;
        RelativeLayout rlVacineContainer;

        public SelectVacineHolder(View itemView) {
            super(itemView);

            ivSelectVacine = itemView.findViewById(R.id.items_select_vacine_list_iv_select_vacine);
            tvVacineLabel = itemView.findViewById(R.id.items_select_vacine_list_tv_vacine_label);
            rlVacineContainer = itemView.findViewById(R.id.items_select_vacine_list_rl_vacine_container);
        }
    }
}
