package com.gia.myrecu.ui.adapters;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.SPUtils;
import com.bumptech.glide.Glide;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.CalendrierVaccin;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.ui.activities.UpdateVaccinActivity;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_VALID_VACCINE_ADAPTER_POSITION;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_VALID_VACCINE_ID;
import static com.gia.myrecu.tools.Constants.AGENT_ID;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;
import static com.gia.myrecu.tools.Constants.CENTRE_ID;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;

/**
 * Created by didier-dev on 27/10/17.
 */

@SuppressWarnings("ALL")
public class VaccinAdapter extends RecyclerView.Adapter<VaccinAdapter.VaccinViewHolder> {


    Context context;
    List<CalendrierVaccin> calendrierVaccins;
    ProgressDialog loading;
    VaccinViewHolder viewHolderGlobal;
    ValidVaccine validVaccineAsyncTask;


    public VaccinAdapter(Context context, List<CalendrierVaccin> calendrierVaccins) {
        this.context = context;
        this.calendrierVaccins = calendrierVaccins;
    }

    @Override
    public VaccinViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.vaccin_list_items, parent, false);
        return new VaccinViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final VaccinViewHolder holder, int position) {
        final CalendrierVaccin calendrierVaccin = calendrierVaccins.get(holder.getLayoutPosition());

        holder.vaccinName.setText(calendrierVaccin.getCalendrierVaccinName());
        holder.vaccinPresence.setText(calendrierVaccin.getCalendrierVaccinPresence());
        holder.vaccinRappel.setText(calendrierVaccin.getCalendrierVaccinRappel());
        holder.vaccinLot.setText(calendrierVaccin.getCalendrierVaccinLot());
        holder.vaccinCentre.setText(calendrierVaccin.getCalendrierVaccinCentre());

        if (calendrierVaccin.getCalendrierImgCarnet() != null) {
            if (calendrierVaccin.getCalendrierImgCarnet().length() > 0) {
                Glide.with(context)
                        .asBitmap()
                        .load(Base64.decode(calendrierVaccin.getCalendrierImgCarnet(), Base64.DEFAULT))
                        .into(holder.vaccinImgCarnet);
            } else {
                holder.vaccinImgCarnet.setImageResource(R.drawable.ic_syringe);
            }
        } else {
            holder.vaccinImgCarnet.setImageResource(R.drawable.ic_syringe);
        }

        if (!StringUtils.isBlank(calendrierVaccin.getCalendrierVaccinPresence()))
            holder.vaccinPresence.setText(calendrierVaccin.getCalendrierVaccinPresence());

        if (!StringUtils.isBlank(calendrierVaccin.getCalendrierVaccinRappel()))
            holder.vaccinRappel.setText(calendrierVaccin.getCalendrierVaccinRappel());

        if (!StringUtils.isBlank(calendrierVaccin.getCalendrierVaccinLot()))
            holder.vaccinLot.setText(calendrierVaccin.getCalendrierVaccinLot());

        if (!StringUtils.isBlank(calendrierVaccin.getCalendrierVaccinCentre()))
            holder.vaccinCentre.setText(calendrierVaccin.getCalendrierVaccinCentre());

        if (calendrierVaccin.getCalendrierVaccinType().equals("2")) {
            holder.vaccinMissed.setVisibility(View.VISIBLE);
            holder.vaccinMissed.setImageResource(R.drawable.ic_not_interested_red_400_24dp);
        } else if (calendrierVaccin.getCalendrierVaccinType().equals("3")) {
            holder.vaccinMissed.setVisibility(View.VISIBLE);
            holder.vaccinMissed.setImageResource(R.drawable.ic_insert_invitation_green_500_24dp);
        }

        if (calendrierVaccin.getCalendrierVaccinType().equals("1")) {
            if (calendrierVaccin.getCalendrierFlagValidation().equals("3")) {
                holder.vaccinName.setTextColor(Color.RED);
                holder.vaccinRappel.setTextColor(Color.RED);
                holder.vaccinLot.setTextColor(Color.RED);
                holder.vaccinPresence.setTextColor(Color.RED);
                holder.vaccinCentre.setTextColor(Color.RED);
                holder.btnDoneVaccine.setVisibility(View.VISIBLE);
            } else {
                holder.btnDoneVaccine.setVisibility(View.GONE);
            }
        }

        holder.rlContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentVacJour = new Intent(context, UpdateVaccinActivity.class);

                switch (calendrierVaccin.getCalendrierVaccinType()) {
                    case "2" :
                        intentVacJour.putExtra("vaccinToUpdate", calendrierVaccin);
                        context.startActivity(intentVacJour);
                    break;

                    case "3" :
                        intentVacJour.putExtra("vaccinToUpdate", calendrierVaccin);
                        context.startActivity(intentVacJour);
                    break;

                    default:
                        if (calendrierVaccin.getCalendrierImgCarnet() != null) {
                            if (calendrierVaccin.getCalendrierImgCarnet().length() > 0) {
                                View viewCarnet = LayoutInflater.from(context).inflate(R.layout.layout_img_carnet, null);
                                AppCompatImageView ivCarnet = viewCarnet.findViewById(R.id.layout_img_carnet_iv_carnet);

                                AlertDialog.Builder dialogBuidler = new AlertDialog.Builder(context)
                                        .setView(viewCarnet);
                                dialogBuidler.create().show();

                                Glide.with(context)
                                        .asBitmap()
                                        .load(Base64.decode(calendrierVaccin.getCalendrierImgCarnet(), Base64.DEFAULT))
                                        .into(ivCarnet);
                            }
                        }
                }
            }
        });


        holder.btnDoneVaccine.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewHolderGlobal = holder;
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_VALID_VACCINE_ID, calendrierVaccin.getCalendrierVaccinId());
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_VALID_VACCINE_ADAPTER_POSITION, holder.getAdapterPosition());

                validVaccineAsyncTask = new ValidVaccine();
                validVaccineAsyncTask.execute();
            }
        });
    }

    @Override
    public int getItemCount() {
        return calendrierVaccins.size();
    }


    static class VaccinViewHolder extends RecyclerView.ViewHolder {

        AppCompatTextView vaccinName;
        AppCompatTextView vaccinPresence;
        AppCompatTextView vaccinRappel;
        AppCompatTextView vaccinLot;
        AppCompatTextView vaccinCentre;
        AppCompatImageView vaccinMissed;
        AppCompatImageView vaccinImgCarnet;
        RelativeLayout rlContainer;
        AppCompatButton btnDoneVaccine;

        public VaccinViewHolder(View itemView) {
            super(itemView);
            vaccinMissed = itemView.findViewById(R.id.vaccin_list_items_iv_indication_missed_vac);
            vaccinImgCarnet = itemView.findViewById(R.id.vaccin_list_items_iv_seringue);
            vaccinCentre = itemView.findViewById(R.id.vaccin_list_items_tv_vaccin_centre);
            vaccinName = itemView.findViewById(R.id.vaccin_list_items_tv_vaccin_name);
            vaccinRappel = itemView.findViewById(R.id.vaccin_list_items_tv_vaccin_rappel);
            vaccinPresence = itemView.findViewById(R.id.vaccin_list_items_tv_vaccin_presence);
            vaccinLot = itemView.findViewById(R.id.vaccin_list_items_tv_vaccin_lot);
            rlContainer = itemView.findViewById(R.id.vaccin_list_items_rl_vaccin_info_container);
            btnDoneVaccine = itemView.findViewById(R.id.vaccin_list_items_btn_done_calendar_vaccine);
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class ValidVaccine extends AsyncTask<Object, Object, Integer> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loading = new ProgressDialog(context);
            loading.setMessage("Validation du carnet en cours...");
            loading.setCancelable(false);
            loading.show();
        }

        @Override
        protected Integer doInBackground(Object[] objects) {

            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(20, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();

                RequestBody formBody = new FormBody.Builder()
                        .add("patId", String.valueOf(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getInt(ABONNE_OLD_ID)))
                        .add("calId", SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_VALID_VACCINE_ID, ""))
                        .add("usrId", String.valueOf(SPUtils.getInstance(AGENT_PREFERENCES).getInt(AGENT_ID, 0)))
                        .add("ctrId", String.valueOf(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID, 0)))
                        .build();

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.VALID_USER_CALENDAR)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();

                if (clientResponse.isSuccessful()) {
                    JSONObject json = new JSONObject(clientResponse.body().string());
                    String statut = json.getString("statut");

                    if (statut.equals("1")) {
                        return 1;
                    } else {
                        return 0;
                    }
                } else {
                    return 0;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                Toaster.toastLong("Une erreur de reseau est survenue. Ressayer plus tard.");
            }
            return 0;
        }


        @Override
        protected void onPostExecute(Integer integer) {
            if (integer == 1) {
                viewHolderGlobal.btnDoneVaccine.setVisibility(View.GONE);
                notifyItemChanged(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getInt(ABONNE_OLD_VALID_VACCINE_ADAPTER_POSITION, 0));
            } else {
                Toaster.toastLong("Impossible de valider ce calendrier");
            }
            loading.dismiss();
        }
    }
}
