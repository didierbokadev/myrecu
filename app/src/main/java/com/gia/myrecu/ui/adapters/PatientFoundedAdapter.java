package com.gia.myrecu.ui.adapters;

import android.content.Context;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Patient;
import com.gia.myrecu.ui.activities.DashboardActivity;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;

import java.util.List;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_EXPIRATION;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_PLANNING_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_TYPE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_BIRTHDAY;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_DATE_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_EMAIL;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOGIN;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PHOTO_CARNET;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE_BEGIN;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE_END;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_STATUS;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_SEXE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_STATUT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_TELEPHONE;
import static com.gia.myrecu.tools.Constants.PATIENT_AGE_STATUS;
import static com.gia.myrecu.tools.Utils.detectAgeStatus;
import static com.gia.myrecu.tools.Utils.isBetween;

/**
 * Created by didier-dev on 24/10/17.
 */

@SuppressWarnings("ALL")
public class PatientFoundedAdapter extends RecyclerView.Adapter<PatientFoundedAdapter.PatientFoundedHolder> {


    List<Patient> patients;
    Context context;


    public PatientFoundedAdapter(Context context, List<Patient> patients) {
        this.patients = patients;
        this.context = context;
    }


    @Override
    public PatientFoundedHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.patient_founded_items, parent, false);
        return new PatientFoundedHolder(view);
    }


    @Override
    public void onBindViewHolder(PatientFoundedHolder holder, int position) {
        final Patient patient = patients.get(holder.getLayoutPosition());

        holder.tvNomPatient.setText(patient.toString());
        holder.tvLogin.setText(context.getResources().getString(R.string.txt_patient_login_template,
                patient.getPatientLogin()));
        holder.tvAbonmt.setText(context.getResources().getString(R.string.txt_patient_abonnement_template,
                patient.getPatientAbonnement().equals("non-defini") ? "01-01-1970" : patient.getPatientAbonnement()));
        holder.tvAbnLabel.setText(patient.getPatientAbonnementLabel() == null ? "..." : patient.getPatientAbonnementLabel());

        // Bébé : de la naissance à 2 ans
        // Enfant : de 2 ans à 10-12 ans
        // Adolescent : de 10-12 ans à 18 ans environ
        // Adulte : de 18 ans à 70 ans
        // Personne âgée : à partir de 70 ans

        int age = DateTime.now().year().get() - DateTime.parse(StringUtils.reverseDelimited(patient.getPatientNaissance(), '-')).year().get();

        if (isBetween(age, 0, 2)) {
            holder.ivOldPatient.setImageResource(R.drawable.ic_baby);
        } else if (isBetween(age, 3, 12)) {
            if (patient.getPatientSexe().equals("F"))
                holder.ivOldPatient.setImageResource(R.drawable.ic_daughter);
            else
                holder.ivOldPatient.setImageResource(R.drawable.ic_son);
        } else if (isBetween(age, 13, 17)) {
            if (patient.getPatientSexe().equals("F"))
                holder.ivOldPatient.setImageResource(R.drawable.ic_daughter);
            else
                holder.ivOldPatient.setImageResource(R.drawable.ic_son);
        } else if (isBetween(age, 18, 70)) {
            if (patient.getPatientSexe().equals("F"))
                holder.ivOldPatient.setImageResource(R.drawable.ic_mother);
            else
                holder.ivOldPatient.setImageResource(R.drawable.ic_father);
        } else if (isBetween(age, 71, 150)) {
            if (patient.getPatientSexe().equals("F"))
                holder.ivOldPatient.setImageResource(R.drawable.ic_grandmother);
            else
                holder.ivOldPatient.setImageResource(R.drawable.ic_grandfather);
        } else {
            if (patient.getPatientSexe().equals("F"))
                holder.ivOldPatient.setImageResource(R.drawable.ic_mother);
            else
                holder.ivOldPatient.setImageResource(R.drawable.ic_father);
        }

        holder.llContainer.setOnClickListener(v -> {
            Intent intent = new Intent(context, DashboardActivity.class);
            context.startActivity(intent);
            ((AppCompatActivity) context).overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).clear();

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ID, String.valueOf(patient.getPatientId()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_STATUT_FIEVRE_JAUNE, patient.getPatientStatutFJ());

            if (patient.getPatientStatutFJ().equals("0")) {
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE_FIEVRE_JAUNE, "null");
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOT_FIEVRE_JAUNE, "null");
            } else {
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE_FIEVRE_JAUNE, patient.getPatientDateFJ());
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOT_FIEVRE_JAUNE, patient.getPatientLotFJ());
            }

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PHOTO_CARNET, patient.getPatientPhotoCarnet());

            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_NAME, StringUtils.defaultString(patient.getPatientNomPrenoms()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_TELEPHONE, StringUtils.defaultString(patient.getPatientTelephone()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_BIRTHDAY, StringUtils.defaultString(patient.getPatientNaissance()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_EMAIL, StringUtils.defaultString(patient.getPatientEmail()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION, StringUtils.reverseDelimited(StringUtils.defaultString(patient.getPatientAbonnement()), '-'));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOGIN, StringUtils.defaultString(patient.getPatientLogin(), "INDEFINI"));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, patient.getPatientSexe());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_ID, Integer.parseInt(patient.getPatientFormule()));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_STATUS, (patient.getPatientPreg().equals("Y") ? "FEMME ENCEINTE" : "FEMME"));
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE, patient.getPatientAgePreg());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE_END, patient.getPatientAccouch());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE_BEGIN, patient.getPatientDebutPreg());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_TYPE_ID, patient.getPatientAbonnementType());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_TYPE_ID, patient.getPatientAbonnementType());
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_PLANNING_ID, patient.getPatientPlanningId());

            switch (patient.getPatientFormule()) {
                case "2":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PREMUIM");
                    break;

                case "3":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PRIVILEGE");
                    break;

                case "12":
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "GRATUIT");
                    break;

                default:
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "STANDARD");
            }

            SPUtils.getInstance().put(PATIENT_AGE_STATUS, detectAgeStatus(patient.getPatientNaissance()).toUpperCase());
        });
    }


    @Override
    public int getItemCount() {
        return patients.size();
    }


    class PatientFoundedHolder extends RecyclerView.ViewHolder {

        AppCompatImageView ivOldPatient;
        AppCompatTextView tvNomPatient;
        AppCompatTextView tvAbnLabel;
        AppCompatTextView tvLogin;
        AppCompatTextView tvAbonmt;
        LinearLayout llContainer;

        public PatientFoundedHolder(View itemView) {
            super(itemView);
            ivOldPatient = itemView.findViewById(R.id.patient_founded_items_old);
            tvNomPatient = itemView.findViewById(R.id.patient_founded_items_tv_nom);
            tvLogin = itemView.findViewById(R.id.patient_founded_items_tv_login);
            tvAbonmt = itemView.findViewById(R.id.patient_founded_items_tv_abonnement);
            tvAbnLabel = itemView.findViewById(R.id.labelAbonnemntLabel);
            llContainer = itemView.findViewById(R.id.patient_founded_items_container);
        }
    }
}
