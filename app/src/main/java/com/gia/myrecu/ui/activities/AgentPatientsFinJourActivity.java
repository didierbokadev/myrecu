package com.gia.myrecu.ui.activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.databases.room.AndroidSmsDatabase;
import com.gia.myrecu.models.v1.Abonnement;
import com.gia.myrecu.models.v1.Patient;
import com.gia.myrecu.network.OPIApiClient;
import com.gia.myrecu.network.OPIApiServices;
import com.gia.myrecu.ui.adapters.AgentFinJourPointsAdapter;
import com.gia.myrecu.ui.adapters.BluetoothDeviceAdapter;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_EXPIRATION;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_PLANNING_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_TYPE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_BIRTHDAY;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_DATE_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_EMAIL;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOGIN;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PHOTO_CARNET;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE_END;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_STATUS;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_SEXE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_STATUT_FIEVRE_JAUNE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_TELEPHONE;
import static com.gia.myrecu.tools.Constants.AGENT_NAME;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;
import static com.gia.myrecu.tools.Constants.CENTRE_NAME;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;


@SuppressWarnings("ALL")
public class AgentPatientsFinJourActivity extends AppCompatActivity implements BluetoothDeviceAdapter.OnClickDeviceBluetooth, AgentFinJourPointsAdapter.OnAgentPointAdapterListener {


    AgentFinJourPointsAdapter finJourPointsAdapter;
    RecyclerView rvList;
    AppCompatTextView tvEmpty;
    List<Abonnement> abonnementList;
    String dateNow = "";
    AppCompatTextView tvPatientsCount;
    AppCompatButton btnPrintRapport;

    int toNbreAbn;
    int toNbreReab;
    int toAmountAbnt;
    int toAmountReab;
    int toAmountGlobal;

    ProgressDialog dialog;

    private Connection btConnection;
    private ZebraPrinter zbPrinter;

    ConnectBTDevice connectBTDevice;
    int counPrintTentative;
    BluetoothAdapter bluetoothAdapter;
    Set<BluetoothDevice> btDevicesAlreadyBondedList;
    BluetoothDevice bluetoothDevice;
    BluetoothDeviceAdapter bluetoothDeviceAdapter;
    List<BluetoothDevice> deviceList;
    AlertDialog dialogPrinters;

    RecyclerView rvListDevices;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_patients_fin_jour);
        dateNow = DateTime.now().toString(DateTimeFormat.forPattern("yyyy-MM-dd"));

        rvList = findViewById(R.id.activity_agent_patients_fin_jour_rv_list);
        tvEmpty = findViewById(R.id.activity_agent_patients_fin_jour_tv_empty);
        tvPatientsCount = findViewById(R.id.activity_agent_patients_fin_jour_tv_patient_count);
        btnPrintRapport = findViewById(R.id.activity_agent_patients_fin_jour_btn_print);

        abonnementList = new ArrayList<>();
        finJourPointsAdapter = new AgentFinJourPointsAdapter(this, abonnementList, this);

        rvList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvList.setAdapter(finJourPointsAdapter);

        counPrintTentative = 0;

        deviceList = new ArrayList<>();
        bluetoothDeviceAdapter = new BluetoothDeviceAdapter(this, deviceList);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
            Toast.makeText(this, "Bluetooth not supported", Toast.LENGTH_SHORT).show();
            return;
        }

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (bluetoothAdapter == null) {
            ToastUtils.showShort("Peripherique bluetooth absent.");
            return;
        }

        btDevicesAlreadyBondedList = new HashSet<>();

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 2017);
            ToastUtils.setGravity(Gravity.CENTER, 0, 0);
            ToastUtils.showShort("Veuillez activer le bluetooth s'il vous plait.");
        } else {
            btDevicesAlreadyBondedList = bluetoothAdapter.getBondedDevices();
        }

        if (btDevicesAlreadyBondedList.size() > 0) {
            // Al ready devices bonded
            for (BluetoothDevice pairedDevice : btDevicesAlreadyBondedList) {
                if (pairedDevice.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.IMAGING) {
                    bluetoothDevice = pairedDevice;
                    deviceList.add(bluetoothDevice);
                }
            }
            bluetoothDeviceAdapter.notifyDataSetChanged();
        }


        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View printersView = inflater.inflate(R.layout.view_printers_bluetooth_presenter, null);

        rvListDevices = printersView.findViewById(R.id.view_printers_bluetooth_presenter_rv_list_devices);
        rvListDevices.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvListDevices.setAdapter(bluetoothDeviceAdapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Choisir une imprimante")
                .setView(printersView)
                .setCancelable(true);
        dialogPrinters = builder.create();

        if (AndroidSmsDatabase.getDatabase(this).abonnementDao().getAbonnementsListByDate(dateNow).size() > 0) {
            abonnementList.clear();
            abonnementList.addAll(AndroidSmsDatabase.getDatabase(this).abonnementDao().getAbonnementsListByDate(dateNow));

            finJourPointsAdapter.notifyDataSetChanged();

            rvList.setVisibility(View.VISIBLE);
            tvEmpty.setVisibility(View.GONE);
        } else {
            rvList.setVisibility(View.GONE);
            tvEmpty.setVisibility(View.VISIBLE);
        }

        tvPatientsCount.setText(String.valueOf(AndroidSmsDatabase.getDatabase(this).abonnementDao().getAbonnementsListByDate(dateNow).size() + " patient(s)"));

        btnPrintRapport.setOnClickListener(v -> {
            if (AndroidSmsDatabase.getDatabase(AgentPatientsFinJourActivity.this).abonnementDao().getAbonnementsList().size() > 0) {
                    toNbreAbn = AndroidSmsDatabase.getDatabase(AgentPatientsFinJourActivity.this).abonnementDao().totalTypeDay(DateTime.now().toString("yyyy-MM-dd"), "ABONNEMENT");
                    toNbreReab = AndroidSmsDatabase.getDatabase(AgentPatientsFinJourActivity.this).abonnementDao().totalTypeDay(DateTime.now().toString("yyyy-MM-dd"), "REABONNEMENT");
                    toAmountAbnt = AndroidSmsDatabase.getDatabase(AgentPatientsFinJourActivity.this).abonnementDao().totalAmountTypeDay(DateTime.now().toString("yyyy-MM-dd"), "ABONNEMENT");
                    toAmountReab = AndroidSmsDatabase.getDatabase(AgentPatientsFinJourActivity.this).abonnementDao().totalAmountTypeDay(DateTime.now().toString("yyyy-MM-dd"), "REABONNEMENT");
                    toAmountGlobal = AndroidSmsDatabase.getDatabase(AgentPatientsFinJourActivity.this).abonnementDao().totalAmountGlobalDay(DateTime.now().toString("yyyy-MM-dd"));

                    if (dialogPrinters != null) {
                        dialogPrinters.show();
                    }
                } else {
                    Toaster.toastLong("PAS DE POINT DISPONIBLE POUR LE MOMENT.");
                }
        });
    }


    @Override
    public void onDeviceClicked(BluetoothDevice bluetoothDevice, int position) {
        if (dialogPrinters.isShowing()) {
            dialogPrinters.dismiss();
        }

        // CONNECT TO PRINTER
        connectBTDevice = new ConnectBTDevice(bluetoothDevice);
        connectBTDevice.execute();
    }


    @Override
    public void throwCallPermission() {
    }


    @Override
    public void getClient(int clientID) {
        fetchClientIfos(clientID);
    }


    class ConnectBTDevice extends AsyncTask<Object, Object, ZebraPrinter> {

        BluetoothDevice device;

        ConnectBTDevice(BluetoothDevice device) {
            this.device = device;
        }

        @Override
        protected void onPreExecute() {
            if (dialog != null) {
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            } else {
                dialog = new ProgressDialog(AgentPatientsFinJourActivity.this);
                dialog.setMessage("Impression du point...");
                dialog.setCancelable(false);
                dialog.show();
            }
            counPrintTentative = counPrintTentative + 1;
        }

        @Override
        protected ZebraPrinter doInBackground(Object[] objects) {
            btConnection = new BluetoothConnection(device.getAddress());
            try {
                btConnection.open();
            } catch(Exception e) {
                e.printStackTrace();
                if (e instanceof ConnectionException) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            connectBTDevice.cancel(true);
                            connectBTDevice = null;
                            if (counPrintTentative > 3) {
                                onBackPressed();
                            }
                            connectBTDevice = new ConnectBTDevice(device);
                            connectBTDevice.execute();
                        }
                    });
                }
            }

            ZebraPrinter zebraPrinter = null;

            if (btConnection.isConnected()) {
                try {
                    zebraPrinter = ZebraPrinterFactory.getInstance(btConnection);
                } catch(Exception e) {
                    e.printStackTrace();
                }
            }

            return zebraPrinter;
        }

        @Override
        protected void onPostExecute(ZebraPrinter zebraPrinter) {
            zbPrinter = zebraPrinter;
            String date = DateTime.now().toString("dd-MM-yyyy");
            String heure = DateTime.now().toString("HH:mm");
            try {
                if (zbPrinter != null) {
                    btConnection.write(getConfigLabel(
                            SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME, ""),
                            heure, date, toNbreAbn, toAmountAbnt, toNbreReab, toAmountReab, toAmountGlobal,
                            SPUtils.getInstance(AGENT_PREFERENCES).getString(AGENT_NAME, "")
                    ));

                    //llAbonnement.setEnabled(true);
                    dialog.dismiss();

                    dialogPrinters.dismiss();

                    disconnect();
                }
            } catch(Exception e) {
                if (dialog != null) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                e.printStackTrace();
            }
        }
    }


    private byte[] getConfigLabel(String centre, String hour, String date, int nbreAbont,
                                  int totalAmountAbont, int nbreReabnt, int totalAmountReabnt, int globalTotal,
                                  String agent) {
        byte[] configLabel = null;

        String cpclConfigLabel =
                "! 0 200 200 890 1\r\n"
                        + "CENTER\r\n"
                        + "PCX 0 0 !<opis.pcx\r\n"
                        + "ML 25\r\n"
                        + "T 5 0 10 150\r\n"
                        + "CARNET DE VACCINATION\r\n"
                        + "ELECTRONIQUE\r\n"
                        + "ENDML\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 220 CENTRE : " + centre + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 250 HEURE : " + hour + "\r\n"
                        + "RIGHT\r\n"
                        + "T 0 2 0 250 DATE : " + date + "\r\n"
                        + "CENTER\r\n"
                        + "T 0 3 0 300 POINT DU JOUR\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 340 ABONNEMENT \r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 370 NOMBRE : " + nbreAbont + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 400 PRIX UNITAIRE : " + 1000 + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 430 TOTAL ------> : " + totalAmountAbont + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 460  __________ \r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 490 REABONNEMENT \r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 520 NOMBRE : " + nbreReabnt +"\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 550 PRIX UNITAIRE : " + 1000 +"\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 580 TOTAL ------> : " + totalAmountReabnt +"\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 610 _________________________________\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 640 TOTAUX ------------> " + globalTotal + "\r\n"
                        + "CENTER\r\n"
                        + "T 0 2 0 685 www.opisms.org / 21-24-34-89\r\n"
                        + "CENTER\r\n"
                        + "T 0 2 0 715 NUM.VERT : 143 (APPEL GRATUIT)\r\n"
                        + "RIGHT\r\n"
                        + "T 0 2 0 765 AGENT : " + agent + "\r\n"
                        + "PRINT\r\n";
        configLabel = cpclConfigLabel.getBytes();
        return configLabel;
    }


    public void disconnect() {
        try {
            //Log.e("MainActivity", "Disconnecting Printer");
            if (btConnection != null) {
                btConnection.close();
                zbPrinter = null;
                btConnection = null;
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }


    void fetchClientIfos(int clientId) {
        OPIApiClient.getClient(this).create(OPIApiServices.class).getPatientById(clientId, BuildConfig.DATABASE, "id")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Patient>() {
                    @Override
                    public void onSuccess(Patient patient) {
                        Intent intent = new Intent(AgentPatientsFinJourActivity.this, DashboardActivity.class);
                        startActivity(intent);
                        ((AppCompatActivity) AgentPatientsFinJourActivity.this).overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).clear();

                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ID, String.valueOf(patient.getPatientId()));
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_STATUT_FIEVRE_JAUNE, patient.getPatientStatutFJ());

                        if (patient.getPatientStatutFJ().equals("0")) {
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE_FIEVRE_JAUNE, "null");
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOT_FIEVRE_JAUNE, "null");
                        } else {
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE_FIEVRE_JAUNE, patient.getPatientDateFJ());
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOT_FIEVRE_JAUNE, patient.getPatientLotFJ());
                        }

                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PHOTO_CARNET, patient.getPatientPhotoCarnet());

                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_NAME, StringUtils.defaultString(patient.getPatientNomPrenoms()));
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_TELEPHONE, StringUtils.defaultString(patient.getPatientTelephone()));
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_BIRTHDAY, StringUtils.defaultString(patient.getPatientNaissance()));
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_EMAIL, StringUtils.defaultString(patient.getPatientEmail()));
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION, StringUtils.reverseDelimited(StringUtils.defaultString(patient.getPatientAbonnement()), '-'));
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOGIN, StringUtils.defaultString(patient.getPatientLogin(), "INDEFINI"));
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_TYPE_ID, patient.getPatientAbonnementType());
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_PLANNING_ID, patient.getPatientPlanningId());
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, patient.getPatientSexe());

                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_STATUS, (patient.getPatientPreg().equals("Y") ? "FEMME ENCEINTE" : "FEMME"));
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE, patient.getPatientAgePreg());
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE_END, patient.getPatientAccouch());

                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_ID, Integer.parseInt(patient.getPatientFormule()));

                        switch (patient.getPatientFormule()) {
                            case "2":
                                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PREMUIM");
                                break;

                            case "3":
                                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PRIVILEGE");
                                break;

                            case "12":
                                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "GRATUIT");
                                break;

                            default:
                                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "STANDARD");
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });
    }
}
