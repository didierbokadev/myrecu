package com.gia.myrecu.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatCheckBox;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;

import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.databases.room.AndroidSmsDatabase;
import com.gia.myrecu.models.v1.CalendrierVaccin;
import com.gia.myrecu.models.v1.Vaccin;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.tools.Utils;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.AGENT_ID;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;
import static com.gia.myrecu.tools.Constants.CENTRE_ID;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;
import static com.gia.myrecu.tools.Constants.VACCIN_CARNET_COMPRESSED;
import static com.gia.myrecu.tools.Constants.VACCIN_DATE_PRESENCE_SELECTED;
import static com.gia.myrecu.tools.Constants.VACCIN_DATE_RAPPEL_SELECTED;
import static com.gia.myrecu.tools.Constants.VACCIN_ID_SELECTED;
import static com.gia.myrecu.tools.Constants.VACCIN_LOT_SELECTED;
import static com.gia.myrecu.tools.Constants.VACCIN_NAME_SELECTED;
import static com.gia.myrecu.tools.Constants.VACCIN_PREFERENCES;

@SuppressWarnings("ALL")
public class UpdateVaccinActivity extends AppCompatActivity {


    AppCompatEditText etDatePresenceVac;
    AppCompatButton btnVaccin;
    AppCompatSpinner spVaccin;
    AppCompatEditText etDateRappelVac;
    AppCompatImageView ivCarnetPhoto;
    AppCompatEditText etLotVac;
    AndroidSmsDatabase database;
    List<Vaccin> vaccinList;
    ArrayAdapter<Vaccin> arrayAdapterVacc;
    ProgressDialog progressDialog;
    CalendrierVaccin calendrierVaccin;
    Toolbar tb;
    ActionBar actionBar;
    final String TAG = "UpdateVaccinActivity";
    LinearLayout llCarnetPhotoContainer;
    String carnetPhotoPath;
    File fileGlobal;
    String from;
    AppCompatCheckBox cbGenerateCalendar;
    UpdateVaccinsEngine updateVaccinsEngine;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_vaccin);

        database = AndroidSmsDatabase.getDatabase(this);

        progressDialog = new ProgressDialog(UpdateVaccinActivity.this);
        progressDialog.setMessage("Mise à jour du vaccin...");
        progressDialog.setCancelable(false);

        tb = findViewById(R.id.activity_update_vaccin_tb_title);
        spVaccin = findViewById(R.id.activity_update_vaccin_select_vaccin);
        etDatePresenceVac = findViewById(R.id.activity_update_vaccin_et_presence_vaccin);
        etDateRappelVac = findViewById(R.id.activity_update_vaccin_et_rappel_vaccin);
        etLotVac = findViewById(R.id.activity_update_vaccin_et_lot_vaccin);
        btnVaccin = findViewById(R.id.activity_update_vaccin_btn_update_vaccin);
        llCarnetPhotoContainer = findViewById(R.id.activity_update_vaccin_ll_carnter_photo_container);
        ivCarnetPhoto = findViewById(R.id.activity_update_vaccin_iv_carnter_photo);
        cbGenerateCalendar = findViewById(R.id.activity_update_vaccin_cb_generate_calendar);

        setSupportActionBar(tb);

        actionBar = getSupportActionBar();

        if (getIntent() != null) {
            from = getIntent().getStringExtra("from");
        } else {
            from = null;
        }

        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeAsUpIndicator(R.drawable.ic_close_white_24dp);
            actionBar.setSubtitle(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME, ""));
        }

        String now = DateTime.now().toString("ddMMyyyy");
        vaccinList = database.vaccinDao().getAllVaccins();

        // Sort list with Collections
        Collections.sort(vaccinList, Vaccin.VaccinNameComparator);

        etDateRappelVac.setText(now);
        arrayAdapterVacc = new ArrayAdapter<>(UpdateVaccinActivity.this, android.R.layout.simple_list_item_1, vaccinList);

        spVaccin.setAdapter(arrayAdapterVacc);

        if (getIntent().getParcelableExtra("vaccinToUpdate") != null) {
            calendrierVaccin = getIntent().getParcelableExtra("vaccinToUpdate");
            if (calendrierVaccin.getCalendrierVaccinType().equals("2")) {
                actionBar.setTitle("MISE A JOUR DU VACCIN MANQUÉ");
            } else {
                actionBar.setTitle("ANTICIPER UN VACCIN");
            }

            etDateRappelVac.setText("" + StringUtils.getDigits(calendrierVaccin.getCalendrierVaccinRappel()));

            for (Vaccin vac : vaccinList) {
                if (vac.getVaccinLabel().equals(calendrierVaccin.getCalendrierVaccinName())) {
                    arrayAdapterVacc.notifyDataSetChanged();
                    spVaccin.setAdapter(arrayAdapterVacc);
                    spVaccin.setSelection(vaccinList.indexOf(vac), true);
                    SPUtils.getInstance(VACCIN_PREFERENCES).remove(VACCIN_ID_SELECTED);
                    break;
                }
            }
        }

        spVaccin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Vaccin vaccin = (Vaccin) parent.getSelectedItem();
                SPUtils.getInstance(VACCIN_PREFERENCES).put(VACCIN_ID_SELECTED, vaccin.getVaccinId());
                SPUtils.getInstance(VACCIN_PREFERENCES).put(VACCIN_NAME_SELECTED, vaccin.getVaccinLabel());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnVaccin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateVaccin();
            }
        });

        etDatePresenceVac.setText(now);

        llCarnetPhotoContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dispatchTakePictureIntent();
            }
        });

    }


    public void updateVaccin() {
        if (etDatePresenceVac.getText().toString().trim().isEmpty() || etDateRappelVac.getText().toString().trim().isEmpty() || etLotVac.getText().toString().trim().isEmpty()) {
            return;
        } else {
            SPUtils.getInstance(VACCIN_PREFERENCES).put(VACCIN_DATE_PRESENCE_SELECTED, StringUtils.reverseDelimited(Utils.formatDate(etDatePresenceVac.getText().toString()), '-'));

            SPUtils.getInstance(VACCIN_PREFERENCES).put(VACCIN_DATE_RAPPEL_SELECTED, StringUtils.reverseDelimited(Utils.formatDate(etDateRappelVac.getText().toString()), '-'));
            SPUtils.getInstance(VACCIN_PREFERENCES).put(VACCIN_LOT_SELECTED, etLotVac.getText().toString());

            updateVaccinsEngine = new UpdateVaccinsEngine();
            updateVaccinsEngine.execute();
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int menuId = item.getItemId();
        if (menuId == android.R.id.home) {
            if (from != null) {
                Intent intentDasboard = new Intent(this, DashboardActivity.class);
                startActivity(intentDasboard);
                finish();
            } else {
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }


    @SuppressLint("StaticFieldLeak")
    private class UpdateVaccinsEngine extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();

                FormBody.Builder formBuilder = new FormBody.Builder();
                Request.Builder requestBuilder = new Request.Builder();

                formBuilder.add("d",  BuildConfig.DATABASE);

                if (calendrierVaccin != null) {
                    formBuilder.add("calId",  calendrierVaccin.getCalendrierVaccinId());
                    formBuilder.add("usrId",  String.valueOf(SPUtils.getInstance(AGENT_PREFERENCES)
                            .getInt(AGENT_ID)));
                    formBuilder.add("ctrId",  String.valueOf(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES)
                            .getInt(CENTRE_ID)));
                    formBuilder.add("patId",  String.valueOf(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                            .getString(ABONNE_OLD_ID)));
                    formBuilder.add("dtPre",  SPUtils.getInstance(VACCIN_PREFERENCES)
                            .getString(VACCIN_DATE_PRESENCE_SELECTED));
                    formBuilder.add("lot",  SPUtils.getInstance(VACCIN_PREFERENCES)
                            .getString(VACCIN_LOT_SELECTED));
                    formBuilder.add("imgCarnet",  SPUtils.getInstance(VACCIN_PREFERENCES)
                            .getString(VACCIN_CARNET_COMPRESSED));
                    formBuilder.add("genCal", (cbGenerateCalendar.isChecked() ? "Oui" : "Non"));

                    requestBuilder.url(UrlBanks.UPDATE_VACCUM_MISSED);
                } else {
                    formBuilder.add("usrId",  String.valueOf(SPUtils.getInstance(AGENT_PREFERENCES)
                            .getInt(AGENT_ID)));
                    formBuilder.add("ctrId",  String.valueOf(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES)
                            .getInt(CENTRE_ID)));
                    formBuilder.add("patId",  SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                            .getString(ABONNE_OLD_ID, "0"));
                    formBuilder.add("vacId",  String.valueOf(SPUtils.getInstance(VACCIN_PREFERENCES)
                            .getInt(VACCIN_ID_SELECTED)));
                    formBuilder.add("dtRap",  SPUtils.getInstance(VACCIN_PREFERENCES)
                            .getString(VACCIN_DATE_RAPPEL_SELECTED));
                    formBuilder.add("dtPre",  SPUtils.getInstance(VACCIN_PREFERENCES)
                            .getString(VACCIN_DATE_PRESENCE_SELECTED));
                    formBuilder.add("lot",  SPUtils.getInstance(VACCIN_PREFERENCES)
                            .getString(VACCIN_LOT_SELECTED));
                    formBuilder.add("imgCarnet",  SPUtils.getInstance(VACCIN_PREFERENCES)
                            .getString(VACCIN_CARNET_COMPRESSED));
                    formBuilder.add("genCal", (cbGenerateCalendar.isChecked() ? "Oui" : "Non"));

                    requestBuilder.url(UrlBanks.UPDATE_VACCUM_PATIENT);
                }

                RequestBody formBody = formBuilder.build();

                requestBuilder.post(formBody);
                Request clientRequest = requestBuilder.build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();

                if (clientResponse.isSuccessful()) {
                    JSONObject json = new JSONObject(clientResponse.body().string());
                    String statut = json.getString("statut");
                    String message = json.getString("message");

                    if (statut.equals("1")) {
                        Toaster.toastLong(message);
                        SPUtils.getInstance(VACCIN_PREFERENCES).clear();

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                etDatePresenceVac.setText(null);
                                etDateRappelVac.setText(null);
                                etLotVac.setText(null);
                                ivCarnetPhoto.setImageBitmap(null);
                                ivCarnetPhoto.setImageResource(R.drawable.ic_carnet_photo);
                                ivCarnetPhoto.setPadding(50, 50, 50, 50);
                                carnetPhotoPath = null;
                                progressDialog.dismiss();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                            }
                        });
                        Toaster.toastLong("Echec de réabonnement.");
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                        }
                    });
                    Toaster.toastLong("Echec de la mise à jour");
                }
            } catch (Exception ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        progressDialog.dismiss();
                    }
                });
                ex.printStackTrace();
                Toaster.toastLong("Echec de la mise à jour.");
            }
            return null;
        }
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File carnetFile = null;
            try {
                carnetFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (carnetFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.gia.myrecu.fileprovider",
                        carnetFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            //Bundle extras = data.getExtras();
            setPic();
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        carnetPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void createImageFileCompressed() {

        File compressedFile = null;
        try {
            fileGlobal = new File(carnetPhotoPath);
            compressedFile = new Compressor(this)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(fileGlobal);



            if (FileUtils.isFileExists(compressedFile)) {
                final File finalCompressedFile = compressedFile;
                FileUtils.copy(compressedFile, fileGlobal, new FileUtils.OnReplaceListener() {
                    @Override
                    public boolean onReplace(File srcFile, File destFile) {
                        //Toaster.toast("File remplace");
                        SPUtils.getInstance(VACCIN_PREFERENCES).put(VACCIN_CARNET_COMPRESSED, encodeFileToBase64Binary(finalCompressedFile));
                        return true;
                    }
                });
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private void setPic() {
        // Get the dimensions of the View
        int targetW = ivCarnetPhoto.getWidth();
        int targetH = ivCarnetPhoto.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        ivCarnetPhoto.setPadding(5, 5, 5, 5);
        ivCarnetPhoto.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    public static String encodeFileToBase64Binary(File file) {
        String encodedBase64 = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedBase64;
    }


    @Override
    public void onBackPressed() {
        if (from != null) {
            Intent intentDasboard = new Intent(this, DashboardActivity.class);
            startActivity(intentDasboard);
            finish();
        } else {
            super.onBackPressed();
        }
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

}
