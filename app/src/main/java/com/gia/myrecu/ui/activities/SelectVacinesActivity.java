package com.gia.myrecu.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.databases.room.AndroidSmsDatabase;
import com.gia.myrecu.messages.VacineSelectedMessage;
import com.gia.myrecu.messages.VacineUnselectedMessage;
import com.gia.myrecu.models.v1.Vaccin;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.ui.adapters.SelectVacineAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_TYPE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_STATUS;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_SEXE;
import static com.gia.myrecu.tools.Constants.PATIENT_AGE_STATUS;
import static com.gia.myrecu.tools.Constants.VACCIN_ADD_TYPE;

@SuppressWarnings("ALL")
public class SelectVacinesActivity extends AppCompatActivity {


    AppCompatImageView ivClearEdit;
    AppCompatImageView ivCloseWindows;
    AppCompatImageView ivSearchVacines;

    AppCompatEditText etSeachVacine;

    AppCompatTextView tvTitle;
    AppCompatButton btnNext;

    RecyclerView rvVacinesList;
    List<Vaccin> vacinsListTemp;
    List<Vaccin> vacinsList;
    List<Vaccin> vacinsListSelected;
    SelectVacineAdapter selectVacineAdapter;
    AndroidSmsDatabase database;
    String from;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_vacines);

        database = AndroidSmsDatabase.getDatabase(this);

        ivClearEdit = findViewById(R.id.activity_select_vacines_iv_clear_edit);
        ivCloseWindows = findViewById(R.id.activity_select_vacines_iv_close);
        btnNext = findViewById(R.id.activity_select_vacines_btn_next);
        ivSearchVacines = findViewById(R.id.activity_select_vacines_iv_search);
        etSeachVacine = findViewById(R.id.activity_select_vacines_et_search_vacine);
        tvTitle = findViewById(R.id.activity_select_vacines_tv_title);
        rvVacinesList = findViewById(R.id.activity_select_vacines_rv_vacines_list);

        if (SPUtils.getInstance().getString(PATIENT_AGE_STATUS, "").equals("ENFANT")) {
            vacinsList = database.vaccinDao().getChlidrenVaccinsList();
        } else if (SPUtils.getInstance().getString(PATIENT_AGE_STATUS, "").equals("ADULTE")) {
            if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_SEXE, "M").equals("F") && SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_PREGNANCY_STATUS).equals("FEMME ENCEINTE")) {
                vacinsList = database.vaccinDao().getPregnancyVaccinsList();
            } else if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("2")) {
                vacinsList = database.vaccinDao().getPlanningVaccinsList();
                tvTitle.setText("LISTE METHODES PF");
            } else  {
                vacinsList = database.vaccinDao().getAdultVaccinsList();
            }
        }

        vacinsListTemp = new ArrayList<>();
        vacinsListSelected = new ArrayList<>();

        vacinsListTemp.addAll(vacinsList);

        selectVacineAdapter = new SelectVacineAdapter(this, vacinsList);
        rvVacinesList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvVacinesList.setAdapter(selectVacineAdapter);

        from = SPUtils.getInstance(Constants.VACCIN_PREFERENCES).getString(VACCIN_ADD_TYPE);

        ivSearchVacines.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                etSeachVacine.setVisibility(View.VISIBLE);
            }
        });

        etSeachVacine.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                vacinsList.clear();

                if (s.toString().trim().length() > 1) {
                    for (Vaccin va : vacinsListTemp) {
                        if (va.getVaccinLabel().toLowerCase().contains(s.toString().toLowerCase().trim())) {
                            vacinsList.add(va);
                        }
                    }
                } else {
                    vacinsList.addAll(vacinsListTemp);
                }

                selectVacineAdapter.notifyDataSetChanged();
            }
        });

        btnNext.setOnClickListener(v -> {
            if (vacinsListSelected.size() == 0) {
                Toaster.toast("AUCUN VACCIN SELECTIONNÉ !");
                return;
            }

            Intent intentVacinesSelected = new Intent(SelectVacinesActivity.this, VacinesSelectedActivity.class);
            intentVacinesSelected.putParcelableArrayListExtra("vacinesSelected", (ArrayList<? extends Parcelable>) vacinsListSelected);
            intentVacinesSelected.putExtra("from", from);
            startActivity(intentVacinesSelected);
            overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
        });

        ivCloseWindows.setOnClickListener(v -> onBackPressed());
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void vacineSelectedEvent(VacineSelectedMessage selectedVacineMessage) {
        vacinsListSelected.add(selectedVacineMessage.getVaccinSelected());

        for (Vaccin vaSelectedUpdate : vacinsListTemp) {
            if (vaSelectedUpdate.getVaccinId() == selectedVacineMessage.getVaccinSelected().getVaccinId()) {
                vaSelectedUpdate.setVaccinChecked("1");
            }
        }
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void vacineSelectedEvent(VacineUnselectedMessage unselectedVacineMessage) {
        vacinsListSelected.remove(unselectedVacineMessage.getVaccinSelected());

        if (vacinsListSelected.size() > 0) {
            for (Vaccin vRem : vacinsListSelected) {
                if (vRem.getVaccinId() == unselectedVacineMessage.getVaccinSelected().getVaccinId()) {
                    vacinsListSelected.remove(vRem);
                }
            }
        }

        for (Vaccin vaUnselectedUpdate : vacinsListTemp) {
            if (vaUnselectedUpdate.getVaccinId() == unselectedVacineMessage.getVaccinSelected().getVaccinId()) {
                vaUnselectedUpdate.setVaccinChecked("0");
            }
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        if (!EventBus.getDefault().isRegistered(this)) EventBus.getDefault().register(this);
    }


    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Override
    public void onBackPressed() {
        if (from != null) {
            if (from.equals("abn_newest")) {
                ActivityUtils.startActivity(ChoixMiseAJourActivity.class);
                finish();
            } else {
                finish();
            }
        } else {
            finish();
        }
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
