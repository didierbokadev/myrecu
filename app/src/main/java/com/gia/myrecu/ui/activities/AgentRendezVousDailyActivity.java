package com.gia.myrecu.ui.activities;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import androidx.annotation.NonNull;

import com.gia.myrecu.BuildConfig;
import com.google.android.material.card.MaterialCardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.View;
import android.widget.DatePicker;

import com.blankj.utilcode.util.BusUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.Patient;
import com.gia.myrecu.network.OPIApiClient;
import com.gia.myrecu.network.OPIApiServices;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.ui.adapters.PatientEnAttenteAdapter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;


@SuppressWarnings("ALL")
public class AgentRendezVousDailyActivity extends AppCompatActivity
        implements PatientEnAttenteAdapter.OnPatientAttenteAdapterListener, DatePickerDialog.OnDateSetListener {


    ProgressDialog loading;
    PatientEnAttenteAdapter patientEnAttenteAdapter;
    List<Patient> patientList;
    RecyclerView rvList;
    AppCompatTextView tvEmpty;
    MaterialCardView cardListContainer;
    OkHttpClient agentHttpClient;
    AppCompatTextView tvPatientsCount;
    AppCompatImageView ivBack;
    AppCompatImageView ivFilter;
    MaterialCardView mcvDateContainer;
    AppCompatTextView tvOpenDate;
    AppCompatTextView tvCloseDate;
    AppCompatButton btnLaunchSearch;
    OPIApiServices opiApiServices;
    Calendar dateCalendar;
    int which = 1;
    String dateOpen;
    String dateClose;
    LinearLayoutManager llmPatients;
    AppCompatTextView tvLoadMore;
    String abonnementType = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agent_rendez_vous_daily);

        dateCalendar = Calendar.getInstance();

        rvList = findViewById(R.id.activity_agent_rendez_vous_daily_rv_list);
        tvEmpty = findViewById(R.id.activity_agent_rendez_vous_daily_tv_empty);
        tvPatientsCount = findViewById(R.id.activity_agent_rendez_vous_daily_tv_patient_count);
        cardListContainer = findViewById(R.id.activity_agent_rendez_vous_daily_mcv_patients_container);

        ivBack = findViewById(R.id.activity_agent_rendez_vous_daily_iv_close);
        ivFilter = findViewById(R.id.activity_agent_rendez_vous_daily_iv_filter);
        mcvDateContainer = findViewById(R.id.activity_agent_rendez_vous_daily_mcv_date_container);
        tvOpenDate = findViewById(R.id.activity_agent_rendez_vous_daily_tv_open_date);
        tvCloseDate = findViewById(R.id.activity_agent_rendez_vous_daily_tv_close_date);
        btnLaunchSearch = findViewById(R.id.activity_agent_rendez_vous_daily_btn_search);
        tvLoadMore = findViewById(R.id.activity_agent_rendez_vous_daily_tv_load_more);

        opiApiServices = OPIApiClient.getClient(this).create(OPIApiServices.class);

        patientList = new ArrayList<>();
        patientEnAttenteAdapter = new PatientEnAttenteAdapter(this, patientList);

        llmPatients = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        rvList.setLayoutManager(llmPatients);
        rvList.setAdapter(patientEnAttenteAdapter);

        loading = new ProgressDialog(this);
        loading.setMessage("Chargement en cours...");
        loading.setCancelable(true);
        loading.show();

        tvOpenDate.setOnClickListener(v -> {
            which = 1;
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    this, this, dateCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH), dateCalendar.get(Calendar.DAY_OF_MONTH));

            // datePickerDialog.getDatePicker().setMaxDate(DateTime.now().getMillis());
            datePickerDialog.show();
        });

        tvOpenDate.setOnClickListener(v -> {
            which = 1;
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    this, this, dateCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH), dateCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.getDatePicker().setMaxDate(DateTime.now().getMillis());
            datePickerDialog.show();
        });


        tvCloseDate.setOnClickListener(v -> {
            which = 2;
            DatePickerDialog datePickerDialog = new DatePickerDialog(
                    this, this, dateCalendar.get(Calendar.YEAR), dateCalendar.get(Calendar.MONTH), dateCalendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        });

        ivFilter.setOnClickListener(v -> {
            if (mcvDateContainer.getVisibility() == View.VISIBLE) {
                mcvDateContainer.setVisibility(View.GONE);
                btnLaunchSearch.setVisibility(View.GONE);
            } else {
                mcvDateContainer.setVisibility(View.VISIBLE);
                btnLaunchSearch.setVisibility(View.VISIBLE);
            }
        });

        btnLaunchSearch.setOnClickListener(v -> {
            loading.show();
            patientList.clear();
            patientEnAttenteAdapter.notifyDataSetChanged();
            tvPatientsCount.setText("0 patient(s)");
            loadPatientsByFilter();
            // countRendezVousInterv();
        });

        ivBack.setOnClickListener(v -> {
            finish();
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        });

        tvLoadMore.setOnClickListener(v -> {
            loading.show();

            if (btnLaunchSearch.getVisibility() == View.VISIBLE) {
                LogUtils.e("Call here...");
                loadPatientsByFilter();
            } else {
                loadPatientsNow();
            }
        });

        // countRendezVousNow();

        String dateNow = DateTime.now().toString("dd MMM yyyy");

        tvOpenDate.setText(dateNow);
        tvCloseDate.setText(dateNow);

        dateOpen = DateTime.now().toString(DateTimeFormat.forPattern("yyyy-MM-dd"));
        dateClose = DateTime.now().toString(DateTimeFormat.forPattern("yyyy-MM-dd"));
        abonnementType = "1";

        loadPatientsByFilter();

        btnLaunchSearch.setVisibility(View.VISIBLE);
    }


    @BusUtils.Bus(tag = "ReachedBottom", threadMode = BusUtils.ThreadMode.MAIN)
    public void onReachedBottomEvent(int offset) {
        tvLoadMore.setVisibility(View.VISIBLE);
    }


    @Override
    protected void onStart() {
        super.onStart();
        BusUtils.register(this);
    }


    @Override
    protected void onStop() {
        BusUtils.unregister(this);
        super.onStop();
    }


    @Override
    public void throwCallPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_DENIED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, 2017);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == 2017) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                SPUtils.getInstance(Constants.AGENT_PREFERENCES).put(Constants.AGENT_CALL_PERMISSION, true);
            } else {
                SPUtils.getInstance(Constants.AGENT_PREFERENCES).put(Constants.AGENT_CALL_PERMISSION, false);
            }
        }
    }


    void countRendezVousNow() {
        opiApiServices.getRdvPatientsDailyTotal(String.valueOf(SPUtils.getInstance(Constants.DISTRICT_CENTRE_PREFERENCES).getInt(Constants.CENTRE_ID, 1)), "" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Integer>() {
                    @Override
                    public void onSuccess(Integer integer) {
                        tvPatientsCount.setText(integer + " patient(s)");
                        // loadPatientsNow();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtils.showShort("Impossible de se connecter au serveur de données !");
                        e.printStackTrace();
                        loading.dismiss();
                        tvEmpty.setVisibility(View.GONE);
                        rvList.setVisibility(View.GONE);
                        cardListContainer.setVisibility(View.GONE);
                    }
                });
    }


    void countRendezVousInterv() {
        opiApiServices.getRdvPatientsIntervTotal(String.valueOf(SPUtils.getInstance(Constants.DISTRICT_CENTRE_PREFERENCES).getInt(Constants.CENTRE_ID, 1)), dateOpen, dateClose, BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<Integer>() {
                    @Override
                    public void onSuccess(Integer integer) {
                        tvPatientsCount.setText(integer + " patient(s)");
                        loadPatientsByFilter();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtils.showShort("Impossible de se connecter au serveur de données !");
                        e.printStackTrace();
                        loading.dismiss();
                        tvEmpty.setVisibility(View.GONE);
                        rvList.setVisibility(View.GONE);
                        cardListContainer.setVisibility(View.GONE);
                    }
                });
    }


    void loadPatientsNow() {
        opiApiServices.getRdvPatientsDaily(String.valueOf(SPUtils.getInstance(Constants.DISTRICT_CENTRE_PREFERENCES).getInt(Constants.CENTRE_ID, 1)), patientList.size(), BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Patient>>() {
                    @Override
                    public void onSuccess(List<Patient> patients) {
                        patientList.addAll(patients);

                        if (patients.size() == 0) {
                            ToastUtils.showShort("Aucun patient n'a de visite aujourd'hui !");
                            tvEmpty.setVisibility(View.VISIBLE);
                            rvList.setVisibility(View.GONE);
                            cardListContainer.setVisibility(View.GONE);
                        } else {
                            tvEmpty.setVisibility(View.GONE);
                            cardListContainer.setVisibility(View.VISIBLE);
                            rvList.setVisibility(View.VISIBLE);
                        }

                        tvLoadMore.setVisibility(View.GONE);
                        patientEnAttenteAdapter.notifyDataSetChanged();
                        loading.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtils.showShort("Impossible de se connecter au serveur de données !");
                        e.printStackTrace();
                        loading.dismiss();
                        tvEmpty.setVisibility(View.GONE);
                        rvList.setVisibility(View.GONE);
                        cardListContainer.setVisibility(View.GONE);
                    }
                });
    }


    void loadPatientsByFilter() {
        opiApiServices.getPatientsByDatesIntervales(
                "" + String.valueOf(SPUtils.getInstance(Constants.DISTRICT_CENTRE_PREFERENCES).getInt(Constants.CENTRE_ID, 1)),
                "" + dateOpen,
                "" + dateClose,
                "" + BuildConfig.DATABASE,
                "" + abonnementType,
                "" + patientList.size())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Patient>>() {
                    @Override
                    public void onSuccess(List<Patient> patients) {
                        patientList.addAll(patients);

                        if (patients.size() == 0) {
                            ToastUtils.showShort("Aucun patient n'a de visite aujourd'hui !");
                            tvEmpty.setVisibility(View.VISIBLE);
                            rvList.setVisibility(View.GONE);
                            cardListContainer.setVisibility(View.GONE);
                        } else {
                            tvPatientsCount.setText(patients.size() + " patient(s)");

                            tvEmpty.setVisibility(View.GONE);
                            cardListContainer.setVisibility(View.VISIBLE);
                            rvList.setVisibility(View.VISIBLE);
                        }

                        tvLoadMore.setVisibility(View.GONE);
                        patientEnAttenteAdapter.notifyDataSetChanged();
                        loading.dismiss();
                    }

                    @Override
                    public void onError(Throwable e) {
                        ToastUtils.showShort("Impossible de se connecter au serveur de données !");
                        e.printStackTrace();
                        loading.dismiss();
                        tvEmpty.setVisibility(View.GONE);
                        rvList.setVisibility(View.GONE);
                        cardListContainer.setVisibility(View.GONE);
                    }
                });
    }


    public void onTypeAbonnementClicked(View view) {
        // Is the button now checked?
        boolean checkedAbonnement = ((AppCompatRadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.radioVaccin : // Show vaccine tools
                if (checkedAbonnement) {
                    abonnementType = "1";
                }
                break;

            case R.id.radioPlanning: // Show planning tools
                if (checkedAbonnement) {
                    abonnementType = "2";
                }
                break;

            case R.id.radioPregnant: // Show Pregnant tools
                if (checkedAbonnement) {
                    abonnementType = "3";
                }
                break;
        }
    }


    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        String monthS = "";
        DateTime dateTime = new DateTime(year, (month + 1), dayOfMonth, 0, 0);

        if (which == 1) {
            dateOpen = dateTime.toString("yyyy-MM-dd");
            tvOpenDate.setText(dateTime.toString("dd MMM yyyy"));
        } else {
            dateClose = dateTime.toString("yyyy-MM-dd");
            tvCloseDate.setText(dateTime.toString("dd MMM yyyy"));
        }
    }
}
