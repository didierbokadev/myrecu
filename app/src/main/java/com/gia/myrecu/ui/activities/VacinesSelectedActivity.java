package com.gia.myrecu.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.KeyboardUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.messages.VacccineListUpdate;
import com.gia.myrecu.messages.VacineSelectedEditMessage;
import com.gia.myrecu.models.v1.Vaccin;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.tools.ImageFilePath;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.ui.adapters.VacineSelectedAdapter;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.joda.time.DateTime;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_NEW_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_TYPE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.AGENT_ID;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;
import static com.gia.myrecu.tools.Constants.CENTRE_ID;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;
import static com.gia.myrecu.tools.Constants.NEW_NAISSANCE_ID;
import static com.gia.myrecu.tools.Constants.NEW_NAISSANCE_PREFERENCES;
import static com.gia.myrecu.tools.Constants.VACCIN_ADD_TYPE;
import static com.gia.myrecu.tools.Constants.VACCIN_CARNET_COMPRESSED;
import static com.gia.myrecu.tools.Constants.VACCIN_PREFERENCES;


@SuppressWarnings("ALL")
public class VacinesSelectedActivity extends AppCompatActivity {


    List<Vaccin> vaccinsSelected;
    RecyclerView rvVacinesSelectedList;
    VacineSelectedAdapter vacineSelectedAdapter;
    AppCompatButton btnAddVacines;
    RelativeLayout rlPhotoContainer;
    AlertDialog loading;
    ProgressDialog progressDialog;
    AppCompatImageView ivImportFile;
    AppCompatImageView ivTakeFile;
    AppCompatImageView ivClose;
    AppCompatImageView ivPhoto;
    AppCompatTextView labelTitle;
    AppCompatTextView labelPhotos;
    String carnetPhotoPath;
    File fileGlobal;
    int photoMethod = 1;
    private ProgressDialog pgdLoading;
    private String from;
    String defaultDate = "";
    AlertDialog dialogPrinters;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vacines_selected);

        AlertDialog.Builder builderLoading = new AlertDialog.Builder(this)
                .setCancelable(false);
        loading = builderLoading.create();

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(true);

        rvVacinesSelectedList = findViewById(R.id.activity_vacines_selected_rv_vacines_list);
        btnAddVacines = findViewById(R.id.activity_vacines_selected_btn_add_vacines);
        rlPhotoContainer = findViewById(R.id.activity_vacines_selected_ll_photo_container);
        ivPhoto = findViewById(R.id.activity_vacines_selected_iv_photo);
        ivImportFile = findViewById(R.id.activity_vacines_selected_iv_import_photo);
        ivTakeFile = findViewById(R.id.activity_vacines_selected_iv_take_photo);
        ivClose = findViewById(R.id.activity_vacines_selected_iv_close);

        labelTitle = findViewById(R.id.activity_vacines_selected_tv_title);
        labelPhotos = findViewById(R.id.activity_vacines_selected_tv_title_photo);

        vaccinsSelected = new ArrayList<>();

        if (getIntent() != null) {
            vaccinsSelected.clear();
            vaccinsSelected.addAll((ArrayList) getIntent().getParcelableArrayListExtra("vacinesSelected"));

            btnAddVacines.setText(String.format(Locale.CANADA_FRENCH, "AJOUTER LES %1$d VISISTE(S)", vaccinsSelected.size()));
            labelTitle.setText("VISITES SÉLECTIONNÉES");
            labelPhotos.setText("PHOTO DES VISITES SELECTIONÉES");

            vacineSelectedAdapter = new VacineSelectedAdapter(this, vaccinsSelected);
            rvVacinesSelectedList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
            rvVacinesSelectedList.setAdapter(vacineSelectedAdapter);

            from = SPUtils.getInstance(VACCIN_PREFERENCES).getString(VACCIN_ADD_TYPE, "scheduler");

            btnAddVacines.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < vaccinsSelected.size(); i++) {

                        Vaccin vaccinAdd = vaccinsSelected.get(i);
                        if (vaccinAdd.getVaccinPresence().length() == 0) {
                            vaccinAdd.setVaccinPresence(DateTime.now().toString("yyyy-MM-dd"));
                        }
                        if (vaccinAdd.getVaccinRappel().length() == 0) {
                            vaccinAdd.setVaccinRappel(defaultDate);
                        }
                        new UpdateVaccinsEngine(vaccinAdd, i).execute();
                    }
                }
            });

            ivImportFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    photoMethod = 2;
                    showFileChooser(1);
                }
            });

            ivTakeFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    photoMethod = 1;
                    dispatchTakePictureIntent();
                }
            });


            if (from.equals("scheduler")) {
                btnAddVacines.setText("PROGRAMMER LES " + vaccinsSelected.size() + " VISITES");
            }
        }

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
                overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
            }
        });

        if (KeyboardUtils.isSoftInputVisible(this)) {
            KeyboardUtils.hideSoftInput(this);
        }

        if (from.equals("scheduler")) {
            rlPhotoContainer.setVisibility(View.GONE);
        }

        LogUtils.e("VACCIN CATE + " + SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1"));
    }


    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    protected void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void vaccinSelectedUpdateEvent(VacccineListUpdate selectedUpdateMessage) {
        btnAddVacines.setText(String.format(Locale.CANADA_FRENCH, "AJOUTER LES %1$d VISITE(S)", vaccinsSelected.size()));
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void vaccinSelectedEditEvent(VacineSelectedEditMessage selectedEditMessage) {
        vaccinsSelected.get(selectedEditMessage.getPosition()).setVaccinLot(selectedEditMessage.getVaccinSelectedEdit().getVaccinLot());
        vaccinsSelected.get(selectedEditMessage.getPosition()).setVaccinPresence(selectedEditMessage.getVaccinSelectedEdit().getVaccinPresence());
        vaccinsSelected.get(selectedEditMessage.getPosition()).setVaccinRappel(selectedEditMessage.getVaccinSelectedEdit().getVaccinRappel());

        defaultDate = selectedEditMessage.getVaccinSelectedEdit().getVaccinRappel();
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File carnetFile = null;
            try {
                carnetFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (carnetFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.gia.myrecu.fileprovider",
                        carnetFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }


    private void showFileChooser(int pView) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent, "Choisir une photo"), pView);
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        carnetPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {

            if (photoMethod == 1) {
                setPic();
            } else {
                Bundle extras = data.getExtras();
                setPic(data.getData());
            }
        }
    }


    private void setPic(Uri uriTraitment) {
        // Get the dimensions of the View
        int targetW = ivPhoto.getWidth();
        int targetH = ivPhoto.getHeight();


        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        carnetPhotoPath = ImageFilePath.getPath(this, uriTraitment);

        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoTakingW = bmOptions.outWidth;
        int photoTakingH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleTakingFactor = Math.min(photoTakingW/targetW, photoTakingH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleTakingFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        ivPhoto.setPadding(5, 5, 5, 5);
        ivPhoto.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    private void setPic() {
        // Get the dimensions of the View
        int targetW = ivPhoto.getWidth();
        int targetH = ivPhoto.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoTakingW = bmOptions.outWidth;
        int photoTakingH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleTakingFactor = Math.min(photoTakingW/targetW, photoTakingH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleTakingFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        ivPhoto.setPadding(5, 5, 5, 5);
        ivPhoto.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    private void createImageFileCompressed() {
        File compressedFile = null;
        try {
            fileGlobal = new File(carnetPhotoPath);
            compressedFile = new Compressor(this)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(fileGlobal);

            if (FileUtils.isFileExists(compressedFile)) {
                final File finalCompressedFile = compressedFile;

                FileUtils.copy(compressedFile, fileGlobal, new FileUtils.OnReplaceListener() {
                    @Override
                    public boolean onReplace(File srcFile, File destFile) {
                        //Toaster.toast("File remplace");
                        SPUtils.getInstance(VACCIN_PREFERENCES).put(VACCIN_CARNET_COMPRESSED, encodeFileToBase64Binary(finalCompressedFile));
                        return true;
                    }
                });
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String encodeFileToBase64Binary(File file) {
        String encodedBase64 = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedBase64;
    }


    private class TreatPhotoPicAsync extends AsyncTask<Void, Void, Bitmap> {

        private Uri uriTraitment;
        private Bitmap bitmapResult;

        TreatPhotoPicAsync(Uri uriTraitment) {
            this.uriTraitment = uriTraitment;
            this.bitmapResult = null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (pgdLoading != null) {
                if (pgdLoading.isShowing()) {
                    pgdLoading.dismiss();
                    pgdLoading.setMessage("Traitement de l'image en cours...");
                    pgdLoading.setCancelable(false);
                    pgdLoading.show();
                } else {
                    pgdLoading.setMessage("Traitement de l'image en cours...");
                    pgdLoading.setCancelable(false);
                    pgdLoading.show();
                }
            } else {
                pgdLoading = new ProgressDialog(VacinesSelectedActivity.this);
                pgdLoading.setMessage("Traitement de l'image en cours...");
                pgdLoading.setCancelable(false);
                pgdLoading.show();
            }
        }

        @Override
        protected Bitmap doInBackground(Void... voids) {
            String finalpathpickgallery;
            File image;
            try {
                finalpathpickgallery = ImageFilePath.getPath(VacinesSelectedActivity.this, uriTraitment);
                if (finalpathpickgallery != null) {

                    Bitmap bitmap2 = null;

                    image = null;
                    image = new File(finalpathpickgallery);

                    fileGlobal = new Compressor(VacinesSelectedActivity.this)
                            .setQuality(75)
                            .setCompressFormat(Bitmap.CompressFormat.JPEG).compressToFile(image);

                    bitmap2 = new Compressor(VacinesSelectedActivity.this)
                            .setQuality(75)
                            .setCompressFormat(Bitmap.CompressFormat.JPEG).compressToBitmap(image);

                    //electeur.setElecteurPhotoLocal2(encodeFileToBase64Binary(globalImage2));
                    //electeur.setElecteurPhoto2(globalImage2.getName());
                    ///electeur.setElecteurPhotoPath2(globalImage2.getPath());

                    bitmapResult = bitmap2;
                }
            } catch (IOException | OutOfMemoryError e) {
                Toaster.toastLong("Erreur photo");
                bitmapResult = null;
            }

            return bitmapResult;
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            if (pgdLoading != null) {
                if (pgdLoading.isShowing()) {
                    pgdLoading.dismiss();
                }
            }

            if (bitmap != null) {
                //ivPhotoB.setPadding(0, 0, 5, 0);
                ivPhoto.setImageBitmap(bitmap);
            } else {
                Toaster.toast("Votre image est assez lourde !");
            }
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class UpdateVaccinsEngine extends AsyncTask<String, Void, Integer> {

        Vaccin vaBackground;
        int position;

        public UpdateVaccinsEngine(Vaccin vaBackground, int position) {
            this.vaBackground = vaBackground;
            this.position = position;
        }

        @Override
        protected void onPreExecute() {
            progressDialog.setMessage("Mise à jour en cours..");

            if (from.equals("scheduler")) {
                progressDialog.setMessage("Programmation en cours..");
            }
            progressDialog.show();
        }

        @Override
        protected Integer doInBackground(String... params) {

            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();

                FormBody.Builder formBuilder = new FormBody.Builder();
                Request.Builder requestBuilder = new Request.Builder();

                formBuilder.add("usrId",  String.valueOf(SPUtils.getInstance(AGENT_PREFERENCES).getInt(AGENT_ID)));
                formBuilder.add("ctrId",  String.valueOf(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID)));
                formBuilder.add("patId",  SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_ID, "0").equals("0")
                        ? SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ID, "0")
                        : SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_ID, "0"));
                formBuilder.add("vacId",  String.valueOf(vaBackground.getVaccinId()));

                if (from.equals("scheduler")) {
                    formBuilder.add("dtRap",  vaBackground.getVaccinRappel());
                    formBuilder.add("dtPre",  "0000-00-00");
                    formBuilder.add("lot",  "");
                    formBuilder.add("type",  "scheduler");
                } else {
                    formBuilder.add("dtRap",  vaBackground.getVaccinRappel());
                    formBuilder.add("dtPre",  vaBackground.getVaccinPresence());
                    formBuilder.add("lot",  vaBackground.getVaccinLot());
                    formBuilder.add("type",  "add");
                }

                formBuilder.add("imgCarnet",  SPUtils.getInstance(VACCIN_PREFERENCES).getString(VACCIN_CARNET_COMPRESSED, ""));
                formBuilder.add("genCal", "Non");
                formBuilder.add("d", BuildConfig.DATABASE);
                formBuilder.add("typeAbnt", "1");
                //formBuilder.add("genCal", (cbGenerateCalendar.isChecked() ? "Oui" : "Non"));

                requestBuilder.url(UrlBanks.UPDATE_VACCUM_PATIENT);

                RequestBody formBody = formBuilder.build();

                requestBuilder.post(formBody);
                Request clientRequest = requestBuilder.build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();
                String clientString = clientResponse.body().string();

                if (clientResponse.isSuccessful()) {
                    JSONObject json = new JSONObject(clientString);
                    String statut = json.getString("statut");
                    String message = json.getString("message");

                    return Integer.parseInt(statut);
                } else {
                    return 0;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
                return 0;
            }
        }

        @Override
        protected void onPostExecute(Integer integer) {
            if (integer == 1) {
                vacineSelectedAdapter.removeVacineSelected(0);
                //vaccinsSelected.remove(0);
            } else {
                Toaster.toast("Echec de mise à jour " + vaBackground.getVaccinLabel());
                vacineSelectedAdapter.notifyDataSetChanged();
                return;
            }

            if (vaccinsSelected.size() == 0) {
                progressDialog.dismiss();
                Toaster.toastLong("VISITES AJOUTÉES !");
                SPUtils.getInstance(VACCIN_PREFERENCES).remove(VACCIN_ADD_TYPE, true);

                ActivityUtils.finishActivity(SelectVacinesActivity.class);

                if (!from.equals("scheduler")) {
                    createAddVaccinDialog();
                }

                if (from.equals("scheduler")) {
                    finish();
                    overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
                }
            }
        }
    }


    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }


    private void createAddVaccinDialog() {
        dialogPrinters = null;

        View viewAddVacuum = LayoutInflater.from(this).inflate(R.layout.dialog_add_vacum_patient, null);
        AppCompatTextView tvLabelDialog = viewAddVacuum.findViewById(R.id.labelTitleDialog);
        AppCompatTextView tvQuestionVacuum = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_question_patient_add_vacuum);
        AppCompatTextView tvNon = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_non);
        AppCompatTextView tvOui = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_oui);

        if (from.equals("scheduler")) {
            tvLabelDialog.setText("PROGRAMMAION VISITE");
        }

        tvQuestionVacuum.setText(getString(R.string.txt_question_schedule_vaccum_visite,
                "visites",
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME, "")));

        AlertDialog.Builder builderAddVacuum = new AlertDialog.Builder(this)
                .setView(viewAddVacuum);

        tvNon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogPrinters != null) dialogPrinters.dismiss();
                dialogPrinters = null;

                if (from.equals("abn_newest")) {
                    Intent intentDashboardPatient = new Intent(VacinesSelectedActivity.this, DashboardActivity.class);
                    startActivity(intentDashboardPatient);
                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).clear();
                }

                overridePendingTransition(R.anim.slide_to_left, R.anim.slide_from_right);
                finish();
            }
        });

        tvOui.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (dialogPrinters != null) dialogPrinters.dismiss();
                dialogPrinters = null;

                startActivity(new Intent(VacinesSelectedActivity.this, SelectVacinesActivity.class));
                SPUtils.getInstance(Constants.VACCIN_PREFERENCES).put(VACCIN_ADD_TYPE, "scheduler");
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);

                SPUtils.getInstance(ABONNE_NEW_PREFERENCES).clear();
                finish();
            }
        });

        dialogPrinters = null;
        dialogPrinters = builderAddVacuum.create();

        dialogPrinters.show();
    }
}
