package com.gia.myrecu.ui.activities;

import android.os.Bundle;
import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import android.view.View;
import android.widget.ProgressBar;

import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.AppController;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.databases.room.AndroidSmsDatabase;
import com.gia.myrecu.models.v1.Centre;
import com.gia.myrecu.models.v1.District;
import com.gia.myrecu.models.v1.Vaccin;
import com.gia.myrecu.models.v2.CommuneModel;
import com.gia.myrecu.models.v2.NiveauClasseModel;
import com.gia.myrecu.models.v2.NiveauEtudeModel;
import com.gia.myrecu.models.v2.ProfessionModel;
import com.gia.myrecu.models.v2.QuartierModel;
import com.gia.myrecu.models.v2.VilleModel;
import com.gia.myrecu.network.OPIApiClient;
import com.gia.myrecu.network.OPIApiServices;

import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.gia.myrecu.tools.Constants.AGENT_LOG_STATUT;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;
import static com.gia.myrecu.tools.Constants.CENTRE_NAME;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;
import static com.gia.myrecu.tools.Constants.DISTRICT_NAME;
import static com.gia.myrecu.tools.Constants.PHONE_IS_CONFIGURED;
import static com.gia.myrecu.tools.Constants.PHONE_PREFERENCES;


@SuppressWarnings("ALL")
public class SplashActivity extends AppCompatActivity {


    public static final String TAG = "SplashActivity";
    AndroidSmsDatabase database;
    District district;
    Centre centre;
    // Vaccin vaccin;
    ProgressBar pgbLoading;
    AppCompatTextView tvInfoConfigs;

    OPIApiServices apiServices;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_activity);

        apiServices = OPIApiClient.getClient(this).create(OPIApiServices.class);

        findViewByIds();

        database = AndroidSmsDatabase.getDatabase(this);

        //new AlertDialog.Builder(this).setMessage("Veuillez patientez").setCancelable(false).show();

        if (SPUtils.getInstance(PHONE_PREFERENCES).getBoolean(PHONE_IS_CONFIGURED)) {
            if (!SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(DISTRICT_NAME)
                    .equals("") && !SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME)
                    .equals("")) {
                if (SPUtils.getInstance(AGENT_PREFERENCES).getBoolean(AGENT_LOG_STATUT)) {
                    if (SPUtils.getInstance(PHONE_PREFERENCES).getBoolean(PHONE_IS_CONFIGURED)) {
                        AppController.startNewTopActivity(SplashActivity.this, StartActivity.class);
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    } else {
                        AppController.startNewTopActivity(SplashActivity.this, ConfigsActivity.class);
                        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                    }
                } else {
                    AppController.startNewTopActivity(SplashActivity.this, ConnexionActivity.class);
                    overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                }
            } else {
                AppController.startNewTopActivity(SplashActivity.this, ConfigsActivity.class);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        } else {
            fetchDistricts();
        }

        /*new AlertDialog.Builder(this)
                .setMessage("Une erreur est survenue")
                .setCancelable(false)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                })
                .create()
                .show();*/
    }


    private void findViewByIds() {
        pgbLoading = findViewById(R.id.splash_activity_pgb_loading);
        tvInfoConfigs = findViewById(R.id.splash_activity_tv_info_configuration);
        tvInfoConfigs.setText("Configuration en cours...");
    }


    private void fetchDistricts() {
        pgbLoading.setVisibility(View.VISIBLE);
        //tvInfoConfigs.setVisibility(View.VISIBLE);

        apiServices.getDistricts("" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<District>>() {
                    @Override
                    public void onSuccess(List<District> districts) {
                        for (District district : districts) {
                            database.districtDao().createDistrict(district);
                        }

                        fetchCentres();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });
    }


    private void fetchCentres() {
        //tvInfoConfigs.setText(getString(R.string.sites_configuration));
        //tvInfoConfigs.setVisibility(View.VISIBLE);

        apiServices.getCentres("" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Centre>>() {
                    @Override
                    public void onSuccess(List<Centre> centres) {
                        for (Centre centre : centres) {
                            database.centreDao().createCentre(centre);
                        }

                        fetchVaccins();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });
    }


    private void fetchVaccins() {
        // tvInfoConfigs.setText(getString(R.string.sites_configuration));
        // tvInfoConfigs.setVisibility(View.VISIBLE);

        apiServices.getVaccins("" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Vaccin>>() {
                    @Override
                    public void onSuccess(List<Vaccin> vaccins) {
                        for (Vaccin vaccin : vaccins) {
                            vaccin.setVaccinChecked("0");
                            database.vaccinDao().createVaccin(vaccin);
                        }

                        fetchVilles();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
        });
    }


    private void fetchVilles() {
        // tvInfoConfigs.setText(getString(R.string.sites_configuration));
        // tvInfoConfigs.setVisibility(View.VISIBLE);

        apiServices.getVilles("" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<VilleModel>>() {
                    @Override
                    public void onSuccess(List<VilleModel> villes) {
                        for (VilleModel ville : villes) {
                            database.villeDao().createVille(ville);
                        }

                        fetchCommunes();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
        });
    }


    private void fetchCommunes() {
        // tvInfoConfigs.setText(getString(R.string.sites_configuration));
        // tvInfoConfigs.setVisibility(View.VISIBLE);

        apiServices.getCommunes("" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<CommuneModel>>() {
                    @Override
                    public void onSuccess(List<CommuneModel> communes) {
                        for (CommuneModel commune : communes) {
                            database.communeDao().createCommune(commune);
                        }

                        fetchQuartiers();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
        });
    }


    private void fetchQuartiers() {
        // tvInfoConfigs.setText(getString(R.string.sites_configuration));
        // tvInfoConfigs.setVisibility(View.VISIBLE);

        apiServices.getQuartiers("" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<QuartierModel>>() {
                    @Override
                    public void onSuccess(List<QuartierModel> quartiers) {
                        for (QuartierModel quartier : quartiers) {
                            database.quartierDao().createQuartier(quartier);
                        }

                        fetchProfessions();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
        });
    }


    private void fetchProfessions() {
        // tvInfoConfigs.setText(getString(R.string.sites_configuration));
        // tvInfoConfigs.setVisibility(View.VISIBLE);

        apiServices.getProfessions("" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<ProfessionModel>>() {
                    @Override
                    public void onSuccess(List<ProfessionModel> professions) {
                        for (ProfessionModel profession : professions) {
                            database.professionDao().createProfession(profession);
                        }

                        fetchEtudes();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
        });
    }


    private void fetchEtudes() {
        // tvInfoConfigs.setText(getString(R.string.sites_configuration));
        // tvInfoConfigs.setVisibility(View.VISIBLE);

        apiServices.getEtudes("" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<NiveauEtudeModel>>() {
                    @Override
                    public void onSuccess(List<NiveauEtudeModel> etudes) {
                        for (NiveauEtudeModel etudeModel : etudes) {
                            database.niveauEtudeDao().createEtude(etudeModel);
                        }

                        fetchClasses();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
        });
    }


    private void fetchClasses() {
        // tvInfoConfigs.setText(getString(R.string.sites_configuration));
        // tvInfoConfigs.setVisibility(View.VISIBLE);

        apiServices.getClasses("" + BuildConfig.DATABASE)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<NiveauClasseModel>>() {
                    @Override
                    public void onSuccess(List<NiveauClasseModel> classes) {
                        for (NiveauClasseModel classeModel : classes) {
                            // vaccin.("0");
                            database.niveauClasseDao().createClasse(classeModel);
                        }

                        CountDownTimer countDownTimer = new CountDownTimer(5000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                                pgbLoading.setVisibility(View.INVISIBLE);
                                tvInfoConfigs.setText("Configuration Ok !");
                            }

                            @Override
                            public void onFinish() {
                                AppController.startNewTopActivity(SplashActivity.this, ConnexionActivity.class);
                                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
                            }
                        }.start();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
        });
    }

}
