package com.gia.myrecu.ui.activities;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.databases.room.AndroidSmsDatabase;
import com.gia.myrecu.models.v1.Abonnement;
import com.gia.myrecu.tools.ImageFilePath;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.tools.Utils;
import com.gia.myrecu.ui.adapters.BluetoothDeviceAdapter;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.*;

@SuppressWarnings("ALL")
public class NewNaissanceActivity extends AppCompatActivity
        implements BluetoothDeviceAdapter.OnClickDeviceBluetooth {


    ProgressDialog dialog;
    AppCompatEditText etNom;
    AppCompatEditText etPrenoms;
    AppCompatEditText etNaissance;
    AppCompatEditText etTelephone;
    AppCompatEditText etNombreAnne;
    AppCompatSpinner spSexe;
    AppCompatSpinner spLangue;

    AppCompatButton btnAbonnement;
    String TAG = "AbonnementActivity";
    final String REGEX = "\\d{8}";
    RadioGroup rgFormule;
    AndroidSmsDatabase database;
    private Connection btConnection;
    private ZebraPrinter zbPrinter;
    ConnectBTDevice connectBTDevice;
    int counPrintTentative;

    AppCompatImageView ivTakePhoto;
    AppCompatImageView ivImportPhoto;

    BluetoothAdapter bluetoothAdapter;
    Set<BluetoothDevice> btDevicesAlreadyBondedList;
    BluetoothDevice bluetoothDevice;
    BluetoothDeviceAdapter bluetoothDeviceAdapter;
    List<BluetoothDevice> deviceList;
    AlertDialog dialogPrinters;

    LinearLayout llPhotoCarnet;
    AppCompatImageView ivPhotoCarnet;

    RecyclerView rvListDevices;
    AppCompatButton btnCancel;

    String carnetPhotoPath;
    File fileGlobal;
    int photoMethod = 1;
    String patientIdStr;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_naissance);

        database = AndroidSmsDatabase.getDatabase(this);

        deviceList = new ArrayList<>();
        btDevicesAlreadyBondedList = new HashSet<>();
        bluetoothDeviceAdapter = new BluetoothDeviceAdapter(this, deviceList);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
            Toast.makeText(this, "Bluetooth not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 2017);
        } else {
            btDevicesAlreadyBondedList = bluetoothAdapter.getBondedDevices();
        }

        if (btDevicesAlreadyBondedList.size() > 0) {
            for (BluetoothDevice pairedDevice : btDevicesAlreadyBondedList) {
                if (pairedDevice.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.IMAGING) {
                    bluetoothDevice = pairedDevice;
                    deviceList.add(bluetoothDevice);
                }
            }
            bluetoothDeviceAdapter.notifyDataSetChanged();
        }

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View printersView = inflater.inflate(R.layout.view_printers_bluetooth_presenter, null);

        rvListDevices = printersView.findViewById(R.id.view_printers_bluetooth_presenter_rv_list_devices);
        btnCancel = printersView.findViewById(R.id.view_printers_bluetooth_presenter_btn_cancel);
        rvListDevices.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvListDevices.setAdapter(bluetoothDeviceAdapter);

        btnCancel.setOnClickListener(v -> {
            etNom.setText(null);
            etPrenoms.setText(null);
            etNaissance.setText(null);
            etTelephone.setText(null);
            btnAbonnement.setEnabled(true);

            dialog.dismiss();

            View viewAddVacuum = LayoutInflater.from(this).inflate(R.layout.dialog_add_vacum_patient, null);
            AppCompatTextView tvQuestionVacuum = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_question_patient_add_vacuum);
            AppCompatTextView tvNon = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_non);
            AppCompatTextView tvOui = viewAddVacuum.findViewById(R.id.dialog_add_vacum_patient_tv_oui);

            tvQuestionVacuum.setText(getString(
                    R.string.txt_question_add_vaccum_visite,
                    "vaccins", SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_NAME, ""))
            );

            AlertDialog.Builder builderAddVacuum = new AlertDialog.Builder(this)
                    .setView(viewAddVacuum);

            tvNon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogPrinters.dismiss();
                    dialogPrinters = null;

                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).clear();
                    finish();
                }
            });

            tvOui.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    dialogPrinters.dismiss();
                    dialogPrinters = null;

                    //SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).clear();

                    /*SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ID, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_ID, "0"));

                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PHOTO_CARNET, SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_PHOTO_CARNET, ""));

                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_NAME, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NAME)));
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_TELEPHONE, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_NUMBER, "00000000")));

                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_BIRTHDAY, StringUtils.defaultString(etNaissance.getText().toString().trim()));
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_EMAIL, "");

                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_EXPIRATION, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_EXPIRATION, "")));

                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_LOGIN, StringUtils.defaultString(SPUtils.getInstance(ABONNE_NEW_PREFERENCES).getString(ABONNE_NEW_LOGIN, ""), "INDEFINI"));
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_SEXE, spSexe.getSelectedItem().toString());*/

                    disconnect();

                    Intent intentAddVacuum = new Intent(NewNaissanceActivity.this, SelectVacinesActivity.class);
                    SPUtils.getInstance(VACCIN_PREFERENCES).put(VACCIN_ADD_TYPE, "newest");
                    startActivity(intentAddVacuum);


                    SPUtils.getInstance(ABONNE_NEW_PREFERENCES).clear();
                    finish();
                }
            });

            dialogPrinters = null;
            dialogPrinters = builderAddVacuum.create();

            dialogPrinters.show();
            //dialogPrinters.dismiss();
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Choisir une imprimante")
                .setView(printersView)
                .setCancelable(false);

        dialogPrinters = builder.create();

        counPrintTentative = 0;
        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).clear();

        dialog = new ProgressDialog(this);
        dialog.setMessage(getResources().getString(R.string.txt_patient_abonnement_wating));
        dialog.setCancelable(false);

        etNom = findViewById(R.id.activity_new_naissance_et_nom_bebe);
        etPrenoms = findViewById(R.id.activity_new_naissance_et_prenom_bebe);
        etNaissance = findViewById(R.id.activity_new_naissance_et_naissance_bebe);
        etTelephone = findViewById(R.id.activity_new_naissance_et_telephone_bebe);
        etNombreAnne = findViewById(R.id.activity_new_naissance_et_nbre_annee);
        spSexe = findViewById(R.id.activity_new_naissance_sp_sexe_bebe);
        spLangue = findViewById(R.id.activity_new_naissance_sp_langue_bebe);
        btnAbonnement = findViewById(R.id.activity_new_naissance_btn_abonne_bebe);
        rgFormule = findViewById(R.id.activity_new_naissance_rg_container_formule);
        ivPhotoCarnet = findViewById(R.id.activity_new_naissance_iv_photo_carnet);
        ivImportPhoto = findViewById(R.id.activity_new_naissance_iv_import_photo);
        ivTakePhoto = findViewById(R.id.activity_new_naissance_iv_take_photo);

        etTelephone.setText(etTelephone.getText().toString().concat(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_TELEPHONE)));

        patientIdStr = SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ID, "0");

        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_PARENT_ID, patientIdStr);

        if (rgFormule.getCheckedRadioButtonId() == R.id.activity_new_naissance_rb_free_formule) {
            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE, 12);
            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE_NAME, "GRATUIT");
        }

        spLangue.setSelection(2);
        spLangue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_LANGUE, parent.getSelectedItem().toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spSexe.setSelection(1);

        spSexe.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 0) {
                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_SEXE, "M");
                } else {
                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_SEXE, "F");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnAbonnement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnAbonnement.setEnabled(false);

                if (etNaissance.getText().toString().matches(REGEX)) {
                    etNaissance.setTextColor(getResources().getColor(R.color.colorPrimaryTextBlack));
                    if (!StringUtils.isBlank(etNom.getText()) && !StringUtils.isBlank(etPrenoms.getText())
                            && !StringUtils.isBlank(etNaissance.getText()) && !StringUtils.isBlank(etTelephone.getText())) {
                        new AbonnementEngine().execute();
                    } else {
                        Toaster.toast("Remplissez correctement les champs");
                        btnAbonnement.setEnabled(true);
                    }
                } else {
                    etNaissance.setTextColor(getResources().getColor(android.R.color.holo_red_light));
                    Toaster.toast("Mauvaise date de naissance");
                    btnAbonnement.setEnabled(true);
                }
            }
        });

        ivTakePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoMethod = 1;
                dispatchTakePictureIntent();
            }
        });

        ivImportPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                photoMethod = 2;
                showFileChooser(1);
            }
        });

        etNombreAnne.setText("1");
    }


    @Override
    public void onDeviceClicked(BluetoothDevice bluetoothDevice, int position) {
        if (dialogPrinters.isShowing()) {
            dialogPrinters.dismiss();
        }

        // CONNECT TO PRINTER
        connectBTDevice = new ConnectBTDevice(bluetoothDevice);
        connectBTDevice.execute();
    }


    class ConnectBTDevice extends AsyncTask<Object, Object, ZebraPrinter> {

        BluetoothDevice device;

        ConnectBTDevice(BluetoothDevice device) {
            this.device = device;
        }

        @Override
        protected void onPreExecute() {
            if (dialog != null) {
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            }
            counPrintTentative = counPrintTentative + 1;
        }

        @Override
        protected ZebraPrinter doInBackground(Object[] objects) {
            btConnection = new BluetoothConnection(device.getAddress());
            try {
                btConnection.open();
            } catch (Exception e) {
                e.printStackTrace();
                if (e instanceof ConnectionException) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            connectBTDevice.cancel(true);
                            connectBTDevice = null;
                            if (counPrintTentative > 3) {
                                finish();
                            }
                            connectBTDevice = new ConnectBTDevice(device);
                            connectBTDevice.execute();
                        }
                    });
                }
            }

            ZebraPrinter zebraPrinter = null;

            if (btConnection.isConnected()) {
                try {
                    zebraPrinter = ZebraPrinterFactory.getInstance(btConnection);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return zebraPrinter;
        }


        @Override
        protected void onPostExecute(ZebraPrinter zebraPrinter) {
            zbPrinter = zebraPrinter;
            try {
                if (zbPrinter != null) {
                    btConnection.write(getConfigLabel(
                            SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME, ""),
                            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_HOUR, ""),
                            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_DATE, ""),
                            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_RECEIPT, ""),
                            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_AVAILABLE, ""),
                            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_NAME, ""),
                            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_LOGIN, ""),
                            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_NUMBER, ""),
                            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getInt(NEW_NAISSANCE_AMOUNT, 0),
                            SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_FORMULE_NAME, ""),
                            SPUtils.getInstance(AGENT_PREFERENCES).getString(AGENT_NAME, "")
                    ));

                    etNom.setText(null);
                    etPrenoms.setText(null);
                    etNaissance.setText(null);
                    etTelephone.setText(null);
                    btnAbonnement.setEnabled(true);

                    dialog.dismiss();

                    dialogPrinters.show();

                }
            } catch (Exception e) {
                if (dialog != null) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                e.printStackTrace();
            }
        }
    }


    public void onFormuleClicked(View view) {
        // Is the button now checked?
        boolean checked = ((AppCompatRadioButton) view).isChecked();

        // Check which radio button was clicked
        switch (view.getId()) {
            case R.id.activity_new_naissance_rb_premuim_formule:
                if (checked) {
                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE, 2);
                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE_NAME, "PREMUIM");
                }
                break;

            case R.id.activity_new_naissance_rb_privilege_formule:
                if (checked) {
                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE, 3);
                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE_NAME, "PRIVILEGE");
                }
                break;

            case R.id.activity_new_naissance_rb_free_formule:
                if (checked) {
                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE, 12);
                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE_NAME, "GRATUIT");
                }
                break;

            default:
                if (checked) {
                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE, 1);
                    SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE_NAME, "STANDARD");
                }
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {
            if (photoMethod == 1) {
                setPic();
            } else {
                Bundle extras = data.getExtras();
                setPic(data.getData());
            }
        }
    }


    private void setPic(Uri uriTraitment) {
        // Get the dimensions of the View
        int targetW = ivPhotoCarnet.getWidth();
        int targetH = ivPhotoCarnet.getHeight();


        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        carnetPhotoPath = ImageFilePath.getPath(this, uriTraitment);

        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoTakingW = bmOptions.outWidth;
        int photoTakingH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleTakingFactor = Math.min(photoTakingW / targetW, photoTakingH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleTakingFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        ivPhotoCarnet.setPadding(5, 5, 5, 5);
        ivPhotoCarnet.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    class AbonnementEngine extends AsyncTask<String, Void, Void> {

        String nom;
        String prenoms;
        String dateNaissance;
        String telephone;
        String nbreAnne;

        AbonnementEngine() {
            this.nom = etNom.getText().toString();
            this.prenoms = etPrenoms.getText().toString();
            this.dateNaissance = etNaissance.getText().toString();
            this.telephone = etTelephone.getText().toString();
            this.nbreAnne = etNombreAnne.getText().toString();
        }

        @Override
        protected void onPreExecute() {
            if (dialog != null) {
                dialog.show();
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build();

                // 18Z3579
                LogUtils.e("Formule: " + String.valueOf(SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getInt(NEW_NAISSANCE_FORMULE)));

                RequestBody formBody = new FormBody.Builder()
                        .add("ctrId", String.valueOf(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID)))
                        .add("usrId", String.valueOf(SPUtils.getInstance(AGENT_PREFERENCES).getInt(AGENT_ID)))
                        .add("nPat", nom)
                        .add("pnPat", prenoms)
                        .add("dtPat", StringUtils.reverseDelimited(Utils.formatDate(dateNaissance), '-'))
                        .add("sxPat", SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_SEXE))
                        .add("lngPat", SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_LANGUE))
                        .add("tPat", telephone)
                        .add("dur", nbreAnne)
                        .add("patId", SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_PARENT_ID, "0"))
                        .add("photoCarnet", SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(ABONNE_PHOTO_CARNET, ""))
                        .add("fm", String.valueOf(SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getInt(NEW_NAISSANCE_FORMULE)))
                        .add("d", BuildConfig.DATABASE)
                        .build();

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.ABONNEMENT_NEW_PATIENT)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();
                String reponse = clientResponse.body().string();

                LogUtils.e("Response: " + reponse);

                if (clientResponse.isSuccessful()) {
                    final JSONObject json = new JSONObject(reponse);
                    String statut = json.getString("statut");

                    if (statut.equals("1")) {
                        //final String patId = json.getString("patId");
                        final String numRecu = json.getString("nuRecu");
                        final String patId = json.getString("idpat");
                        final String login = json.getString("login");
                        final String heure = DateTime.now().toString("HH:mm");
                        final String day = DateTime.now().toString("dd-MM-yyyy");
                        String dateExp = json.getString("dtExp");
                        DateTime date = new DateTime(dateExp);

                        final String abonnementDate = date.toString("dd MMMM yyyy", Locale.CANADA_FRENCH);
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_LOGIN, login);
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_ID, patId);
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_RECEIPT, numRecu);
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_AVAILABLE, abonnementDate);
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_EXPIRATION, dateExp);
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_HOUR, heure);
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_DATE, day);
                        Abonnement abonnement = new Abonnement();
                        abonnement.setAbonnementCentre(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME));
                        abonnement.setAbonnementRecu(numRecu);
                        abonnement.setAbonnementValidite(abonnementDate);
                        abonnement.setAbonnementClientNom(nom.concat(" ").concat(prenoms));
                        abonnement.setAbonnementClientNumero(telephone);
                        abonnement.setAbonnementMontant(json.getInt("mt"));
                        abonnement.setAbonnementFormule(SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).getString(NEW_NAISSANCE_FORMULE_NAME));
                        abonnement.setAbonnementType("ABONNEMENT");

                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_NUMBER, abonnement.getAbonnementClientNumero());
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_NAME, abonnement.getAbonnementClientNom());
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_AMOUNT, abonnement.getAbonnementMontant());
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(NEW_NAISSANCE_FORMULE_NAME, abonnement.getAbonnementFormule());
                        SPUtils.getInstance().put(PATIENT_AGE_STATUS, Utils.detectAgeStatus(Utils.formatDate(dateNaissance)).toUpperCase());

                        abonnement.setAbonnementDate(DateTime.now().toString("yyyy-MM-dd"));
                        database.abonnementDao().createAbonnement(abonnement);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                                dialog.setMessage("Impression du recu en cours...");
                                dialogPrinters.show();
                            }
                        });

                    } else {
                        String message = json.getString("message");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnAbonnement.setEnabled(true);
                                dialog.dismiss();
                            }
                        });
                        Toaster.toastLong(message);
                    }
                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnAbonnement.setEnabled(true);
                            dialog.dismiss();
                        }
                    });
                    Toaster.toastLong("Echec abonnement.");
                }
            } catch (final Exception ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if (ex instanceof SocketTimeoutException || ex instanceof UnknownHostException) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(NewNaissanceActivity.this)
                                    .setCancelable(false)
                                    .setMessage("Un problème est survénue. Veuillez réessayer.")
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                            builder.create().show();
                        }

                        btnAbonnement.setEnabled(true);
                        dialog.dismiss();
                    }
                });
                ex.printStackTrace();
                Toaster.toastLong("Echec abonnement.");
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            btnAbonnement.setEnabled(true);
            if (dialog != null) {
                if (dialog.isShowing()) {
                    dialog.dismiss();
                }
            }
        }
    }


    @Override
    protected void onDestroy() {
        disconnect();
        super.onDestroy();
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }


    private byte[] getConfigLabel(String centre, String hour, String date, String recu, String valid,
                                  String abonne, String login, String telephone, int montant,
                                  String formule, String agent) {
        byte[] configLabel = null;

        String cpclConfigLabel =
                "! 0 200 200 690 1\r\n"
                        + "CENTER\r\n"
                        + "PCX 0 0 !<opis.pcx\r\n"
                        + "ML 25\r\n"
                        + "T 5 0 10 150\r\n"
                        + "CARNET DE VACCINATION\r\n"
                        + "ELECTRONIQUE\r\n"
                        + "ENDML\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 220 CENTRE : " + centre + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 250 HEURE : " + hour + "\r\n"
                        + "RIGHT\r\n"
                        + "T 0 2 0 250 DATE : " + date + "\r\n"
                        + "CENTER\r\n"
                        + "T 0 3 0 300 ABONNEMENT\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 340 RECU : " + recu + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 370 VALIDITE : " + valid + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 400 NOM : " + abonne + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 430 LOGIN : " + login + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 460 TELEPHONE : " + telephone + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 490 MONTANT : " + montant + " FCFA\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 520 TYPE FORMULE : " + formule + "\r\n"
                        + "CENTER\r\n"
                        + "T 0 2 0 560 www.opisms.org / 21-24-34-89\r\n"
                        + "CENTER\r\n"
                        + "T 0 2 0 585 NUM.VERT : 143 (APPEL GRATUIT)\r\n"
                        + "RIGHT\r\n"
                        + "T 0 2 0 615 AGENT : " + agent + "\r\n"
                        + "PRINT\r\n";
        configLabel = cpclConfigLabel.getBytes();
        return configLabel;
    }


    public void disconnect() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    //Log.e("MainActivity", "Disconnecting Printer");
                    if (btConnection != null) {
                        btConnection.close();
                        zbPrinter = null;
                        btConnection = null;
                    }
                    //Log.e("MainActivity", "Printer Disconnected");
                } catch (ConnectionException e) {
                    e.printStackTrace();
                    //Log.e("MainActivity", "COMM Error! Disconnected");
                }
            }
        }).start();
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File carnetFile = null;
            try {
                carnetFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (carnetFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this,
                        "com.gia.myrecu.fileprovider",
                        carnetFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        carnetPhotoPath = image.getAbsolutePath();
        return image;
    }


    private void setPic() {
        // Get the dimensions of the View
        int targetW = ivPhotoCarnet.getWidth();
        int targetH = ivPhotoCarnet.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        ivPhotoCarnet.setPadding(5, 5, 5, 5);
        ivPhotoCarnet.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    private void createImageFileCompressed() {

        File compressedFile = null;
        try {
            fileGlobal = new File(carnetPhotoPath);
            compressedFile = new Compressor(this)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(fileGlobal);
            if (FileUtils.isFileExists(compressedFile)) {
                final File finalCompressedFile = compressedFile;

                FileUtils.copy(compressedFile, fileGlobal, new FileUtils.OnReplaceListener() {
                    @Override
                    public boolean onReplace(File srcFile, File destFile) {
                        //Toaster.toast("File remplace");
                        SPUtils.getInstance(NEW_NAISSANCE_PREFERENCES).put(ABONNE_PHOTO_CARNET, encodeFileToBase64Binary(finalCompressedFile));
                        return true;
                    }
                });
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String encodeFileToBase64Binary(File file) {
        String encodedBase64 = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int) file.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedBase64;
    }


    private void showFileChooser(int pView) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent, "Choisir une photo"), pView);
        }
    }
}
