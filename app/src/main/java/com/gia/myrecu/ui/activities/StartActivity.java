package com.gia.myrecu.ui.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.databases.room.AndroidSmsDatabase;
import com.gia.myrecu.models.v1.Centre;
import com.gia.myrecu.models.v1.District;
import com.gia.myrecu.models.v1.Vaccin;
import com.gia.myrecu.models.v2.Carnet;
import com.gia.myrecu.models.v2.CarnetRequest;
import com.gia.myrecu.models.v2.CarnetResponse;
import com.gia.myrecu.network.OPIApiClient;
import com.gia.myrecu.network.OPIApiServices;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.ui.adapters.BluetoothDeviceAdapter;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static com.gia.myrecu.tools.Constants.AGENT_LOAD_CARNET;
import static com.gia.myrecu.tools.Constants.AGENT_NAME;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;
import static com.gia.myrecu.tools.Constants.CENTRE_ID;
import static com.gia.myrecu.tools.Constants.CENTRE_NAME;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;


@SuppressWarnings("ALL")
public class StartActivity extends AppCompatActivity implements BluetoothDeviceAdapter.OnClickDeviceBluetooth {


    LinearLayout llSearchPatient;
    LinearLayout llAbonnement;
    LinearLayout llSearchNfc;
    LinearLayout llPointDay;
    LinearLayout llNaissanceDaily;
    LinearLayout llDeathDaily;
    LinearLayout llRendezVousDaily;
    LinearLayout llGlobalPoint;
    LinearLayout llRapportAntigene;

    AppCompatImageView imageRefresh;

    AppCompatTextView tvCentre;
    AppCompatTextView tvAgent;
    AppCompatTextView tvVersion;

    private Connection btConnection;
    private ZebraPrinter zbPrinter;

    ConnectBTDevice connectBTDevice;
    int counPrintTentative;

    BluetoothAdapter bluetoothAdapter;
    Set<BluetoothDevice> btDevicesAlreadyBondedList;
    BluetoothDevice bluetoothDevice;
    BluetoothDeviceAdapter bluetoothDeviceAdapter;
    List<BluetoothDevice> deviceList;
    AlertDialog dialogPrinters;
    AndroidSmsDatabase database;
    RecyclerView rvListDevices;
    ProgressDialog dialog;
    int toNbreAbn;
    int toNbreReab;
    int toAmountAbnt;
    int toAmountReab;
    int toAmountGlobal;
    AlertDialog dialogAskBluetooth;
    private OPIApiServices apiServices;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);

        String version = "";
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        version = BuildConfig.VERSION;

        llAbonnement = findViewById(R.id.activity_start_ll_abonnement);
        llSearchPatient = findViewById(R.id.activity_start_ll_search_abonne);
        llSearchNfc = findViewById(R.id.activity_start_ll_create_badge);
        llPointDay = findViewById(R.id.activity_start_ll_point);
        llRendezVousDaily = findViewById(R.id.activity_start_ll_rdv);
        llRapportAntigene = findViewById(R.id.linear_antigene_rapport);
        llNaissanceDaily = findViewById(R.id.activity_start_ll_new_borth_dailies);
        llDeathDaily = findViewById(R.id.activity_start_ll_missed);
        tvAgent = findViewById(R.id.activity_start_tv_agent_name);
        tvCentre = findViewById(R.id.activity_start_tv_agent_center);
        llGlobalPoint = findViewById(R.id.activity_start_ll_global_point);
        tvVersion = findViewById(R.id.activity_start_tb_title_version);
        imageRefresh = findViewById(R.id.image_refresh);


        //final NfcAdapter nfcAdapter = NfcAdapter.getDefaultAdapter(StartActivity.this);
        tvVersion.setText(version);

        tvCentre.setText("CENTRE: " + SPUtils.getInstance(Constants.DISTRICT_CENTRE_PREFERENCES).getString(Constants.CENTRE_NAME, ""));
        tvAgent.setText("AGENT: " + SPUtils.getInstance(Constants.AGENT_PREFERENCES).getString(Constants.AGENT_NAME, ""));

        setSupportActionBar((Toolbar) findViewById(R.id.activity_start_tb_title));
        database = AndroidSmsDatabase.getDatabase(this);

        llAbonnement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, AbonnementActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });

        llSearchPatient.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(StartActivity.this, SearchActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });

        llPointDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Compute point

                /*if (database.abonnementDao().getAbonnementsList().size() > 0) {
                    toNbreAbn = database.abonnementDao().totalTypeDay(DateTime.now().toString("yyyy-MM-dd"), "ABONNEMENT");
                    toNbreReab = database.abonnementDao().totalTypeDay(DateTime.now().toString("yyyy-MM-dd"), "REABONNEMENT");
                    toAmountAbnt = database.abonnementDao().totalAmountTypeDay(DateTime.now().toString("yyyy-MM-dd"), "ABONNEMENT");
                    toAmountReab = database.abonnementDao().totalAmountTypeDay(DateTime.now().toString("yyyy-MM-dd"), "REABONNEMENT");
                    toAmountGlobal = database.abonnementDao().totalAmountGlobalDay(DateTime.now().toString("yyyy-MM-dd"));

                    if (dialogPrinters != null) {
                        dialogPrinters.show();
                    }
                } else {
                    Toaster.toastLong("PAS DE POINT DISPONIBLE POUR LE MOMENT.");
                }*/

                ActivityUtils.startActivity(AgentPatientsFinJourActivity.class);
            }
        });

        llNaissanceDaily.setOnClickListener(view -> {
            ActivityUtils.startActivity(BirthdaysDailyActivity.class, R.anim.slide_from_right, R.anim.slide_to_left);
        });

        llDeathDaily.setOnClickListener(view -> {
            ActivityUtils.startActivity(PerduDeVueActivity.class, R.anim.slide_from_right, R.anim.slide_to_left);
        });

        llGlobalPoint.setOnClickListener(view -> {
            ActivityUtils.startActivity(GlobalCentrePointActivity.class, R.anim.slide_from_right, R.anim.slide_to_left);
        });

        llRendezVousDaily.setOnClickListener(view -> {
            ActivityUtils.startActivity(AgentRendezVousDailyActivity.class, R.anim.slide_from_right, R.anim.slide_to_left);
        });

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.CALL_PHONE}, 1010);
        }

        tvVersion.setOnClickListener(v -> {
            ActivityUtils.startActivity(ConfigsActivity.class, R.anim.slide_from_left, R.anim.slide_to_right);
        });

        dialogAskBluetooth = new AlertDialog.Builder(this)
                .setMessage("Voulez-vous activer le Bluetooth ?")
                .setPositiveButton("Oui", (dialog, which) -> {
                    if (!bluetoothAdapter.isEnabled()) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, 2017);
                        ToastUtils.setGravity(Gravity.CENTER, 0, 0);
                        ToastUtils.showShort("Veuillez activer le bluetooth s'il vous plait.");
                    } else {
                        btDevicesAlreadyBondedList = bluetoothAdapter.getBondedDevices();
                    }

                    if (btDevicesAlreadyBondedList.size() > 0) {
                        // Al ready devices bonded
                        for (BluetoothDevice pairedDevice : btDevicesAlreadyBondedList) {
                            if (pairedDevice.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.IMAGING) {
                                bluetoothDevice = pairedDevice;
                                deviceList.add(bluetoothDevice);
                            }
                        }
                        bluetoothDeviceAdapter.notifyDataSetChanged();

                        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
                        View printersView = inflater.inflate(R.layout.view_printers_bluetooth_presenter, null);

                        rvListDevices = printersView.findViewById(R.id.view_printers_bluetooth_presenter_rv_list_devices);
                        rvListDevices.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
                        rvListDevices.setAdapter(bluetoothDeviceAdapter);

                        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                                .setTitle("Choisir une imprimante")
                                .setView(printersView)
                                .setCancelable(false);
                        dialogPrinters = builder.create();
                    }
                    SPUtils.getInstance(Constants.PHONE_PREFERENCES).put(Constants.PHONE_BLUETOOTH_QUESTION_ALREADY_ASKED, true);
                    SPUtils.getInstance(Constants.PHONE_PREFERENCES).put(Constants.PHONE_PRINT, "YES");
                })
                .setNegativeButton("Non", (dialog, which) -> {
                    SPUtils.getInstance(Constants.PHONE_PREFERENCES).put(Constants.PHONE_BLUETOOTH_QUESTION_ALREADY_ASKED, true);
                    SPUtils.getInstance(Constants.PHONE_PREFERENCES).put(Constants.PHONE_PRINT, "NO");
                    dialog.dismiss();
                })
                .setCancelable(false)
                .create();

        //throw new RuntimeException("Erreur generer");
        counPrintTentative = 0;

        deviceList = new ArrayList<>();
        bluetoothDeviceAdapter = new BluetoothDeviceAdapter(this, deviceList);

        if (getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
            //return;
            bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

            if (!SPUtils.getInstance(Constants.PHONE_PREFERENCES).getBoolean(Constants.PHONE_BLUETOOTH_QUESTION_ALREADY_ASKED))
                dialogAskBluetooth.show();
        } else {
            Toast.makeText(this, "Bluetooth not supported", Toast.LENGTH_SHORT).show();
        }

        llRapportAntigene.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityUtils.startActivity(RapportAntigeneActivity.class, R.anim.slide_from_right, R.anim.slide_to_left);
            }
        });

        imageRefresh.setOnClickListener(v -> {
            fetchDistricts();
        });

        if (!SPUtils.getInstance(AGENT_PREFERENCES).getBoolean(AGENT_LOAD_CARNET, false)) {
            fetchCarnetsCentre();
        }
    }


    @Override
    public void onDeviceClicked(BluetoothDevice bluetoothDevice, int position) {
        if (dialogPrinters.isShowing()) {
            dialogPrinters.dismiss();
        }

        // CONNECT TO PRINTER
        connectBTDevice = new ConnectBTDevice(bluetoothDevice);
        connectBTDevice.execute();
    }


    @Override
    protected void onResume() {
        super.onResume();
        SPUtils.getInstance(Constants.ABONNE_OLD_PREFERENCES).clear();
        SPUtils.getInstance(Constants.ABONNE_NEW_PREFERENCES).clear();
    }


    @SuppressLint("StaticFieldLeak")
    class ConnectBTDevice extends AsyncTask<Object, Object, ZebraPrinter> {

        BluetoothDevice device;

        ConnectBTDevice(BluetoothDevice device) {
            this.device = device;
        }

        @Override
        protected void onPreExecute() {
            if (dialog != null) {
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            } else {
                dialog = new ProgressDialog(StartActivity.this);
                dialog.setMessage("Impression du point...");
                dialog.setCancelable(false);
                dialog.show();
            }
            counPrintTentative = counPrintTentative + 1;
        }

        @Override
        protected ZebraPrinter doInBackground(Object[] objects) {
            btConnection = new BluetoothConnection(device.getAddress());
            try {
                btConnection.open();
            } catch (Exception e) {
                e.printStackTrace();
                if (e instanceof ConnectionException) {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            connectBTDevice.cancel(true);
                            connectBTDevice = null;
                            if (counPrintTentative > 3) {
                                onBackPressed();
                            }
                            connectBTDevice = new ConnectBTDevice(device);
                            connectBTDevice.execute();
                        }
                    });
                }
            }

            ZebraPrinter zebraPrinter = null;

            if (btConnection.isConnected()) {
                try {
                    zebraPrinter = ZebraPrinterFactory.getInstance(btConnection);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return zebraPrinter;
        }

        @Override
        protected void onPostExecute(ZebraPrinter zebraPrinter) {
            zbPrinter = zebraPrinter;
            String date = DateTime.now().toString("dd-MM-yyyy");
            String heure = DateTime.now().toString("HH:mm");
            try {
                if (zbPrinter != null) {
                    btConnection.write(getConfigLabel(
                            SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME, ""),
                            heure, date, toNbreAbn, toAmountAbnt, toNbreReab, toAmountReab, toAmountGlobal,
                            SPUtils.getInstance(AGENT_PREFERENCES).getString(AGENT_NAME, "")
                    ));

                    llAbonnement.setEnabled(true);
                    dialog.dismiss();

                    dialogPrinters.dismiss();

                    disconnect();
                }
            } catch (Exception e) {
                if (dialog != null) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                e.printStackTrace();
            }
        }
    }


    private byte[] getConfigLabel(String centre, String hour, String date, int nbreAbont,
                                  int totalAmountAbont, int nbreReabnt, int totalAmountReabnt, int globalTotal,
                                  String agent) {
        byte[] configLabel = null;

        String cpclConfigLabel =
                "! 0 200 200 890 1\r\n"
                        + "CENTER\r\n"
                        + "PCX 0 0 !<opis.pcx\r\n"
                        + "ML 25\r\n"
                        + "T 5 0 10 150\r\n"
                        + "CARNET DE VACCINATION\r\n"
                        + "ELECTRONIQUE\r\n"
                        + "ENDML\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 220 CENTRE : " + centre + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 250 HEURE : " + hour + "\r\n"
                        + "RIGHT\r\n"
                        + "T 0 2 0 250 DATE : " + date + "\r\n"
                        + "CENTER\r\n"
                        + "T 0 3 0 300 POINT DU JOUR\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 340 ABONNEMENT \r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 370 NOMBRE : " + nbreAbont + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 400 PRIX UNITAIRE : " + 1000 + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 430 TOTAL ------> : " + totalAmountAbont + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 460  __________ \r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 490 REABONNEMENT \r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 520 NOMBRE : " + nbreReabnt + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 550 PRIX UNITAIRE : " + 1000 + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 580 TOTAL ------> : " + totalAmountReabnt + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 610 _________________________________\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 640 TOTAUX ------------> " + globalTotal + "\r\n"
                        + "CENTER\r\n"
                        + "T 0 2 0 685 www.opisms.org / 21-24-34-89\r\n"
                        + "CENTER\r\n"
                        + "T 0 2 0 715 NUM.VERT : 143 (APPEL GRATUIT)\r\n"
                        + "RIGHT\r\n"
                        + "T 0 2 0 765 AGENT : " + agent + "\r\n"
                        + "PRINT\r\n";
        configLabel = cpclConfigLabel.getBytes();
        return configLabel;
    }


    public void disconnect() {
        try {
            //Log.e("MainActivity", "Disconnecting Printer");
            if (btConnection != null) {
                btConnection.close();
                zbPrinter = null;
                btConnection = null;
            }
        } catch (ConnectionException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }


    private void fetchDistricts() {
        database.districtDao().deleteDatas();

        if (dialog != null) {
            dialog.dismiss();
            dialog.setMessage("Mise à jour des données en cours...");
            dialog.setCancelable(false);
        } else {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Mise à jour des données en cours...");
            dialog.setCancelable(false);
        }

        dialog.show();

        apiServices = OPIApiClient.getClient(this).create(OPIApiServices.class);

        apiServices.getDistricts("default")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<District>>() {
                    @Override
                    public void onSuccess(List<District> districts) {
                        for (District district : districts) {
                            database.districtDao().createDistrict(district);
                        }

                        fetchCentres();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });
    }


    private void fetchCentres() {
        database.districtDao().deleteDatas();

        apiServices.getCentres("default")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Centre>>() {
                    @Override
                    public void onSuccess(List<Centre> centres) {
                        for (Centre centre : centres) {
                            database.centreDao().createCentre(centre);
                        }

                        fetchVaccins();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });
    }


    private void fetchVaccins() {
        database.districtDao().deleteDatas();

        apiServices.getVaccins("default")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<List<Vaccin>>() {
                    @Override
                    public void onSuccess(List<Vaccin> vaccins) {
                        for (Vaccin vaccin : vaccins) {
                            vaccin.setVaccinChecked("0");
                            database.vaccinDao().createVaccin(vaccin);
                        }

                        fetchCarnetsCentre();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }
                });
    }


    void fetchCarnetsCentre() {
        if (dialog != null) {
            dialog.dismiss();
            dialog.setMessage("Mise à jour des données en cours...");
            dialog.setCancelable(false);
        } else {
            dialog = new ProgressDialog(this);
            dialog.setMessage("Mise à jour des données en cours...");
            dialog.setCancelable(false);
        }

        dialog.show();

        OPIApiServices apiServicesCarnet = OPIApiClient.getClient(this, true).create(OPIApiServices.class);

        CarnetRequest carnetRequest = new CarnetRequest();
        carnetRequest.setCentreId(String.valueOf(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID, 0)));
        carnetRequest.setTransacId("124578911115");

        apiServicesCarnet.getCarnetsCentre(carnetRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<CarnetResponse>() {
                    @Override
                    public void onSuccess(CarnetResponse carnetResponse) {
                        LogUtils.e("Data -> " + GsonUtils.toJson(carnetResponse));

                        if (carnetResponse.getStatut() != 0) {
                            LogUtils.e("DON FOR REGISTRE");

                            for (Carnet crt : carnetResponse.getData()) {
                                database.carnetDao().createCarnet(crt);
                            }

                            SPUtils.getInstance(AGENT_PREFERENCES).put(AGENT_LOAD_CARNET, true);
                        } else {
                            LogUtils.e("DENIED FOR REGISTRE");
                            ToastUtils.showLong("Ce centre n'a pas de carnet affecté !");
                        }

                        CountDownTimer countDownTimer = new CountDownTimer(5000, 1000) {
                            @Override
                            public void onTick(long millisUntilFinished) {
                            }

                            @Override
                            public void onFinish() {
                                dialog.dismiss();
                                ToastUtils.showShort("Mise à jour terminée !");
                            }
                        }.start();
                    }

                    @Override
                    public void onError(Throwable e) {
                        LogUtils.e(e);
                    }
                });
    }
}
