package com.gia.myrecu.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import androidx.core.content.FileProvider;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.blankj.utilcode.util.FileUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.bumptech.glide.Glide;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v1.UpdateInfosPatient;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.tools.ImageFilePath;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.tools.Utils;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.Weeks;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import id.zelory.compressor.Compressor;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_TYPE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_BIRTHDAY;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_EMAIL;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PHOTO_CARNET;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_AGE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREGNANCY_STATUS;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_SEXE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_TELEPHONE;
import static com.gia.myrecu.tools.Constants.VACCIN_CARNET_COMPRESSED;
import static com.gia.myrecu.tools.Constants.VACCIN_PREFERENCES;



@SuppressWarnings("ALL")
public class UpdateInfosPersActivity extends AppCompatActivity {


    AppCompatEditText etNom;
    AppCompatEditText etPrenoms;
    AppCompatEditText etNaissance;
    AppCompatEditText etTelephone;
    AppCompatEditText etEmail;

    AppCompatTextView tvFormule;

    RadioGroup rgSexeContainer;
    AppCompatRadioButton rbHomme;
    AppCompatRadioButton rbFemme;

    AppCompatImageView ivTakePhoto;
    AppCompatImageView ivImportPhoto;

    AppCompatImageView ivCarnetPhoto;

    String carnetPhotoPath;
    File fileGlobal;
    int photoMethod = 1;

    AppCompatButton btnUpdate;
    UpdateInfosPatient updateInfosPatient;
    ProgressDialog progressDialog;
    final String REGEX = "\\d{2}-\\d{2}-\\d{4}";
    String dateFormat;
    String sexeChecked = "";
    LinearLayout llPregContainer;
    AppCompatTextView tvPregDateEnd;
    AppCompatEditText etAgePreg;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_infos_pers);

        try {
            progressDialog = new ProgressDialog(UpdateInfosPersActivity.this);
            progressDialog.setMessage("Mise en cours...");
            progressDialog.setCancelable(false);

            tvFormule = findViewById(R.id.activity_update_infos_pers_tv_formule);
            etNom = findViewById(R.id.activity_update_infos_pers_et_nom_patient);
            etPrenoms = findViewById(R.id.activity_update_infos_pers_et_prenoms_patient);
            llPregContainer = findViewById(R.id.activity_update_infos_pers_preg_container);
            etNaissance = findViewById(R.id.activity_update_infos_pers_et_naissance_patient);
            etTelephone = findViewById(R.id.activity_update_infos_pers_et_telephone_patient);
            etEmail = findViewById(R.id.activity_update_infos_pers_et_email_patient);
            etAgePreg = findViewById(R.id.activity_update_infos_pers_et_preg_age);
            tvPregDateEnd = findViewById(R.id.activity_update_infos_pers_tv_preg_date_end);
            btnUpdate = findViewById(R.id.activity_update_infos_pers_btn_update_patient);
            ivCarnetPhoto = findViewById(R.id.activity_update_infos_pers_iv_photo_carnet);
            ivImportPhoto = findViewById(R.id.activity_update_infos_pers_iv_import_photo);
            ivTakePhoto = findViewById(R.id.activity_update_infos_pers_iv_take_photo);
            rbFemme = findViewById(R.id.activity_update_infos_pers_rb_sexe_femme);
            rbHomme = findViewById(R.id.activity_update_infos_pers_rb_sexe_homme);
            rgSexeContainer = findViewById(R.id.activity_update_infos_pers_rg_sexe_container);


            if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME).length() > 2) {
                String[] nomPrenoms = StringUtils.split(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME), " ", 2);

                etNom.setText(nomPrenoms[0]);
                etPrenoms.setText(nomPrenoms[1]);
                etNaissance.setText(String.valueOf(StringUtils.getDigits(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                        .getString(ABONNE_OLD_BIRTHDAY))));
                etTelephone.setText(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                        .getString(ABONNE_OLD_TELEPHONE));
                etEmail.setText(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                        .getString(ABONNE_OLD_EMAIL));

                if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("1"))
                    tvFormule.setText(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                        .getString(ABONNE_OLD_FORMULE_NAME));

                if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("2"))
                    tvFormule.setText("PLANNING FAMILIAL");

                if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("3"))
                    tvFormule.setText("GROSSESSE");

                if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_SEXE, "").equals("M")) {
                    rbHomme.setChecked(true);
                    sexeChecked = "M";
                } else {
                    rbFemme.setChecked(true);
                    sexeChecked = "F";
                }

                if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(Constants.ABONNE_OLD_PREGNANCY_STATUS, "FEMME").equals("FEMME ENCEINTE")) {
                    llPregContainer.setVisibility(View.VISIBLE);

                    // Compute pregnant age
                    tvPregDateEnd.setText(StringUtils.reverseDelimited(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(Constants.ABONNE_OLD_PREGNANCY_AGE_END), '-'));


                    String interStart = SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(Constants.ABONNE_OLD_PREGNANCY_AGE_BEGIN, "1970-01-01");

                    DateTime dateTimeStart = DateTime.parse(interStart);
                    DateTime dateTimeEnd = DateTime.now();
                    etAgePreg.setText("" + (Integer.parseInt(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(Constants.ABONNE_OLD_PREGNANCY_AGE, "0")) + Weeks.weeksBetween(dateTimeStart, dateTimeEnd).getWeeks()));
                    //etAgePreg.setClickable(false);
                    //etAgePreg.setFocusable(false);
                }

                Glide.with(this)
                        .asBitmap()
                        .load(Base64.decode(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_PHOTO_CARNET, ""), Base64.DEFAULT))
                        .into(ivCarnetPhoto);
            }

            btnUpdate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    updatePersonInfo();
                }
            });

            ivImportPhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    photoMethod = 2;
                    showFileChooser(1);
                }
            });

            ivTakePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    photoMethod = 1;
                    dispatchTakePictureIntent();
                }
            });

            rgSexeContainer.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup group, int checkedId) {
                    if (checkedId == rbHomme.getId()) {
                        sexeChecked = "M";
                    } else {
                        sexeChecked = "F";
                    }
                }
            });

            ivCarnetPhoto.setOnClickListener(v -> {
                if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_PHOTO_CARNET, "").equals("")) {
                    ToastUtils.showShort("Ajouter une photo svp !");
                } else {
                    View viewCarnet = LayoutInflater.from(this).inflate(R.layout.layout_img_carnet, null);
                    AppCompatImageView ivCarnet = viewCarnet.findViewById(R.id.layout_img_carnet_iv_carnet);

                    AlertDialog.Builder dialogBuidler = new AlertDialog.Builder(this)
                            .setView(viewCarnet);
                    dialogBuidler.create().show();

                    Glide.with(this)
                            .asBitmap()
                            .load(Base64.decode(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_PHOTO_CARNET, ""), Base64.DEFAULT))
                            .into(ivCarnet);


                }
            });
        } catch(Exception e) {
            e.printStackTrace();
        }
    }


    public void updatePersonInfo() {
        btnUpdate.setEnabled(false);
        dateFormat = Utils.formatDate(etNaissance.getText().toString());

        if (sexeChecked.equals("")) {
            ToastUtils.showShort("Veuillez choisir le sexe du patient");
            return;
        }

        if (dateFormat.matches(REGEX)) {
            etNaissance.setTextColor(getResources().getColor(R.color.colorPrimaryTextBlack));
            updateInfosPatient = new UpdateInfosPatient();
            updateInfosPatient.setUpdateInfosNom(etNom.getText().toString().trim());
            updateInfosPatient.setUpdateInfosPrenoms(etPrenoms.getText().toString().trim());
            updateInfosPatient.setUpdateInfosNaissance(StringUtils.reverseDelimited(dateFormat, '-'));
            updateInfosPatient.setUpdateInfosTelephone(etTelephone.getText().toString().trim());
            updateInfosPatient.setUpdateInfosEmail(etEmail.getText().toString().trim());
            updateInfosPatient.setUpdateInfosSexe(sexeChecked);
            new UpdateInfosPatientEngine().execute();
        } else {
            btnUpdate.setEnabled(true);
            etNaissance.setTextColor(getResources().getColor(android.R.color.holo_red_dark));
            Toaster.toast("Mauvaise date de naissance !");
        }
    }


    private class UpdateInfosPatientEngine extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            updateInfosPatient.setUpdateInfosAgePreg(etAgePreg.getText().toString().trim());
            updateInfosPatient.setUpdateInfosPreg(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_PREGNANCY_STATUS, "FEMME").equals("FEMME ENCEINTE") ? "Y" : "N");

            progressDialog.show();
        }

        @Override
        protected Void doInBackground(String... params) {

            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(20, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();

                String photoString = "";
                updateInfosPatient.setUpdateInfosPhoto(photoString);
                updateInfosPatient.setUpdateInfosSexe(sexeChecked);

                if (!SPUtils.getInstance(VACCIN_PREFERENCES).getString(VACCIN_CARNET_COMPRESSED, "").equals("")) {
                    photoString = SPUtils.getInstance(VACCIN_PREFERENCES).getString(VACCIN_CARNET_COMPRESSED, "");
                    updateInfosPatient.setUpdateInfosPhoto(photoString);
                }

                RequestBody formBody = new FormBody.Builder()
                        .add("patId",  String.valueOf(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ID)))
                        .add("nPat", updateInfosPatient.getUpdateInfosNom())
                        .add("pnPat", updateInfosPatient.getUpdateInfosPrenoms())
                        .add("dtPat", updateInfosPatient.getUpdateInfosNaissance())
                        .add("tPat", updateInfosPatient.getUpdateInfosTelephone())
                        .add("ePat", updateInfosPatient.getUpdateInfosEmail())
                        .add("sPat", updateInfosPatient.getUpdateInfosSexe())
                        .add("preg", updateInfosPatient.getUpdateInfosPreg())
                        .add("agePreg", updateInfosPatient.getUpdateInfosAgePreg())
                        .add("imgCarnet", updateInfosPatient.getUpdateInfosPhoto())
                        .add("d", BuildConfig.DATABASE)
                        .build();

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.UPDATE_INFOS_PATIENT)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();
                String responseString = clientResponse.body().string();

                LogUtils.e("Response: " + responseString);

                if (clientResponse.isSuccessful()) {
                    JSONObject json = new JSONObject(responseString);
                    String statut = json.getString("statut");
                    String message = json.getString("message");

                    if (statut.equals("1")) {
                        Toaster.toastLong(message);

                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_NAME, updateInfosPatient.getUpdateInfosNom() + " " + updateInfosPatient.getUpdateInfosPrenoms());
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_EMAIL, updateInfosPatient.getUpdateInfosEmail());
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_BIRTHDAY, StringUtils.reverseDelimited(updateInfosPatient.getUpdateInfosNaissance(), '-'));
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_TELEPHONE, updateInfosPatient.getUpdateInfosTelephone());
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_PREGNANCY_AGE, updateInfosPatient.getUpdateInfosAgePreg());

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnUpdate.setEnabled(true);
                                progressDialog.dismiss();
                                finish();
                            }
                        });
                    } else {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnUpdate.setEnabled(true);
                                progressDialog.dismiss();
                            }
                        });
                        Toaster.toastLong(json.getString("message"));
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            btnUpdate.setEnabled(true);
                            progressDialog.dismiss();
                        }
                    });
                    Toaster.toastLong("Echec de la mise à jour");
                }
            } catch (Exception ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btnUpdate.setEnabled(true);
                        progressDialog.dismiss();
                    }
                });
                ex.printStackTrace();
                Toaster.toastLong("Echec de la mise à jour.");
            }
            return null;
        }
    }


    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File carnetFile = null;
            try {
                carnetFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (carnetFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this, "com.gia.myrecu.fileprovider",
                        carnetFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, 1);
            }
        }
    }


    private void showFileChooser(int pView) {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");

        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(Intent.createChooser(intent, "Choisir une photo"), pView);
        }
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        carnetPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1 && resultCode == RESULT_OK) {

            if (photoMethod == 1) {
                setPic();
            } else {
                Bundle extras = data.getExtras();
                setPic(data.getData());
            }
        }
    }


    private void setPic(Uri uriTraitment) {
        // Get the dimensions of the View
        int targetW = ivCarnetPhoto.getWidth();
        int targetH = ivCarnetPhoto.getHeight();


        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        carnetPhotoPath = ImageFilePath.getPath(this, uriTraitment);

        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoTakingW = bmOptions.outWidth;
        int photoTakingH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleTakingFactor = Math.min(photoTakingW/targetW, photoTakingH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleTakingFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        ivCarnetPhoto.setPadding(5, 5, 5, 5);
        ivCarnetPhoto.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    private void setPic() {
        // Get the dimensions of the View
        int targetW = ivCarnetPhoto.getWidth();
        int targetH = ivCarnetPhoto.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        int photoTakingW = bmOptions.outWidth;
        int photoTakingH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleTakingFactor = Math.min(photoTakingW/targetW, photoTakingH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleTakingFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(carnetPhotoPath, bmOptions);
        ivCarnetPhoto.setPadding(5, 5, 5, 5);
        ivCarnetPhoto.setImageBitmap(bitmap);
        createImageFileCompressed();
    }


    private void createImageFileCompressed() {
        File compressedFile = null;
        try {
            fileGlobal = new File(carnetPhotoPath);
            compressedFile = new Compressor(this)
                    .setQuality(75)
                    .setCompressFormat(Bitmap.CompressFormat.JPEG)
                    .compressToFile(fileGlobal);

            if (FileUtils.isFileExists(compressedFile)) {
                final File finalCompressedFile = compressedFile;

                FileUtils.copy(compressedFile, fileGlobal, new FileUtils.OnReplaceListener() {
                    @Override
                    public boolean onReplace(File srcFile, File destFile) {
                        //Toaster.toast("File remplace");
                        SPUtils.getInstance(VACCIN_PREFERENCES).put(VACCIN_CARNET_COMPRESSED, encodeFileToBase64Binary(finalCompressedFile));

                        Glide.with(UpdateInfosPersActivity.this)
                                .asBitmap()
                                .load(Base64.decode(SPUtils.getInstance(VACCIN_PREFERENCES).getString(VACCIN_CARNET_COMPRESSED), Base64.DEFAULT))
                                .into(ivCarnetPhoto);

                        return true;
                    }
                });
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static String encodeFileToBase64Binary(File file) {
        String encodedBase64 = null;
        try {
            FileInputStream fileInputStreamReader = new FileInputStream(file);
            byte[] bytes = new byte[(int)file.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = Base64.encodeToString(bytes, Base64.DEFAULT);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return encodedBase64;
    }


    @Override
    public void onBackPressed() {
        SPUtils.getInstance(Constants.VACCIN_PREFERENCES).remove(VACCIN_CARNET_COMPRESSED, true);
        super.onBackPressed();
    }
}
