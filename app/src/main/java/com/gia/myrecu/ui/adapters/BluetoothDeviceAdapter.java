package com.gia.myrecu.ui.adapters;


import android.bluetooth.BluetoothDevice;
import android.content.Context;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.gia.myrecu.R;

import java.util.List;

/**
 * Created by didierboka on 8/5/17.
 */

@SuppressWarnings("ALL")
public class BluetoothDeviceAdapter extends RecyclerView.Adapter<BluetoothDeviceAdapter.BluetoothDeviceHolder> {


    Context context;
    List<BluetoothDevice> bluetoothDeviceList;
    OnClickDeviceBluetooth onClickDeviceBluetooth;


    public BluetoothDeviceAdapter(Context context, List<BluetoothDevice> bluetoothDeviceList) {
        this.context = context;
        this.bluetoothDeviceList = bluetoothDeviceList;

        try {
            onClickDeviceBluetooth = (OnClickDeviceBluetooth) context;
        } catch (ClassCastException cce) {
            throw new ClassCastException("Activity or Fragment must implement this listener");
        }
    }


    @Override
    public BluetoothDeviceHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.bluetooth_device_item_list, parent, false);
        return new BluetoothDeviceHolder(view);
    }


    @Override
    public void onBindViewHolder(final BluetoothDeviceHolder holder, int position) {
        final BluetoothDevice bluetoothDevice = bluetoothDeviceList.get(holder.getLayoutPosition());
        final int holderPosition = holder.getLayoutPosition();
        holder.tvDeviceLabel.setText(bluetoothDevice.getName());

        holder.llDeviceContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClickDeviceBluetooth.onDeviceClicked(bluetoothDevice, holderPosition);
            }
        });
    }


    @Override
    public int getItemCount() {
        return bluetoothDeviceList.size();
    }

    public class BluetoothDeviceHolder extends RecyclerView.ViewHolder {

        private AppCompatTextView tvDeviceLabel;
        private AppCompatTextView tvDeviceMacAdress;
        private LinearLayout llDeviceContainer;

        public BluetoothDeviceHolder(View itemView) {
            super(itemView);
            tvDeviceLabel = itemView.findViewById(R.id.bluetooth_device_item_list_tv_label);
            tvDeviceMacAdress = itemView.findViewById(R.id.bluetooth_device_item_list_tv_mac_adress);
            llDeviceContainer = itemView.findViewById(R.id.bluetooth_device_item_list_ll_container);
        }
    }


    public interface OnClickDeviceBluetooth {
        void onDeviceClicked(BluetoothDevice bluetoothDevice, int position);
    }



}
