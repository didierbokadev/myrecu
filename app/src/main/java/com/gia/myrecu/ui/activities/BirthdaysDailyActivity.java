package com.gia.myrecu.ui.activities;

import android.os.AsyncTask;
import android.os.CountDownTimer;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.appcompat.widget.AppCompatTextView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.ProgressBar;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.JsonUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.gia.myrecu.R;
import com.gia.myrecu.models.v2.Patient;
import com.gia.myrecu.tools.Constants;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.ui.adapters.NewBirthAdapter;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;


@SuppressWarnings("ALL")
public class BirthdaysDailyActivity extends AppCompatActivity {


    CountDownTimer fakeDatasCounterDonwTimer;
    ProgressBar loadingProgressBar;
    RecyclerView rvList;
    AppCompatTextView tvEmpty;
    Toolbar tbClose;
    NewBirthAdapter birthAdapter;
    OkHttpClient httpClient;
    List<Patient> patientsList;
    Type typeNewBirth = new TypeToken<ArrayList<Patient>>(){}.getType();
    LoadNewBirthdayTodayAsync newBirthdayTodayAsync;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_birthdays_daily);

        patientsList = new ArrayList<>();
        birthAdapter = new NewBirthAdapter(this, patientsList);

        loadingProgressBar = findViewById(R.id.activity_birthdays_daily_pb_loading);
        tvEmpty = findViewById(R.id.activity_birthdays_daily_tv_empty);
        rvList = findViewById(R.id.activity_birthdays_daily_rv_list);

        rvList.setVisibility(View.GONE);
        tvEmpty.setVisibility(View.GONE);

        rvList.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvList.setAdapter(birthAdapter);

        newBirthdayTodayAsync = new LoadNewBirthdayTodayAsync();
        newBirthdayTodayAsync.execute();
    }


    class LoadNewBirthdayTodayAsync extends AsyncTask<Void, Void, Integer> {

        public LoadNewBirthdayTodayAsync() {
            super();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            loadingProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected Integer doInBackground(Void... voids) {

            int code = 0;

            try {
                httpClient = new OkHttpClient.Builder()
                        .connectTimeout(30, TimeUnit.SECONDS)
                        .writeTimeout(60, TimeUnit.SECONDS)
                        .readTimeout(60, TimeUnit.SECONDS)
                        .build();

                HttpUrl.Builder urlBuilderNewBirth = HttpUrl.parse(UrlBanks.CENTRE_NEW_NAISSANCE_POINT).newBuilder();
                urlBuilderNewBirth.addQueryParameter("centreId", String.valueOf(SPUtils.getInstance(Constants.DISTRICT_CENTRE_PREFERENCES).getInt(Constants.CENTRE_ID, 0)));

                Request requestNewBirth = new Request.Builder()
                        .method("GET", null)
                        .url(urlBuilderNewBirth.build().toString())
                        .build();

                Response responseNewBirth = httpClient.newCall(requestNewBirth).execute();
                String stringNewBirth = responseNewBirth.body().string();

                if (responseNewBirth.isSuccessful()) {
                    code = JsonUtils.getInt(stringNewBirth, "code");

                    if (code == 0) {
                        patientsList.clear();
                        patientsList.addAll((ArrayList<Patient>) GsonUtils.fromJson(JsonUtils.getJSONArray(stringNewBirth, "data", null).toString(), typeNewBirth));
                    }
                } else {
                    code = 2;
                }

            } catch (Exception ex) {
                code = 2;
                ex.printStackTrace();
            }

            return code;
        }

        @Override
        protected void onPostExecute(Integer integer) {
            super.onPostExecute(integer);
            loadingProgressBar.setVisibility(View.GONE);

            switch (integer) {
                case 0 :
                    birthAdapter.notifyDataSetChanged();
                    rvList.setVisibility(View.VISIBLE);
                    tvEmpty.setVisibility(View.GONE);
                break;

                case 1 :
                case 2 :
                    rvList.setVisibility(View.GONE);
                    tvEmpty.setVisibility(View.VISIBLE);
                break;
            }

            ToastUtils.showShort(patientsList.size() + " nouvelles naissances !");

        }
    }
}
