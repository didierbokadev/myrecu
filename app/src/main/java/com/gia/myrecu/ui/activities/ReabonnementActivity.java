package com.gia.myrecu.ui.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.AppCompatRadioButton;
import androidx.appcompat.widget.AppCompatSpinner;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.blankj.utilcode.util.GsonUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.ToastUtils;
import com.gia.myrecu.BuildConfig;
import com.gia.myrecu.R;
import com.gia.myrecu.databases.room.AndroidSmsDatabase;
import com.gia.myrecu.models.CommonResponse;
import com.gia.myrecu.models.v1.Abonnement;
import com.gia.myrecu.models.v1.ReaboData;
import com.gia.myrecu.models.v1.Vaccin;
import com.gia.myrecu.models.v2.Carnet;
import com.gia.myrecu.models.v2.CarnetRequest;
import com.gia.myrecu.models.v2.RecuUsedResponse;
import com.gia.myrecu.network.OPIApiClient;
import com.gia.myrecu.network.OPIApiServices;
import com.gia.myrecu.tools.UrlBanks;
import com.gia.myrecu.tools.Utils;
import com.gia.myrecu.ui.adapters.BluetoothDeviceAdapter;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;
import com.zebra.sdk.comm.BluetoothConnection;
import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.printer.PrinterLanguage;
import com.zebra.sdk.printer.ZebraPrinter;
import com.zebra.sdk.printer.ZebraPrinterFactory;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.json.JSONObject;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import xdroid.toaster.Toaster;

import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_PLANNING_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ABONNEMENT_TYPE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_AMOUNT;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_AVAILABLE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_DATE;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_FORMULE_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_HOUR;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_ID;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_LOGIN;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_NAME;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_PREFERENCES;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_REABONNEMENT_EXPIRATION;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_RECEIPT;
import static com.gia.myrecu.tools.Constants.ABONNE_OLD_TELEPHONE;
import static com.gia.myrecu.tools.Constants.AGENT_ID;
import static com.gia.myrecu.tools.Constants.AGENT_NAME;
import static com.gia.myrecu.tools.Constants.AGENT_PREFERENCES;
import static com.gia.myrecu.tools.Constants.CENTRE_ID;
import static com.gia.myrecu.tools.Constants.CENTRE_NAME;
import static com.gia.myrecu.tools.Constants.DISTRICT_CENTRE_PREFERENCES;
import static com.gia.myrecu.tools.Utils.pickDate;


@SuppressWarnings("ALL")
public class ReabonnementActivity extends AppCompatActivity implements BluetoothDeviceAdapter.OnClickDeviceBluetooth {


    AppCompatButton btnReab;
    AppCompatEditText etNombreAnne;
    SearchableSpinner spRecus;
    List<Carnet> carnetsList;
    Gson gson;
    ReaboData reaboData;
    ProgressDialog dialog;
    int montant;
    AndroidSmsDatabase database;
    Connection btConnection;
    String TAG = "ReabonnementActivity";
    ZebraPrinter zbPrinter;
    ConnectBTDevice connectBTDevice;
    int countPrintTentative;

    LinearLayout linearVaccinContainer;

    RadioGroup rgChooseReaboType;
    AppCompatRadioButton rbPlanning;
    AppCompatRadioButton rbVaccin;

    LinearLayout linearPlanningContainer;
    AppCompatImageView imageDatePlanning;
    AppCompatEditText etLotPlanning;
    AppCompatEditText etExactDatePlanning;
    AppCompatEditText etPlanningMonth;
    LinearLayout linearPlanningMonthContainer;
    LinearLayout linearPlanningMonthLotContainer;
    LinearLayout linearPlanningMonthExactDateContainer;
    LinearLayout linearVaccinPlanningContainer;
    ArrayAdapter<Vaccin> planningVaccinAdapter;
    List<Vaccin> planningVaccinsList;
    Vaccin planningVaccinSelected;
    AppCompatSpinner spPlanningVaccin;

    BluetoothAdapter bluetoothAdapter;
    Set<BluetoothDevice> btDevicesAlreadyBondedList;
    BluetoothDevice bluetoothDevice;
    BluetoothDeviceAdapter bluetoothDeviceAdapter;
    List<BluetoothDevice> deviceList;
    AlertDialog dialogPrinters;

    RecyclerView rvListDevices;
    RadioGroup rgFormule;
    AppCompatButton btnCancelPrint;
    ArrayAdapter<Carnet> carnetAdapter;
    private CarnetRequest carnetRequest;
    String recuNumber = "";


    public void onReabonnementTypeClicked(View view) {
        // Is the button now checked?
        boolean checkedAbonnement = ((AppCompatRadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.activity_reabonnement_choice_vaccine : // Show vaccine tools
                if (checkedAbonnement) {
                    linearVaccinContainer.setVisibility(View.VISIBLE);
                    linearPlanningContainer.setVisibility(View.GONE);
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1");
                }
                break;

            case R.id.activity_reabonnement_choice_planing: // Show planning tools
                if (checkedAbonnement) {
                    linearVaccinContainer.setVisibility(View.GONE);
                    linearPlanningContainer.setVisibility(View.VISIBLE);
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_ABONNEMENT_TYPE_ID, "2");
                }
                break;
        }
    }


    void selectPreviousPlanning(List<Vaccin> planningsList, int previousPlanningId, AppCompatSpinner spinnerPlanning) {
        for (int i = 0; i < planningsList.size(); i++) {
            if (planningsList.get(i).getVaccinId() == previousPlanningId) {
                spinnerPlanning.setSelection(i);
                break;
            }
        }
    }


    @Override
    public void onDeviceClicked(BluetoothDevice bluetoothDevice, int position) {
        if (dialogPrinters.isShowing()) {
            dialogPrinters.dismiss();
        }

        connectBTDevice = new ConnectBTDevice(bluetoothDevice);
        connectBTDevice.execute();
    }


    private void doReabonnement() {
        if (etNombreAnne.getText().toString().length() > 0 && !etNombreAnne.getText().toString().equals("0")) {
            montant = Integer.valueOf(etNombreAnne.getText().toString()) * 1000;

            reaboData = new ReaboData();
            reaboData.setReaboDataIdReq(2);
            reaboData.setReaboDataNombreAnne(etNombreAnne.getText().toString());
            reaboData.setReaboDataPatientId(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ID, "0"));
            reaboData.setReaboDataCentreId(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID));
            reaboData.setReaboDataUserId(SPUtils.getInstance(AGENT_PREFERENCES).getInt(AGENT_ID));
            reaboData.setReaboDataFormule(String.valueOf(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getInt(ABONNE_OLD_FORMULE_ID, 0)));

            String sms = gson.toJson(reaboData);

            new ReabonnementEngine().execute();
        } else {
            return;
        }
    }


    void doPlanningReabonnement(String centre, String agent, String recu, String telephone, String planningProbable, String planningPiluleDuration,
                                String clientId) {
        if (dialog != null) {
            if (!dialog.isShowing()) {
                dialog.show();
            }
        }

        OPIApiClient.getClient(this).create(OPIApiServices.class).reabonnementPlanningClient(
                "" + centre,
                "" + agent,
                "" + recu,
                "" + telephone,
                "" + planningProbable,
                "" + planningPiluleDuration,
                "" + clientId,
                "PROD"
        ).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableSingleObserver<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                dialog.dismiss();

                final String numRecu = recu;
                final String heure = DateTime.now().toString("HH:mm");
                final String day = DateTime.now().toString("dd-MM-yyyy");
                String dateExp = commonResponse.getExpiredDate();
                DateTime date = new DateTime();
                final String reaboDate = date.toString("dd MMMM yyyy", Locale.CANADA_FRENCH);

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_RECEIPT, numRecu);
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_REABONNEMENT_EXPIRATION, dateExp);
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_HOUR, heure);
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE, day);
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_AVAILABLE, reaboDate);

                Abonnement abonnement = new Abonnement();
                abonnement.setAbonnementCentre(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME));
                abonnement.setAbonnementRecu(numRecu);
                abonnement.setAbonnementValidite(reaboDate);
                abonnement.setAbonnementClientNom(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME));
                abonnement.setAbonnementClientNumero(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_TELEPHONE));
                abonnement.setAbonnementMontant(0);
                //abonnement.setAbonnementFormule(SPUtils.getInstance().getString("formuleNameAbonne"));
                abonnement.setAbonnementType("REABONNEMENT");

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_AMOUNT, abonnement.getAbonnementMontant());

                abonnement.setAbonnementDate(DateTime.now().toString("yyyy-MM-dd"));

                database.abonnementDao().createAbonnement(abonnement);

                ToastUtils.showLong("Reabonnement effectué avec succes !");
                finish();
            }

            @Override
            public void onError(Throwable e) {
                dialog.dismiss();
                e.printStackTrace();
            }
        });
    }


    @Override
    protected void onStart() {
        super.onStart();
    }


    @Override
    protected void onResume() {
        super.onResume();
    }


    @Override
    protected void onPause() {
        super.onPause();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                disconnect();
            }
        }).start();
        super.onDestroy();
    }


    public void disconnect() {
        try {
            if (btConnection != null) {
                btConnection.close();
                zbPrinter = null;
                btConnection = null;
            }
        } catch (ConnectionException e) {
            Log.e("ReabonnementActivity", "COMM Error! Disconnected");
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class ReabonnementEngine extends AsyncTask<String, Void, Void> {

        @Override
        protected void onPreExecute() {
            if (dialog != null) {
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            }
        }

        @Override
        protected Void doInBackground(String... params) {
            try {
                final OkHttpClient clientHttp = new OkHttpClient.Builder()
                        .connectTimeout(10, TimeUnit.SECONDS)
                        .writeTimeout(30, TimeUnit.SECONDS)
                        .readTimeout(30, TimeUnit.SECONDS)
                        .build();

                RequestBody formBody = new FormBody.Builder()
                        .add("ctrId", String.valueOf(reaboData.getReaboDataCentreId()))
                        .add("usrId", String.valueOf(reaboData.getReaboDataUserId()))
                        .add("patId", reaboData.getReaboDataPatientId())
                        .add("recu", recuNumber)
                        .add("dur", reaboData.getReaboDataNombreAnne())
                        .add("fm", reaboData.getReaboDataFormule())
                        .add("d", "DEV")
                        .build();

                Request clientRequest = new Request.Builder()
                        .url(UrlBanks.REABONNEMENT_PATIENT)
                        .post(formBody)
                        .build();

                Response clientResponse = clientHttp.newCall(clientRequest).execute();
                String resp = clientResponse.body().string();


                if (clientResponse.isSuccessful()) {

                    final JSONObject json = new JSONObject(resp);
                    String statut = json.getString("statut");

                    if (statut.equals("1")) {
                        // updateRecuNumber();

                        final String numRecu = json.getString("nuRecu");
                        final String heure = DateTime.now().toString("HH:mm");
                        final String day = DateTime.now().toString("dd-MM-yyyy");
                        String dateExp = json.getString("dtExp");
                        DateTime date = new DateTime(dateExp);
                        final String reaboDate = date.toString("dd MMMM yyyy", Locale.CANADA_FRENCH);

                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_RECEIPT, numRecu);
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_REABONNEMENT_EXPIRATION, dateExp);
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_HOUR, heure);
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE, day);
                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_AVAILABLE, reaboDate);

                        Abonnement abonnement = new Abonnement();
                        abonnement.setAbonnementCentre(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME));
                        abonnement.setAbonnementRecu(numRecu);
                        abonnement.setAbonnementValidite(reaboDate);
                        abonnement.setAbonnementClientNom(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME));
                        abonnement.setAbonnementClientNumero(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_TELEPHONE));
                        abonnement.setAbonnementMontant(json.getInt("mt"));
                        //abonnement.setAbonnementFormule(SPUtils.getInstance().getString("formuleNameAbonne"));
                        abonnement.setAbonnementType("REABONNEMENT");

                        SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_AMOUNT, abonnement.getAbonnementMontant());

                        abonnement.setAbonnementDate(DateTime.now().toString("yyyy-MM-dd"));

                        database.abonnementDao().createAbonnement(abonnement);

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                                dialog.setMessage("Impression du recu en cours...");
                                dialogPrinters.show();
                            }
                        });
                    } else {
                        Toaster.toastLong(json.getString("message"));
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                dialog.dismiss();
                            }
                        });
                    }

                } else {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            dialog.dismiss();
                        }
                    });
                    Toaster.toastLong("Echec de réabonnement.");
                }
            } catch (final Exception ex) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();

                        if (ex instanceof SocketTimeoutException) {
                            AlertDialog.Builder builder = new AlertDialog.Builder(ReabonnementActivity.this)
                                    .setCancelable(false)
                                    .setMessage("Un problème est survénue. Veuillez réessayer.")
                                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            dialogInterface.dismiss();
                                        }
                                    });
                            builder.create().show();
                        }
                    }
                });
                ex.printStackTrace();
                Toaster.toastLong("Echec de réabonnement.");
            }
            return null;
        }
    }


    @SuppressLint("StaticFieldLeak")
    private class ConnectBTDevice extends AsyncTask<Object, Object, ZebraPrinter> {


        BluetoothDevice device;

        public ConnectBTDevice(BluetoothDevice device) {
            this.device = device;
        }


        @Override
        protected void onPreExecute() {
            if (dialog != null) {
                if (!dialog.isShowing()) {
                    dialog.show();
                }
            }

            countPrintTentative = countPrintTentative + 1;
        }

        @Override
        protected ZebraPrinter doInBackground(Object[] objects) {

            btConnection = new BluetoothConnection(device.getAddress());

            try {
                btConnection.open();
            } catch(Exception e) {
                e.printStackTrace();
                Toaster.toast("Impossible de se connecter à l'imprimante");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        connectBTDevice.cancel(true);
                        connectBTDevice = null;
                        if (countPrintTentative == 3) {
                            onBackPressed();
                        }
                        connectBTDevice = new ConnectBTDevice(device);
                        connectBTDevice.execute();
                    }
                });
            }

            ZebraPrinter zebraPrinter = null;

            if (btConnection.isConnected()) {
                Log.e("ReabonnementActivity", "Device connected");

                try {
                    zebraPrinter = ZebraPrinterFactory.getInstance(btConnection);
                    PrinterLanguage ptLanguage = zebraPrinter.getPrinterControlLanguage();
                } catch(Exception e) {
                    e.printStackTrace();
                    Toaster.toast("Impossible de se connecter à l'imprimante");
                }
            }

            return zebraPrinter;
        }


        @Override
        protected void onPostExecute(ZebraPrinter zebraPrinter) {
            zbPrinter = zebraPrinter;

            try {
                if (zbPrinter != null) {
                    btConnection.write(getConfigLabel(
                            SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME, ""),
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_HOUR, ""),
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_DATE, ""),
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_RECEIPT, ""),
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_AVAILABLE, ""),
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME, ""),
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_LOGIN, ""),
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_TELEPHONE, ""),
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getInt(ABONNE_OLD_AMOUNT, 0),
                            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_FORMULE_NAME, "NON DEFINI"),
                            SPUtils.getInstance(AGENT_PREFERENCES).getString(AGENT_NAME, "")
                    ));

                    if (dialog != null) {
                        if (dialog.isShowing()) {
                            dialog.dismiss();
                        }
                    }

                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).remove(ABONNE_OLD_HOUR);
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).remove(ABONNE_OLD_DATE);
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).remove(ABONNE_OLD_RECEIPT);
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).remove(ABONNE_OLD_AVAILABLE);

                    dialog.dismiss();

                    onBackPressed();
                }
            } catch(Exception e) {
                if (dialog != null) {
                    if (dialog.isShowing()) {
                        dialog.dismiss();
                    }
                }
                e.printStackTrace();
                Toaster.toast("Impossible d'imprimer le recu");
            }
        }
    }


    private byte[] getConfigLabel(String centre, String hour, String date, String recu, String valid,
                                  String abonne, String login, String telephone, int montant,
                                  String formule, String agent) {

        byte[] configLabel = null;

        String cpclConfigLabel =
                "! 0 200 200 650 1\r\n"
                        + "CENTER\r\n"
                        + "PCX 0 0 !<opis.pcx\r\n"
                        + "ML 25\r\n"
                        + "T 5 0 10 150\r\n"
                        + "CARNET DE VACCINATION\r\n"
                        + "ELECTRONIQUE\r\n"
                        + "ENDML\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 220 CENTRE : " + centre + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 250 HEURE : " + hour + "\r\n"
                        + "RIGHT\r\n"
                        + "T 0 2 0 250 DATE : " + date + "\r\n"
                        + "CENTER\r\n"
                        + "T 0 3 0 300 REABONNEMENT\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 340 RECU : " + recu + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 370 VALIDITE : " + valid + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 400 NOM : " + abonne + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 430 TELEPHONE : " + telephone +"\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 460 MONTANT : " + montant + " FCFA\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 490 FORMULE : " + formule + "\r\n"
                        + "LEFT\r\n"
                        + "T 0 2 0 520 LOGIN : " + login + "\r\n"
                        + "CENTER\r\n"
                        + "T 0 2 0 550 www.opisms.org / 21-24-34-89\r\n"
                        + "CENTER\r\n"
                        + "T 0 2 0 570 NUM.VERT : 143 (APPEL GRATUIT)\r\n"
                        + "RIGHT\r\n"
                        + "T 0 2 0 610 AGENT : " + agent + "\r\n"
                        + "PRINT\r\n\n";

        configLabel = cpclConfigLabel.getBytes();
        return configLabel;
    }


    public void onFormuleClicked(View view) {
        // Is the button now checked?
        boolean checked = ((AppCompatRadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.activity_reabonnement_rb_premuim_formule :
                if (checked) {
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_ID, 2);
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PREMUIM");
                }
                break;

            case R.id.activity_reabonnement_rb_privilege_formule :
                if (checked) {
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_ID, 3);
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "PRIVILEGE");
                }
                break;

            case R.id.activity_reabonnement_rb_free_formule :
                if (checked) {
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_ID, 12);
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "GRATUIT");
                }
                break;

            default :
                if (checked) {
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_ID, 1);
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "STANDARD");
                }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }


    void updateRecuNumber() {
        OPIApiServices apiRecu = OPIApiClient.getClient(this, true).create(OPIApiServices.class);

        apiRecu.updateRecuNumber(carnetRequest)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new DisposableSingleObserver<RecuUsedResponse>() {
                    @Override
                    public void onSuccess(RecuUsedResponse recuUsedResponse) {
                        if (recuUsedResponse.isVerifid()) {
                            AndroidSmsDatabase.getDatabase(ReabonnementActivity.this).carnetDao().updateRecuNumber(carnetRequest.getCentreId());

                            carnetsList.clear();

                            Carnet carnetSelector = new Carnet();
                            carnetSelector.setId("0");
                            carnetSelector.setRecuNum("Choisir recu");

                            carnetRequest.setCentreId("0");
                            carnetsList.add(carnetSelector);

                            carnetsList.addAll(AndroidSmsDatabase.getDatabase(ReabonnementActivity.this).carnetDao().getAllCarnetsUnsed());
                            carnetAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }
                });
    }


    void reabonnementPlanningClient(String centre, String agent, String clientId, String recu, String clientTelephone,
                                    String planningProbableDate, String planningPiluleDuration) {
        OPIApiClient.getClient(this).create(OPIApiServices.class).reabonnementPlanningClient(
                "" + centre,
                "" + agent,
                "" + recu,
                "" + clientTelephone,
                "" + planningProbableDate,
                ""+ planningPiluleDuration,
                "" + clientId,
                "" + BuildConfig.DATABASE
        ).subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeWith(new DisposableSingleObserver<CommonResponse>() {
            @Override
            public void onSuccess(CommonResponse commonResponse) {
                // updateRecuNumber();

                final String numRecu = recu;
                final String heure = DateTime.now().toString("HH:mm");
                final String day = DateTime.now().toString("dd-MM-yyyy");
                String dateExp = commonResponse.getExpiredDate();
                DateTime date = new DateTime(dateExp);
                final String reaboDate = date.toString("dd MMMM yyyy", Locale.CANADA_FRENCH);

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_RECEIPT, numRecu);
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_REABONNEMENT_EXPIRATION, dateExp);
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_HOUR, heure);
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_DATE, day);
                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_AVAILABLE, reaboDate);

                Abonnement abonnement = new Abonnement();
                abonnement.setAbonnementCentre(SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getString(CENTRE_NAME));
                abonnement.setAbonnementRecu(numRecu);
                abonnement.setAbonnementValidite(reaboDate);
                abonnement.setAbonnementClientNom(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_NAME));
                abonnement.setAbonnementClientNumero(SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_TELEPHONE));
                abonnement.setAbonnementMontant(0);
                //abonnement.setAbonnementFormule(SPUtils.getInstance().getString("formuleNameAbonne"));
                abonnement.setAbonnementType("REABONNEMENT");

                SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_AMOUNT, abonnement.getAbonnementMontant());
                abonnement.setAbonnementDate(DateTime.now().toString("yyyy-MM-dd"));
                database.abonnementDao().createAbonnement(abonnement);
            }

            @Override
            public void onError(Throwable e) {

            }
        });
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reabonnement);

        //establishConnection();
        countPrintTentative = 0;

        database = AndroidSmsDatabase.getDatabase(this);

        deviceList = new ArrayList<>();
        btDevicesAlreadyBondedList = new HashSet<>();

        bluetoothDeviceAdapter = new BluetoothDeviceAdapter(this, deviceList);

        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH)) {
            Toast.makeText(this, "Bluetooth not supported", Toast.LENGTH_SHORT).show();
            onBackPressed();
        }

        bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (!bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 2017);
            ToastUtils.showShort("Veuillez activer le bluetooth s'il vous plait.");
            onBackPressed();
        } else {
            btDevicesAlreadyBondedList = bluetoothAdapter.getBondedDevices();
        }

        if (btDevicesAlreadyBondedList.size() > 0) {
            // Al ready devices bonded
            for (BluetoothDevice pairedDevice : btDevicesAlreadyBondedList) {
                if (pairedDevice.getBluetoothClass().getMajorDeviceClass() == BluetoothClass.Device.Major.IMAGING) {
                    bluetoothDevice = pairedDevice;
                    deviceList.add(bluetoothDevice);
                }

            }
            bluetoothDeviceAdapter.notifyDataSetChanged();
        }

        LayoutInflater inflater = (LayoutInflater) getSystemService(LAYOUT_INFLATER_SERVICE);
        View printersView = inflater.inflate(R.layout.view_printers_bluetooth_presenter, null);

        btnCancelPrint = printersView.findViewById(R.id.view_printers_bluetooth_presenter_btn_cancel);
        rvListDevices = printersView.findViewById(R.id.view_printers_bluetooth_presenter_rv_list_devices);

        btnCancelPrint.setOnClickListener(v -> {
            dialogPrinters.dismiss();

            finish();
            overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
        });

        rvListDevices.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        rvListDevices.setAdapter(bluetoothDeviceAdapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setTitle("Choisir une imprimante")
                .setView(printersView)
                .setCancelable(false);
        dialogPrinters = builder.create();

        setSupportActionBar((Toolbar) findViewById(R.id.activity_reabonnement_tb_title));
        getSupportActionBar().setSubtitle(SPUtils.getInstance(ABONNE_OLD_PREFERENCES)
                .getString(ABONNE_OLD_NAME, ""));

        dialog = new ProgressDialog(this);
        dialog.setMessage("Reabonnement en cours...\nVeuillez patientez pour le recu.");
        dialog.setCancelable(false);

        btnReab = findViewById(R.id.activity_reabonnement_btn_reabo);
        spRecus = findViewById(R.id.activity_reabonnement_sp_recu_patient);
        etNombreAnne = findViewById(R.id.activity_reabonnement_et_annee);
        rgFormule = findViewById(R.id.activity_reabonnement_rg_container_formule);

        etExactDatePlanning = findViewById(R.id.editPlanningMonthExactDate);
        etLotPlanning = findViewById(R.id.editPlanningMonthLot);
        imageDatePlanning = findViewById(R.id.imageCalendarDatePlanning);
        linearPlanningMonthContainer = findViewById(R.id.linearPlanningMonthContainer);
        linearPlanningMonthLotContainer = findViewById(R.id.linearPlanningMonthLotContainer);
        linearPlanningMonthExactDateContainer = findViewById(R.id.linearPlanningMonthExactDateContainer);
        etPlanningMonth = findViewById(R.id.editPllanningMonthDate);
        linearVaccinPlanningContainer = findViewById(R.id.linearVaccinPlanningContainer);
        spPlanningVaccin = findViewById(R.id.selectPlanningVaccin);
        linearPlanningContainer = findViewById(R.id.linearPlanningContainer);

        rgChooseReaboType = findViewById(R.id.activity_reabonnement_rg_container_reabonnement_type);
        rbPlanning = findViewById(R.id.activity_reabonnement_choice_planing);
        rbVaccin = findViewById(R.id.activity_reabonnement_choice_vaccine);

        linearVaccinContainer = findViewById(R.id.activity_reabonnement_ll_container);

        carnetRequest = new CarnetRequest();
        carnetRequest.setTransacId("124578911115");

        carnetsList = new ArrayList<>();

        Carnet carnetSelector = new Carnet();
        carnetSelector.setId("0");
        carnetSelector.setRecuNum("Choisir recu");

        carnetRequest.setCentreId("0");

        carnetsList.add(carnetSelector);

        carnetsList.addAll(AndroidSmsDatabase.getDatabase(this).carnetDao().getAllCarnetsUnsed());
        carnetAdapter = new ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, carnetsList);
        spRecus.setTitle("Recherche recu...");
        spRecus.setAdapter(carnetAdapter);

        gson = new GsonBuilder().serializeNulls().create();

        etNombreAnne.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH
                        || actionId == EditorInfo.IME_ACTION_DONE
                        || event.getAction() == KeyEvent.ACTION_DOWN
                        && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {

                    if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("2")) {
                        String planningProbableDate = Utils.formatDate(StringUtils.reverseDelimited(etExactDatePlanning.getText().toString().trim(), '-'));
                        String planningMonth = etPlanningMonth.getText().toString().trim().length() == 0
                                ? "+1months"
                                : Integer.parseInt(etPlanningMonth.getText().toString().trim()) <= 0
                                ? "+1months"
                                : "+" + etPlanningMonth.getText().toString().trim() + "months";

                        doPlanningReabonnement(
                                "" + SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID),
                                "" + SPUtils.getInstance(AGENT_PREFERENCES).getInt(AGENT_ID),
                                "" + recuNumber,
                                "" + SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_TELEPHONE, "0"),
                                "" + planningProbableDate,
                                "" + StringUtils.getDigits(planningMonth),
                                "DEV"
                        );
                    }

                    if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("1")) {
                        doReabonnement();
                    }
                    return true;
                }
                return false;
            }
        });

        btnReab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (carnetRequest.getCentreId().equals("0")) {
                    ToastUtils.showShort("Selectionnez un numero de recu");
                    return;
                }

                if (rbVaccin.getId() == rgChooseReaboType.getCheckedRadioButtonId()) {


                }

                if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("2")) {
                    String planningProbableDate = Utils.formatDate(StringUtils.reverseDelimited(etExactDatePlanning.getText().toString().trim(), '-'));
                    String planningMonth = etPlanningMonth.getText().toString().trim().length() == 0
                            ? "+1months"
                            : Integer.parseInt(etPlanningMonth.getText().toString().trim()) <= 0
                            ? "+1months"
                            : "+" + etPlanningMonth.getText().toString().trim() + "months";

                    doPlanningReabonnement(
                            "" + SPUtils.getInstance(DISTRICT_CENTRE_PREFERENCES).getInt(CENTRE_ID),
                            "" + SPUtils.getInstance(AGENT_PREFERENCES).getInt(AGENT_ID),
                            "" + recuNumber,
                            "" + SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_TELEPHONE, "0"),
                            "" + planningProbableDate,
                            "" + StringUtils.getDigits(planningMonth),
                            "DEV"
                    );
                }

                if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("1")) {
                    doReabonnement();
                }
            }
        });

        if (rgFormule.getCheckedRadioButtonId() == R.id.activity_reabonnement_rb_standart_formule) {
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_ID, 1);
            SPUtils.getInstance(ABONNE_OLD_PREFERENCES).put(ABONNE_OLD_FORMULE_NAME, "STANDARD");
        }

        spRecus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                carnetRequest.setCentreId(carnetsList.get(position).getId());
                recuNumber = carnetsList.get(position).getRecuNum();

                LogUtils.e(GsonUtils.toJson(carnetRequest));
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        planningVaccinsList = new ArrayList<>();
        Vaccin vaccinIndicator = new Vaccin();
        vaccinIndicator.setVaccinId(0);
        vaccinIndicator.setVaccinLabel("Choisir la Methode de Planning");
        vaccinIndicator.setVaccinPeriode("-1");
        planningVaccinsList.add(vaccinIndicator);
        planningVaccinsList.addAll(AndroidSmsDatabase.getDatabase(this).vaccinDao().getPlanningVaccinsList());
        planningVaccinAdapter = new ArrayAdapter(this, android.R.layout.simple_dropdown_item_1line, planningVaccinsList);
        spPlanningVaccin.setAdapter(planningVaccinAdapter);

        etPlanningMonth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.toString().trim().length() == 0) return;
                if (Integer.valueOf(StringUtils.getDigits(s.toString())) < 1) return;

                etExactDatePlanning.setText(Utils.addMomentDate("+" + s.toString().trim() + "months"));
            }
        });

        spPlanningVaccin.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                planningVaccinSelected = planningVaccinsList.get(position);
                LogUtils.e("DATE: " + planningVaccinSelected.getVaccinPeriode());

                if (planningVaccinSelected.getVaccinId() > 0) ToastUtils.showLong("Planning de " + planningVaccinSelected.getVaccinPeriode());

                if (planningVaccinSelected.getVaccinId() > 0) {
                    linearPlanningMonthExactDateContainer.setVisibility(View.VISIBLE);
                    linearPlanningMonthLotContainer.setVisibility(View.VISIBLE);

                    if (planningVaccinSelected.getVaccinPeriode().equals("0")) {
                        etPlanningMonth.setText(null);
                        etExactDatePlanning.setText(null);
                        linearPlanningMonthContainer.setVisibility(View.VISIBLE);
                        return;
                    }

                    if (!planningVaccinSelected.getVaccinPeriode().equals("0")) {
                        etExactDatePlanning.setText(null);
                        linearPlanningMonthContainer.setVisibility(View.GONE);

                        etExactDatePlanning.setText(Utils.addMomentDate(planningVaccinSelected.getVaccinPeriode()));
                        return;
                    }
                }

                etPlanningMonth.setText(null);
                linearPlanningMonthContainer.setVisibility(View.GONE);
                linearPlanningMonthExactDateContainer.setVisibility(View.GONE);
                etExactDatePlanning.setText(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        imageDatePlanning.setOnClickListener(v -> {
            pickDate(etExactDatePlanning, this, true, false);
        });

        if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("2")) {
            selectPreviousPlanning(
                    planningVaccinsList,
                    SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getInt(ABONNE_OLD_ABONNEMENT_PLANNING_ID, 0),
                    spPlanningVaccin);
        }

        if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("2")) {
            rgChooseReaboType.check(rbPlanning.getId());
            onReabonnementTypeClicked(rgChooseReaboType.findViewById(rgChooseReaboType.getCheckedRadioButtonId()));
        }

        if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("1")) {
            rgChooseReaboType.check(rbVaccin.getId());
            onReabonnementTypeClicked(rgChooseReaboType.findViewById(rgChooseReaboType.getCheckedRadioButtonId()));
        }

        if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("2")) {
            linearPlanningContainer.setVisibility(View.VISIBLE);
            return;
        }

        if (SPUtils.getInstance(ABONNE_OLD_PREFERENCES).getString(ABONNE_OLD_ABONNEMENT_TYPE_ID, "1").equals("1")) {
            linearVaccinContainer.setVisibility(View.VISIBLE);
            return;
        }
    }

}
