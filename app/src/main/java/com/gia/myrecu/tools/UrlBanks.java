package com.gia.myrecu.tools;

import com.gia.myrecu.BuildConfig;

/**
 * Created by didierboka on 8/2/17.
 **/


@SuppressWarnings("ALL")
public class UrlBanks {

    public static final String URL = BuildConfig.BASE_URL;
    public static final String CARNET_ENDPOINT = "https://opisms.net/api-carnets/";

    public static final String ALL_DISTRICTS_PATH = "/android/webservicesandroidtosms/provider/get_districts.php";
    public static final String ALL_CENTRES_PATH = "/android/webservicesandroidtosms/provider/get_centres.php";
    public static final String ALL_VACCINS_PATH = "/android/webservicesandroidtosms/provider/get_vaccins.php";

    // SEARCH PATIENT
    public static final String AGENT_CONNEXION = URL + "login";
    public static final String SEARCH_PATIENT = URL + "search";
    public static final String REABONNEMENT_PATIENT = URL + "reabonnement";
    public static final String ABONNEMENT_PATIENT = URL + "abonnement";
    public static final String ABONNEMENT_NEW_PATIENT = URL + "patient/naissance";
    public static final String UPDATE_INFOS_PATIENT = URL + "patient/maj";
    public static final String UPDATE_VACCUM_PATIENT = URL + "vaccin/maj";
    public static final String LIST_VACCINS_A_EFFECTUER = URL + "vaccin/rdv/prochain";
    public static final String LIST_VACCINS_MANQUES = URL + "vaccin/rdv/manque";
    public static final String LIST_VACCINS_A_JOUR = URL + "vaccin/rdv/ajour";
    public static final String UPDATE_VACCUM_MISSED = URL + "vaccin/rdv/maj";
    public static final String VALID_USER_CALENDAR = URL + "vaccin/valid";
    public static final String CENTRE_NEW_NAISSANCE_POINT = URL + "centre/new/naissance/point";
    public static final String USER_RDV_ATTENTES = URL + "agent/rdv/daily";
    public static final String USER_RDV_ATTENTES_ = URL + "agent/rdv/daily";
    public static final String CENTRE_POINT = URL + "centre/point/jour";
    public static final String DEFINE_DEATH = URL + "patient/deces";
    public static final String SMS_PATIENT = URL + "patient/sms";

    // OPISMS PASS
    // SfeX*5lGQfG_
}
