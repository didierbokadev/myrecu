package com.gia.myrecu.tools;

import android.app.DatePickerDialog;
import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;
import android.widget.DatePicker;

import com.blankj.utilcode.util.LogUtils;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import java.util.Calendar;
import java.util.UUID;

/**
 * Created by didierboka on 8/3/17.
 */

@SuppressWarnings("ALL")
public class Utils {


    public static String generateToken() {
        return UUID.randomUUID().toString().substring(0, 5);
    }



    public static String formatDate(String dateToFormat) {
        // Is return this format dd-MM-yyyy
        String date = "";
        date = String.valueOf(StringUtils.getDigits(dateToFormat));
        String day = StringUtils.substring(date, 0, 2);
        //LogUtils.e(day);
        String month = StringUtils.substring(date, 2, 4);
        //LogUtils.e(month);
        String year = StringUtils.substring(date, 4);
        //LogUtils.e(year);
        date = StringUtils.joinWith("-", day, month, year);
        return date;
    }


    public static final void  pickDate(AppCompatEditText inputToReceive, Context pContext, boolean hasMin, boolean hasMax) {
        Calendar calendarDate = Calendar.getInstance();
        int year = calendarDate.get(Calendar.YEAR);
        int month = calendarDate.get(Calendar.MONTH);
        int day = calendarDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialogDatePicker = new DatePickerDialog(pContext, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                String dayFormat = "";
                String monthFormat = "";

                dayFormat = String.valueOf(dayOfMonth);
                if (dayOfMonth < 10) {
                    dayFormat = "0" + dayOfMonth;
                }

                monthFormat = String.valueOf(month + 1);
                if ((month + 1) < 10) {
                    monthFormat = "0" + (month + 1);
                }

                inputToReceive.setText(dayFormat + monthFormat + year);
                LogUtils.e(dayFormat + monthFormat + year);
            }
        }, year, month, day);

        if (hasMin) dialogDatePicker.getDatePicker().setMinDate(DateTime.now().getMillis());
        if (hasMax) dialogDatePicker.getDatePicker().setMaxDate(DateTime.now().getMillis());
        dialogDatePicker.show();
    }


    public static String addMomentDate(String momentToAdd) {
        // Extract number
        int numberAddition = Integer.valueOf(StringUtils.getDigits(momentToAdd).trim());

        // Detect what moment
        boolean isMonth = momentToAdd.contains("months");

        if (!isMonth) return DateTime.now().plusYears(numberAddition).toString(DateTimeFormat.forPattern("ddMMyyyy"));
        return DateTime.now().plusMonths(numberAddition).toString(DateTimeFormat.forPattern("ddMMyyyy"));
    }


    public static String detectAgeStatus(String birthday) {
        LogUtils.e("Age : " + birthday);
        int age = DateTime.now().year().get() - DateTime.parse(StringUtils.reverseDelimited(birthday, '-')).year().get();
        String ageStatus = "";


        if (isBetween(age, 0, 1)) {
            ageStatus = "enfant";
        } else if (isBetween(age, 2, 150)) {
            ageStatus = "adulte";
        } else {
            ageStatus = "";
        }

        LogUtils.e("Age status : " + ageStatus);
        return ageStatus;
    }


    public static boolean isBetween(int x, int lower, int upper) {
        return lower <= x && x <= upper;
    }

}
