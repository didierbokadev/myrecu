package com.gia.myrecu.tools;

/**
 * Created by didierboka on 8/2/17.
 */



public class Constants {

    /*PHONE CONFIGURATION*/
    public static final String PHONE_PREFERENCES = "com.gia.myrecu.phone_preferences";
    public static final String PHONE_IS_CONFIGURED = "phone_is_configured";
    public static final String PHONE_BLUETOOTH_QUESTION_ALREADY_ASKED = "phone_bluetooth_already_asked";
    public static final String PHONE_PRINT = "phone_print";

    /*VACCIN CONFIGURATION*/
    public static final String VACCIN_PREFERENCES = "com.gia.myrecu.vaccin_preferences";
    public static final String VACCIN_ID_SELECTED = "vaccin_id_selected";
    public static final String VACCIN_NAME_SELECTED = "vaccin_name_selected";
    public static final String VACCIN_DATE_PRESENCE_SELECTED = "vaccin_date_presence_selected";
    public static final String VACCIN_DATE_RAPPEL_SELECTED = "vaccin_date_rappel_selected";
    public static final String VACCIN_LOT_SELECTED = "vaccin_lot_selected";
    public static final String VACCIN_ADD_TYPE = "vaccin_add_type";
    public static final String VACCIN_CARNET_COMPRESSED = "vaccin_carnet_compressed";

    /*ABONNEMENT CONFIGURATION*/
    public static final String ABONNE_NEW_PREFERENCES = "com.gia.myrecu.new_abonne_preferences";
    public static final String ABONNE_NEW_FORMULE_ID = "abonne_new_formule_id";
    public static final String ABONNE_NEW_FORMULE_NAME = "abonne_new_formule_name";
    public static final String ABONNE_NEW_ABONNEMENT_TYPE_ID = "abonne_new_abonnement_type_id";
    public static final String ABONNE_NEW_ABONNEMENT_TYPE_NAME = "abonne_new_abonnement_type_name";
    public static final String ABONNE_NEW_SEXE = "abonne_new_sexe";
    public static final String ABONNE_NEW_LANGUE = "abonne_new_langue";
    public static final String ABONNE_NEW_HOUR = "abonne_new_hour";
    public static final String ABONNE_NEW_NAISSANCE = "abonne_new_naissance";
    public static final String ABONNE_NEW_DATE = "abonne_new_date";
    public static final String ABONNE_NEW_RECEIPT = "abonne_new_receipt";
    public static final String ABONNE_NEW_PREGNANCY_STATUS = "abonne_new_pregnancy_status";
    public static final String ABONNE_NEW_AVAILABLE = "abonne_new_available";
    public static final String ABONNE_NEW_EXPIRATION = "abonne_new_expiration";
    public static final String ABONNE_NEW_NAME = "abonne_new_name";
    public static final String ABONNE_NEW_LOGIN = "abonne_new_login";
    public static final String ABONNE_NEW_ID = "abonne_new_id";
    public static final String ABONNE_NEW_PREGNANCY_AGE = "abonne_new_pregnancy_age";
    public static final String ABONNE_NEW_PREGNANCY_AGE_END = "abonne_new_pregnancy_age_end";
    public static final String ABONNE_NEW_PREGNANCY_AGE_BEGIN = "abonne_new_pregnancy_age_debug";
    public static final String ABONNE_PHOTO_CARNET = "abonne_new_photo_carnet";
    public static final String ABONNE_NEW_NUMBER = "abonne_new_number";
    public static final String ABONNE_NEW_AMOUNT = "abonne_new_amount";

    /* ABONNEMENT NOUVELLE NAISSANCE */
    public static final String NEW_NAISSANCE_PREFERENCES = "com.gia.myrecu.new_naissance_preferences";
    public static final String NEW_NAISSANCE_NAME = "new_naissance_nom";
    public static final String NEW_NAISSANCE_PRENOMS = "new_naissance_prenoms";
    public static final String PATIENT_AGE_STATUS = "patient_age_status";
    public static final String NEW_NAISSANCE_SEXE = "new_naissance_sexe";
    public static final String NEW_NAISSANCE_AMOUNT = "new_naissance_amount";
    public static final String NEW_NAISSANCE_LOGIN = "new_naissance_login";
    public static final String NEW_NAISSANCE_ID = "new_naissance_id";
    public static final String NEW_NAISSANCE_RECEIPT = "new_naissance_receipt";
    public static final String NEW_NAISSANCE_AVAILABLE = "new_naissance_available";
    public static final String NEW_NAISSANCE_EXPIRATION = "new_naissance_expiration";
    public static final String NEW_NAISSANCE_HOUR = "new_naissance_hour";
    public static final String NEW_NAISSANCE_DATE = "new_naissance_date";
    public static final String NEW_NAISSANCE_NUMBER = "new_naissance_number";
    public static final String NEW_NAISSANCE_FORMULE = "new_naissance_formule";
    public static final String NEW_NAISSANCE_LANGUE = "new_naissance_langue";
    public static final String NEW_NAISSANCE_FORMULE_NAME = "new_naissance_formule_name";
    public static final String NEW_NAISSANCE_DATE_NAISSANCE = "new_naissance_naissance";
    public static final String NEW_NAISSANCE_PARENT_ID = "new_naissance_parent_id";


    /*REABONNEMENT CONFIGURATION*/
    public static final String ABONNE_OLD_PREFERENCES = "com.gia.myrecu.old_abonne_preferences";
    public static final String ABONNE_OLD_ID = "abonne_old_id";
    public static final String ABONNE_OLD_PHOTO_CARNET = "abonne_old_photo_carnet";
    public static final String ABONNE_OLD_NAME = "abonne_old_name";
    public static final String ABONNE_OLD_STATUT_FIEVRE_JAUNE = "abonne_old_statut_jaune";
    public static final String ABONNE_OLD_TELEPHONE = "abonne_old_telephone";
    public static final String ABONNE_OLD_DATE_FIEVRE_JAUNE = "abonne_old_date_fievre_jaune";
    public static final String ABONNE_OLD_LOT_FIEVRE_JAUNE = "abonne_old_lot_fievre_jaune";
    public static final String ABONNE_OLD_BIRTHDAY = "abonne_old_birthday";
    public static final String ABONNE_OLD_EMAIL = "abonne_old_email";
    public static final String ABONNE_OLD_FORMULE_ID = "abonne_old_formule_id";
    public static final String ABONNE_OLD_FORMULE_NAME = "abonne_old_formule_name";
    public static final String ABONNE_OLD_ABONNEMENT_EXPIRATION = "abonne_old_abonnement_expiration";
    public static final String ABONNE_OLD_ABONNEMENT_PLANNING_ID = "abonne_old_abonnement_planning_id";
    public static final String ABONNE_OLD_LOGIN = "abonne_old_login";
    public static final String ABONNE_OLD_SEXE = "abonne_old_sexe";
    public static final String ABONNE_OLD_ABONNEMENT_TYPE_ID = "abonne_old_abonnement_type_id";
    public static final String ABONNE_OLD_PREGNANCY_STATUS = "abonne_old_pregnancy_status";
    public static final String ABONNE_OLD_PREGNANCY_AGE = "abonne_old_pregnancy_age";
    public static final String ABONNE_OLD_PREGNANCY_AGE_END = "abonne_old_pregnancy_age_end";
    public static final String ABONNE_OLD_PREGNANCY_AGE_BEGIN = "abonne_old_pregnancy_age_debug";
    public static final String ABONNE_OLD_RECEIPT = "abonne_old_receipt";
    public static final String ABONNE_OLD_HOUR = "abonne_old_hour";
    public static final String ABONNE_OLD_DATE = "abonne_old_date";
    public static final String ABONNE_OLD_AMOUNT = "abonne_old_amount";
    public static final String ABONNE_OLD_AVAILABLE = "abonne_old_available";
    public static final String ABONNE_OLD_VALID_VACCINE_ID = "abonne_old_valid_vaccine_id";
    public static final String ABONNE_OLD_VALID_VACCINE_ADAPTER_POSITION = "abonne_old_valid_vaccine_adapter_position";
    public static final String ABONNE_OLD_REABONNEMENT_EXPIRATION = "abonne_old_reabonnement_expiration";

    /*AGENT CONFIGURATION*/
    public static final String AGENT_PREFERENCES = "com.gia.myrecu.agent_preferences";
    public static final String AGENT_NAME = "agent_name";
    public static final String AGENT_LOGIN = "agent_login";
    public static final String AGENT_ID = "agent_id";
    public static final String AGENT_CALL_PERMISSION = "agent_call_permission";
    public static final String AGENT_LOG_STATUT = "agent_is_login";
    public static final String AGENT_LOAD_CARNET = "agent_load_carnet";

    /*DISTRICT CENTRE CONFIGURATION*/
    public static final String DISTRICT_CENTRE_PREFERENCES = "com.gia.myrecu.district_centre_preferences";
    public static final String CENTRE_ID = "centre_id";
    public static final String CENTRE_NAME = "centre_name";
    public static final String DISTRICT_NAME = "district_name";
    public static final String DISTRICT_ID = "district_id";

    /*DISTRICT CENTRE CONFIGURATION*/
    public static final String POINT_PREFERENCES = "com.gia.myrecu.point_preferences";
    public static final String POINT_HEURE = "point_heure";
    public static final String POINT_DATE = "point_date";

    // "ayoka", "5U^1AK=C6ul0"
    // 'opisms', "SfeX*5lGQfG_"
    // 'esante', "kf[EB)GN4]l"

}
