package com.gia.myrecu.databases.room.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.gia.myrecu.models.v2.CommuneModel;
import com.gia.myrecu.models.v2.QuartierModel;

import java.util.List;

/**
 * Created by didier-dev on 20/2/18.
 */

@Dao
public interface QuartierDao {


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createQuartier(QuartierModel model);


    @Transaction
    @Query("select * from quartier")
    List<QuartierModel> getAllQuartiers();


    @Transaction
    @Query("select * from quartier where communeId = :communeId")
    List<QuartierModel> getAllQuartiersByCommune(String communeId);


    @Transaction
    @Query("DELETE FROM quartier")
    void deleteDatas();
}
