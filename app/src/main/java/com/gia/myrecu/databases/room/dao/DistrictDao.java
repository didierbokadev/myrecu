package com.gia.myrecu.databases.room.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.gia.myrecu.models.v1.District;

import java.util.List;

/**
 * Created by didier-dev on 20/2/18.
 */

@Dao
public interface DistrictDao {


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createDistrict(District district);





    @Transaction
    @Query("select * from district")
    List<District> getAllDistricts();


    @Transaction
    @Query("DELETE FROM vaccin")
    void deleteDatas();
}
