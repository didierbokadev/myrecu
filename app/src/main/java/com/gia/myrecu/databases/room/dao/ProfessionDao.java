package com.gia.myrecu.databases.room.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.gia.myrecu.models.v1.Centre;
import com.gia.myrecu.models.v2.ProfessionModel;

import java.util.List;

/**
 * Created by didier-dev on 20/2/18.
 */

@Dao
public interface ProfessionDao {


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createProfession(ProfessionModel model);


    @Transaction
    @Query("select * from profession")
    List<ProfessionModel> getAllProfessions();


    @Transaction
    @Query("DELETE FROM profession")
    void deleteDatas();
}
