package com.gia.myrecu.databases.room.dao;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.gia.myrecu.models.v2.Carnet;

import java.util.List;

/**
 * Created by didier-dev on 20/2/18.
 */

@Dao
public interface CarnetDao {


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createCarnet(Carnet carnet);


    @Transaction
    @Query("select * from carnet")
    List<Carnet> getAllCarnets();


    @Transaction
    @Query("select * from carnet where hasUsed = 0")
    List<Carnet> getAllCarnetsUnsed();


    @Transaction
    @Query("update carnet set hasUsed = 1 where id = :carnetID")
    int updateRecuNumber(String carnetID);


    @Transaction
    @Query("select * from carnet where hasUsed = 1")
    List<Carnet> getAllCarnetsUsed();


    @Transaction
    @Query("DELETE FROM carnet")
    void deleteDatas();
}
