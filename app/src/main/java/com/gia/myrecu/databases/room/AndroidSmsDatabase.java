package com.gia.myrecu.databases.room;


import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import android.content.Context;

import com.gia.myrecu.databases.room.dao.AbonnementDao;
import com.gia.myrecu.databases.room.dao.CarnetDao;
import com.gia.myrecu.databases.room.dao.CentreDao;
import com.gia.myrecu.databases.room.dao.CommuneDao;
import com.gia.myrecu.databases.room.dao.DistrictDao;
import com.gia.myrecu.databases.room.dao.NiveauClasseDao;
import com.gia.myrecu.databases.room.dao.NiveauEtudeDao;
import com.gia.myrecu.databases.room.dao.ProfessionDao;
import com.gia.myrecu.databases.room.dao.QuartierDao;
import com.gia.myrecu.databases.room.dao.VaccinDao;
import com.gia.myrecu.databases.room.dao.VilleDao;
import com.gia.myrecu.models.v1.Abonnement;
import com.gia.myrecu.models.v1.Centre;
import com.gia.myrecu.models.v1.District;
import com.gia.myrecu.models.v1.Vaccin;
import com.gia.myrecu.models.v2.Carnet;
import com.gia.myrecu.models.v2.CommuneModel;
import com.gia.myrecu.models.v2.NiveauClasseModel;
import com.gia.myrecu.models.v2.NiveauEtudeModel;
import com.gia.myrecu.models.v2.ProfessionModel;
import com.gia.myrecu.models.v2.QuartierModel;
import com.gia.myrecu.models.v2.VilleModel;

/**
 * Created by didier-dev on 20/2/18.
 */

@Database(
        entities = {
                Abonnement.class,
                Centre.class,
                District.class,
                Vaccin.class,
                Carnet.class,
                ProfessionModel.class,
                NiveauClasseModel.class,
                NiveauEtudeModel.class,
                VilleModel.class,
                CommuneModel.class,
                QuartierModel.class
        },
        version = 1,
        exportSchema = false
)

public abstract class AndroidSmsDatabase extends RoomDatabase {


    private static AndroidSmsDatabase databaseInstance;
    public abstract VaccinDao vaccinDao();
    public abstract DistrictDao districtDao();
    public abstract AbonnementDao abonnementDao();
    public abstract CentreDao centreDao();
    public abstract CarnetDao carnetDao();
    public abstract ProfessionDao professionDao();
    public abstract NiveauClasseDao niveauClasseDao();
    public abstract NiveauEtudeDao niveauEtudeDao();
    public abstract VilleDao villeDao();
    public abstract CommuneDao communeDao();
    public abstract QuartierDao quartierDao();

    // 28135750421


    static final Migration MIGRATION_1_2 = new Migration(1, 2) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Since we didn't alter the table, there's nothing else to do here.
        }
    };


    static final Migration MIGRATION_1_3 = new Migration(1, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Since we didn't alter the table, there's nothing else to do here.

        }
    };


    static final Migration MIGRATION_2_1 = new Migration(1, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Since we didn't alter the table, there's nothing else to do here.

        }
    };


    static final Migration MIGRATION_2_3 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Since we didn't alter the table, there's nothing else to do here.

        }
    };


    static final Migration MIGRATION_3_4 = new Migration(3, 4) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            // Since we didn't alter the table, there's nothing else to do here.

        }
    };


    public static AndroidSmsDatabase getDatabase(Context context) {
        if (databaseInstance == null) {
            databaseInstance =
                    Room.databaseBuilder(context, AndroidSmsDatabase.class, "androidSms.db")
                            .allowMainThreadQueries()
                            //.fallbackToDestructiveMigrationFrom()
                            .addMigrations(MIGRATION_1_2, MIGRATION_2_3,MIGRATION_3_4)
                            .build();
        }
        return databaseInstance;
    }

}
