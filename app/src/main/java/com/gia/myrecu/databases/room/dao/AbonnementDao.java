package com.gia.myrecu.databases.room.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.gia.myrecu.models.v1.Abonnement;

import java.util.List;

/**
 * Created by didier-dev on 20/2/18.
 */

@Dao
@SuppressWarnings("ALL")
public interface AbonnementDao {


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createAbonnement(Abonnement abonnement);


    @Transaction
    @Query("select * from abonnement_reabonnement")
    List<Abonnement> getAbonnementsList();

    @Transaction
    @Query("select * from abonnement_reabonnement where DATE = :date")
    List<Abonnement> getAbonnementsListByDate(String date);

    @Transaction
    @Query("select count(*) from abonnement_reabonnement where DATE = :day and TYPE = :type")
    int totalTypeDay(String day, String type);

    @Transaction
    @Query("select sum(AMOUNT) from abonnement_reabonnement where DATE = :day and TYPE = :type")
    int totalAmountTypeDay(String day, String type);

    @Transaction
    @Query("select sum(AMOUNT) from abonnement_reabonnement where DATE = :day")
    int totalAmountGlobalDay(String day);


    @Transaction
    @Query("DELETE FROM vaccin")
    void deleteDatas();
}
