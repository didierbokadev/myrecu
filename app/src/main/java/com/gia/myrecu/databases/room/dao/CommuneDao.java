package com.gia.myrecu.databases.room.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.gia.myrecu.models.v2.CommuneModel;
import com.gia.myrecu.models.v2.VilleModel;

import java.util.List;

/**
 * Created by didier-dev on 20/2/18.
 */

@Dao
public interface CommuneDao {


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createCommune(CommuneModel model);


    @Transaction
    @Query("select * from commune")
    List<CommuneModel> getAllCommunes();


    @Transaction
    @Query("select * from commune where villeId = :villeId")
    List<CommuneModel> getAllCommunesByVille(String villeId);


    @Transaction
    @Query("DELETE FROM commune")
    void deleteDatas();
}
