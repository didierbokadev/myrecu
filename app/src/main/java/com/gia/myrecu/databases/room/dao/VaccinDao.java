package com.gia.myrecu.databases.room.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.gia.myrecu.models.v1.Vaccin;

import java.util.List;

/**
 * Created by didier-dev on 20/2/18.
 */

@SuppressWarnings("ALL")
@Dao
public interface VaccinDao {


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createVaccin(Vaccin vaccin);


    @Transaction
    @Query("select * from vaccin order by AGEVACCIN asc, VACCIN_LABEL asc")
    List<Vaccin> getAllVaccins();


    // Query for filter vaccin for children
    @Transaction
    @Query("SELECT * FROM vaccin WHERE AGEVACCIN IN ('0MOIS', '1MOIS 1/2', '2MOIS 1/2', '3MOIS 1/2', '6MOIS', '7MOIS', '9MOIS') ORDER BY AGEVACCIN ASC")
    List<Vaccin> getChlidrenVaccinsList();


    // Query for filter vaccin for FE
    @Transaction
    @Query("SELECT * FROM vaccin WHERE (VACCIN_TYPE = 'femme enceinte' OR AGEVACCIN = '0')  GROUP BY VACCIN_LABEL ORDER BY VACCIN_LABEL ASC")
    List<Vaccin> getPregnancyVaccinsList();


    @Transaction
    @Query("SELECT * FROM vaccin WHERE (AGEVACCIN NOT IN ('0', '0MOIS', '1MOIS 1/2', '2MOIS 1/2', '3MOIS 1/2', '9MOIS') AND VACCIN_TYPE NOT IN('femme enceinte', 'femme poste natal')) GROUP BY VACCIN_LABEL ORDER BY VACCIN_LABEL ASC")
    List<Vaccin> getAdultVaccinsList();


    @Transaction
    @Query("SELECT * FROM vaccin WHERE VACCIN_TYPE = 'planning familial' ORDER BY VACCIN_LABEL ASC")
    List<Vaccin> getPlanningVaccinsList();


    @Transaction
    @Query("DELETE FROM vaccin")
    void deleteDatas();
}
