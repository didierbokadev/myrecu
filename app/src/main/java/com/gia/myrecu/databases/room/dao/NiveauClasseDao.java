package com.gia.myrecu.databases.room.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.gia.myrecu.models.v1.Centre;
import com.gia.myrecu.models.v2.NiveauClasseModel;

import java.util.List;

/**
 * Created by didier-dev on 20/2/18.
 */

@Dao
public interface NiveauClasseDao {


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createClasse(NiveauClasseModel model);


    @Transaction
    @Query("select * from niveauclasse")
    List<NiveauClasseModel> getAllClasses();


    @Transaction
    @Query("select * from niveauclasse where etudeId = :etude")
    List<NiveauClasseModel> getClassesByEtude(int etude);


    @Transaction
    @Query("DELETE FROM niveauclasse")
    void deleteDatas();
}
