package com.gia.myrecu.databases.room.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.gia.myrecu.models.v2.NiveauEtudeModel;
import com.gia.myrecu.models.v2.VilleModel;

import java.util.List;

/**
 * Created by didier-dev on 20/2/18.
 */

@Dao
public interface VilleDao {


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createVille(VilleModel model);


    @Transaction
    @Query("select * from ville")
    List<VilleModel> getAllVilles();


    @Transaction
    @Query("DELETE FROM ville")
    void deleteDatas();
}
