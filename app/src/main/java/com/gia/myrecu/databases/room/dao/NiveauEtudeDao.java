package com.gia.myrecu.databases.room.dao;


import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Transaction;

import com.gia.myrecu.models.v1.Centre;
import com.gia.myrecu.models.v2.NiveauClasseModel;
import com.gia.myrecu.models.v2.NiveauEtudeModel;

import java.util.List;

/**
 * Created by didier-dev on 20/2/18.
 */

@Dao
public interface NiveauEtudeDao {


    @Transaction
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long createEtude(NiveauEtudeModel model);


    @Transaction
    @Query("select * from niveauetude")
    List<NiveauEtudeModel> getAllEtudes();


    @Transaction
    @Query("DELETE FROM niveauetude")
    void deleteDatas();
}
