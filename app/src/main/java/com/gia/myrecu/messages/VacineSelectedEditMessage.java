package com.gia.myrecu.messages;

import com.gia.myrecu.models.v1.Vaccin;

public class VacineSelectedEditMessage {


    Vaccin vaccinSelectedEdit;
    int position;


    public VacineSelectedEditMessage(Vaccin vaccinSelectedEdit, int position) {
        this.vaccinSelectedEdit = vaccinSelectedEdit;
        this.position = position;
    }


    public Vaccin getVaccinSelectedEdit() {
        return vaccinSelectedEdit;
    }

    public int getPosition() {
        return position;
    }
}
