package com.gia.myrecu.messages;

import com.gia.myrecu.models.v1.Vaccin;

public class VacccineListUpdate {

    Vaccin vaccine;
    int position = 0;

    public VacccineListUpdate(Vaccin vaccine, int position) {
        this.vaccine = vaccine;
        this.position = position;
    }


    public Vaccin getVaccine() {
        return vaccine;
    }

    public int getPosition() {
        return position;
    }
}
