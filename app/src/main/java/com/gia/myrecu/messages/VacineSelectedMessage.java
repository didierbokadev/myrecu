package com.gia.myrecu.messages;

import com.gia.myrecu.models.v1.Vaccin;

public class VacineSelectedMessage {

    private Vaccin vaccinSelected;

    public VacineSelectedMessage(Vaccin vaccinSelected) {
        this.vaccinSelected = vaccinSelected;
    }

    public Vaccin getVaccinSelected() {
        return vaccinSelected;
    }
}
