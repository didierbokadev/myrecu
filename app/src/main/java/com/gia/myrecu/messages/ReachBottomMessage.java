package com.gia.myrecu.messages;

/**
 * Created by Cedric BOKA on 2020-02-05.
 */

@SuppressWarnings("ALL")
public class ReachBottomMessage {


    private Object data;


    public ReachBottomMessage(Object data) {
        this.data = data;
    }


    public Object getData() {
        return data;
    }
}
