package com.gia.myrecu.messages;

import com.gia.myrecu.models.v1.Vaccin;

public class VacineUnselectedMessage {

    private Vaccin vaccinUnselected;

    public VacineUnselectedMessage(Vaccin vaccinUnselected) {
        this.vaccinUnselected = vaccinUnselected;
    }

    public Vaccin getVaccinSelected() {
        return vaccinUnselected;
    }
}
