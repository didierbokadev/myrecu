package com.gia.myrecu.models.v2;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Didier BOKA, email: didierboka.developer@gmail.com
 * on 08/09/2022.
 **/


@Entity(
        tableName = "commune",
        indices = @Index(
                unique = true,
                value = "id"
        )
)
public class CommuneModel implements Parcelable {


    @PrimaryKey(autoGenerate = true)
    @Expose(deserialize = false, serialize = false)
    private long _id;
    @SerializedName("ID")
    private int id;
    @SerializedName("IDVILLE")
    private String villeId;
    @SerializedName("COMMUNE")
    private String commune;
    @SerializedName("LAT")
    private double lat;
    @SerializedName("LON")
    private double lon;


    public CommuneModel() {
    }


    public long getID() {
        return _id;
    }


    public void setID(long _id) {
        this._id = _id;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getVilleId() {
        return villeId;
    }

    public void setVilleId(String villeId) {
        this.villeId = villeId;
    }

    public String getCommune() {
        return commune;
    }

    public void setCommune(String commune) {
        this.commune = commune;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
        dest.writeInt(this.id);
        dest.writeString(this.villeId);
        dest.writeString(this.commune);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lon);
    }

    public void readFromParcel(Parcel source) {
        this._id = source.readLong();
        this.id = source.readInt();
        this.villeId = source.readString();
        this.commune = source.readString();
        this.lat = source.readDouble();
        this.lon = source.readDouble();
    }

    protected CommuneModel(Parcel in) {
        this._id = in.readLong();
        this.id = in.readInt();
        this.villeId = in.readString();
        this.commune = in.readString();
        this.lat = in.readDouble();
        this.lon = in.readDouble();
    }

    public static final Parcelable.Creator<CommuneModel> CREATOR = new Parcelable.Creator<CommuneModel>() {
        @Override
        public CommuneModel createFromParcel(Parcel source) {
            return new CommuneModel(source);
        }

        @Override
        public CommuneModel[] newArray(int size) {
            return new CommuneModel[size];
        }
    };


    @NonNull
    @Override
    public String toString() {
        return getCommune();
    }
}
