package com.gia.myrecu.models.v1;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Didier BOKA, email: didierboka.developer@gmail.com
 * on 17/06/2022.
 **/

public abstract class AbonnementResponse {


    @Expose
    @SerializedName("idpat")
    private int idpat;
    @Expose
    @SerializedName("login")
    private int login;
    @Expose
    @SerializedName("nuRecu")
    private String nuRecu;
    @Expose
    @SerializedName("mt")
    private int mt;
    @Expose
    @SerializedName("dtExp")
    private String dtExp;
    @Expose
    @SerializedName("statut")
    private int statut;

    public int getIdpat() {
        return idpat;
    }

    public void setIdpat(int idpat) {
        this.idpat = idpat;
    }

    public int getLogin() {
        return login;
    }

    public void setLogin(int login) {
        this.login = login;
    }

    public String getNuRecu() {
        return nuRecu;
    }

    public void setNuRecu(String nuRecu) {
        this.nuRecu = nuRecu;
    }

    public int getMt() {
        return mt;
    }

    public void setMt(int mt) {
        this.mt = mt;
    }

    public String getDtExp() {
        return dtExp;
    }

    public void setDtExp(String dtExp) {
        this.dtExp = dtExp;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }
}
