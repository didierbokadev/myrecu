package com.gia.myrecu.models.v1;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class Message implements Parcelable {


    /**
     * ID_MSG : 1
     * IDCLT : 1
     * MESSAGE : ISSOUF, Vaccin a faire HEPATITE B+HEPATITE B. Voir carnet.Info:143 / 54050405 opisms.org
     * DateMSG : 2018-08-11
     * HEUREMSG : 22:28:16
     * NUMCELL : 22548376426
     * Etatmsg : T
     * NATUREMSG : VAC
     * ERREUR : 0
     * ORIGINE : OPI
     * DATE_ENVOI : 2018-08-11 22:28:24
     * DIFFERE : 0
     * ERRORSMS : OKMODEM
     * SPECIAL : 0
     * RESEAU : ORANGE
     * IDOPERATEUR_SAISIE : 0
     * NOMOPERATEUR_SAISIE :
     * EMAILOPERATEUR_SAISIE :
     * idclient :
     * IDCAL : 8480440
     * daterappel : 2018-08-13
     * LOGIN : 58504180
     * id_langue_vocale : 0
     * duree_msg_vocal : 0
     * statut_vocal : 0
     * MESSAGETTS :
     * PORT :
     */

    @SerializedName("ID_MSG")
    private String id;
    @SerializedName("IDCLT")
    private String clientId;
    @SerializedName("MESSAGE")
    private String messageBody;
    @SerializedName("DateMSG")
    private String messageDate;
    @SerializedName("HEUREMSG")
    private String messageHour;
    @SerializedName("NUMCELL")
    private String messageNum;
    @SerializedName("Etatmsg")
    private String messageEtat;
    @SerializedName("NATUREMSG")
    private String messageNature;
    @SerializedName("ERREUR")
    private String messageErreur;
    @SerializedName("ORIGINE")
    private String messageOrigin;
    @SerializedName("DATE_ENVOI")
    private String messageDateEnvoi;
    @SerializedName("DIFFERE")
    private String messageDiffere;
    @SerializedName("ERRORSMS")
    private String messageError;
    @SerializedName("SPECIAL")
    private String messageSpecial;
    @SerializedName("RESEAU")
    private String messageReseau;
    @SerializedName("IDOPERATEUR_SAISIE")
    private String agentId;
    @SerializedName("NOMOPERATEUR_SAISIE")
    private String agentNom;
    @SerializedName("EMAILOPERATEUR_SAISIE")
    private String agentEmail;
    @SerializedName("idclient")
    private String client2Id;
    @SerializedName("IDCAL")
    private String calendrierId;
    @SerializedName("daterappel")
    private String calendrierRappel;
    @SerializedName("LOGIN")
    private String clientLogin;
    @SerializedName("id_langue_vocale")
    private String clientLangue;
    @SerializedName("duree_msg_vocal")
    private String messageDureVocal;
    @SerializedName("statut_vocal")
    private String messageVocalStatut;
    @SerializedName("MESSAGETTS")
    private String messageTts;
    @SerializedName("PORT")
    private String messagePort;


    public Message() {
    }



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public String getMessageDate() {
        return messageDate;
    }

    public void setMessageDate(String messageDate) {
        this.messageDate = messageDate;
    }

    public String getMessageHour() {
        return messageHour;
    }

    public void setMessageHour(String messageHour) {
        this.messageHour = messageHour;
    }

    public String getMessageNum() {
        return messageNum;
    }

    public void setMessageNum(String messageNum) {
        this.messageNum = messageNum;
    }

    public String getMessageEtat() {
        return messageEtat;
    }

    public void setMessageEtat(String messageEtat) {
        this.messageEtat = messageEtat;
    }

    public String getMessageNature() {
        return messageNature;
    }

    public void setMessageNature(String messageNature) {
        this.messageNature = messageNature;
    }

    public String getMessageErreur() {
        return messageErreur;
    }

    public void setMessageErreur(String messageErreur) {
        this.messageErreur = messageErreur;
    }

    public String getMessageOrigin() {
        return messageOrigin;
    }

    public void setMessageOrigin(String messageOrigin) {
        this.messageOrigin = messageOrigin;
    }

    public String getMessageDateEnvoi() {
        return messageDateEnvoi;
    }

    public void setMessageDateEnvoi(String messageDateEnvoi) {
        this.messageDateEnvoi = messageDateEnvoi;
    }

    public String getMessageDiffere() {
        return messageDiffere;
    }

    public void setMessageDiffere(String messageDiffere) {
        this.messageDiffere = messageDiffere;
    }

    public String getMessageError() {
        return messageError;
    }

    public void setMessageError(String messageError) {
        this.messageError = messageError;
    }

    public String getMessageSpecial() {
        return messageSpecial;
    }

    public void setMessageSpecial(String messageSpecial) {
        this.messageSpecial = messageSpecial;
    }

    public String getMessageReseau() {
        return messageReseau;
    }

    public void setMessageReseau(String messageReseau) {
        this.messageReseau = messageReseau;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getAgentNom() {
        return agentNom;
    }

    public void setAgentNom(String agentNom) {
        this.agentNom = agentNom;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public String getClient2Id() {
        return client2Id;
    }

    public void setClient2Id(String client2Id) {
        this.client2Id = client2Id;
    }

    public String getCalendrierId() {
        return calendrierId;
    }

    public void setCalendrierId(String calendrierId) {
        this.calendrierId = calendrierId;
    }

    public String getCalendrierRappel() {
        return calendrierRappel;
    }

    public void setCalendrierRappel(String calendrierRappel) {
        this.calendrierRappel = calendrierRappel;
    }

    public String getClientLogin() {
        return clientLogin;
    }

    public void setClientLogin(String clientLogin) {
        this.clientLogin = clientLogin;
    }

    public String getClientLangue() {
        return clientLangue;
    }

    public void setClientLangue(String clientLangue) {
        this.clientLangue = clientLangue;
    }

    public String getMessageDureVocal() {
        return messageDureVocal;
    }

    public void setMessageDureVocal(String messageDureVocal) {
        this.messageDureVocal = messageDureVocal;
    }

    public String getMessageVocalStatut() {
        return messageVocalStatut;
    }

    public void setMessageVocalStatut(String messageVocalStatut) {
        this.messageVocalStatut = messageVocalStatut;
    }

    public String getMessageTts() {
        return messageTts;
    }

    public void setMessageTts(String messageTts) {
        this.messageTts = messageTts;
    }

    public String getMessagePort() {
        return messagePort;
    }

    public void setMessagePort(String messagePort) {
        this.messagePort = messagePort;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.clientId);
        dest.writeString(this.messageBody);
        dest.writeString(this.messageDate);
        dest.writeString(this.messageHour);
        dest.writeString(this.messageNum);
        dest.writeString(this.messageEtat);
        dest.writeString(this.messageNature);
        dest.writeString(this.messageErreur);
        dest.writeString(this.messageOrigin);
        dest.writeString(this.messageDateEnvoi);
        dest.writeString(this.messageDiffere);
        dest.writeString(this.messageError);
        dest.writeString(this.messageSpecial);
        dest.writeString(this.messageReseau);
        dest.writeString(this.agentId);
        dest.writeString(this.agentNom);
        dest.writeString(this.agentEmail);
        dest.writeString(this.client2Id);
        dest.writeString(this.calendrierId);
        dest.writeString(this.calendrierRappel);
        dest.writeString(this.clientLogin);
        dest.writeString(this.clientLangue);
        dest.writeString(this.messageDureVocal);
        dest.writeString(this.messageVocalStatut);
        dest.writeString(this.messageTts);
        dest.writeString(this.messagePort);
    }

    protected Message(Parcel in) {
        this.id = in.readString();
        this.clientId = in.readString();
        this.messageBody = in.readString();
        this.messageDate = in.readString();
        this.messageHour = in.readString();
        this.messageNum = in.readString();
        this.messageEtat = in.readString();
        this.messageNature = in.readString();
        this.messageErreur = in.readString();
        this.messageOrigin = in.readString();
        this.messageDateEnvoi = in.readString();
        this.messageDiffere = in.readString();
        this.messageError = in.readString();
        this.messageSpecial = in.readString();
        this.messageReseau = in.readString();
        this.agentId = in.readString();
        this.agentNom = in.readString();
        this.agentEmail = in.readString();
        this.client2Id = in.readString();
        this.calendrierId = in.readString();
        this.calendrierRappel = in.readString();
        this.clientLogin = in.readString();
        this.clientLangue = in.readString();
        this.messageDureVocal = in.readString();
        this.messageVocalStatut = in.readString();
        this.messageTts = in.readString();
        this.messagePort = in.readString();
    }

    public static final Creator<Message> CREATOR = new Creator<Message>() {
        @Override
        public Message createFromParcel(Parcel source) {
            return new Message(source);
        }

        @Override
        public Message[] newArray(int size) {
            return new Message[size];
        }
    };
}
