package com.gia.myrecu.models.v1;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by didierboka on 8/2/17.
 */

public class MiseAJour implements Parcelable {


    @SerializedName("dI")
    private int miseAJourDistrictId = 0;
    @SerializedName("sI")
    private int miseAJourSiteId = 0;
    @SerializedName("dT")
    private String miseAJourDate = "0000-00-00";
    @SerializedName("nR")
    private String miseAJourNumeroRecu = "";
    @SerializedName("nP")
    private String miseAJourNomPrenoms = "";
    @SerializedName("cT")
    private String miseAJourContacts = "";
    @SerializedName("rR")
    private String miseAJourReclamations = "";
    @SerializedName("vI")
    private int miseAJourVaccinId = 0;
    @SerializedName("nL")
    private String miseAJourNumeroLot = "";
    @SerializedName("pR")
    private String miseAJourDateRdv = "0000-00-00";
    @SerializedName("tM")
    private String miseAJourToken = "";


    public MiseAJour() {
    }


    public String getMiseAJourToken() {
        return miseAJourToken;
    }

    public void setMiseAJourToken(String miseAJourToken) {
        this.miseAJourToken = miseAJourToken;
    }


    public String getMiseAJourDate() {
        return miseAJourDate;
    }

    public void setMiseAJourDate(String miseAJourDate) {
        this.miseAJourDate = miseAJourDate;
    }

    public String getMiseAJourNumeroRecu() {
        return miseAJourNumeroRecu;
    }

    public void setMiseAJourNumeroRecu(String miseAJourNumeroRecu) {
        this.miseAJourNumeroRecu = miseAJourNumeroRecu;
    }

    public String getMiseAJourNomPrenoms() {
        return miseAJourNomPrenoms;
    }

    public void setMiseAJourNomPrenoms(String miseAJourNomPrenoms) {
        this.miseAJourNomPrenoms = miseAJourNomPrenoms;
    }

    public String getMiseAJourContacts() {
        return miseAJourContacts;
    }

    public void setMiseAJourContacts(String miseAJourContacts) {
        this.miseAJourContacts = miseAJourContacts;
    }

    public String getMiseAJourReclamations() {
        return miseAJourReclamations;
    }

    public void setMiseAJourReclamations(String miseAJourReclamations) {
        this.miseAJourReclamations = miseAJourReclamations;
    }

    public int getMiseAJourDistrictId() {
        return miseAJourDistrictId;
    }

    public void setMiseAJourDistrictId(int miseAJourDistrictId) {
        this.miseAJourDistrictId = miseAJourDistrictId;
    }

    public int getMiseAJourSiteId() {
        return miseAJourSiteId;
    }

    public void setMiseAJourSiteId(int miseAJourSiteId) {
        this.miseAJourSiteId = miseAJourSiteId;
    }

    public int getMiseAJourVaccinId() {
        return miseAJourVaccinId;
    }

    public void setMiseAJourVaccinId(int miseAJourVaccinId) {
        this.miseAJourVaccinId = miseAJourVaccinId;
    }

    public String getMiseAJourNumeroLot() {
        return miseAJourNumeroLot;
    }

    public void setMiseAJourNumeroLot(String miseAJourNumeroLot) {
        this.miseAJourNumeroLot = miseAJourNumeroLot;
    }

    public String getMiseAJourDateRdv() {
        return miseAJourDateRdv;
    }

    public void setMiseAJourDateRdv(String miseAJourDateRdv) {
        this.miseAJourDateRdv = miseAJourDateRdv;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.miseAJourDistrictId);
        dest.writeInt(this.miseAJourSiteId);
        dest.writeString(this.miseAJourDate);
        dest.writeString(this.miseAJourNumeroRecu);
        dest.writeString(this.miseAJourNomPrenoms);
        dest.writeString(this.miseAJourContacts);
        dest.writeString(this.miseAJourReclamations);
        dest.writeInt(this.miseAJourVaccinId);
        dest.writeString(this.miseAJourNumeroLot);
        dest.writeString(this.miseAJourDateRdv);
        dest.writeString(this.miseAJourToken);
    }

    protected MiseAJour(Parcel in) {
        this.miseAJourDistrictId = in.readInt();
        this.miseAJourSiteId = in.readInt();
        this.miseAJourDate = in.readString();
        this.miseAJourNumeroRecu = in.readString();
        this.miseAJourNomPrenoms = in.readString();
        this.miseAJourContacts = in.readString();
        this.miseAJourReclamations = in.readString();
        this.miseAJourVaccinId = in.readInt();
        this.miseAJourNumeroLot = in.readString();
        this.miseAJourDateRdv = in.readString();
        this.miseAJourToken = in.readString();
    }

    public static final Creator<MiseAJour> CREATOR = new Creator<MiseAJour>() {
        @Override
        public MiseAJour createFromParcel(Parcel source) {
            return new MiseAJour(source);
        }

        @Override
        public MiseAJour[] newArray(int size) {
            return new MiseAJour[size];
        }
    };
}
