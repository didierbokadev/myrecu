package com.gia.myrecu.models.v1;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by didier-dev on 25/10/17.
 */

public class UpdateInfosPatient implements Parcelable {


    private String updateInfosNom = "";
    private String updateInfosPrenoms = "";
    private String updateInfosNaissance = "";
    private String updateInfosTelephone = "";
    private String updateInfosEmail = "";
    private String updateInfosPhoto = "";
    private String updateInfosAgePreg = "0";
    private String updateInfosPreg = "0";
    private String updateInfosSexe = "";


    public UpdateInfosPatient() {
    }

    public String getUpdateInfosPreg() {
        return updateInfosPreg;
    }

    public void setUpdateInfosPreg(String updateInfosPreg) {
        this.updateInfosPreg = updateInfosPreg;
    }

    public String getUpdateInfosAgePreg() {
        return updateInfosAgePreg;
    }

    public void setUpdateInfosAgePreg(String updateInfosAgePreg) {
        this.updateInfosAgePreg = updateInfosAgePreg;
    }

    public String getUpdateInfosSexe() {
        return updateInfosSexe;
    }

    public void setUpdateInfosSexe(String updateInfosSexe) {
        this.updateInfosSexe = updateInfosSexe;
    }

    public String getUpdateInfosPhoto() {
        return updateInfosPhoto;
    }

    public void setUpdateInfosPhoto(String updateInfosPhoto) {
        this.updateInfosPhoto = updateInfosPhoto;
    }

    public String getUpdateInfosNom() {
        return updateInfosNom;
    }

    public void setUpdateInfosNom(String updateInfosNom) {
        this.updateInfosNom = updateInfosNom;
    }

    public String getUpdateInfosPrenoms() {
        return updateInfosPrenoms;
    }

    public void setUpdateInfosPrenoms(String updateInfosPrenoms) {
        this.updateInfosPrenoms = updateInfosPrenoms;
    }

    public String getUpdateInfosNaissance() {
        return updateInfosNaissance;
    }

    public void setUpdateInfosNaissance(String updateInfosNaissance) {
        this.updateInfosNaissance = updateInfosNaissance;
    }

    public String getUpdateInfosTelephone() {
        return updateInfosTelephone;
    }

    public void setUpdateInfosTelephone(String updateInfosTelephone) {
        this.updateInfosTelephone = updateInfosTelephone;
    }

    public String getUpdateInfosEmail() {
        return updateInfosEmail;
    }

    public void setUpdateInfosEmail(String updateInfosEmail) {
        this.updateInfosEmail = updateInfosEmail;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.updateInfosNom);
        dest.writeString(this.updateInfosPrenoms);
        dest.writeString(this.updateInfosNaissance);
        dest.writeString(this.updateInfosTelephone);
        dest.writeString(this.updateInfosEmail);
        dest.writeString(this.updateInfosPhoto);
        dest.writeString(this.updateInfosAgePreg);
        dest.writeString(this.updateInfosPreg);
        dest.writeString(this.updateInfosSexe);
    }

    protected UpdateInfosPatient(Parcel in) {
        this.updateInfosNom = in.readString();
        this.updateInfosPrenoms = in.readString();
        this.updateInfosNaissance = in.readString();
        this.updateInfosTelephone = in.readString();
        this.updateInfosEmail = in.readString();
        this.updateInfosPhoto = in.readString();
        this.updateInfosAgePreg = in.readString();
        this.updateInfosPreg = in.readString();
        this.updateInfosSexe = in.readString();
    }

    public static final Creator<UpdateInfosPatient> CREATOR = new Creator<UpdateInfosPatient>() {
        @Override
        public UpdateInfosPatient createFromParcel(Parcel source) {
            return new UpdateInfosPatient(source);
        }

        @Override
        public UpdateInfosPatient[] newArray(int size) {
            return new UpdateInfosPatient[size];
        }
    };
}
