package com.gia.myrecu.models.v1;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by didier-dev on 24/10/17.
 */

public class ResponseSearch implements Parcelable {


    @SerializedName("statut")
    private String responseSearchStatut = "0";
    @SerializedName("data")
    private List<Patient> responseSearchDatas;


    public ResponseSearch() {
    }


    public String getResponseSearchStatut() {
        return responseSearchStatut;
    }

    public void setResponseSearchStatut(String responseSearchStatut) {
        this.responseSearchStatut = responseSearchStatut;
    }

    public List<Patient> getResponseSearchDatas() {
        return responseSearchDatas;
    }

    public void setResponseSearchDatas(List<Patient> responseSearchDatas) {
        this.responseSearchDatas = responseSearchDatas;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.responseSearchStatut);
        dest.writeTypedList(this.responseSearchDatas);
    }

    protected ResponseSearch(Parcel in) {
        this.responseSearchStatut = in.readString();
        this.responseSearchDatas = in.createTypedArrayList(Patient.CREATOR);
    }

    public static final Creator<ResponseSearch> CREATOR = new Creator<ResponseSearch>() {
        @Override
        public ResponseSearch createFromParcel(Parcel source) {
            return new ResponseSearch(source);
        }

        @Override
        public ResponseSearch[] newArray(int size) {
            return new ResponseSearch[size];
        }
    };
}
