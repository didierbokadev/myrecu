package com.gia.myrecu.models.v1;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by didierboka on 8/28/17.
 */

public class SearchData implements Parcelable {


    @SerializedName("reqId")
    private int searchDataReqId = 0;
    @SerializedName("reqKey")
    private String searchDataReqKey = "";
    @SerializedName("reqValue")
    private String searchDataReqValue = "";


    public SearchData() {
    }


    public int getSearchDataReqId() {
        return searchDataReqId;
    }

    public void setSearchDataReqId(int searchDataReqId) {
        this.searchDataReqId = searchDataReqId;
    }

    public String getSearchDataReqKey() {
        return searchDataReqKey;
    }

    public void setSearchDataReqKey(String searchDataReqKey) {
        this.searchDataReqKey = searchDataReqKey;
    }

    public String getSearchDataReqValue() {
        return searchDataReqValue;
    }

    public void setSearchDataReqValue(String searchDataReqValue) {
        this.searchDataReqValue = searchDataReqValue;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.searchDataReqId);
        dest.writeString(this.searchDataReqKey);
        dest.writeString(this.searchDataReqValue);
    }

    protected SearchData(Parcel in) {
        this.searchDataReqId = in.readInt();
        this.searchDataReqKey = in.readString();
        this.searchDataReqValue = in.readString();
    }

    public static final Creator<SearchData> CREATOR = new Creator<SearchData>() {
        @Override
        public SearchData createFromParcel(Parcel source) {
            return new SearchData(source);
        }

        @Override
        public SearchData[] newArray(int size) {
            return new SearchData[size];
        }
    };
}
