package com.gia.myrecu.models.v2;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Didier BOKA, email: didierboka.developer@gmail.com
 * on 28/08/2022.
 **/

public class AntigenData implements Parcelable {


    @SerializedName("IDVAC")
    public String idVac;
    @SerializedName("VACCIN")
    public String vaccin;
    @SerializedName("IDAGENT")
    public String idAgent;
    @SerializedName("PERSONNESVACCINEES")
    public String personnesVaccinees;
    @SerializedName("DATEVACCINEE")
    public String dateVaccinee;
    @SerializedName("FEMME")
    public Integer femme;
    @SerializedName("HOMME")
    public Integer homme;


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.idVac);
        dest.writeString(this.vaccin);
        dest.writeString(this.idAgent);
        dest.writeString(this.personnesVaccinees);
        dest.writeString(this.dateVaccinee);
        dest.writeValue(this.femme);
        dest.writeValue(this.homme);
    }

    public void readFromParcel(Parcel source) {
        this.idVac = source.readString();
        this.vaccin = source.readString();
        this.idAgent = source.readString();
        this.personnesVaccinees = source.readString();
        this.dateVaccinee = source.readString();
        this.femme = (Integer) source.readValue(Integer.class.getClassLoader());
        this.homme = (Integer) source.readValue(Integer.class.getClassLoader());
    }

    public AntigenData() {
    }

    protected AntigenData(Parcel in) {
        this.idVac = in.readString();
        this.vaccin = in.readString();
        this.idAgent = in.readString();
        this.personnesVaccinees = in.readString();
        this.dateVaccinee = in.readString();
        this.femme = (Integer) in.readValue(Integer.class.getClassLoader());
        this.homme = (Integer) in.readValue(Integer.class.getClassLoader());
    }

    public static final Parcelable.Creator<AntigenData> CREATOR = new Parcelable.Creator<AntigenData>() {
        @Override
        public AntigenData createFromParcel(Parcel source) {
            return new AntigenData(source);
        }

        @Override
        public AntigenData[] newArray(int size) {
            return new AntigenData[size];
        }
    };
}
