package com.gia.myrecu.models.v2;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;
import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


@SuppressWarnings("ALL")
@Entity(
        tableName = "carnet",
        indices = @Index(
                unique = true,
                value = "recuNum"
        )
)
public class Carnet implements Parcelable {


    /**
     * ID : 69
     * NUMCARNET : 739/20
     * NUMDEBUT : 20A31951
     * NUMFIN : 20A31975
     * DATEMVT : 2020-06-21
     * NUMRECU : 20A31969
     * STATUT : 1
     * IDAGENT : 21
     */

    @Expose(deserialize = false, serialize = false)
    @PrimaryKey(autoGenerate = true)
    private long localID;
    @SerializedName("ID")
    private String id;
    @SerializedName("NUMCARNET")
    private String carnetNum;
    @SerializedName("NUMDEBUT")
    private String debut;
    @SerializedName("NUMFIN")
    private String fin;
    @SerializedName("DATEMVT")
    private String mouvement;
    @SerializedName("NUMRECU")
    private String recuNum;
    @SerializedName("STATUT")
    private String statut;
    @SerializedName("IDAGENT")
    private String agentId;
    @Expose(deserialize = false, serialize = false)
    private int hasUsed = 0;


    public Carnet() {
    }

    public long getLocalID() {
        return localID;
    }

    public void setLocalID(long localID) {
        this.localID = localID;
    }

    public int getHasUsed() {
        return hasUsed;
    }

    public void setHasUsed(int hasUsed) {
        this.hasUsed = hasUsed;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCarnetNum() {
        return carnetNum;
    }

    public void setCarnetNum(String carnetNum) {
        this.carnetNum = carnetNum;
    }

    public String getDebut() {
        return debut;
    }

    public void setDebut(String debut) {
        this.debut = debut;
    }

    public String getFin() {
        return fin;
    }

    public void setFin(String fin) {
        this.fin = fin;
    }

    public String getMouvement() {
        return mouvement;
    }

    public void setMouvement(String mouvement) {
        this.mouvement = mouvement;
    }

    public String getRecuNum() {
        return recuNum;
    }

    public void setRecuNum(String recuNum) {
        this.recuNum = recuNum;
    }

    public String getStatut() {
        return statut;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.carnetNum);
        dest.writeString(this.debut);
        dest.writeString(this.fin);
        dest.writeString(this.mouvement);
        dest.writeString(this.recuNum);
        dest.writeString(this.statut);
        dest.writeString(this.agentId);
    }

    protected Carnet(Parcel in) {
        this.id = in.readString();
        this.carnetNum = in.readString();
        this.debut = in.readString();
        this.fin = in.readString();
        this.mouvement = in.readString();
        this.recuNum = in.readString();
        this.statut = in.readString();
        this.agentId = in.readString();
    }

    public static final Parcelable.Creator<Carnet> CREATOR = new Parcelable.Creator<Carnet>() {
        @Override
        public Carnet createFromParcel(Parcel source) {
            return new Carnet(source);
        }

        @Override
        public Carnet[] newArray(int size) {
            return new Carnet[size];
        }
    };

    @NonNull
    @Override
    public String toString() {
        return getRecuNum();
    }
}
