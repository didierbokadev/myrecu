package com.gia.myrecu.models.v2;

public class RecuUsedResponse {


    /**
     * statut : 1
     * verifid : true
     * transactionID : 124578911115
     * rep : 1
     **/


    private int statut;
    private boolean verifid;
    private String transactionID;
    private int rep;


    public RecuUsedResponse() {
    }


    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public boolean isVerifid() {
        return verifid;
    }

    public void setVerifid(boolean verifid) {
        this.verifid = verifid;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public int getRep() {
        return rep;
    }

    public void setRep(int rep) {
        this.rep = rep;
    }
}
