package com.gia.myrecu.models.v1;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by didierboka on 8/5/17.
 */

public class BluetoothDevice implements Parcelable {


    private String bluetoothDeviceLabel;
    private String bluetoothDeviceMacAdress;
    private String bluetoothDeviceClass;


    public BluetoothDevice() {
    }


    public String getBluetoothDeviceLabel() {
        return bluetoothDeviceLabel;
    }

    public void setBluetoothDeviceLabel(String bluetoothDeviceLabel) {
        this.bluetoothDeviceLabel = bluetoothDeviceLabel;
    }

    public String getBluetoothDeviceMacAdress() {
        return bluetoothDeviceMacAdress;
    }

    public void setBluetoothDeviceMacAdress(String bluetoothDeviceMacAdress) {
        this.bluetoothDeviceMacAdress = bluetoothDeviceMacAdress;
    }

    public String getBluetoothDeviceClass() {
        return bluetoothDeviceClass;
    }

    public void setBluetoothDeviceClass(String bluetoothDeviceClass) {
        this.bluetoothDeviceClass = bluetoothDeviceClass;
    }


    @Override
    public String toString() {
        return getBluetoothDeviceLabel();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.bluetoothDeviceLabel);
        dest.writeString(this.bluetoothDeviceMacAdress);
        dest.writeString(this.bluetoothDeviceClass);
    }

    protected BluetoothDevice(Parcel in) {
        this.bluetoothDeviceLabel = in.readString();
        this.bluetoothDeviceMacAdress = in.readString();
        this.bluetoothDeviceClass = in.readString();
    }

    public static final Creator<BluetoothDevice> CREATOR = new Creator<BluetoothDevice>() {
        @Override
        public BluetoothDevice createFromParcel(Parcel source) {
            return new BluetoothDevice(source);
        }

        @Override
        public BluetoothDevice[] newArray(int size) {
            return new BluetoothDevice[size];
        }
    };
}
