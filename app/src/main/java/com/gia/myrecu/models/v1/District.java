package com.gia.myrecu.models.v1;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

/**
 * Created by didierboka on 8/1/17.
 */

@Entity(
        tableName = "district",
        indices = @Index(
                unique = true,
                value = "ID"
        )
)
@SuppressWarnings("ALL")
public class District implements Parcelable {

    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    private long _id;
    @ColumnInfo(name = "ID")
    @SerializedName("IDDIST")
    private int districtId;
    @ColumnInfo(name = "REGION_ID")
    @SerializedName("IDREG")
    private int districtRegionId;
    @ColumnInfo(name = "DISTRICT_LABEL")
    @SerializedName("NOMDIST")
    private String districtLabel;
    @ColumnInfo(name = "RESPONSABLE")
    @SerializedName("RESPONSABLE")
    private String districtResponsable;
    @ColumnInfo(name = "CONTACT")
    @SerializedName("CONTACT")
    private String districtResponsableContact;
    @ColumnInfo(name = "LAT")
    @SerializedName("LAT")
    private String districtLat;
    @ColumnInfo(name = "LONG")
    @SerializedName("LON")
    private String districtLong;


    public District() {
    }


    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public int getDistrictRegionId() {
        return districtRegionId;
    }

    public void setDistrictRegionId(int districtRegionId) {
        this.districtRegionId = districtRegionId;
    }

    public String getDistrictLabel() {
        return districtLabel;
    }

    public void setDistrictLabel(String districtLabel) {
        this.districtLabel = districtLabel;
    }

    public String getDistrictResponsable() {
        return districtResponsable;
    }

    public void setDistrictResponsable(String districtResponsable) {
        this.districtResponsable = districtResponsable;
    }

    public String getDistrictResponsableContact() {
        return districtResponsableContact;
    }

    public void setDistrictResponsableContact(String districtResponsableContact) {
        this.districtResponsableContact = districtResponsableContact;
    }

    public String getDistrictLat() {
        return districtLat;
    }

    public void setDistrictLat(String districtLat) {
        this.districtLat = districtLat;
    }

    public String getDistrictLong() {
        return districtLong;
    }

    public void setDistrictLong(String districtLong) {
        this.districtLong = districtLong;
    }


    @Override
    public String toString() {
        return getDistrictLabel();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
        dest.writeInt(this.districtId);
        dest.writeInt(this.districtRegionId);
        dest.writeString(this.districtLabel);
        dest.writeString(this.districtResponsable);
        dest.writeString(this.districtResponsableContact);
        dest.writeString(this.districtLat);
        dest.writeString(this.districtLong);
    }

    protected District(Parcel in) {
        this._id = in.readLong();
        this.districtId = in.readInt();
        this.districtRegionId = in.readInt();
        this.districtLabel = in.readString();
        this.districtResponsable = in.readString();
        this.districtResponsableContact = in.readString();
        this.districtLat = in.readString();
        this.districtLong = in.readString();
    }

    public static final Creator<District> CREATOR = new Creator<District>() {
        @Override
        public District createFromParcel(Parcel source) {
            return new District(source);
        }

        @Override
        public District[] newArray(int size) {
            return new District[size];
        }
    };


    public static Comparator<District> DistrictNameComparator = (district1, district2) -> {

        String districtName1 = district1.getDistrictLabel().toUpperCase();
        String districtName2 = district2.getDistrictLabel().toUpperCase();
        //ascending order
        return districtName1.compareTo(districtName2);
    };
}
