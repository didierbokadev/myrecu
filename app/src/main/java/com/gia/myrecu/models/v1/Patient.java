package com.gia.myrecu.models.v1;

import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by didierboka on 8/9/17.
 */
@Entity(
        tableName = "patient",
        indices = @Index(
                unique = true,
                value = "patientId"
        )
)
public class Patient implements Parcelable {

    @SerializedName("idPat")
    @PrimaryKey(autoGenerate = true)
    private int patientId = 0;
    @SerializedName("idpat")
    private int patientRemoteId = 0;
    @SerializedName("statut")
    private int patientAbonnementStatut = 0;
    @SerializedName("plgId")
    private int patientPlanningId = 0;
    @SerializedName("nPat")
    private String patientNomPrenoms = "";
    @SerializedName("login")
    private String patLogin = "";
    @SerializedName("tPat")
    private String patientTelephone = "00000000";
    @SerializedName("dtExp")
    private String patientDateExpiration = "00000000";
    @SerializedName("dtPat")
    private String patientNaissance = "00-00-0000";
    @SerializedName("lPat")
    private String patientLogin = "0000000";
    @SerializedName("ePat")
    private String patientEmail = "patient@opisms.org";
    @SerializedName("sPat")
    private String patientSexe = "";
    @SerializedName("preg")
    private String patientPreg = "N";
    @SerializedName("agePreg")
    private String patientAgePreg = "0";
    @SerializedName("debutPreg")
    private String patientDebutPreg = "0000-00-00";
    @SerializedName("datePregEnd")
    private String patientAccouch = "0000-00-00";
    @SerializedName("expAbn")
    private String patientAbonnement = "";
    @SerializedName("fm")
    private String patientFormule = "0";
    @SerializedName("stFJ")
    private String patientStatutFJ = "0";
    @SerializedName("dtFJ") private String patientDateFJ = "";
    @SerializedName("abonType") private String patientAbonnementType = "";
    @SerializedName("abonLabel") private String patientAbonnementLabel = "";
    @SerializedName("photoCarnet")
    private String patientPhotoCarnet = "";
    @SerializedName("datePres")
    private String patientDatePres = "";
    @SerializedName("ltFJ")
    private String patientLotFJ = "";


    public Patient() {
    }


    public String getPatientAbonnementLabel() {
        return patientAbonnementLabel;
    }


    public void setPatientAbonnementLabel(String patientAbonnementLabel) {
        this.patientAbonnementLabel = patientAbonnementLabel;
    }


    public int getPatientPlanningId() {
        return patientPlanningId;
    }


    public void setPatientPlanningId(int patientPlanningId) {
        this.patientPlanningId = patientPlanningId;
    }


    public String getPatLogin() {
        return patLogin;
    }


    public void setPatLogin(String patLogin) {
        this.patLogin = patLogin;
    }


    public int getPatientAbonnementStatut() {
        return patientAbonnementStatut;
    }


    public void setPatientAbonnementStatut(int patientAbonnementStatut) {
        this.patientAbonnementStatut = patientAbonnementStatut;
    }


    public String getPatientDateExpiration() {
        return patientDateExpiration;
    }


    public void setPatientDateExpiration(String patientDateExpiration) {
        this.patientDateExpiration = patientDateExpiration;
    }


    public int getPatientRemoteId() {
        return patientRemoteId;
    }


    public void setPatientRemoteId(int patientRemoteId) {
        this.patientRemoteId = patientRemoteId;
    }


    public String getPatientAbonnementType() {
        return patientAbonnementType;
    }


    public void setPatientAbonnementType(String patientAbonnementType) {
        this.patientAbonnementType = patientAbonnementType;
    }


    public String getPatientDebutPreg() {
        return patientDebutPreg;
    }


    public void setPatientDebutPreg(String patientDebutPreg) {
        this.patientDebutPreg = patientDebutPreg;
    }


    public String getPatientPreg() {
        return patientPreg;
    }


    public void setPatientPreg(String patientPreg) {
        this.patientPreg = patientPreg;
    }


    public String getPatientAgePreg() {
        return patientAgePreg;
    }


    public void setPatientAgePreg(String patientAgePreg) {
        this.patientAgePreg = patientAgePreg;
    }


    public String getPatientAccouch() {
        return patientAccouch;
    }


    public void setPatientAccouch(String patientAccouch) {
        this.patientAccouch = patientAccouch;
    }


    public String getPatientFormule() {
        return patientFormule;
    }


    public void setPatientFormule(String patientFormule) {
        this.patientFormule = patientFormule;
    }


    public String getPatientDatePres() {
        return patientDatePres;
    }


    public void setPatientDatePres(String patientDatePres) {
        this.patientDatePres = patientDatePres;
    }


    public String getPatientPhotoCarnet() {
        return patientPhotoCarnet;
    }


    public void setPatientPhotoCarnet(String patientPhotoCarnet) {
        this.patientPhotoCarnet = patientPhotoCarnet;
    }


    public String getPatientStatutFJ() {
        return patientStatutFJ;
    }


    public void setPatientStatutFJ(String patientStatutFJ) {
        this.patientStatutFJ = patientStatutFJ;
    }


    public String getPatientDateFJ() {
        return patientDateFJ;
    }


    public void setPatientDateFJ(String patientDateFJ) {
        this.patientDateFJ = patientDateFJ;
    }


    public String getPatientLotFJ() {
        return patientLotFJ;
    }


    public void setPatientLotFJ(String patientLotFJ) {
        this.patientLotFJ = patientLotFJ;
    }


    public String getPatientAbonnement() {
        return patientAbonnement;
    }


    public void setPatientAbonnement(String patientAbonnement) {
        this.patientAbonnement = patientAbonnement;
    }


    public int getPatientId() {
        return patientId;
    }


    public void setPatientId(int patientId) {
        this.patientId = patientId;
    }


    public String getPatientNomPrenoms() {
        return patientNomPrenoms;
    }


    public void setPatientNomPrenoms(String patientNomPrenoms) {
        this.patientNomPrenoms = patientNomPrenoms;
    }


    public String getPatientTelephone() {
        return patientTelephone;
    }


    public void setPatientTelephone(String patientTelephone) {
        this.patientTelephone = patientTelephone;
    }


    public String getPatientNaissance() {
        return patientNaissance;
    }


    public void setPatientNaissance(String patientNaissance) {
        this.patientNaissance = patientNaissance;
    }


    public String getPatientEmail() {
        return patientEmail;
    }


    public void setPatientEmail(String patientEmail) {
        this.patientEmail = patientEmail;
    }


    public String getPatientSexe() {
        return patientSexe;
    }


    public void setPatientSexe(String patientSexe) {
        this.patientSexe = patientSexe;
    }


    public String getPatientLogin() {
        return patientLogin;
    }


    public void setPatientLogin(String patientLogin) {
        this.patientLogin = patientLogin;
    }


    @Override
    public String toString() {
        return getPatientNomPrenoms();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.patientId);
        dest.writeInt(this.patientRemoteId);
        dest.writeInt(this.patientAbonnementStatut);
        dest.writeInt(this.patientPlanningId);
        dest.writeString(this.patientNomPrenoms);
        dest.writeString(this.patLogin);
        dest.writeString(this.patientTelephone);
        dest.writeString(this.patientDateExpiration);
        dest.writeString(this.patientNaissance);
        dest.writeString(this.patientLogin);
        dest.writeString(this.patientEmail);
        dest.writeString(this.patientSexe);
        dest.writeString(this.patientPreg);
        dest.writeString(this.patientAgePreg);
        dest.writeString(this.patientDebutPreg);
        dest.writeString(this.patientAccouch);
        dest.writeString(this.patientAbonnement);
        dest.writeString(this.patientFormule);
        dest.writeString(this.patientStatutFJ);
        dest.writeString(this.patientDateFJ);
        dest.writeString(this.patientAbonnementType);
        dest.writeString(this.patientAbonnementLabel);
        dest.writeString(this.patientPhotoCarnet);
        dest.writeString(this.patientDatePres);
        dest.writeString(this.patientLotFJ);
    }

    public void readFromParcel(Parcel source) {
        this.patientId = source.readInt();
        this.patientRemoteId = source.readInt();
        this.patientAbonnementStatut = source.readInt();
        this.patientPlanningId = source.readInt();
        this.patientNomPrenoms = source.readString();
        this.patLogin = source.readString();
        this.patientTelephone = source.readString();
        this.patientDateExpiration = source.readString();
        this.patientNaissance = source.readString();
        this.patientLogin = source.readString();
        this.patientEmail = source.readString();
        this.patientSexe = source.readString();
        this.patientPreg = source.readString();
        this.patientAgePreg = source.readString();
        this.patientDebutPreg = source.readString();
        this.patientAccouch = source.readString();
        this.patientAbonnement = source.readString();
        this.patientFormule = source.readString();
        this.patientStatutFJ = source.readString();
        this.patientDateFJ = source.readString();
        this.patientAbonnementType = source.readString();
        this.patientAbonnementLabel = source.readString();
        this.patientPhotoCarnet = source.readString();
        this.patientDatePres = source.readString();
        this.patientLotFJ = source.readString();
    }

    protected Patient(Parcel in) {
        this.patientId = in.readInt();
        this.patientRemoteId = in.readInt();
        this.patientAbonnementStatut = in.readInt();
        this.patientPlanningId = in.readInt();
        this.patientNomPrenoms = in.readString();
        this.patLogin = in.readString();
        this.patientTelephone = in.readString();
        this.patientDateExpiration = in.readString();
        this.patientNaissance = in.readString();
        this.patientLogin = in.readString();
        this.patientEmail = in.readString();
        this.patientSexe = in.readString();
        this.patientPreg = in.readString();
        this.patientAgePreg = in.readString();
        this.patientDebutPreg = in.readString();
        this.patientAccouch = in.readString();
        this.patientAbonnement = in.readString();
        this.patientFormule = in.readString();
        this.patientStatutFJ = in.readString();
        this.patientDateFJ = in.readString();
        this.patientAbonnementType = in.readString();
        this.patientAbonnementLabel = in.readString();
        this.patientPhotoCarnet = in.readString();
        this.patientDatePres = in.readString();
        this.patientLotFJ = in.readString();
    }

    public static final Creator<Patient> CREATOR = new Creator<Patient>() {
        @Override
        public Patient createFromParcel(Parcel source) {
            return new Patient(source);
        }

        @Override
        public Patient[] newArray(int size) {
            return new Patient[size];
        }
    };
}
