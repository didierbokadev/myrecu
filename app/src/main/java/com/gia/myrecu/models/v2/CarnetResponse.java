package com.gia.myrecu.models.v2;

import java.util.List;


@SuppressWarnings("ALL")
public class CarnetResponse {


    private int statut;
    private boolean verifid;
    private String transactionID;
    private List<Carnet> data;

    public CarnetResponse() {
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public boolean isVerifid() {
        return verifid;
    }

    public void setVerifid(boolean verifid) {
        this.verifid = verifid;
    }

    public String getTransactionID() {
        return transactionID;
    }

    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    public List<Carnet> getData() {
        return data;
    }

    public void setData(List<Carnet> data) {
        this.data = data;
    }
}
