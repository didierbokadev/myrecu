package com.gia.myrecu.models.v1;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

/**
 * Created by didierboka on 8/1/17.
 */

@Entity(
        tableName = "centre",
        indices = @Index(
                unique = true,
                value = "ID"
        )
)
public class Centre implements Parcelable {


    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    private long _id;
    @ColumnInfo(name = "ID")
    @SerializedName("IDCENTR")
    private int centreId;
    @ColumnInfo(name = "DISTRICT_ID")
    @SerializedName("IDDIST")
    private int centreDistrictId;
    @ColumnInfo(name = "CENTRE_NOM")
    @SerializedName("NOMCENTR")
    private String centreLabel;
    @ColumnInfo(name = "RESPONSABLE")
    @SerializedName("RESPONSABLE")
    private String centreResponsable;
    @ColumnInfo(name = "CONTACT")
    @SerializedName("CONTACT")
    private String centreResponsableContact;
    @ColumnInfo(name = "LAT")
    @SerializedName("LAT")
    private String centreLat;
    @ColumnInfo(name = "LONG")
    @SerializedName("LON")
    private String centreLong;


    public Centre() {
    }



    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getCentreId() {
        return centreId;
    }

    public void setCentreId(int centreId) {
        this.centreId = centreId;
    }

    public int getCentreDistrictId() {
        return centreDistrictId;
    }

    public void setCentreDistrictId(int centreDistrictId) {
        this.centreDistrictId = centreDistrictId;
    }

    public String getCentreLabel() {
        return centreLabel;
    }

    public void setCentreLabel(String centreLabel) {
        this.centreLabel = centreLabel;
    }

    public String getCentreResponsable() {
        return centreResponsable;
    }

    public void setCentreResponsable(String centreResponsable) {
        this.centreResponsable = centreResponsable;
    }

    public String getCentreResponsableContact() {
        return centreResponsableContact;
    }

    public void setCentreResponsableContact(String centreResponsableContact) {
        this.centreResponsableContact = centreResponsableContact;
    }

    public String getCentreLat() {
        return centreLat;
    }

    public void setCentreLat(String centreLat) {
        this.centreLat = centreLat;
    }

    public String getCentreLong() {
        return centreLong;
    }

    public void setCentreLong(String centreLong) {
        this.centreLong = centreLong;
    }


    @Override
    public String toString() {
        return getCentreLabel();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
        dest.writeInt(this.centreId);
        dest.writeInt(this.centreDistrictId);
        dest.writeString(this.centreLabel);
        dest.writeString(this.centreResponsable);
        dest.writeString(this.centreResponsableContact);
        dest.writeString(this.centreLat);
        dest.writeString(this.centreLong);
    }

    protected Centre(Parcel in) {
        this._id = in.readLong();
        this.centreId = in.readInt();
        this.centreDistrictId = in.readInt();
        this.centreLabel = in.readString();
        this.centreResponsable = in.readString();
        this.centreResponsableContact = in.readString();
        this.centreLat = in.readString();
        this.centreLong = in.readString();
    }

    public static final Creator<Centre> CREATOR = new Creator<Centre>() {
        @Override
        public Centre createFromParcel(Parcel source) {
            return new Centre(source);
        }

        @Override
        public Centre[] newArray(int size) {
            return new Centre[size];
        }
    };


    public static Comparator<Centre> CentreNameComparator = (centre1, centre2) -> {

        String centreName1 = centre1.getCentreLabel().toUpperCase();
        String centreName2 = centre2.getCentreLabel().toUpperCase();
        //ascending order
        return centreName1.compareTo(centreName2);
    };
}
