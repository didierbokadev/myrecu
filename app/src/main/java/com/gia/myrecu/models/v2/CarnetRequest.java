package com.gia.myrecu.models.v2;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class CarnetRequest implements Parcelable {


    /**
     * transactionID : 124578911115
     * id : 21
     */

    @SerializedName("transactionID")
    private String transacId;
    @SerializedName("id")
    private String centreId;

    public CarnetRequest() {
    }

    public String getTransacId() {
        return transacId;
    }

    public void setTransacId(String transacId) {
        this.transacId = transacId;
    }

    public String getCentreId() {
        return centreId;
    }

    public void setCentreId(String centreId) {
        this.centreId = centreId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.transacId);
        dest.writeString(this.centreId);
    }

    protected CarnetRequest(Parcel in) {
        this.transacId = in.readString();
        this.centreId = in.readString();
    }

    public static final Parcelable.Creator<CarnetRequest> CREATOR = new Parcelable.Creator<CarnetRequest>() {
        @Override
        public CarnetRequest createFromParcel(Parcel source) {
            return new CarnetRequest(source);
        }

        @Override
        public CarnetRequest[] newArray(int size) {
            return new CarnetRequest[size];
        }
    };
}
