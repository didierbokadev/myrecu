package com.gia.myrecu.models.v1;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by didier-dev on 9/11/17.
 */

@Entity(
        tableName = "abonnement_reabonnement"
)
public class Abonnement implements Parcelable {


    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    private long _id;
    @ColumnInfo(name = "ID")
    private int abonnementId = 0;
    @ColumnInfo(name = "CENTRE_ID")
    private String abonnementCentre = "";
    @ColumnInfo(name = "RECU_NUMBER")
    private String abonnementRecu = "";
    @ColumnInfo(name = "RECU_DEADLINE")
    private String abonnementValidite = "";
    @ColumnInfo(name = "CLIENT_NAME")
    private String abonnementClientNom = "";
    @ColumnInfo(name = "CLIENT_SEXE")
    private String abonnementClientSexe = "";
    @ColumnInfo(name = "CLIENT_NUMBER")
    private String abonnementClientNumero = "";
    @ColumnInfo(name = "CLIENT_NAISSANCE")
    private String abonnementClientNaissance = "";
    @ColumnInfo(name = "AMOUNT")
    private int abonnementMontant = 0;
    @ColumnInfo(name = "FORMULE")
    private String abonnementFormule = "";
    @ColumnInfo(name = "DATE")
    private String abonnementDate = "";
    @ColumnInfo(name = "TYPE")
    private String abonnementType = "";


    public Abonnement() {
    }


    public String getAbonnementClientSexe() {
        return abonnementClientSexe;
    }

    public void setAbonnementClientSexe(String abonnementClientSexe) {
        this.abonnementClientSexe = abonnementClientSexe;
    }

    public String getAbonnementClientNaissance() {
        return abonnementClientNaissance;
    }

    public void setAbonnementClientNaissance(String abonnementClientNaissance) {
        this.abonnementClientNaissance = abonnementClientNaissance;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getAbonnementId() {
        return abonnementId;
    }


    public String getAbonnementType() {
        return abonnementType;
    }

    public void setAbonnementType(String abonnementType) {
        this.abonnementType = abonnementType;
    }

    public void setAbonnementId(int abonnementId) {
        this.abonnementId = abonnementId;
    }

    public String getAbonnementCentre() {
        return abonnementCentre;
    }

    public void setAbonnementCentre(String abonnementCentre) {
        this.abonnementCentre = abonnementCentre;
    }

    public String getAbonnementRecu() {
        return abonnementRecu;
    }

    public void setAbonnementRecu(String abonnementRecu) {
        this.abonnementRecu = abonnementRecu;
    }

    public String getAbonnementValidite() {
        return abonnementValidite;
    }

    public void setAbonnementValidite(String abonnementValidite) {
        this.abonnementValidite = abonnementValidite;
    }

    public String getAbonnementClientNom() {
        return abonnementClientNom;
    }

    public void setAbonnementClientNom(String abonnementClientNom) {
        this.abonnementClientNom = abonnementClientNom;
    }

    public String getAbonnementClientNumero() {
        return abonnementClientNumero;
    }

    public void setAbonnementClientNumero(String abonnementClientNumero) {
        this.abonnementClientNumero = abonnementClientNumero;
    }

    public int getAbonnementMontant() {
        return abonnementMontant;
    }

    public void setAbonnementMontant(int abonnementMontant) {
        this.abonnementMontant = abonnementMontant;
    }

    public String getAbonnementFormule() {
        return abonnementFormule;
    }

    public void setAbonnementFormule(String abonnementFormule) {
        this.abonnementFormule = abonnementFormule;
    }

    public String getAbonnementDate() {
        return abonnementDate;
    }

    public void setAbonnementDate(String abonnementDate) {
        this.abonnementDate = abonnementDate;
    }

    @Override
    public String toString() {
        return getAbonnementRecu() + " " + getAbonnementClientNom() + " " + getAbonnementMontant();
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
        dest.writeInt(this.abonnementId);
        dest.writeString(this.abonnementCentre);
        dest.writeString(this.abonnementRecu);
        dest.writeString(this.abonnementValidite);
        dest.writeString(this.abonnementClientNom);
        dest.writeString(this.abonnementClientSexe);
        dest.writeString(this.abonnementClientNumero);
        dest.writeString(this.abonnementClientNaissance);
        dest.writeInt(this.abonnementMontant);
        dest.writeString(this.abonnementFormule);
        dest.writeString(this.abonnementDate);
        dest.writeString(this.abonnementType);
    }

    public void readFromParcel(Parcel source) {
        this._id = source.readLong();
        this.abonnementId = source.readInt();
        this.abonnementCentre = source.readString();
        this.abonnementRecu = source.readString();
        this.abonnementValidite = source.readString();
        this.abonnementClientNom = source.readString();
        this.abonnementClientSexe = source.readString();
        this.abonnementClientNumero = source.readString();
        this.abonnementClientNaissance = source.readString();
        this.abonnementMontant = source.readInt();
        this.abonnementFormule = source.readString();
        this.abonnementDate = source.readString();
        this.abonnementType = source.readString();
    }

    protected Abonnement(Parcel in) {
        this._id = in.readLong();
        this.abonnementId = in.readInt();
        this.abonnementCentre = in.readString();
        this.abonnementRecu = in.readString();
        this.abonnementValidite = in.readString();
        this.abonnementClientNom = in.readString();
        this.abonnementClientSexe = in.readString();
        this.abonnementClientNumero = in.readString();
        this.abonnementClientNaissance = in.readString();
        this.abonnementMontant = in.readInt();
        this.abonnementFormule = in.readString();
        this.abonnementDate = in.readString();
        this.abonnementType = in.readString();
    }

    public static final Creator<Abonnement> CREATOR = new Creator<Abonnement>() {
        @Override
        public Abonnement createFromParcel(Parcel source) {
            return new Abonnement(source);
        }

        @Override
        public Abonnement[] newArray(int size) {
            return new Abonnement[size];
        }
    };
}
