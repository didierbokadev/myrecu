package com.gia.myrecu.models.v2;

import android.os.Parcel;
import android.os.Parcelable;

import com.gia.myrecu.models.v1.Agent;
import com.google.gson.annotations.SerializedName;

public class Patient implements Parcelable {


    /**
     * IDPAT : 5
     * IDAGENT : 93
     * IDCENTR : 25
     * IDDIST : 86
     * IDREG : 11
     * NOMPAT : DJOUGA
     * PRENOMPAT : KOUDOU LAURENT
     * DATEPAT : 1985-12-25
     * SEXEPAT : M
     * NUMEROPAT : 22548115353
     * NUMEROPAT2 : 22504387509
     * EMAILPAT :
     * NOMMERE :
     * AGE_GROSSESSE : null
     * NUMPASSEPORT :
     * AGEMERE :
     * LANGUEPARLEE : 3
     * ACTIVITE : INDETERMINE
     * NIVEAU : INDETERMINE
     * PROVENANCE :
     * PHOTOPAT :
     * PHOTOCARNET :
     * DATESAISIE : 2010-08-10
     * SPECIAL : 0
     * PARENT_ID : 0
     * NUMDOSSIER : 0
 */

    @SerializedName("IDPAT")
    private String patId;
    @SerializedName("IDAGENT")
    private String agentId;
    @SerializedName("IDCENTR")
    private String centreId;
    @SerializedName("IDDIST")
    private String districtId;
    @SerializedName("IDREG")
    private String regionId;
    @SerializedName("NOMPAT")
    private String nomPat;
    @SerializedName("PRENOMPAT")
    private String prenomsPat;
    @SerializedName("DATEPAT")
    private String naisaancePat;
    @SerializedName("SEXEPAT")
    private String sexePat;
    @SerializedName("NUMEROPAT")
    private String numeroPat;
    @SerializedName("NUMEROPAT2")
    private String numero2Pat;
    @SerializedName("EMAILPAT")
    private String emailPat;
    @SerializedName("NOMMERE")
    private String nomMerePat;
    @SerializedName("AGE_GROSSESSE")
    private String ageGrossessePat;
    @SerializedName("NUMPASSEPORT")
    private String numeroPassportPat;
    @SerializedName("AGEMERE")
    private String ageMerePat;
    @SerializedName("LANGUEPARLEE")
    private String langueParleePat;
    @SerializedName("ACTIVITE")
    private String activitePat;
    @SerializedName("NIVEAU")
    private String niveauPat;
    @SerializedName("PROVENANCE")
    private String provenancePat;
    @SerializedName("ABNT")
    private String abontExpiration;
    @SerializedName("PHOTOPAT")
    private String photoPat;
    @SerializedName("PHOTOCARNET")
    private String photoCarnetPat;
    @SerializedName("DATESAISIE")
    private String dateEnregPat;
    @SerializedName("SPECIAL")
    private String specialPat;
    @SerializedName("PARENT_ID")
    private String mereIdPat;
    @SerializedName("NUMDOSSIER")
    private String numeroDossierPat;
    @SerializedName("stFJ")
    private String fievreJaune;
    @SerializedName("datFJ")
    private String dateFievreJaune;
    @SerializedName("lFJ")
    private String lotFievreJaune;
    @SerializedName("FM")
    private String formule;

    @SerializedName("AGENT")
    private Agent agentBean;


    public Patient() {
    }

    public String getFormule() {
        return formule;
    }

    public void setFormule(String formule) {
        this.formule = formule;
    }

    public String getAbontExpiration() {
        return abontExpiration;
    }

    public void setAbontExpiration(String abontExpiration) {
        this.abontExpiration = abontExpiration;
    }

    public String getDateFievreJaune() {
        return dateFievreJaune;
    }

    public void setDateFievreJaune(String dateFievreJaune) {
        this.dateFievreJaune = dateFievreJaune;
    }

    public String getLotFievreJaune() {
        return lotFievreJaune;
    }

    public void setLotFievreJaune(String lotFievreJaune) {
        this.lotFievreJaune = lotFievreJaune;
    }

    public String getFievreJaune() {
        return fievreJaune;
    }

    public void setFievreJaune(String fievreJaune) {
        this.fievreJaune = fievreJaune;
    }

    public Agent getAgentBean() {
        return agentBean;
    }

    public void setAgentBean(Agent agentBean) {
        this.agentBean = agentBean;
    }

    public String getPatId() {
        return patId;
    }

    public void setPatId(String patId) {
        this.patId = patId;
    }

    public String getAgentId() {
        return agentId;
    }

    public void setAgentId(String agentId) {
        this.agentId = agentId;
    }

    public String getCentreId() {
        return centreId;
    }

    public void setCentreId(String centreId) {
        this.centreId = centreId;
    }

    public String getDistrictId() {
        return districtId;
    }

    public void setDistrictId(String districtId) {
        this.districtId = districtId;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getNomPat() {
        return nomPat;
    }

    public void setNomPat(String nomPat) {
        this.nomPat = nomPat;
    }

    public String getPrenomsPat() {
        return prenomsPat;
    }

    public void setPrenomsPat(String prenomsPat) {
        this.prenomsPat = prenomsPat;
    }

    public String getNaisaancePat() {
        return naisaancePat;
    }

    public void setNaisaancePat(String naisaancePat) {
        this.naisaancePat = naisaancePat;
    }

    public String getSexePat() {
        return sexePat;
    }

    public void setSexePat(String sexePat) {
        this.sexePat = sexePat;
    }

    public String getNumeroPat() {
        return numeroPat;
    }

    public void setNumeroPat(String numeroPat) {
        this.numeroPat = numeroPat;
    }

    public String getNumero2Pat() {
        return numero2Pat;
    }

    public void setNumero2Pat(String numero2Pat) {
        this.numero2Pat = numero2Pat;
    }

    public String getEmailPat() {
        return emailPat;
    }

    public void setEmailPat(String emailPat) {
        this.emailPat = emailPat;
    }

    public String getNomMerePat() {
        return nomMerePat;
    }

    public void setNomMerePat(String nomMerePat) {
        this.nomMerePat = nomMerePat;
    }

    public String getAgeGrossessePat() {
        return ageGrossessePat;
    }

    public void setAgeGrossessePat(String ageGrossessePat) {
        this.ageGrossessePat = ageGrossessePat;
    }

    public String getNumeroPassportPat() {
        return numeroPassportPat;
    }

    public void setNumeroPassportPat(String numeroPassportPat) {
        this.numeroPassportPat = numeroPassportPat;
    }

    public String getAgeMerePat() {
        return ageMerePat;
    }

    public void setAgeMerePat(String ageMerePat) {
        this.ageMerePat = ageMerePat;
    }

    public String getLangueParleePat() {
        return langueParleePat;
    }

    public void setLangueParleePat(String langueParleePat) {
        this.langueParleePat = langueParleePat;
    }

    public String getActivitePat() {
        return activitePat;
    }

    public void setActivitePat(String activitePat) {
        this.activitePat = activitePat;
    }

    public String getNiveauPat() {
        return niveauPat;
    }

    public void setNiveauPat(String niveauPat) {
        this.niveauPat = niveauPat;
    }

    public String getProvenancePat() {
        return provenancePat;
    }

    public void setProvenancePat(String provenancePat) {
        this.provenancePat = provenancePat;
    }

    public String getPhotoPat() {
        return photoPat;
    }

    public void setPhotoPat(String photoPat) {
        this.photoPat = photoPat;
    }

    public String getPhotoCarnetPat() {
        return photoCarnetPat;
    }

    public void setPhotoCarnetPat(String photoCarnetPat) {
        this.photoCarnetPat = photoCarnetPat;
    }

    public String getDateEnregPat() {
        return dateEnregPat;
    }

    public void setDateEnregPat(String dateEnregPat) {
        this.dateEnregPat = dateEnregPat;
    }

    public String getSpecialPat() {
        return specialPat;
    }

    public void setSpecialPat(String specialPat) {
        this.specialPat = specialPat;
    }

    public String getMereIdPat() {
        return mereIdPat;
    }

    public void setMereIdPat(String mereIdPat) {
        this.mereIdPat = mereIdPat;
    }

    public String getNumeroDossierPat() {
        return numeroDossierPat;
    }

    public void setNumeroDossierPat(String numeroDossierPat) {
        this.numeroDossierPat = numeroDossierPat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.patId);
        dest.writeString(this.agentId);
        dest.writeString(this.centreId);
        dest.writeString(this.districtId);
        dest.writeString(this.regionId);
        dest.writeString(this.nomPat);
        dest.writeString(this.prenomsPat);
        dest.writeString(this.naisaancePat);
        dest.writeString(this.sexePat);
        dest.writeString(this.numeroPat);
        dest.writeString(this.numero2Pat);
        dest.writeString(this.emailPat);
        dest.writeString(this.nomMerePat);
        dest.writeString(this.ageGrossessePat);
        dest.writeString(this.numeroPassportPat);
        dest.writeString(this.ageMerePat);
        dest.writeString(this.langueParleePat);
        dest.writeString(this.activitePat);
        dest.writeString(this.niveauPat);
        dest.writeString(this.provenancePat);
        dest.writeString(this.abontExpiration);
        dest.writeString(this.photoPat);
        dest.writeString(this.photoCarnetPat);
        dest.writeString(this.dateEnregPat);
        dest.writeString(this.specialPat);
        dest.writeString(this.mereIdPat);
        dest.writeString(this.numeroDossierPat);
        dest.writeString(this.fievreJaune);
        dest.writeString(this.dateFievreJaune);
        dest.writeString(this.lotFievreJaune);
        dest.writeString(this.formule);
        dest.writeParcelable(this.agentBean, flags);
    }

    protected Patient(Parcel in) {
        this.patId = in.readString();
        this.agentId = in.readString();
        this.centreId = in.readString();
        this.districtId = in.readString();
        this.regionId = in.readString();
        this.nomPat = in.readString();
        this.prenomsPat = in.readString();
        this.naisaancePat = in.readString();
        this.sexePat = in.readString();
        this.numeroPat = in.readString();
        this.numero2Pat = in.readString();
        this.emailPat = in.readString();
        this.nomMerePat = in.readString();
        this.ageGrossessePat = in.readString();
        this.numeroPassportPat = in.readString();
        this.ageMerePat = in.readString();
        this.langueParleePat = in.readString();
        this.activitePat = in.readString();
        this.niveauPat = in.readString();
        this.provenancePat = in.readString();
        this.abontExpiration = in.readString();
        this.photoPat = in.readString();
        this.photoCarnetPat = in.readString();
        this.dateEnregPat = in.readString();
        this.specialPat = in.readString();
        this.mereIdPat = in.readString();
        this.numeroDossierPat = in.readString();
        this.fievreJaune = in.readString();
        this.dateFievreJaune = in.readString();
        this.lotFievreJaune = in.readString();
        this.formule = in.readString();
        this.agentBean = in.readParcelable(Agent.class.getClassLoader());
    }

    public static final Creator<Patient> CREATOR = new Creator<Patient>() {
        @Override
        public Patient createFromParcel(Parcel source) {
            return new Patient(source);
        }

        @Override
        public Patient[] newArray(int size) {
            return new Patient[size];
        }
    };
}