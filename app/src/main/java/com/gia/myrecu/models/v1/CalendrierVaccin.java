package com.gia.myrecu.models.v1;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by didier-dev on 27/10/17.
 */


public class CalendrierVaccin implements Parcelable {


    @SerializedName("idcalendrier")
    private String calendrierVaccinId = "";
    @SerializedName("daterapel")
    private String calendrierVaccinRappel = "";
    @SerializedName("datepresence")
    private String calendrierVaccinPresence = "";
    @SerializedName("centre")
    private String calendrierVaccinCentre = "";
    @SerializedName("vaccin")
    private String calendrierVaccinName = "";
    @SerializedName("lot")
    private String calendrierVaccinLot = "";
    @SerializedName("type")
    private String calendrierVaccinType = "";
    @SerializedName("flagvalidation")
    private String calendrierFlagValidation;
    @SerializedName("imgcarnet")
    private String calendrierImgCarnet;
    @SerializedName("genCal")
    private String calendrierGenerate;


    public CalendrierVaccin() {
    }


    public String getCalendrierGenerate() {
        return calendrierGenerate;
    }

    public void setCalendrierGenerate(String calendrierGenerate) {
        this.calendrierGenerate = calendrierGenerate;
    }

    public String getCalendrierImgCarnet() {
        return calendrierImgCarnet;
    }

    public void setCalendrierImgCarnet(String calendrierImgCarnet) {
        this.calendrierImgCarnet = calendrierImgCarnet;
    }

    public String getCalendrierFlagValidation() {
        return calendrierFlagValidation;
    }

    public void setCalendrierFlagValidation(String calendrierFlagValidation) {
        this.calendrierFlagValidation = calendrierFlagValidation;
    }

    public String getCalendrierVaccinId() {
        return calendrierVaccinId;
    }

    public void setCalendrierVaccinId(String calendrierVaccinId) {
        this.calendrierVaccinId = calendrierVaccinId;
    }

    public String getCalendrierVaccinRappel() {
        return calendrierVaccinRappel;
    }

    public void setCalendrierVaccinRappel(String calendrierVaccinRappel) {
        this.calendrierVaccinRappel = calendrierVaccinRappel;
    }

    public String getCalendrierVaccinPresence() {
        return calendrierVaccinPresence;
    }

    public void setCalendrierVaccinPresence(String calendrierVaccinPresence) {
        this.calendrierVaccinPresence = calendrierVaccinPresence;
    }

    public String getCalendrierVaccinCentre() {
        return calendrierVaccinCentre;
    }

    public void setCalendrierVaccinCentre(String calendrierVaccinCentre) {
        this.calendrierVaccinCentre = calendrierVaccinCentre;
    }

    public String getCalendrierVaccinName() {
        return calendrierVaccinName;
    }

    public void setCalendrierVaccinName(String calendrierVaccinName) {
        this.calendrierVaccinName = calendrierVaccinName;
    }

    public String getCalendrierVaccinLot() {
        return calendrierVaccinLot;
    }

    public void setCalendrierVaccinLot(String calendrierVaccinLot) {
        this.calendrierVaccinLot = calendrierVaccinLot;
    }


    public String getCalendrierVaccinType() {
        return calendrierVaccinType;
    }

    public void setCalendrierVaccinType(String calendrierVaccinType) {
        this.calendrierVaccinType = calendrierVaccinType;
    }


    @Override
    public String toString() {
        return "CalendrierVaccin{" +
                "calendrierVaccinId='" + calendrierVaccinId + '\'' +
                ", calendrierVaccinRappel='" + calendrierVaccinRappel + '\'' +
                ", calendrierVaccinPresence='" + calendrierVaccinPresence + '\'' +
                ", calendrierVaccinCentre='" + calendrierVaccinCentre + '\'' +
                ", calendrierVaccinName='" + calendrierVaccinName + '\'' +
                ", calendrierVaccinLot='" + calendrierVaccinLot + '\'' +
                ", calendrierVaccinType='" + calendrierVaccinType + '\'' +
                ", calendrierFlagValidation='" + calendrierFlagValidation + '\'' +
                ", calendrierImgCarnet='" + calendrierImgCarnet + '\'' +
                ", calendrierGenerate='" + calendrierGenerate + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.calendrierVaccinId);
        dest.writeString(this.calendrierVaccinRappel);
        dest.writeString(this.calendrierVaccinPresence);
        dest.writeString(this.calendrierVaccinCentre);
        dest.writeString(this.calendrierVaccinName);
        dest.writeString(this.calendrierVaccinLot);
        dest.writeString(this.calendrierVaccinType);
        dest.writeString(this.calendrierFlagValidation);
        dest.writeString(this.calendrierImgCarnet);
        dest.writeString(this.calendrierGenerate);
    }

    protected CalendrierVaccin(Parcel in) {
        this.calendrierVaccinId = in.readString();
        this.calendrierVaccinRappel = in.readString();
        this.calendrierVaccinPresence = in.readString();
        this.calendrierVaccinCentre = in.readString();
        this.calendrierVaccinName = in.readString();
        this.calendrierVaccinLot = in.readString();
        this.calendrierVaccinType = in.readString();
        this.calendrierFlagValidation = in.readString();
        this.calendrierImgCarnet = in.readString();
        this.calendrierGenerate = in.readString();
    }

    public static final Creator<CalendrierVaccin> CREATOR = new Creator<CalendrierVaccin>() {
        @Override
        public CalendrierVaccin createFromParcel(Parcel source) {
            return new CalendrierVaccin(source);
        }

        @Override
        public CalendrierVaccin[] newArray(int size) {
            return new CalendrierVaccin[size];
        }
    };
}
