package com.gia.myrecu.models.v1;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.Index;
import androidx.room.PrimaryKey;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.Comparator;

/**
 * Created by didierboka on 8/2/17.
 */

@Entity(
        tableName = "vaccin",
        indices = @Index(
                unique = true,
                value = "ID"
        )
)
public class Vaccin implements Parcelable {


    @ColumnInfo(name = "_id")
    @PrimaryKey(autoGenerate = true)
    private long _id;
    @ColumnInfo(name = "ID")
    @SerializedName("IDVAC")
    private int vaccinId;
    @ColumnInfo(name = "VACCIN_LABEL")
    @SerializedName("NOMVAC")
    private String vaccinLabel;
    @ColumnInfo(name = "VACCIN_PERIODE")
    @SerializedName("PERIODEVAC")
    private String vaccinPeriode;
    @ColumnInfo(name = "VACCIN_TYPE")
    @SerializedName("TYPEVAC")
    private String vaccinType;
    @SerializedName("AGEVACCIN")
    @ColumnInfo(name = "AGEVACCIN")
    private String vaccinAge;
    @ColumnInfo(name = "SUBVENTION")
    @SerializedName("SUBVENTION")
    private String vaccinSubvention;
    @ColumnInfo(name = "OBLIGATOIRE")
    @SerializedName("OBLIGATOIRE")
    private String vaccinObligatoire;
    @ColumnInfo(name = "AMOUNT")
    @SerializedName("TARIF")
    private String vaccinTarif;
    @ColumnInfo(name = "CHECKED") @SerializedName("CHECKED") private String vaccinChecked;
    @ColumnInfo(name = "PERIODE_PRISE") @SerializedName("PERIODEPRISE") private String vaccinPeriodePrise;
    @Ignore private String vaccinLot = "";
    @Ignore private String vaccinPresence = "";
    @Ignore private String vaccinRappel = "";


    public Vaccin() {
    }


    public String getVaccinAge() {
        return vaccinAge;
    }

    public void setVaccinAge(String vaccinAge) {
        this.vaccinAge = vaccinAge;
    }

    public String getVaccinLot() {
        return vaccinLot;
    }

    public void setVaccinLot(String vaccinLot) {
        this.vaccinLot = vaccinLot;
    }

    public String getVaccinPresence() {
        return vaccinPresence;
    }

    public void setVaccinPresence(String vaccinPresence) {
        this.vaccinPresence = vaccinPresence;
    }

    public String getVaccinRappel() {
        return vaccinRappel;
    }

    public void setVaccinRappel(String vaccinRappel) {
        this.vaccinRappel = vaccinRappel;
    }

    public long get_id() {
        return _id;
    }

    public void set_id(long _id) {
        this._id = _id;
    }

    public int getVaccinId() {
        return vaccinId;
    }

    public void setVaccinId(int vaccinId) {
        this.vaccinId = vaccinId;
    }

    public String getVaccinLabel() {
        return vaccinLabel;
    }

    public void setVaccinLabel(String vaccinLabel) {
        this.vaccinLabel = vaccinLabel;
    }

    public String getVaccinPeriode() {
        return vaccinPeriode;
    }

    public void setVaccinPeriode(String vaccinPeriode) {
        this.vaccinPeriode = vaccinPeriode;
    }

    public String getVaccinType() {
        return vaccinType;
    }

    public void setVaccinType(String vaccinType) {
        this.vaccinType = vaccinType;
    }

    public String getVaccinSubvention() {
        return vaccinSubvention;
    }

    public void setVaccinSubvention(String vaccinSubvention) {
        this.vaccinSubvention = vaccinSubvention;
    }

    public String getVaccinObligatoire() {
        return vaccinObligatoire;
    }

    public void setVaccinObligatoire(String vaccinObligatoire) {
        this.vaccinObligatoire = vaccinObligatoire;
    }

    public String getVaccinTarif() {
        return vaccinTarif;
    }

    public void setVaccinTarif(String vaccinTarif) {
        this.vaccinTarif = vaccinTarif;
    }

    public String getVaccinChecked() {
        return vaccinChecked;
    }

    public void setVaccinChecked(String vaccinChecked) {
        this.vaccinChecked = vaccinChecked;
    }

    public String getVaccinPeriodePrise() {
        return vaccinPeriodePrise;
    }

    public void setVaccinPeriodePrise(String vaccinPeriodePrise) {
        this.vaccinPeriodePrise = vaccinPeriodePrise;
    }


    @Override
    public String toString() {
        return getVaccinLabel();
    }


    public static Comparator<Vaccin> VaccinNameComparator
            = new Comparator<Vaccin>() {

        public int compare(Vaccin vaccin1, Vaccin vaccin2) {

            String vaccinName1 = vaccin1.getVaccinLabel().toUpperCase();
            String vaccinName2 = vaccin2.getVaccinLabel().toUpperCase();

            //ascending order
            return vaccinName1.compareTo(vaccinName2);

            //descending order
            //return fruitName2.compareTo(fruitName1);
        }

    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
        dest.writeInt(this.vaccinId);
        dest.writeString(this.vaccinLabel);
        dest.writeString(this.vaccinPeriode);
        dest.writeString(this.vaccinType);
        dest.writeString(this.vaccinAge);
        dest.writeString(this.vaccinSubvention);
        dest.writeString(this.vaccinObligatoire);
        dest.writeString(this.vaccinTarif);
        dest.writeString(this.vaccinChecked);
        dest.writeString(this.vaccinPeriodePrise);
        dest.writeString(this.vaccinLot);
        dest.writeString(this.vaccinPresence);
        dest.writeString(this.vaccinRappel);
    }

    protected Vaccin(Parcel in) {
        this._id = in.readLong();
        this.vaccinId = in.readInt();
        this.vaccinLabel = in.readString();
        this.vaccinPeriode = in.readString();
        this.vaccinType = in.readString();
        this.vaccinAge = in.readString();
        this.vaccinSubvention = in.readString();
        this.vaccinObligatoire = in.readString();
        this.vaccinTarif = in.readString();
        this.vaccinChecked = in.readString();
        this.vaccinPeriodePrise = in.readString();
        this.vaccinLot = in.readString();
        this.vaccinPresence = in.readString();
        this.vaccinRappel = in.readString();
    }

    public static final Creator<Vaccin> CREATOR = new Creator<Vaccin>() {
        @Override
        public Vaccin createFromParcel(Parcel source) {
            return new Vaccin(source);
        }

        @Override
        public Vaccin[] newArray(int size) {
            return new Vaccin[size];
        }
    };
}
