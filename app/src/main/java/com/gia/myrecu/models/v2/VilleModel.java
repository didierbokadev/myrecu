package com.gia.myrecu.models.v2;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Didier BOKA, email: didierboka.developer@gmail.com
 * on 08/09/2022.
 **/


@Entity(
        tableName = "ville",
        indices = @Index(
                unique = true,
                value = "id"
        )
)
public class VilleModel implements Parcelable {


    @PrimaryKey(autoGenerate = true)
    @Expose(deserialize = false, serialize = false)
    private long _id;
    @SerializedName("ID")
    private int id;
    @SerializedName("IDREG")
    private String regionId;
    @SerializedName("VILLE")
    private String ville;
    @SerializedName("LAT")
    private double lat;
    @SerializedName("LON")
    private double lon;


    public VilleModel() {

    }


    public long getID() {
        return _id;
    }

    public void setID(long _id) {
        this._id = _id;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegionId() {
        return regionId;
    }

    public void setRegionId(String regionId) {
        this.regionId = regionId;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
        dest.writeInt(this.id);
        dest.writeString(this.regionId);
        dest.writeString(this.ville);
        dest.writeDouble(this.lat);
        dest.writeDouble(this.lon);
    }

    public void readFromParcel(Parcel source) {
        this._id = source.readLong();
        this.id = source.readInt();
        this.regionId = source.readString();
        this.ville = source.readString();
        this.lat = source.readDouble();
        this.lon = source.readDouble();
    }

    protected VilleModel(Parcel in) {
        this._id = in.readLong();
        this.id = in.readInt();
        this.regionId = in.readString();
        this.ville = in.readString();
        this.lat = in.readDouble();
        this.lon = in.readDouble();
    }

    public static final Parcelable.Creator<VilleModel> CREATOR = new Parcelable.Creator<VilleModel>() {
        @Override
        public VilleModel createFromParcel(Parcel source) {
            return new VilleModel(source);
        }

        @Override
        public VilleModel[] newArray(int size) {
            return new VilleModel[size];
        }
    };


    @NonNull
    @Override
    public String toString() {
        return getVille();
    }
}
