package com.gia.myrecu.models.v1;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

@SuppressWarnings("ALL")
public class Agent implements Parcelable {


    /**
     * utilisateur_id : 80
     * role_id : 5
     * id_rdc : 0
     * client_id : 212
     * login : hamidouramatou2015@gmail.com
     * nom_prenom : Hamidou Ramatou
     * telephone : 22559737153
     * email : hamidouramatou2015@gmail.com
     * date_creation : 2018-01-18
     * statut_id : 1
     */

    @SerializedName("utilisateur_id")
    private String id;
    @SerializedName("role_id")
    private String agentRoleId;
    @SerializedName("id_rdc")
    private String agentRdcId;
    @SerializedName("client_id")
    private String agentClientId;
    @SerializedName("login")
    private String agentLogin;
    @SerializedName("nom_prenom")
    private String agentNomPrenoms;
    @SerializedName("telephone")
    private String agentTelephone;
    @SerializedName("email")
    private String agentEmail;
    @SerializedName("date_creation")
    private String agentDateCreation;
    @SerializedName("statut_id")
    private String agentStatusId;


    public Agent() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAgentRoleId() {
        return agentRoleId;
    }

    public void setAgentRoleId(String agentRoleId) {
        this.agentRoleId = agentRoleId;
    }

    public String getAgentRdcId() {
        return agentRdcId;
    }

    public void setAgentRdcId(String agentRdcId) {
        this.agentRdcId = agentRdcId;
    }

    public String getAgentClientId() {
        return agentClientId;
    }

    public void setAgentClientId(String agentClientId) {
        this.agentClientId = agentClientId;
    }

    public String getAgentLogin() {
        return agentLogin;
    }

    public void setAgentLogin(String agentLogin) {
        this.agentLogin = agentLogin;
    }

    public String getAgentNomPrenoms() {
        return agentNomPrenoms;
    }

    public void setAgentNomPrenoms(String agentNomPrenoms) {
        this.agentNomPrenoms = agentNomPrenoms;
    }

    public String getAgentTelephone() {
        return agentTelephone;
    }

    public void setAgentTelephone(String agentTelephone) {
        this.agentTelephone = agentTelephone;
    }

    public String getAgentEmail() {
        return agentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        this.agentEmail = agentEmail;
    }

    public String getAgentDateCreation() {
        return agentDateCreation;
    }

    public void setAgentDateCreation(String agentDateCreation) {
        this.agentDateCreation = agentDateCreation;
    }

    public String getAgentStatusId() {
        return agentStatusId;
    }

    public void setAgentStatusId(String agentStatusId) {
        this.agentStatusId = agentStatusId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.agentRoleId);
        dest.writeString(this.agentRdcId);
        dest.writeString(this.agentClientId);
        dest.writeString(this.agentLogin);
        dest.writeString(this.agentNomPrenoms);
        dest.writeString(this.agentTelephone);
        dest.writeString(this.agentEmail);
        dest.writeString(this.agentDateCreation);
        dest.writeString(this.agentStatusId);
    }

    protected Agent(Parcel in) {
        this.id = in.readString();
        this.agentRoleId = in.readString();
        this.agentRdcId = in.readString();
        this.agentClientId = in.readString();
        this.agentLogin = in.readString();
        this.agentNomPrenoms = in.readString();
        this.agentTelephone = in.readString();
        this.agentEmail = in.readString();
        this.agentDateCreation = in.readString();
        this.agentStatusId = in.readString();
    }

    public static final Creator<Agent> CREATOR = new Creator<Agent>() {
        @Override
        public Agent createFromParcel(Parcel source) {
            return new Agent(source);
        }

        @Override
        public Agent[] newArray(int size) {
            return new Agent[size];
        }
    };
}