package com.gia.myrecu.models.v2;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Didier BOKA, email: didierboka.developer@gmail.com
 * on 06/09/2022.
 **/

@Entity(
        tableName = "niveauetude",
        indices = @Index(
                unique = true,
                value = "id"
        )
)
public class NiveauEtudeModel implements Parcelable {


    @PrimaryKey(autoGenerate = true)
    @Expose(deserialize = false, serialize = false)
    private long _id;
    @SerializedName("ID")
    private int id;
    @SerializedName("LIBELLE")
    private String libelle;
    @SerializedName("CREATEDAT")
    @Expose(deserialize = false, serialize = false)
    private String createdAt;
    @SerializedName("UPDATEDAT")
    @Expose(deserialize = false, serialize = false)
    private String updatedAt;


    public NiveauEtudeModel() {
    }

    public long getID() {
        return _id;
    }

    public void setID(long _id) {
        this._id = _id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.libelle);
        dest.writeString(this.createdAt);
        dest.writeString(this.updatedAt);
    }

    public void readFromParcel(Parcel source) {
        this.id = source.readInt();
        this.libelle = source.readString();
        this.createdAt = source.readString();
        this.updatedAt = source.readString();
    }

    protected NiveauEtudeModel(Parcel in) {
        this.id = in.readInt();
        this.libelle = in.readString();
        this.createdAt = in.readString();
        this.updatedAt = in.readString();
    }

    public static final Creator<NiveauEtudeModel> CREATOR = new Creator<NiveauEtudeModel>() {
        @Override
        public NiveauEtudeModel createFromParcel(Parcel source) {
            return new NiveauEtudeModel(source);
        }

        @Override
        public NiveauEtudeModel[] newArray(int size) {
            return new NiveauEtudeModel[size];
        }
    };


    @NonNull
    @Override
    public String toString() {
        return getLibelle();
    }
}
