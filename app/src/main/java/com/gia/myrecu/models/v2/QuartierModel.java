package com.gia.myrecu.models.v2;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.Index;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NonNls;

/**
 * Created by Didier BOKA, email: didierboka.developer@gmail.com
 * on 08/09/2022.
 **/

@Entity(
        tableName = "quartier",
        indices = @Index(
                unique = true,
                value = "id"
        )
)
public class QuartierModel implements Parcelable {


    @PrimaryKey(autoGenerate = true)
    @Expose(deserialize = false, serialize = false)
    private long _id;
    @SerializedName("ID")
    private int id;
    @SerializedName("IDCOMMUNE")
    private String communeId;
    @SerializedName("QUARTIER")
    private String quartier;
    @SerializedName("LAT")
    private String lat;
    @SerializedName("LON")
    private String lon;


    public QuartierModel() {
    }


    public long getID() {
        return _id;
    }

    public void setID(long _id) {
        this._id = _id;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCommuneId() {
        return communeId;
    }

    public void setCommuneId(String communeId) {
        this.communeId = communeId;
    }

    public String getQuartier() {
        return quartier;
    }

    public void setQuartier(String quartier) {
        this.quartier = quartier;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(this._id);
        dest.writeInt(this.id);
        dest.writeString(this.communeId);
        dest.writeString(this.quartier);
        dest.writeString(this.lat);
        dest.writeString(this.lon);
    }



    protected QuartierModel(Parcel in) {
        this._id = in.readLong();
        this.id = in.readInt();
        this.communeId = in.readString();
        this.quartier = in.readString();
        this.lat = in.readString();
        this.lon = in.readString();
    }

    public static final Creator<QuartierModel> CREATOR = new Creator<QuartierModel>() {
        @Override
        public QuartierModel createFromParcel(Parcel source) {
            return new QuartierModel(source);
        }

        @Override
        public QuartierModel[] newArray(int size) {
            return new QuartierModel[size];
        }
    };


    @NonNull
    @Override
    public String toString() {
        return getQuartier();
    }
}
