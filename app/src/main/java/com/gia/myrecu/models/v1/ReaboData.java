package com.gia.myrecu.models.v1;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by didierboka on 8/28/17.
 */

public class ReaboData implements Parcelable {

    @SerializedName("reqId")
    private int reaboDataIdReq = 0;
    @SerializedName("dur")
    private String reaboDataNombreAnne = "";
    @SerializedName("patId")
    private String reaboDataPatientId = "0";
    @SerializedName("ctrId")
    private int reaboDataCentreId = 0;
    @SerializedName("usrId")
    private int reaboDataUserId = 0;
    @SerializedName("fm")
    private String reaboDataFormule = "";


    public ReaboData() {
    }


    public String getReaboDataFormule() {
        return reaboDataFormule;
    }

    public void setReaboDataFormule(String reaboDataFormule) {
        this.reaboDataFormule = reaboDataFormule;
    }

    public int getReaboDataIdReq() {
        return reaboDataIdReq;
    }

    public void setReaboDataIdReq(int reaboDataIdReq) {
        this.reaboDataIdReq = reaboDataIdReq;
    }

    public String getReaboDataNombreAnne() {
        return reaboDataNombreAnne;
    }

    public void setReaboDataNombreAnne(String reaboDataNombreAnne) {
        this.reaboDataNombreAnne = reaboDataNombreAnne;
    }

    public String getReaboDataPatientId() {
        return reaboDataPatientId;
    }

    public void setReaboDataPatientId(String reaboDataPatientId) {
        this.reaboDataPatientId = reaboDataPatientId;
    }

    public int getReaboDataCentreId() {
        return reaboDataCentreId;
    }

    public void setReaboDataCentreId(int reaboDataCentreId) {
        this.reaboDataCentreId = reaboDataCentreId;
    }

    public int getReaboDataUserId() {
        return reaboDataUserId;
    }

    public void setReaboDataUserId(int reaboDataUserId) {
        this.reaboDataUserId = reaboDataUserId;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.reaboDataIdReq);
        dest.writeString(this.reaboDataNombreAnne);
        dest.writeString(this.reaboDataPatientId);
        dest.writeInt(this.reaboDataCentreId);
        dest.writeInt(this.reaboDataUserId);
        dest.writeString(this.reaboDataFormule);
    }

    protected ReaboData(Parcel in) {
        this.reaboDataIdReq = in.readInt();
        this.reaboDataNombreAnne = in.readString();
        this.reaboDataPatientId = in.readString();
        this.reaboDataCentreId = in.readInt();
        this.reaboDataUserId = in.readInt();
        this.reaboDataFormule = in.readString();
    }

    public static final Creator<ReaboData> CREATOR = new Creator<ReaboData>() {
        @Override
        public ReaboData createFromParcel(Parcel source) {
            return new ReaboData(source);
        }

        @Override
        public ReaboData[] newArray(int size) {
            return new ReaboData[size];
        }
    };
}
