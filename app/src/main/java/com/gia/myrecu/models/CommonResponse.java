package com.gia.myrecu.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Didier BOKA, email: didierboka.developer@gmail.com
 * on 21/06/2022.
 **/

public class CommonResponse implements Parcelable {


    @Expose @SerializedName("message") private String message;
    @Expose @SerializedName("statut") private int statut;
    @Expose @SerializedName("expiredDate") private String expiredDate;


    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatut() {
        return statut;
    }

    public void setStatut(int statut) {
        this.statut = statut;
    }

    public String getExpiredDate() {
        return expiredDate;
    }

    public void setExpiredDate(String expiredDate) {
        this.expiredDate = expiredDate;
    }

    public CommonResponse() {
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.message);
        dest.writeInt(this.statut);
        dest.writeString(this.expiredDate);
    }

    public void readFromParcel(Parcel source) {
        this.message = source.readString();
        this.statut = source.readInt();
        this.expiredDate = source.readString();
    }

    protected CommonResponse(Parcel in) {
        this.message = in.readString();
        this.statut = in.readInt();
        this.expiredDate = in.readString();
    }

    public static final Creator<CommonResponse> CREATOR = new Creator<CommonResponse>() {
        @Override
        public CommonResponse createFromParcel(Parcel source) {
            return new CommonResponse(source);
        }

        @Override
        public CommonResponse[] newArray(int size) {
            return new CommonResponse[size];
        }
    };
}
