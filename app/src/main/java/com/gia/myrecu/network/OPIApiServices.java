package com.gia.myrecu.network;


import com.gia.myrecu.models.CommonResponse;
import com.gia.myrecu.models.v1.Centre;
import com.gia.myrecu.models.v1.District;
import com.gia.myrecu.models.v1.Patient;
import com.gia.myrecu.models.v1.Vaccin;
import com.gia.myrecu.models.v2.AntigenData;
import com.gia.myrecu.models.v2.CarnetRequest;
import com.gia.myrecu.models.v2.CarnetResponse;
import com.gia.myrecu.models.v2.CommuneModel;
import com.gia.myrecu.models.v2.NiveauClasseModel;
import com.gia.myrecu.models.v2.NiveauEtudeModel;
import com.gia.myrecu.models.v2.ProfessionModel;
import com.gia.myrecu.models.v2.QuartierModel;
import com.gia.myrecu.models.v2.RecuUsedResponse;
import com.gia.myrecu.models.v2.VilleModel;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

@SuppressWarnings("ALL")
public interface OPIApiServices {




    @GET("centre/list/{d}")
    Single<List<Centre>> getCentres(@Path("d") String database);


    @GET("district/list/{d}")
    Single<List<District>> getDistricts(@Path("d") String database);


    @GET("niveauclasse/list/{d}")
    Single<List<NiveauClasseModel>> getClasses(@Path("d") String database);


    @GET("niveauetude/list/{d}")
    Single<List<NiveauEtudeModel>> getEtudes(@Path("d") String database);



    @GET("quartier/list/{d}")
    Single<List<QuartierModel>> getQuartiers(@Path("d") String database);



    @GET("commune/list/{d}")
    Single<List<CommuneModel>> getCommunes(@Path("d") String database);



    @GET("ville/list/{d}")
    Single<List<VilleModel>> getVilles(@Path("d") String database);


    @GET("profession/list/{d}")
    Single<List<ProfessionModel>> getProfessions(@Path("d") String database);


    @GET("vaccin/list/{d}")
    Single<List<Vaccin>> getVaccins(@Path("d") String database);


    @GET("agent/rdv/filter")
    Single<List<Patient>> getPatientsByDatesIntervales(@Query("centreId") String centreId,
                                                       @Query("dateOpenInterv") String dateOpenInterv,
                                                       @Query("dateCloseInterv") String dateCloseInterv,
                                                       @Query("d") String databaseWork,
                                                       @Query("abnType") String abonnementType,
                                                       @Query("offset") String offset);

    @GET("agent/rdv/day")
    Single<List<Patient>> getRdvPatientsDaily(@Query("centreId") String centreId,
                                              @Query("offset") int offset,
                                              @Query("d") String databaseWork
    );

    @GET("agent/stats/{agentid}")
    Single<List<AntigenData>> getAntigensStats(
            @Path("agentid") String agentId,
            @Query("dateOpen") String dateOpen,
            @Query("dateClose") String dateClose,
            @Query("type") String type,
            @Query("d") String database
    );


    @GET("patient/infos/{id}")
    Single<Patient> getPatientById(@Path("id") int clientId,
                                   @Query("d") String databaseWork,
                                   @Query("reqKey") String request);

    @GET("agent/rdv/day/count")
    Single<Integer> getRdvPatientsDailyTotal(@Query("centreId") String centreId,
                                             @Query("d") String databaseWork
    );


    @GET("agent/rdv/interv/count")
    Single<Integer> getRdvPatientsIntervTotal(@Query("centreId") String centreId,
                                              @Query("dateOpen") String dateOpenInterv,
                                              @Query("dateClose") String dateCloseInterv,
                                              @Query("d") String databaseWork
    );


    @FormUrlEncoded
    @POST("patient/deces")
    Completable declareDeath(@Field("patientId") String patientId,
                             @Field("deathDate") String deathDate,
                             @Field("d") String databaseWork
    );

    @GET("patient/missed")
    Single<List<Patient>> getPatientMissed(@Query("centreId") String centreId,
                                           @Query("dateOpenMissed") String dateOpenMissed,
                                           @Query("dateCloseMissed") String dateCloseMissed,
                                           @Query("typeVisite") String typeVisite,
                                           @Query("d") String databaseWork
    );


    // CARNET API
    @POST("centrecarnetlistrecusnonutilise/")
    Single<CarnetResponse> getCarnetsCentre(@Body CarnetRequest carnetRequest);


    @POST("agentcarnetlistrecusnonutilise/")
    Single<CarnetResponse> getCarnetsAgent(@Body CarnetRequest carnetRequest);


    @POST("updaterecuagent/")
    Single<RecuUsedResponse> updateRecuNumber(@Body CarnetRequest carnetRequest);


    @POST("vaccin/maj")
    @FormUrlEncoded
    Single<CommonResponse> scheduleVaccin(
            @Field("ctrId") String centreId,
            @Field("usrId") String agentId,
            @Field("patId") String patientId,
            @Field("dtRap") String rappelDate,
            @Field("dtPre") String presenceDate,
            @Field("lot") String vaccinLot,
            @Field("vacId") String vaccinId,
            @Field("type") String type,
            @Field("d") String databaseWork
    );


    @POST("abonnement")
    @FormUrlEncoded
    Single<Patient> abonnementPatient(
            @Field("ctrId") String centreId,
            @Field("usrId") String agentId,
            @Field("nPat") String nomPatient,
            @Field("pnPat") String prenomsPatient,
            @Field("dtPat") String naissancePatient,
            @Field("sxPat") String sexePatient,
            @Field("lngPat") String languePatient,
            @Field("tPat") String telephonePatient,
            @Field("abonType") String typeAbonnementPatient,
            @Field("dur") String dureAbonnementPatient,
            @Field("preg") String enceinteOuiNonPatient,
            @Field("recu") String recuPatient,
            @Field("agePreg") String ageGrossessePatient,
            @Field("datePregEnd") String dateProbableAccouchPatient,
            @Field("photoCarnet") String photoCarnetPatient,
            @Field("fm") String formulePatient,
            @Field("planningVaccinId") String planningVaccinId,
            @Field("planningVaccinMonth") String planningVaccinMonth,
            @Field("planningVaccinRappelDate") String planningVaccinRappelDate,
            @Field("d") String databaseWork
    );


    @POST("abonnement/planningfamillial")
    @FormUrlEncoded
    Single<Patient> abonnementPlanningClient(
            @Field("clientRecu") String recuPatient,
            @Field("ctrId") String centreId,
            @Field("usrId") String agentId,
            @Field("nPat") String nomPatient,
            @Field("pnPat") String prenomsPatient,
            @Field("dtPat") String naissancePatient,
            @Field("sxPat") String sexePatient,
            @Field("lngPat") String languePatient,
            @Field("tPat") String telephonePatient,
            // @Field("photoCarnet") String photoCarnetPatient,
            @Field("plgId") String plgId,
            @Field("plgProbableDate") String planningProbableDate,
            @Field("plgLot") String planningLot,
            @Field("plgPiluleDur") String planningPiluleDuration,
            @Field("clientFormule") String formuleClient,
            @Field("d") String databaseWork
    );


    @POST("reabonnement/planningfamillial")
    @FormUrlEncoded
    Single<CommonResponse> reabonnementPlanningClient(
            @Field("ctrId") String centreId,
            @Field("usrId") String agentId,
            @Field("cltRec") String recuNumber,
            @Field("cltTel") String clientTelephone,
            @Field("plgProbableDate") String planningProbableDate,
            @Field("plgPiluleDur") String planningPiluleDuration,
            @Field("cltId") String clientId,
            @Field("d") String clientDatabaseWork
    );

}
