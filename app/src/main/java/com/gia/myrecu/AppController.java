package com.gia.myrecu;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Parcelable;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import com.blankj.utilcode.util.Utils;



@SuppressWarnings("ALL")
public class AppController extends Application {


    public static final String TAG = "AppController";
    private static AppController mInstance;
    private static Context context;
    TelephonyManager telephonyManager;
    public static SmsManager smsManager;


    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        context = getApplicationContext();
        telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        smsManager = SmsManager.getDefault();
        //DebugDB.initialize(this, new DebugDBFactory());
        Utils.init(this);

        /*RxJavaPlugins.setErrorHandler(throwable -> {
            throwable.printStackTrace();
            LogUtils.e("RxAndroid", "An error is encountered");
        });*/
    }


    /**
     *
     * @return
     */
    public static Context getContext() {
        return context;
    }


    /**
     * @param context
     * @param newTopActivityClass
     */
    public static void startNewActivity(Context context, Class<? extends Activity> newTopActivityClass) {
        Intent intent = new Intent(context, newTopActivityClass);
        context.startActivity(intent);
    }


    /**
     *
     * @param context
     * @param newTopActivityClass
     */
    public static void startNewActivityWithData(Context context, Class<? extends Activity> newTopActivityClass, Parcelable datas) {
        Intent intent = new Intent(context, newTopActivityClass);
        intent.putExtra("datas", datas);
        context.startActivity(intent);
    }


    /**
     *
     * @param context
     * @param newTopActivityClass
     */
    @SuppressLint("WrongConstant")
    public static void startNewTopActivity(Context context, Class<? extends Activity> newTopActivityClass) {
        Intent intent = new Intent(context, newTopActivityClass);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            intent.addFlags(0x8000); // equal to Intent.FLAG_ACTIVITY_CLEAR_TASK which is only available from API level 11
        }
        context.startActivity(intent);
    }


    /**
     *
     * @param context
     * @param intent
     */
    @SuppressLint("WrongConstant")
    public static void startNewTopActivityFromIntent(Context context, Intent intent) {
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            intent.addFlags(0x8000); // equal to Intent.FLAG_ACTIVITY_CLEAR_TASK which is only available from API level 11
        }
        context.startActivity(intent);
    }


    public static synchronized AppController getInstance() {
        return mInstance;
    }

}
